#include "CAPI.h"
#include <Adafruit_MCP23X17.h>
#include <WiFi.h>
#include <ArduinoJson.h>
#include <pthread.h>

CAPI::CAPI() {
	this->powerPin = 2;
	this->A = 16;
	this->B = 17;
	this->C = 33;
	this->D = 14;
	this->demux0 = 25;
	this->demux1 = 26;
	this->demux2 = 27;
	this->button1 = 34;
	this->button2 = 35;
	this->button3 = 36;
	// MCP23017 pins
	this->HOUR_A_LD = 0;
	this->HOUR_B_LD = 1;
	this->MIN_A_LD = 2;
	this->MIN_B_LD = 3;
	this->SEC_A_LD = 4;
	this->SEC_B_LD = 5;
	this->DAY_A_LD = 6;
	this->DAY_B_LD = 7;
	this->MONTH_A_LD = 8;
	this->MONTH_B_LD = 9;
	this->YEAR_A_LD = 10;
	this->YEAR_B_LD = 11;
	this->YEAR_C_LD = 12;
	this->YEAR_D_LD = 13;
	this->TEMP_A_LD = 14;
	this->TEMP_B_LD = 15;

	pinMode(this->powerPin, OUTPUT);
	pinMode(this->A, OUTPUT);
	pinMode(this->B, OUTPUT);
	pinMode(this->C, OUTPUT);
	pinMode(this->D, OUTPUT);

	pinMode(this->button1, INPUT);
	pinMode(this->button2, INPUT);
	pinMode(this->button3, INPUT);

	digitalWrite(this->A, LOW);
	digitalWrite(this->B, LOW);
	digitalWrite(this->C, LOW);
	digitalWrite(this->D, LOW);

	pinMode(this->demux0, OUTPUT);
	pinMode(this->demux1, OUTPUT);
	pinMode(this->demux2, OUTPUT);

	this->mcp = new Adafruit_MCP23X17();
	this->mcp->begin_I2C();
	if (this->mcp->begin_I2C() == false) {
		Serial.println("Couldn't start I2C");
	}
	for (int i = 0; i < 16; ++i) {
		this->mcp->pinMode(i, OUTPUT);
		this->mcp->digitalWrite(i, HIGH);
		this->mcp->digitalWrite(i, LOW);
	}

	this->settings = new CSettings();
}

void CAPI::createTemperatureThread() {
	// Temperature thread
	this->temperatureUpdateThread = new pthread_t();

	int threadCreateFail = pthread_create(this->temperatureUpdateThread, NULL, &CAPI::updateTemperature, this);
	if (threadCreateFail) {
		Serial.println("Failed to create temperature thread!");
	}
}

bool CAPI::connectWlan() {
	bool success = false;
	//connect to WiFi
	Serial.print("Connecting to  ");
	Serial.println(this->settings->getWlanSsid());
	WiFi.begin(this->settings->getWlanSsid().c_str(), this->settings->getWlanPassword().c_str());
	while (WiFi.status() != WL_CONNECTED) {
		if (WiFi.status() == WL_CONNECT_FAILED) {
			Serial.println("Wlan connect fail!");
			success = false;
			break;
		}
		delay(500);
		Serial.print(".");
	}
	Serial.println(" CONNECTED");
	Serial.print("IP Address: ");
	Serial.println(WiFi.localIP().toString());
	
	return success;
}

CSettings* CAPI::getSettings() {
	return this->settings;
}

void CAPI::updateNTP() {
	Serial.println("Updating NTP");
	if (WiFi.isConnected() == false) {
		Serial.println("Wlan isn't connected. Connecting...");
		this->connectWlan();
	}
	configTzTime(this->settings->getNtpTimezone().c_str(), this->settings->getNtpServer().c_str());

	this->previousHourUpdatedNTP = getCurrentTime().tm_hour;
}
struct tm CAPI::getCurrentTime() {
	struct tm timeinfo;
	if(!getLocalTime(&timeinfo)){
		Serial.println("Failed to obtain time");
	}
	return timeinfo;
}

// If NTP was updated an hour ago
bool CAPI::shouldUpdateNTP() {
	return this->getCurrentTime().tm_hour != this->previousHourUpdatedNTP;
}

// Returns true, if string only contains numbers
// TODO remove?
bool CAPI::stringIsDigit(String value) {
	bool success = true;
	for (int i = 0; i < value.length(); ++i) {
		if (isDigit(value.charAt(i)) == false && value.charAt(i) != '\n') {
			Serial.print("Value: ");
			Serial.print(value);
			Serial.print(", char ");
			Serial.print(value.charAt(i));
			Serial.println(" is not a digit");
			success = false;
			break;
		}
	}

	return success;
}

void CAPI::setNumber(int number, int LD) {
	int bitArray[] = {0, 0, 0, 0};
	for (int i = 0; i < 4; ++i) {
		bitArray[3 - i] = bitRead(number, i);
	}
	this->mcp->digitalWrite(LD, HIGH);
	digitalWrite(this->A, bitArray[3]);
	digitalWrite(this->B, bitArray[2]);
	digitalWrite(this->C, bitArray[1]);
	digitalWrite(this->D, bitArray[0]);
	this->mcp->digitalWrite(LD, LOW);
}

void CAPI::setTemperature(int temperature) {
	
	if (temperature >= 0) {

		this->temperatureErrorAmount = 0;

		int temperature10 = temperature / 10;
		int temperature1 = temperature % 10;

		Serial.print("Setting temperature ");
		Serial.print(temperature10);
		Serial.println(temperature1);

		this->setNumber(temperature10, this->TEMP_A_LD);
		this->setNumber(temperature1, this->TEMP_B_LD);
	}
	// Temperature is negative, something is wrong
	else {
		++this->temperatureErrorAmount;

		// Closing temperature if 10 previous temperatures were faulty (2 per minute -> 5 minutes of errors)
		if (this->temperatureErrorAmount > 10) {
			this->setNumber(10, this->TEMP_A_LD);
			this->setNumber(10, this->TEMP_B_LD);
		}
	}
}

void CAPI::printLocalTime()
{
	struct tm timeinfo;
	if(!getLocalTime(&timeinfo)){
		Serial.println("Failed to print time");
		return;
	}
	Serial.println(&timeinfo, "%A, %B %d %Y %H:%M:%S");
}

void CAPI::updateDateTimeScreen() {
	
	struct tm timeinfo = this->getCurrentTime();
	
	//int seconds = timeinfo.tm_sec;
	this->currentSec = timeinfo.tm_sec;
	int seconds10 = this->currentSec / 10;
	int seconds1 = this->currentSec % 10;

	// Hour and minute is only updated when minute is close to changing
	if (this->firstTimeUpdatingNumbers || this->currentSec == 0 || this->currentSec == 1) {
		this->currentHour = timeinfo.tm_hour;
		int hour10 = this->currentHour / 10;
		int hour1 = this->currentHour % 10;
		this->currentMin = timeinfo.tm_min;
		int minutes10 = this->currentMin / 10;
		int minutes1 = this->currentMin % 10;

		this->setNumber(hour10, this->HOUR_A_LD);
		this->setNumber(hour1, this->HOUR_B_LD);
		this->setNumber(minutes10, this->MIN_A_LD);
		this->setNumber(minutes1, this->MIN_B_LD);
		this->firstTimeUpdatingNumbers = false;

		// Also updating temperature
		int temperature = this->currentTemperature;
		this->setTemperature(temperature);
	}

	this->setNumber(seconds10, this->SEC_A_LD);
	this->setNumber(seconds1, this->SEC_B_LD);

	// Only update dates once per day
	// This is to fix a bug where every second the date numbers would blink
	if (this->currentDay != timeinfo.tm_mday) {
		
		this->currentDay = timeinfo.tm_mday;

		int day10 = (this->currentDay) / 10;
		int day1 = (this->currentDay) % 10;
		Serial.print("Setting day number: ");
		Serial.print(day10);
		Serial.println(day1);

		this->currentMonth = timeinfo.tm_mon + 1;
		int month10 = this->currentMonth / 10;
		int month1 = this->currentMonth % 10;

		this->currentYear = timeinfo.tm_year + 1900;
		int year1000 = this->currentYear / 1000;
		int year100 = this->currentYear % 1000 / 100;
		int year10 = this->currentYear % 1000 % 100 / 10;
		int year1 = this->currentYear % 1000 % 100 % 10;

		this->setNumber(day10, this->DAY_A_LD);
		this->setNumber(day1, this->DAY_B_LD);
		this->setNumber(month10, this->MONTH_A_LD);
		this->setNumber(month1, this->MONTH_B_LD);
		this->setNumber(year1000, this->YEAR_A_LD);
		this->setNumber(year100, this->YEAR_B_LD);
		this->setNumber(year10, this->YEAR_C_LD);
		this->setNumber(year1, this->YEAR_D_LD);
		
		int weekday = timeinfo.tm_wday;
		this->setDay(weekday);
	}
}

void CAPI::setDay(int dayNumber) {
	int bitArray[] = {0, 0, 0};
	for (int i = 0; i < 3; ++i) {
		bitArray[2 - i] = bitRead(dayNumber, i);
	}
	digitalWrite(this->demux0, bitArray[2]);
	digitalWrite(this->demux1, bitArray[1]);
	digitalWrite(this->demux2, bitArray[0]);
}

int CAPI::getCurrentTemperature() {
	return this->currentTemperature;
}
void CAPI::setCurrentTemperature(int value) {
	this->currentTemperature = value;
}

void* CAPI::updateTemperature(void *cApiVoid) {
	
	while (true) {
		CAPI* cApi = static_cast<CAPI*>(cApiVoid);

		//Serial.println((int)threadid);
		if (WiFi.isConnected() == false) {
			Serial.println("Wlan isn't connected. Connecting...");
			cApi->connectWlan();
		}

		// Connecting to socket
		WiFiClient client;
		if (!client.connect(cApi->getSettings()->getTemperatureServerIp().c_str(), cApi->getSettings()->getTemperatureServerPort())) {

			Serial.println("Connection to host failed");
			// Turning off temperature numbers
			cApi->setCurrentTemperature(-1);
		}
		else {
			Serial.println("Connected to server successfully!");

			client.print("getTemperature");
			String message = client.readString();
			Serial.print("Response \"");
			Serial.print(message);
			Serial.println("\"");
			Serial.println("Disconnecting...");
			client.stop();

			StaticJsonDocument<200> doc;
			DeserializationError error = deserializeJson(doc, message);
			if (error) {
				Serial.print(F("deserializeJson() failed: "));
				Serial.println(error.f_str());
				// Turning off temperature numbers
				cApi->setCurrentTemperature(-1);
			}
			else if (doc["success"] == true) {
				double temperature = doc["temperatureC"];
				Serial.println("JSON VALUES");
				Serial.println(temperature);
				//int tempInt = (int)round(temp);
				cApi->setCurrentTemperature((int)round(temperature));
			}
			else {
				Serial.println("Server returned success false!");
				// Turning off temperature numbers
				cApi->setCurrentTemperature(-1);
			}
		}

		delay(cApi->getSettings()->getTemperatureUpdateTime() * 1000);
	}
}

void CAPI::setPower() {
	if (powerShouldBeOn()) {
		digitalWrite(this->powerPin, HIGH);
	}
	else {
		digitalWrite(this->powerPin, LOW);
	}
}
// Check start and end times, if time should be on now
bool CAPI::powerShouldBeOn() {

	bool button1Pressed = digitalRead(this->button1);
	bool button2Pressed = digitalRead(this->button2);
	bool button3Pressed = digitalRead(this->button3);

	struct tm timeinfo = this->getCurrentTime();
	int weekday = timeinfo.tm_wday;
	double currentHour = (double)timeinfo.tm_hour;
	double currentMin = (double)timeinfo.tm_min;
	double currentTime = currentHour + currentMin / 60;
	
	double dayStartTime = this->settings->getPowerStartTime()[weekday];
	double dayEndTime = this->settings->getPowerEndTime()[weekday];

	bool returnValue = currentTime >= dayStartTime && currentTime < dayEndTime;

	return returnValue || button1Pressed || button2Pressed || button3Pressed;
}

int CAPI::millisecodsUntilNextSecond() {
	struct timeval tv;
	gettimeofday(&tv, NULL);
	int milli = tv.tv_usec / 1000;
	return 1000 - milli;
}
