#ifndef CSETTINGS_H
#define CSETTINGS_H

#include <Arduino.h>

class CSettings {
	private:
		// SD-card settings
		String wlanSsid;
		String wlanPassword;
		String ntpTimezone;
		String ntpServer;
		bool temperatureEnabled;
		int temperatureUpdateTime;
		String temperatureServerIp;
		int temperatureServerPort;

		double powerStartTime[7];
		double powerEndTime[7];

		bool setupSD();
		bool getSettings();

	public:
		CSettings();

		String getWlanSsid();
		String getWlanPassword();
		String getNtpTimezone();
		String getNtpServer();
		bool isTemperatureEnabled();
		int getTemperatureUpdateTime();
		String getTemperatureServerIp();
		int getTemperatureServerPort();
		double* getPowerStartTime();
		double* getPowerEndTime();
};

#endif