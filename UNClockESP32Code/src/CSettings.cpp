#include "CSettings.h"
#include <Arduino.h>
#include "FS.h"
#include "SD.h"
#include "SPI.h"
#include <ArduinoJson.h>

CSettings::CSettings() {

	this->temperatureEnabled = false;

	if (this->setupSD() == false) {
		Serial.println("Couldn't start SD");
	}
	if (this->getSettings() == false) {
		Serial.println("Couldn't get settings!");
	}
}

bool CSettings::setupSD() {
	if(!SD.begin(5)){
		Serial.println("Card Mount Failed");
		return false;
	}
	uint8_t cardType = SD.cardType();

	if(cardType == CARD_NONE){
		Serial.println("No SD card attached");
		return false;
	}

	Serial.print("SD Card Type: ");
	if(cardType == CARD_MMC){
		Serial.println("MMC");
	}
	else if(cardType == CARD_SD){
		Serial.println("SDSC");
	}
	else if(cardType == CARD_SDHC){
		Serial.println("SDHC");
	}
	else {
		Serial.println("UNKNOWN");
	}

	uint64_t cardSize = SD.cardSize() / (1024 * 1024);
	Serial.printf("SD Card Size: %lluMB\n", cardSize);

	return true;
}

bool CSettings::getSettings() {
	
	bool configSuccess = true;
	StaticJsonDocument<1000> config;
	File file = SD.open("/unclock.conf");
	if (!file) {
		Serial.println("Failed to open config file for reading");
		return false;
	}
	String fileStr = "";
	while (file.available()){
		char character = file.read();
		fileStr += character;
	}
	DeserializationError error = deserializeJson(config, fileStr);
	if (error) {
		Serial.print(F("deserializeJson() failed for config: "));
		Serial.println(error.f_str());
		configSuccess = false;
	}
	file.close();

	if (config.containsKey("wlan_ssid")) {
		this->wlanSsid = config.getMember("wlan_ssid").as<const char*>();
		Serial.print("ssid: ");
		Serial.println(this->wlanSsid);
	}
	else {
		Serial.println("wlan_ssid is missing!");
		configSuccess = false;
	}
	if (config.containsKey("wlan_password")) {
		this->wlanPassword = config.getMember("wlan_password").as<const char*>();
	}
	else {
		Serial.println("wlan_password is missing!");
		configSuccess = false;
	}
	if (config.containsKey("ntp_server")) {
		this->ntpServer = config.getMember("ntp_server").as<const char*>();
	}
	else {
		Serial.println("ntp_server is missing!");
		configSuccess = false;
	}
	if (config.containsKey("ntp_timezone")) {
		this->ntpTimezone = config.getMember("ntp_timezone").as<const char*>();
	}
	else {
		Serial.println("ntp_timezone is missing!");
		configSuccess = false;
	}
	if (config.containsKey("temperature_enabled")) {
		this->temperatureEnabled = config.getMember("temperature_enabled").as<bool>();
	}
	else {
		Serial.println("temperature_enabled is missing!");
		this->temperatureEnabled = false;
	}
	if (config.containsKey("temperature_update_time")) {
		this->temperatureUpdateTime = config.getMember("temperature_update_time").as<int>();
	}
	else {
		Serial.println("temperature_update_time is missing!");
		this->temperatureUpdateTime = 60;
	}
	if (config.containsKey("temperature_server_ip")) {
		this->temperatureServerIp = config.getMember("temperature_server_ip").as<const char*>();
	}
	else {
		Serial.println("temperature_server_port is missing!");
		this->temperatureEnabled = false;
	}
	if (config.containsKey("temperature_server_port")) {
		this->temperatureServerPort = config.getMember("temperature_server_port").as<int>();
	}
	else {
		Serial.println("temperature_server_port is missing!");
		this->temperatureEnabled = false;
	}
	if (config.containsKey("power") && config["power"].containsKey("start") && config["power"].containsKey("end")) {
		JsonArray powerOnJsonArray = config["power"]["start"];
		if (powerOnJsonArray.size() != 7) {
			Serial.println("power.start array does not contain 7 values!");
		}
		for (int i = 0; i < powerOnJsonArray.size(); ++i) {
			String timeStr = powerOnJsonArray.getElement(i).as<String>();
			int colonIndex = timeStr.indexOf(':');
			if (colonIndex > 0) {
				String hourStr = timeStr.substring(0, colonIndex);
				String minStr = timeStr.substring(colonIndex + 1);

				double hour = atof(hourStr.c_str());
				double min = atof(minStr.c_str());
				double timeValue = hour + min / 60;
				this->powerStartTime[i] = timeValue;
			}
			else {
				Serial.print(i);
				Serial.print(": ");
				Serial.print(timeStr);
				Serial.println(" is an invalid time!");
				return false;
			}
		}
		JsonArray powerOffJsonArray = config["power"]["end"];
		if (powerOffJsonArray.size() != 7) {
			Serial.println("power.end array does not contain 7 values!");
		}
		for (int i = 0; i < powerOffJsonArray.size(); ++i) {
			String timeStr = powerOffJsonArray.getElement(i).as<String>();
			int colonIndex = timeStr.indexOf(':');
			if (colonIndex > 0) {
				String hourStr = timeStr.substring(0, colonIndex);
				String minStr = timeStr.substring(colonIndex + 1);

				double hour = atof(hourStr.c_str());
				double min = atof(minStr.c_str());
				double timeValue = hour + min / 60;
				this->powerEndTime[i] = timeValue;
			}
			else {
				Serial.print(i);
				Serial.print(": ");
				Serial.print(timeStr);
				Serial.println(" is an invalid time!");
				return false;
			}
		}
	}
	else {
		Serial.println("Power start and end times are missing!");
		configSuccess = false;
	}

	if (configSuccess == false) {
		Serial.println("Could not get configs!");
		return false;
	}

	return configSuccess;
}

String CSettings::getWlanSsid() {
	return this->wlanSsid;
}
String CSettings::getWlanPassword() {
	return this->wlanPassword;
}
String CSettings::getNtpTimezone() {
	return this->ntpTimezone;
}
String CSettings::getNtpServer() {
	return this->ntpServer;
}
bool CSettings::isTemperatureEnabled() {
	return this->temperatureEnabled;
}
int CSettings::getTemperatureUpdateTime() {
	return this->temperatureUpdateTime;
}
String CSettings::getTemperatureServerIp() {
	return this->temperatureServerIp;
}
int CSettings::getTemperatureServerPort() {
	return this->temperatureServerPort;
}
double* CSettings::getPowerStartTime() {
	return this->powerStartTime;
}
double* CSettings::getPowerEndTime() {
	return this->powerEndTime;
}