#ifndef CAPI_H
#define CAPI_H

#include <Adafruit_MCP23X17.h>
#include "CSettings.h"

class CAPI {
	private:
		int powerPin;
		int A;
		int B;
		int C;
		int D;
		int demux0;
		int demux1;
		int demux2;
		int button1;
		int button2;
		int button3;
		// MCP23017 pins
		int HOUR_A_LD;
		int HOUR_B_LD;
		int MIN_A_LD;
		int MIN_B_LD;
		int SEC_A_LD;
		int SEC_B_LD;
		int DAY_A_LD;
		int DAY_B_LD;
		int MONTH_A_LD;
		int MONTH_B_LD;
		int YEAR_A_LD;
		int YEAR_B_LD;
		int YEAR_C_LD;
		int YEAR_D_LD;
		int TEMP_A_LD;
		int TEMP_B_LD;

		Adafruit_MCP23X17* mcp;
		CSettings* settings;
		pthread_t* temperatureUpdateThread;
		static void *updateTemperature(void *cApiVoid);

		int currentHour = -1;
		int currentMin = -1;
		int currentSec = -1;
		int currentDay = -1;
		int currentMonth = -1;
		int currentYear = -1;

		//int previousDayUpdatedNTP = -1;
		int previousHourUpdatedNTP = -1;
		//int previousDayUpdatedDateNumbers = -1;
		bool firstTimeUpdatingNumbers = true;
		int currentTemperature = 0;
		int temperatureErrorAmount = 0;

	public:
		CAPI();
		CSettings* getSettings();
		
		// Move to private
		bool connectWlan();
		void updateNTP();
		bool shouldUpdateNTP();
		bool stringIsDigit(String value);
		void setNumber(int number, int LD);
		void setTemperature(int temperature);
		void printLocalTime();
		void updateDateTimeScreen();
		void setDay(int dayNumber);

		struct tm getCurrentTime();
		int getCurrentTemperature();
		void setCurrentTemperature(int value);
		void createTemperatureThread();
		
		void setPower();
		bool powerShouldBeOn();
		int millisecodsUntilNextSecond();
};

#endif