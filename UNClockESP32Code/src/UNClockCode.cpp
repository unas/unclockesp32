#include <WiFi.h>
#include <unordered_map>
#include "time.h"
#include "FS.h"
#include "SD.h"
#include "SPI.h"
#include <ctype.h>
#include <ArduinoJson.h>
#include <pthread.h>
#include <math.h>

#include "CAPI.h"

CAPI* cApi;

void setup() {
	Serial.begin(115200);

	Serial.println("Setup starting");
	
	cApi = new CAPI();	

	cApi->updateNTP();
	cApi->printLocalTime();

	if (cApi->getSettings()->isTemperatureEnabled()) {
		cApi->createTemperatureThread();
	}

	cApi->setPower();
}

void loop() {
	if (cApi->shouldUpdateNTP()) {
		Serial.println("NTP needs an update");
		cApi->updateNTP();
	}
	cApi->updateDateTimeScreen();
	cApi->setPower();

	// Added 500 milliseconds, so that second will be updated in the middle of a second,
	// because some seconds were not getting shown because esp woke up right before second changed
	delay(cApi->millisecodsUntilNextSecond() + 500); // Update once per second
}