<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="7.6.0">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="2" name="Route2" color="1" fill="3" visible="no" active="no"/>
<layer number="3" name="Route3" color="4" fill="3" visible="no" active="no"/>
<layer number="4" name="Route4" color="1" fill="4" visible="no" active="no"/>
<layer number="5" name="Route5" color="4" fill="4" visible="no" active="no"/>
<layer number="6" name="Route6" color="1" fill="8" visible="no" active="no"/>
<layer number="7" name="Route7" color="4" fill="8" visible="no" active="no"/>
<layer number="8" name="Route8" color="1" fill="2" visible="no" active="no"/>
<layer number="9" name="Route9" color="4" fill="2" visible="no" active="no"/>
<layer number="10" name="Route10" color="1" fill="7" visible="no" active="no"/>
<layer number="11" name="Route11" color="4" fill="7" visible="no" active="no"/>
<layer number="12" name="Route12" color="1" fill="5" visible="no" active="no"/>
<layer number="13" name="Route13" color="4" fill="5" visible="no" active="no"/>
<layer number="14" name="Route14" color="1" fill="6" visible="no" active="no"/>
<layer number="15" name="Route15" color="4" fill="6" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="15" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="50" name="dxf" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="53" name="tGND_GNDA" color="7" fill="9" visible="no" active="no"/>
<layer number="54" name="bGND_GNDA" color="1" fill="9" visible="no" active="no"/>
<layer number="56" name="wert" color="7" fill="1" visible="no" active="no"/>
<layer number="57" name="tCAD" color="7" fill="1" visible="no" active="no"/>
<layer number="59" name="tCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="60" name="bCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="88" name="SimResults" color="9" fill="1" visible="yes" active="yes"/>
<layer number="89" name="SimProbes" color="9" fill="1" visible="yes" active="yes"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
<layer number="99" name="SpiceOrder" color="7" fill="1" visible="yes" active="yes"/>
<layer number="100" name="Muster" color="7" fill="1" visible="no" active="no"/>
<layer number="101" name="Patch_Top" color="12" fill="4" visible="yes" active="yes"/>
<layer number="102" name="Vscore" color="7" fill="1" visible="yes" active="yes"/>
<layer number="103" name="tMap" color="7" fill="1" visible="yes" active="yes"/>
<layer number="104" name="Name" color="16" fill="1" visible="yes" active="yes"/>
<layer number="105" name="tPlate" color="7" fill="1" visible="yes" active="yes"/>
<layer number="106" name="bPlate" color="7" fill="1" visible="yes" active="yes"/>
<layer number="107" name="Crop" color="7" fill="1" visible="yes" active="yes"/>
<layer number="108" name="tplace-old" color="10" fill="1" visible="yes" active="yes"/>
<layer number="109" name="ref-old" color="11" fill="1" visible="yes" active="yes"/>
<layer number="110" name="fp0" color="7" fill="1" visible="yes" active="yes"/>
<layer number="111" name="LPC17xx" color="7" fill="1" visible="yes" active="yes"/>
<layer number="112" name="tSilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="113" name="IDFDebug" color="7" fill="1" visible="yes" active="yes"/>
<layer number="116" name="Patch_BOT" color="9" fill="4" visible="yes" active="yes"/>
<layer number="118" name="Rect_Pads" color="7" fill="1" visible="no" active="no"/>
<layer number="121" name="_tsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="122" name="_bsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="123" name="tTestmark" color="7" fill="1" visible="yes" active="yes"/>
<layer number="124" name="bTestmark" color="7" fill="1" visible="yes" active="yes"/>
<layer number="125" name="_tNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="126" name="_bNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="127" name="_tValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="128" name="_bValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="129" name="Mask" color="7" fill="1" visible="yes" active="yes"/>
<layer number="131" name="tAdjust" color="7" fill="1" visible="yes" active="yes"/>
<layer number="132" name="bAdjust" color="7" fill="1" visible="yes" active="yes"/>
<layer number="144" name="Drill_legend" color="7" fill="1" visible="yes" active="yes"/>
<layer number="150" name="Notes" color="7" fill="1" visible="yes" active="yes"/>
<layer number="151" name="HeatSink" color="7" fill="1" visible="yes" active="yes"/>
<layer number="152" name="_bDocu" color="7" fill="1" visible="yes" active="yes"/>
<layer number="153" name="FabDoc1" color="7" fill="1" visible="yes" active="yes"/>
<layer number="154" name="FabDoc2" color="7" fill="1" visible="yes" active="yes"/>
<layer number="155" name="FabDoc3" color="7" fill="1" visible="yes" active="yes"/>
<layer number="199" name="Contour" color="7" fill="1" visible="yes" active="yes"/>
<layer number="200" name="200bmp" color="1" fill="10" visible="yes" active="yes"/>
<layer number="201" name="201bmp" color="2" fill="10" visible="yes" active="yes"/>
<layer number="202" name="202bmp" color="3" fill="10" visible="yes" active="yes"/>
<layer number="203" name="203bmp" color="4" fill="10" visible="yes" active="yes"/>
<layer number="204" name="204bmp" color="5" fill="10" visible="yes" active="yes"/>
<layer number="205" name="205bmp" color="6" fill="10" visible="yes" active="yes"/>
<layer number="206" name="206bmp" color="7" fill="10" visible="yes" active="yes"/>
<layer number="207" name="207bmp" color="8" fill="10" visible="yes" active="yes"/>
<layer number="208" name="208bmp" color="9" fill="10" visible="yes" active="yes"/>
<layer number="209" name="209bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="210" name="210bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="211" name="211bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="212" name="212bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="213" name="213bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="214" name="214bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="215" name="215bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="216" name="216bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="217" name="217bmp" color="18" fill="1" visible="no" active="no"/>
<layer number="218" name="218bmp" color="19" fill="1" visible="no" active="no"/>
<layer number="219" name="219bmp" color="20" fill="1" visible="no" active="no"/>
<layer number="220" name="220bmp" color="21" fill="1" visible="no" active="no"/>
<layer number="221" name="221bmp" color="22" fill="1" visible="no" active="no"/>
<layer number="222" name="222bmp" color="23" fill="1" visible="no" active="no"/>
<layer number="223" name="223bmp" color="24" fill="1" visible="no" active="no"/>
<layer number="224" name="224bmp" color="25" fill="1" visible="no" active="no"/>
<layer number="225" name="225bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="226" name="226bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="227" name="227bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="228" name="228bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="229" name="229bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="230" name="230bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="231" name="231bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="232" name="Eagle3D_PG2" color="7" fill="1" visible="no" active="no"/>
<layer number="233" name="Eagle3D_PG3" color="7" fill="1" visible="no" active="no"/>
<layer number="248" name="Housing" color="7" fill="1" visible="yes" active="yes"/>
<layer number="249" name="Edge" color="7" fill="1" visible="yes" active="yes"/>
<layer number="250" name="Descript" color="3" fill="1" visible="no" active="no"/>
<layer number="251" name="SMDround" color="12" fill="11" visible="no" active="no"/>
<layer number="254" name="cooling" color="7" fill="1" visible="yes" active="yes"/>
<layer number="255" name="routoute" color="7" fill="1" visible="yes" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="UNLibrary">
<packages>
<package name="ESP32_AZ">
<pad name="5V" x="1.5" y="4.64" drill="0.85" shape="square"/>
<pad name="GND-V5" x="1.5" y="7.18" drill="0.85" shape="square"/>
<pad name="SD3" x="1.5" y="9.72" drill="0.85" shape="square"/>
<pad name="SD2" x="1.5" y="12.26" drill="0.85" shape="square"/>
<pad name="G13" x="1.5" y="14.8" drill="0.85" shape="square"/>
<pad name="GND1" x="1.5" y="17.34" drill="0.85" shape="square"/>
<pad name="G12" x="1.5" y="19.88" drill="0.85" shape="square"/>
<pad name="G14" x="1.5" y="22.42" drill="0.85" shape="square"/>
<pad name="G27" x="1.5" y="24.96" drill="0.85" shape="square"/>
<pad name="G26" x="1.5" y="27.5" drill="0.85" shape="square"/>
<pad name="G25" x="1.5" y="30.04" drill="0.85" shape="square"/>
<pad name="G33" x="1.5" y="32.58" drill="0.85" shape="square"/>
<pad name="G32" x="1.5" y="35.12" drill="0.85" shape="square"/>
<pad name="G35" x="1.5" y="37.66" drill="0.85" shape="square"/>
<pad name="G34" x="1.5" y="40.2" drill="0.85" shape="square"/>
<pad name="SN" x="1.5" y="42.74" drill="0.85" shape="square"/>
<pad name="SP" x="1.5" y="45.28" drill="0.85" shape="square"/>
<pad name="EN" x="1.5" y="47.82" drill="0.85" shape="square"/>
<pad name="3.3V" x="1.5" y="50.36" drill="0.85" shape="square"/>
<pad name="CLK" x="26.5" y="4.64" drill="0.85" shape="square"/>
<pad name="SD0" x="26.5" y="7.18" drill="0.85" shape="square"/>
<pad name="SD1" x="26.5" y="9.72" drill="0.85" shape="square"/>
<pad name="G15" x="26.5" y="12.26" drill="0.85" shape="square"/>
<pad name="G2" x="26.5" y="14.8" drill="0.85" shape="square"/>
<pad name="G0" x="26.5" y="17.34" drill="0.85" shape="square"/>
<pad name="G4" x="26.5" y="19.88" drill="0.85" shape="square"/>
<pad name="G16" x="26.5" y="22.42" drill="0.85" shape="square"/>
<pad name="G17" x="26.5" y="24.96" drill="0.85" shape="square"/>
<pad name="G5" x="26.5" y="27.5" drill="0.85" shape="square"/>
<pad name="G18" x="26.5" y="30.04" drill="0.85" shape="square"/>
<pad name="G19" x="26.5" y="32.58" drill="0.85" shape="square"/>
<pad name="GND3" x="26.5" y="35.12" drill="0.85" shape="square"/>
<pad name="G21" x="26.5" y="37.66" drill="0.85" shape="square"/>
<pad name="RXD" x="26.5" y="40.2" drill="0.85" shape="square"/>
<pad name="TXD" x="26.5" y="42.74" drill="0.85" shape="square"/>
<pad name="G22" x="26.5" y="45.28" drill="0.85" shape="square"/>
<pad name="G23" x="26.5" y="47.82" drill="0.85" shape="square"/>
<pad name="GND2" x="26.5" y="50.36" drill="0.85" shape="square"/>
<wire x1="0" y1="0" x2="0" y2="55" width="0.127" layer="21"/>
<wire x1="0" y1="55" x2="28" y2="55" width="0.127" layer="21"/>
<wire x1="28" y1="55" x2="28" y2="0" width="0.127" layer="21"/>
<wire x1="28" y1="0" x2="0" y2="0" width="0.127" layer="21"/>
<polygon width="0.127" layer="21">
<vertex x="10.25" y="0"/>
<vertex x="10.25" y="2.54"/>
<vertex x="17.75" y="2.54"/>
<vertex x="17.75" y="0"/>
</polygon>
<text x="14" y="3" size="1.27" layer="21" align="bottom-center">USB</text>
<text x="3" y="49.86" size="1.27" layer="21">3V3</text>
<text x="3" y="47.32" size="1.27" layer="21">EN</text>
<text x="3" y="44.78" size="1.27" layer="21">G36/SP</text>
<text x="3" y="42.24" size="1.27" layer="21">G39/SN</text>
<text x="3" y="39.7" size="1.27" layer="21">G34</text>
<text x="3" y="37.16" size="1.27" layer="21">G35</text>
<text x="3" y="34.62" size="1.27" layer="21">G32</text>
<text x="3" y="32.08" size="1.27" layer="21">G33</text>
<text x="3" y="29.54" size="1.27" layer="21">G25</text>
<text x="3" y="27" size="1.27" layer="21">G26</text>
<text x="3" y="24.46" size="1.27" layer="21">G27</text>
<text x="3" y="21.92" size="1.27" layer="21">G14</text>
<text x="3" y="19.38" size="1.27" layer="21">G12</text>
<text x="3" y="16.84" size="1.27" layer="21">GND</text>
<text x="3.04" y="14.3" size="1.27" layer="21">G13</text>
<text x="3" y="11.76" size="1.27" layer="21">G9/SD2</text>
<text x="3" y="9.22" size="1.27" layer="21">G10/SD3</text>
<text x="3" y="6.68" size="1.27" layer="21">CMD</text>
<text x="3" y="4.14" size="1.27" layer="21">V5</text>
<text x="25" y="5.14" size="1.27" layer="21" rot="R180">G6/CLK</text>
<text x="25" y="7.68" size="1.27" layer="21" rot="R180">G7/SD0</text>
<text x="25" y="10.22" size="1.27" layer="21" rot="R180">G8/SD1</text>
<text x="25" y="12.76" size="1.27" layer="21" rot="R180">G15</text>
<text x="25" y="15.3" size="1.27" layer="21" rot="R180">G2</text>
<text x="25" y="17.84" size="1.27" layer="21" rot="R180">G0</text>
<text x="25" y="20.38" size="1.27" layer="21" rot="R180">G4</text>
<text x="25" y="22.92" size="1.27" layer="21" rot="R180">G16</text>
<text x="25" y="25.46" size="1.27" layer="21" rot="R180">G17</text>
<text x="25" y="28" size="1.27" layer="21" rot="R180">G5</text>
<text x="25" y="30.54" size="1.27" layer="21" rot="R180">G18</text>
<text x="25" y="33.08" size="1.27" layer="21" rot="R180">G19</text>
<text x="25" y="35.62" size="1.27" layer="21" rot="R180">GND</text>
<text x="25" y="38.16" size="1.27" layer="21" rot="R180">G21</text>
<text x="25" y="40.7" size="1.27" layer="21" rot="R180">G3/RXD</text>
<text x="25" y="43.24" size="1.27" layer="21" rot="R180">G1/TXD</text>
<text x="25" y="45.78" size="1.27" layer="21" rot="R180">G22</text>
<text x="25" y="48.32" size="1.27" layer="21" rot="R180">G23</text>
<text x="25" y="50.86" size="1.27" layer="21" rot="R180">GND</text>
</package>
<package name="7SEG-CA_2.9INCH">
<description>7-segment &lt;B&gt;DISPLAY&lt;/B&gt;&lt;p&gt;
13 mm</description>
<wire x1="65" y1="0" x2="65" y2="86" width="0.1524" layer="21"/>
<wire x1="0" y1="86" x2="65" y2="86" width="0.1524" layer="21"/>
<wire x1="0" y1="86" x2="0" y2="0" width="0.1524" layer="21"/>
<wire x1="65" y1="0" x2="0" y2="0" width="0.1524" layer="21"/>
<wire x1="35.5306" y1="45.7498" x2="36.4196" y2="46.461" width="0.254" layer="21"/>
<wire x1="36.4196" y1="46.461" x2="36.8768" y2="46.1308" width="0.254" layer="21"/>
<wire x1="34.8448" y1="42.0414" x2="35.9878" y2="41.4064" width="0.254" layer="21"/>
<wire x1="35.9878" y1="41.4064" x2="36.8768" y2="46.1308" width="0.254" layer="21"/>
<wire x1="35.5306" y1="45.7498" x2="34.8448" y2="42.0414" width="0.254" layer="21"/>
<wire x1="34.54" y1="41.635" x2="35.683" y2="41" width="0.254" layer="21"/>
<wire x1="35.175" y1="46.08" x2="36.0386" y2="46.7658" width="0.254" layer="21"/>
<wire x1="36.0386" y1="46.7658" x2="35.2766" y2="47.35" width="0.254" layer="21"/>
<wire x1="34.413" y1="40.365" x2="35.683" y2="41" width="0.254" layer="21"/>
<wire x1="34.4638" y1="39.8062" x2="35.81" y2="40.492" width="0.254" layer="21"/>
<wire x1="35.81" y1="40.492" x2="34.921" y2="35.5898" width="0.254" layer="21"/>
<wire x1="34.4638" y1="39.8062" x2="33.8034" y2="36.2502" width="0.254" layer="21"/>
<wire x1="33.4732" y1="35.92" x2="34.2352" y2="35.0056" width="0.254" layer="21"/>
<wire x1="34.2352" y1="35.0056" x2="33.8288" y2="34.65" width="0.254" layer="21"/>
<wire x1="33.8034" y1="36.2502" x2="34.5908" y2="35.3104" width="0.254" layer="21"/>
<wire x1="34.5908" y1="35.3104" x2="34.921" y2="35.5898" width="0.254" layer="21"/>
<wire x1="29.46" y1="40.365" x2="28.317" y2="41" width="0.254" layer="21"/>
<wire x1="28.825" y1="35.92" x2="27.9614" y2="35.2342" width="0.254" layer="21"/>
<wire x1="27.9614" y1="35.2342" x2="28.7234" y2="34.65" width="0.254" layer="21"/>
<wire x1="28.7234" y1="34.65" x2="33.8288" y2="34.65" width="0.254" layer="21"/>
<wire x1="28.825" y1="35.92" x2="33.4732" y2="35.92" width="0.254" layer="21"/>
<wire x1="34.413" y1="40.365" x2="29.46" y2="40.365" width="0.254" layer="21"/>
<wire x1="29.587" y1="41.635" x2="28.317" y2="41" width="0.254" layer="21"/>
<wire x1="29.587" y1="41.635" x2="34.54" y2="41.635" width="0.254" layer="21"/>
<wire x1="30.5268" y1="46.08" x2="29.7648" y2="46.9944" width="0.254" layer="21"/>
<wire x1="29.7648" y1="46.9944" x2="30.1712" y2="47.35" width="0.254" layer="21"/>
<wire x1="30.1712" y1="47.35" x2="35.2766" y2="47.35" width="0.254" layer="21"/>
<wire x1="30.5268" y1="46.08" x2="35.175" y2="46.08" width="0.254" layer="21"/>
<wire x1="28.698" y1="35.285" x2="33.397" y2="35.285" width="1.016" layer="21"/>
<wire x1="34.413" y1="36.301" x2="35.048" y2="39.476" width="1.016" layer="21"/>
<wire x1="34.54" y1="35.793" x2="34.667" y2="35.92" width="0.762" layer="21"/>
<wire x1="35.556" y1="42.397" x2="36.191" y2="45.699" width="1.016" layer="21"/>
<wire x1="36.445" y1="46.08" x2="36.572" y2="45.953" width="0.508" layer="21"/>
<wire x1="35.175" y1="42.27" x2="35.81" y2="41.889" width="0.6604" layer="21"/>
<wire x1="35.302" y1="46.715" x2="30.603" y2="46.715" width="1.016" layer="21"/>
<wire x1="29.968" y1="46.842" x2="30.349" y2="47.223" width="0.4064" layer="21"/>
<wire x1="28.4694" y1="36.2502" x2="27.5804" y2="35.539" width="0.254" layer="21"/>
<wire x1="27.5804" y1="35.539" x2="27.1232" y2="35.8692" width="0.254" layer="21"/>
<wire x1="29.1552" y1="39.9586" x2="28.0122" y2="40.5936" width="0.254" layer="21"/>
<wire x1="28.0122" y1="40.5936" x2="27.1232" y2="35.8692" width="0.254" layer="21"/>
<wire x1="28.4694" y1="36.2502" x2="29.1552" y2="39.9586" width="0.254" layer="21"/>
<wire x1="29.5362" y1="42.1938" x2="28.19" y2="41.508" width="0.254" layer="21"/>
<wire x1="28.19" y1="41.508" x2="29.079" y2="46.4102" width="0.254" layer="21"/>
<wire x1="29.5362" y1="42.1938" x2="30.1966" y2="45.7498" width="0.254" layer="21"/>
<wire x1="30.1966" y1="45.7498" x2="29.4092" y2="46.6896" width="0.254" layer="21"/>
<wire x1="29.4092" y1="46.6896" x2="29.079" y2="46.4102" width="0.254" layer="21"/>
<wire x1="29.587" y1="45.699" x2="28.952" y2="42.524" width="1.016" layer="21"/>
<wire x1="29.46" y1="46.207" x2="29.333" y2="46.08" width="0.762" layer="21"/>
<wire x1="28.444" y1="39.603" x2="27.809" y2="36.301" width="1.016" layer="21"/>
<wire x1="27.555" y1="35.92" x2="27.428" y2="36.047" width="0.508" layer="21"/>
<wire x1="28.825" y1="39.73" x2="28.19" y2="40.111" width="0.6604" layer="21"/>
<circle x="36.572" y="35.285" radius="0.381" width="0.762" layer="21"/>
<pad name="1" x="27.42" y="3.75" drill="0.8128" shape="long" rot="R90"/>
<pad name="2" x="29.96" y="3.75" drill="0.8128" shape="long" rot="R90"/>
<pad name="3" x="32.5" y="3.75" drill="0.8128" shape="long" rot="R90"/>
<pad name="4" x="35.04" y="3.75" drill="0.8128" shape="long" rot="R90"/>
<pad name="5" x="37.58" y="3.75" drill="0.8128" shape="long" rot="R90"/>
<pad name="6" x="37.58" y="82.25" drill="0.8128" shape="long" rot="R90"/>
<pad name="7" x="35.04" y="82.25" drill="0.8128" shape="long" rot="R90"/>
<pad name="8" x="32.5" y="82.25" drill="0.8128" shape="long" rot="R90"/>
<pad name="9" x="29.96" y="82.25" drill="0.8128" shape="long" rot="R90"/>
<pad name="10" x="27.42" y="82.25" drill="0.8128" shape="long" rot="R90"/>
<text x="29.904" y="67.144" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="28.904" y="17.078" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="33.524" y1="34.777" x2="34.159" y2="35.285" layer="21"/>
<rectangle x1="35.175" y1="39.73" x2="35.683" y2="40.238" layer="21"/>
<rectangle x1="35.048" y1="40.746" x2="35.429" y2="41.254" layer="21"/>
<rectangle x1="28.571" y1="40.873" x2="28.952" y2="41.127" layer="21"/>
<rectangle x1="28.317" y1="41.762" x2="28.825" y2="42.27" layer="21"/>
<rectangle x1="36.318" y1="35.158" x2="36.826" y2="35.412" layer="21"/>
</package>
<package name="7SEG-13">
<description>7-segment &lt;B&gt;DISPLAY&lt;/B&gt;&lt;p&gt;
13 mm</description>
<wire x1="6.096" y1="-8.636" x2="6.096" y2="8.636" width="0.1524" layer="21"/>
<wire x1="-6.096" y1="8.636" x2="6.096" y2="8.636" width="0.1524" layer="21"/>
<wire x1="-6.096" y1="8.636" x2="-6.096" y2="-8.636" width="0.1524" layer="21"/>
<wire x1="6.096" y1="-8.636" x2="-6.096" y2="-8.636" width="0.1524" layer="21"/>
<wire x1="3.5306" y1="4.7498" x2="4.4196" y2="5.461" width="0.254" layer="21"/>
<wire x1="4.4196" y1="5.461" x2="4.8768" y2="5.1308" width="0.254" layer="21"/>
<wire x1="2.8448" y1="1.0414" x2="3.9878" y2="0.4064" width="0.254" layer="21"/>
<wire x1="3.9878" y1="0.4064" x2="4.8768" y2="5.1308" width="0.254" layer="21"/>
<wire x1="3.5306" y1="4.7498" x2="2.8448" y2="1.0414" width="0.254" layer="21"/>
<wire x1="2.54" y1="0.635" x2="3.683" y2="0" width="0.254" layer="21"/>
<wire x1="3.175" y1="5.08" x2="4.0386" y2="5.7658" width="0.254" layer="21"/>
<wire x1="4.0386" y1="5.7658" x2="3.2766" y2="6.35" width="0.254" layer="21"/>
<wire x1="2.413" y1="-0.635" x2="3.683" y2="0" width="0.254" layer="21"/>
<wire x1="2.4638" y1="-1.1938" x2="3.81" y2="-0.508" width="0.254" layer="21"/>
<wire x1="3.81" y1="-0.508" x2="2.921" y2="-5.4102" width="0.254" layer="21"/>
<wire x1="2.4638" y1="-1.1938" x2="1.8034" y2="-4.7498" width="0.254" layer="21"/>
<wire x1="1.4732" y1="-5.08" x2="2.2352" y2="-5.9944" width="0.254" layer="21"/>
<wire x1="2.2352" y1="-5.9944" x2="1.8288" y2="-6.35" width="0.254" layer="21"/>
<wire x1="1.8034" y1="-4.7498" x2="2.5908" y2="-5.6896" width="0.254" layer="21"/>
<wire x1="2.5908" y1="-5.6896" x2="2.921" y2="-5.4102" width="0.254" layer="21"/>
<wire x1="-2.54" y1="-0.635" x2="-3.683" y2="0" width="0.254" layer="21"/>
<wire x1="-3.175" y1="-5.08" x2="-4.0386" y2="-5.7658" width="0.254" layer="21"/>
<wire x1="-4.0386" y1="-5.7658" x2="-3.2766" y2="-6.35" width="0.254" layer="21"/>
<wire x1="-3.2766" y1="-6.35" x2="1.8288" y2="-6.35" width="0.254" layer="21"/>
<wire x1="-3.175" y1="-5.08" x2="1.4732" y2="-5.08" width="0.254" layer="21"/>
<wire x1="2.413" y1="-0.635" x2="-2.54" y2="-0.635" width="0.254" layer="21"/>
<wire x1="-2.413" y1="0.635" x2="-3.683" y2="0" width="0.254" layer="21"/>
<wire x1="-2.413" y1="0.635" x2="2.54" y2="0.635" width="0.254" layer="21"/>
<wire x1="-1.4732" y1="5.08" x2="-2.2352" y2="5.9944" width="0.254" layer="21"/>
<wire x1="-2.2352" y1="5.9944" x2="-1.8288" y2="6.35" width="0.254" layer="21"/>
<wire x1="-1.8288" y1="6.35" x2="3.2766" y2="6.35" width="0.254" layer="21"/>
<wire x1="-1.4732" y1="5.08" x2="3.175" y2="5.08" width="0.254" layer="21"/>
<wire x1="-3.302" y1="-5.715" x2="1.397" y2="-5.715" width="1.016" layer="21"/>
<wire x1="2.413" y1="-4.699" x2="3.048" y2="-1.524" width="1.016" layer="21"/>
<wire x1="-2.667" y1="0" x2="2.667" y2="0" width="1.016" layer="21"/>
<wire x1="2.54" y1="-5.207" x2="2.667" y2="-5.08" width="0.762" layer="21"/>
<wire x1="3.556" y1="1.397" x2="4.191" y2="4.699" width="1.016" layer="21"/>
<wire x1="4.445" y1="5.08" x2="4.572" y2="4.953" width="0.508" layer="21"/>
<wire x1="3.175" y1="1.27" x2="3.81" y2="0.889" width="0.6604" layer="21"/>
<wire x1="3.302" y1="5.715" x2="-1.397" y2="5.715" width="1.016" layer="21"/>
<wire x1="-2.032" y1="5.842" x2="-1.651" y2="6.223" width="0.4064" layer="21"/>
<wire x1="-3.5306" y1="-4.7498" x2="-4.4196" y2="-5.461" width="0.254" layer="21"/>
<wire x1="-4.4196" y1="-5.461" x2="-4.8768" y2="-5.1308" width="0.254" layer="21"/>
<wire x1="-2.8448" y1="-1.0414" x2="-3.9878" y2="-0.4064" width="0.254" layer="21"/>
<wire x1="-3.9878" y1="-0.4064" x2="-4.8768" y2="-5.1308" width="0.254" layer="21"/>
<wire x1="-3.5306" y1="-4.7498" x2="-2.8448" y2="-1.0414" width="0.254" layer="21"/>
<wire x1="-2.4638" y1="1.1938" x2="-3.81" y2="0.508" width="0.254" layer="21"/>
<wire x1="-3.81" y1="0.508" x2="-2.921" y2="5.4102" width="0.254" layer="21"/>
<wire x1="-2.4638" y1="1.1938" x2="-1.8034" y2="4.7498" width="0.254" layer="21"/>
<wire x1="-1.8034" y1="4.7498" x2="-2.5908" y2="5.6896" width="0.254" layer="21"/>
<wire x1="-2.5908" y1="5.6896" x2="-2.921" y2="5.4102" width="0.254" layer="21"/>
<wire x1="-2.413" y1="4.699" x2="-3.048" y2="1.524" width="1.016" layer="21"/>
<wire x1="-2.54" y1="5.207" x2="-2.667" y2="5.08" width="0.762" layer="21"/>
<wire x1="-3.556" y1="-1.397" x2="-4.191" y2="-4.699" width="1.016" layer="21"/>
<wire x1="-4.445" y1="-5.08" x2="-4.572" y2="-4.953" width="0.508" layer="21"/>
<wire x1="-3.175" y1="-1.27" x2="-3.81" y2="-0.889" width="0.6604" layer="21"/>
<circle x="4.572" y="-5.715" radius="0.381" width="0.762" layer="21"/>
<pad name="1" x="-5.08" y="-7.62" drill="0.8128" shape="long" rot="R90"/>
<pad name="2" x="-2.54" y="-7.62" drill="0.8128" shape="long" rot="R90"/>
<pad name="3" x="0" y="-7.62" drill="0.8128" shape="long" rot="R90"/>
<pad name="4" x="2.54" y="-7.62" drill="0.8128" shape="long" rot="R90"/>
<pad name="5" x="5.08" y="-7.62" drill="0.8128" shape="long" rot="R90"/>
<pad name="6" x="5.08" y="7.62" drill="0.8128" shape="long" rot="R90"/>
<pad name="7" x="2.54" y="7.62" drill="0.8128" shape="long" rot="R90"/>
<pad name="8" x="0" y="7.62" drill="0.8128" shape="long" rot="R90"/>
<pad name="9" x="-2.54" y="7.62" drill="0.8128" shape="long" rot="R90"/>
<pad name="10" x="-5.08" y="7.62" drill="0.8128" shape="long" rot="R90"/>
<text x="-6.096" y="9.144" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-6.096" y="-10.922" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="1.524" y1="-6.223" x2="2.159" y2="-5.715" layer="21"/>
<rectangle x1="3.175" y1="-1.27" x2="3.683" y2="-0.762" layer="21"/>
<rectangle x1="3.048" y1="-0.254" x2="3.429" y2="0.254" layer="21"/>
<rectangle x1="-3.429" y1="-0.127" x2="-3.048" y2="0.127" layer="21"/>
<rectangle x1="-3.683" y1="0.762" x2="-3.175" y2="1.27" layer="21"/>
<rectangle x1="4.318" y1="-5.842" x2="4.826" y2="-5.588" layer="21"/>
</package>
<package name="DIL28-3">
<description>&lt;B&gt;Dual In Line&lt;/B&gt;&lt;p&gt;
package type P</description>
<wire x1="-17.78" y1="-1.27" x2="-17.78" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-17.78" y1="1.27" x2="-17.78" y2="-1.27" width="0.1524" layer="21" curve="-180"/>
<wire x1="17.78" y1="-2.54" x2="17.78" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-17.78" y1="2.54" x2="-17.78" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-17.78" y1="2.54" x2="17.78" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-17.653" y1="-2.54" x2="17.78" y2="-2.54" width="0.1524" layer="21"/>
<pad name="1" x="-16.51" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="2" x="-13.97" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="3" x="-11.43" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="4" x="-8.89" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="5" x="-6.35" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="6" x="-3.81" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="7" x="-1.27" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="8" x="1.27" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="9" x="3.81" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="10" x="6.35" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="11" x="8.89" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="12" x="11.43" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="13" x="13.97" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="14" x="16.51" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="15" x="16.51" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="16" x="13.97" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="17" x="11.43" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="18" x="8.89" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="19" x="6.35" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="20" x="3.81" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="21" x="1.27" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="22" x="-1.27" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="23" x="-3.81" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="24" x="-6.35" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="25" x="-8.89" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="26" x="-11.43" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="27" x="-13.97" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="28" x="-16.51" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<text x="-17.907" y="-2.54" size="1.778" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<text x="-15.748" y="-0.9398" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="SO28W">
<description>&lt;B&gt;28-Lead Plastic Small Outline (SO) &lt;/B&gt; Wide, 300 mil Body (SOIC)&lt;/B&gt;&lt;p&gt;
Source: http://ww1.microchip.com/downloads/en/devicedoc/39632c.pdf</description>
<wire x1="-8.1788" y1="-3.7132" x2="9.4742" y2="-3.7132" width="0.1524" layer="21"/>
<wire x1="9.4742" y1="-3.7132" x2="9.4742" y2="3.7132" width="0.1524" layer="21"/>
<wire x1="9.4742" y1="3.7132" x2="-8.1788" y2="3.7132" width="0.1524" layer="21"/>
<wire x1="-8.1788" y1="3.7132" x2="-8.1788" y2="-3.7132" width="0.1524" layer="21"/>
<circle x="-7.239" y="-3.1496" radius="0.5334" width="0.1524" layer="21"/>
<smd name="1" x="-7.62" y="-4.78" dx="0.65" dy="1.7" layer="1"/>
<smd name="2" x="-6.35" y="-4.78" dx="0.65" dy="1.7" layer="1"/>
<smd name="3" x="-5.08" y="-4.78" dx="0.65" dy="1.7" layer="1"/>
<smd name="4" x="-3.81" y="-4.78" dx="0.65" dy="1.7" layer="1"/>
<smd name="5" x="-2.54" y="-4.78" dx="0.65" dy="1.7" layer="1"/>
<smd name="6" x="-1.27" y="-4.78" dx="0.65" dy="1.7" layer="1"/>
<smd name="7" x="0" y="-4.78" dx="0.65" dy="1.7" layer="1"/>
<smd name="8" x="1.27" y="-4.78" dx="0.65" dy="1.7" layer="1"/>
<smd name="9" x="2.54" y="-4.78" dx="0.65" dy="1.7" layer="1"/>
<smd name="10" x="3.81" y="-4.78" dx="0.65" dy="1.7" layer="1"/>
<smd name="20" x="2.54" y="4.78" dx="0.65" dy="1.7" layer="1"/>
<smd name="19" x="3.81" y="4.78" dx="0.65" dy="1.7" layer="1"/>
<smd name="18" x="5.08" y="4.78" dx="0.65" dy="1.7" layer="1"/>
<smd name="17" x="6.35" y="4.78" dx="0.65" dy="1.7" layer="1"/>
<smd name="16" x="7.62" y="4.78" dx="0.65" dy="1.7" layer="1"/>
<smd name="15" x="8.89" y="4.78" dx="0.65" dy="1.7" layer="1"/>
<smd name="14" x="8.89" y="-4.78" dx="0.65" dy="1.7" layer="1"/>
<smd name="13" x="7.62" y="-4.78" dx="0.65" dy="1.7" layer="1"/>
<smd name="12" x="6.35" y="-4.78" dx="0.65" dy="1.7" layer="1"/>
<smd name="11" x="5.08" y="-4.78" dx="0.65" dy="1.7" layer="1"/>
<smd name="21" x="1.27" y="4.78" dx="0.65" dy="1.7" layer="1"/>
<smd name="22" x="0" y="4.78" dx="0.65" dy="1.7" layer="1"/>
<smd name="23" x="-1.27" y="4.78" dx="0.65" dy="1.7" layer="1"/>
<smd name="24" x="-2.54" y="4.78" dx="0.65" dy="1.7" layer="1"/>
<smd name="25" x="-3.81" y="4.78" dx="0.65" dy="1.7" layer="1"/>
<smd name="26" x="-5.08" y="4.78" dx="0.65" dy="1.7" layer="1"/>
<smd name="27" x="-6.35" y="4.78" dx="0.65" dy="1.7" layer="1"/>
<smd name="28" x="-7.62" y="4.78" dx="0.65" dy="1.7" layer="1"/>
<text x="-8.509" y="-4.064" size="1.778" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<text x="11.557" y="-4.064" size="1.778" layer="27" ratio="10" rot="R90">&gt;VALUE</text>
<rectangle x1="-7.874" y1="-5.2626" x2="-7.366" y2="-3.7386" layer="51"/>
<rectangle x1="-6.604" y1="-5.2626" x2="-6.096" y2="-3.7386" layer="51"/>
<rectangle x1="-5.334" y1="-5.2626" x2="-4.826" y2="-3.7386" layer="51"/>
<rectangle x1="-4.064" y1="-5.2626" x2="-3.556" y2="-3.7386" layer="51"/>
<rectangle x1="-2.794" y1="-5.2626" x2="-2.286" y2="-3.7386" layer="51"/>
<rectangle x1="-1.524" y1="-5.2626" x2="-1.016" y2="-3.7386" layer="51"/>
<rectangle x1="-0.254" y1="-5.2626" x2="0.254" y2="-3.7386" layer="51"/>
<rectangle x1="1.016" y1="-5.2626" x2="1.524" y2="-3.7386" layer="51"/>
<rectangle x1="2.286" y1="-5.2626" x2="2.794" y2="-3.7386" layer="51"/>
<rectangle x1="3.556" y1="-5.2626" x2="4.064" y2="-3.7386" layer="51"/>
<rectangle x1="4.826" y1="-5.2626" x2="5.334" y2="-3.7386" layer="51"/>
<rectangle x1="6.096" y1="-5.2626" x2="6.604" y2="-3.7386" layer="51"/>
<rectangle x1="7.366" y1="-5.2626" x2="7.874" y2="-3.7386" layer="51"/>
<rectangle x1="8.636" y1="-5.2626" x2="9.144" y2="-3.7386" layer="51"/>
<rectangle x1="8.636" y1="3.7386" x2="9.144" y2="5.2626" layer="51"/>
<rectangle x1="7.366" y1="3.7386" x2="7.874" y2="5.2626" layer="51"/>
<rectangle x1="6.096" y1="3.7386" x2="6.604" y2="5.2626" layer="51"/>
<rectangle x1="4.826" y1="3.7386" x2="5.334" y2="5.2626" layer="51"/>
<rectangle x1="3.556" y1="3.7386" x2="4.064" y2="5.2626" layer="51"/>
<rectangle x1="2.286" y1="3.7386" x2="2.794" y2="5.2626" layer="51"/>
<rectangle x1="1.016" y1="3.7386" x2="1.524" y2="5.2626" layer="51"/>
<rectangle x1="-0.254" y1="3.7386" x2="0.254" y2="5.2626" layer="51"/>
<rectangle x1="-1.524" y1="3.7386" x2="-1.016" y2="5.2626" layer="51"/>
<rectangle x1="-2.794" y1="3.7386" x2="-2.286" y2="5.2626" layer="51"/>
<rectangle x1="-4.064" y1="3.7386" x2="-3.556" y2="5.2626" layer="51"/>
<rectangle x1="-5.334" y1="3.7386" x2="-4.826" y2="5.2626" layer="51"/>
<rectangle x1="-6.604" y1="3.7386" x2="-6.096" y2="5.2626" layer="51"/>
<rectangle x1="-7.874" y1="3.7386" x2="-7.366" y2="5.2626" layer="51"/>
</package>
<package name="SSOP28">
<description>&lt;b&gt;Shrink Small Outline Package&lt;/b&gt;&lt;p&gt;
package type SS</description>
<wire x1="-5.1" y1="-2.6" x2="5.1" y2="-2.6" width="0.2032" layer="21"/>
<wire x1="5.1" y1="-2.6" x2="5.1" y2="2.6" width="0.2032" layer="21"/>
<wire x1="5.1" y1="2.6" x2="-5.1" y2="2.6" width="0.2032" layer="21"/>
<smd name="1" x="-4.225" y="-3.625" dx="0.4" dy="1.5" layer="1"/>
<smd name="2" x="-3.575" y="-3.625" dx="0.4" dy="1.5" layer="1"/>
<smd name="3" x="-2.925" y="-3.625" dx="0.4" dy="1.5" layer="1"/>
<smd name="4" x="-2.275" y="-3.625" dx="0.4" dy="1.5" layer="1"/>
<smd name="5" x="-1.625" y="-3.625" dx="0.4" dy="1.5" layer="1"/>
<smd name="6" x="-0.975" y="-3.625" dx="0.4" dy="1.5" layer="1"/>
<smd name="7" x="-0.325" y="-3.625" dx="0.4" dy="1.5" layer="1"/>
<smd name="8" x="0.325" y="-3.625" dx="0.4" dy="1.5" layer="1"/>
<smd name="9" x="0.975" y="-3.625" dx="0.4" dy="1.5" layer="1"/>
<smd name="10" x="1.625" y="-3.625" dx="0.4" dy="1.5" layer="1"/>
<smd name="11" x="2.275" y="-3.625" dx="0.4" dy="1.5" layer="1"/>
<smd name="12" x="2.925" y="-3.625" dx="0.4" dy="1.5" layer="1"/>
<smd name="13" x="3.575" y="-3.625" dx="0.4" dy="1.5" layer="1"/>
<smd name="14" x="4.225" y="-3.625" dx="0.4" dy="1.5" layer="1"/>
<smd name="15" x="4.225" y="3.625" dx="0.4" dy="1.5" layer="1"/>
<smd name="16" x="3.575" y="3.625" dx="0.4" dy="1.5" layer="1"/>
<smd name="17" x="2.925" y="3.625" dx="0.4" dy="1.5" layer="1"/>
<smd name="18" x="2.275" y="3.625" dx="0.4" dy="1.5" layer="1"/>
<smd name="19" x="1.625" y="3.625" dx="0.4" dy="1.5" layer="1"/>
<smd name="20" x="0.975" y="3.625" dx="0.4" dy="1.5" layer="1"/>
<smd name="21" x="0.325" y="3.625" dx="0.4" dy="1.5" layer="1"/>
<smd name="22" x="-0.325" y="3.625" dx="0.4" dy="1.5" layer="1"/>
<smd name="23" x="-0.975" y="3.625" dx="0.4" dy="1.5" layer="1"/>
<smd name="24" x="-1.625" y="3.625" dx="0.4" dy="1.5" layer="1"/>
<smd name="25" x="-2.275" y="3.625" dx="0.4" dy="1.5" layer="1"/>
<smd name="26" x="-2.925" y="3.625" dx="0.4" dy="1.5" layer="1"/>
<smd name="27" x="-3.575" y="3.625" dx="0.4" dy="1.5" layer="1"/>
<smd name="28" x="-4.225" y="3.625" dx="0.4" dy="1.5" layer="1"/>
<text x="-5.476" y="-2.6299" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="-3.8999" y="-0.68" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-4.4028" y1="-3.937" x2="-4.0472" y2="-2.6416" layer="51"/>
<rectangle x1="-3.7529" y1="-3.937" x2="-3.3973" y2="-2.6416" layer="51"/>
<rectangle x1="-3.1029" y1="-3.937" x2="-2.7473" y2="-2.6416" layer="51"/>
<rectangle x1="-2.4529" y1="-3.937" x2="-2.0973" y2="-2.6416" layer="51"/>
<rectangle x1="-1.8029" y1="-3.937" x2="-1.4473" y2="-2.6416" layer="51"/>
<rectangle x1="-1.1529" y1="-3.937" x2="-0.7973" y2="-2.6416" layer="51"/>
<rectangle x1="-0.5029" y1="-3.937" x2="-0.1473" y2="-2.6416" layer="51"/>
<rectangle x1="0.1473" y1="-3.937" x2="0.5029" y2="-2.6416" layer="51"/>
<rectangle x1="0.7973" y1="-3.937" x2="1.1529" y2="-2.6416" layer="51"/>
<rectangle x1="1.4473" y1="-3.937" x2="1.8029" y2="-2.6416" layer="51"/>
<rectangle x1="2.0973" y1="-3.937" x2="2.4529" y2="-2.6416" layer="51"/>
<rectangle x1="2.7473" y1="-3.937" x2="3.1029" y2="-2.6416" layer="51"/>
<rectangle x1="3.3973" y1="-3.937" x2="3.7529" y2="-2.6416" layer="51"/>
<rectangle x1="4.0472" y1="-3.937" x2="4.4028" y2="-2.6416" layer="51"/>
<rectangle x1="4.0472" y1="2.6416" x2="4.4028" y2="3.937" layer="51"/>
<rectangle x1="3.3973" y1="2.6416" x2="3.7529" y2="3.937" layer="51"/>
<rectangle x1="2.7473" y1="2.6416" x2="3.1029" y2="3.937" layer="51"/>
<rectangle x1="2.0973" y1="2.6416" x2="2.4529" y2="3.937" layer="51"/>
<rectangle x1="1.4473" y1="2.6416" x2="1.8029" y2="3.937" layer="51"/>
<rectangle x1="0.7973" y1="2.6416" x2="1.1529" y2="3.937" layer="51"/>
<rectangle x1="0.1473" y1="2.6416" x2="0.5029" y2="3.937" layer="51"/>
<rectangle x1="-0.5029" y1="2.6416" x2="-0.1473" y2="3.937" layer="51"/>
<rectangle x1="-1.1529" y1="2.6416" x2="-0.7973" y2="3.937" layer="51"/>
<rectangle x1="-1.8029" y1="2.6416" x2="-1.4473" y2="3.937" layer="51"/>
<rectangle x1="-2.4529" y1="2.6416" x2="-2.0973" y2="3.937" layer="51"/>
<rectangle x1="-3.1029" y1="2.6416" x2="-2.7473" y2="3.937" layer="51"/>
<rectangle x1="-3.7529" y1="2.6416" x2="-3.3973" y2="3.937" layer="51"/>
<rectangle x1="-4.4028" y1="2.6416" x2="-4.0472" y2="3.937" layer="51"/>
<rectangle x1="-5.1999" y1="-2.5999" x2="-4.225" y2="2.5999" layer="27"/>
</package>
<package name="QFN28-ML_6X6MM">
<description>&lt;b&gt;QFN28-ML_6X6MM&lt;/b&gt;&lt;p&gt;
Source: http://www.microchip.com .. 39637a.pdf</description>
<wire x1="-2.8984" y1="-2.8984" x2="2.8984" y2="-2.8984" width="0.2032" layer="51"/>
<wire x1="2.8984" y1="-2.8984" x2="2.8984" y2="2.8984" width="0.2032" layer="51"/>
<wire x1="2.8984" y1="2.8984" x2="-2.22" y2="2.8984" width="0.2032" layer="51"/>
<wire x1="-2.22" y1="2.8984" x2="-2.22" y2="2.9" width="0.2032" layer="21"/>
<wire x1="-2.8984" y1="2.8984" x2="-2.22" y2="2.8984" width="0.2032" layer="21"/>
<wire x1="-2.22" y1="2.9" x2="-2.8984" y2="2.2216" width="0.2032" layer="21"/>
<wire x1="-2.8984" y1="2.2216" x2="-2.8984" y2="-2.8984" width="0.2032" layer="51"/>
<wire x1="-2.8984" y1="2.2216" x2="-2.8984" y2="2.8984" width="0.2032" layer="21"/>
<smd name="1" x="-2.7" y="1.95" dx="0.7" dy="0.35" layer="1" roundness="20" stop="no" cream="no"/>
<smd name="2" x="-2.7" y="1.3" dx="0.7" dy="0.35" layer="1" roundness="20" stop="no" cream="no"/>
<smd name="3" x="-2.7" y="0.65" dx="0.7" dy="0.35" layer="1" roundness="20" stop="no" cream="no"/>
<smd name="4" x="-2.7" y="0" dx="0.7" dy="0.35" layer="1" roundness="20" stop="no" cream="no"/>
<smd name="5" x="-2.7" y="-0.65" dx="0.7" dy="0.35" layer="1" roundness="20" stop="no" cream="no"/>
<smd name="6" x="-2.7" y="-1.3" dx="0.7" dy="0.35" layer="1" roundness="20" stop="no" cream="no"/>
<smd name="7" x="-2.7" y="-1.95" dx="0.7" dy="0.35" layer="1" roundness="20" stop="no" cream="no"/>
<smd name="8" x="-1.95" y="-2.7" dx="0.35" dy="0.7" layer="1" roundness="20" stop="no" cream="no"/>
<smd name="9" x="-1.3" y="-2.7" dx="0.35" dy="0.7" layer="1" roundness="20" stop="no" cream="no"/>
<smd name="10" x="-0.65" y="-2.7" dx="0.35" dy="0.7" layer="1" roundness="20" stop="no" cream="no"/>
<smd name="11" x="0" y="-2.7" dx="0.35" dy="0.7" layer="1" roundness="20" stop="no" cream="no"/>
<smd name="12" x="0.65" y="-2.7" dx="0.35" dy="0.7" layer="1" roundness="20" stop="no" cream="no"/>
<smd name="13" x="1.3" y="-2.7" dx="0.35" dy="0.7" layer="1" roundness="20" stop="no" cream="no"/>
<smd name="14" x="1.95" y="-2.7" dx="0.35" dy="0.7" layer="1" roundness="20" stop="no" cream="no"/>
<smd name="15" x="2.7" y="-1.95" dx="0.7" dy="0.35" layer="1" roundness="20" stop="no" cream="no"/>
<smd name="16" x="2.7" y="-1.3" dx="0.7" dy="0.35" layer="1" roundness="20" stop="no" cream="no"/>
<smd name="17" x="2.7" y="-0.65" dx="0.7" dy="0.35" layer="1" roundness="20" stop="no" cream="no"/>
<smd name="18" x="2.7" y="0" dx="0.7" dy="0.35" layer="1" roundness="20" stop="no" cream="no"/>
<smd name="19" x="2.7" y="0.65" dx="0.7" dy="0.35" layer="1" roundness="20" stop="no" cream="no"/>
<smd name="20" x="2.7" y="1.3" dx="0.7" dy="0.35" layer="1" roundness="20" stop="no" cream="no"/>
<smd name="21" x="2.7" y="1.95" dx="0.7" dy="0.35" layer="1" roundness="20" stop="no" cream="no"/>
<smd name="22" x="1.95" y="2.7" dx="0.35" dy="0.7" layer="1" roundness="20" stop="no" cream="no"/>
<smd name="23" x="1.3" y="2.7" dx="0.35" dy="0.7" layer="1" roundness="20" stop="no" cream="no"/>
<smd name="24" x="0.65" y="2.7" dx="0.35" dy="0.7" layer="1" roundness="20" stop="no" cream="no"/>
<smd name="25" x="0" y="2.7" dx="0.35" dy="0.7" layer="1" roundness="20" stop="no" cream="no"/>
<smd name="26" x="-0.65" y="2.7" dx="0.35" dy="0.7" layer="1" roundness="20" stop="no" cream="no"/>
<smd name="27" x="-1.3" y="2.7" dx="0.35" dy="0.7" layer="1" roundness="20" stop="no" cream="no"/>
<smd name="28" x="-1.95" y="2.7" dx="0.35" dy="0.7" layer="1" roundness="20" stop="no" cream="no"/>
<smd name="EXP" x="0" y="0" dx="3.7" dy="3.7" layer="1" roundness="20" stop="no" cream="no"/>
<text x="-3.175" y="3.175" size="1.27" layer="25">&gt;NAME</text>
<text x="-3.175" y="-4.445" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-3.055" y1="1.768" x2="-2.3465" y2="2.132" layer="29"/>
<rectangle x1="-3.042" y1="1.7875" x2="-2.3595" y2="2.1125" layer="31"/>
<rectangle x1="-3.055" y1="1.118" x2="-2.3465" y2="1.482" layer="29"/>
<rectangle x1="-3.042" y1="1.1375" x2="-2.3595" y2="1.4625" layer="31"/>
<rectangle x1="-3.055" y1="0.468" x2="-2.3465" y2="0.832" layer="29"/>
<rectangle x1="-3.042" y1="0.4875" x2="-2.3595" y2="0.8125" layer="31"/>
<rectangle x1="-3.055" y1="-0.182" x2="-2.3465" y2="0.182" layer="29"/>
<rectangle x1="-3.042" y1="-0.1625" x2="-2.3595" y2="0.1625" layer="31"/>
<rectangle x1="-3.055" y1="-0.832" x2="-2.3465" y2="-0.468" layer="29"/>
<rectangle x1="-3.042" y1="-0.8125" x2="-2.3595" y2="-0.4875" layer="31"/>
<rectangle x1="-3.055" y1="-1.482" x2="-2.3465" y2="-1.118" layer="29"/>
<rectangle x1="-3.042" y1="-1.4625" x2="-2.3595" y2="-1.1375" layer="31"/>
<rectangle x1="-3.055" y1="-2.132" x2="-2.3465" y2="-1.768" layer="29"/>
<rectangle x1="-3.042" y1="-2.1125" x2="-2.3595" y2="-1.7875" layer="31"/>
<rectangle x1="-2.3042" y1="-2.8827" x2="-1.5958" y2="-2.5187" layer="29" rot="R90"/>
<rectangle x1="-2.2912" y1="-2.8632" x2="-1.6088" y2="-2.5382" layer="31" rot="R90"/>
<rectangle x1="-1.6542" y1="-2.8827" x2="-0.9458" y2="-2.5187" layer="29" rot="R90"/>
<rectangle x1="-1.6412" y1="-2.8632" x2="-0.9588" y2="-2.5382" layer="31" rot="R90"/>
<rectangle x1="-1.0042" y1="-2.8827" x2="-0.2958" y2="-2.5187" layer="29" rot="R90"/>
<rectangle x1="-0.9912" y1="-2.8632" x2="-0.3088" y2="-2.5382" layer="31" rot="R90"/>
<rectangle x1="-0.3542" y1="-2.8827" x2="0.3542" y2="-2.5187" layer="29" rot="R90"/>
<rectangle x1="-0.3412" y1="-2.8632" x2="0.3412" y2="-2.5382" layer="31" rot="R90"/>
<rectangle x1="0.2958" y1="-2.8827" x2="1.0042" y2="-2.5187" layer="29" rot="R90"/>
<rectangle x1="0.3088" y1="-2.8632" x2="0.9912" y2="-2.5382" layer="31" rot="R90"/>
<rectangle x1="0.9458" y1="-2.8827" x2="1.6542" y2="-2.5187" layer="29" rot="R90"/>
<rectangle x1="0.9588" y1="-2.8632" x2="1.6412" y2="-2.5382" layer="31" rot="R90"/>
<rectangle x1="1.5958" y1="-2.8827" x2="2.3042" y2="-2.5187" layer="29" rot="R90"/>
<rectangle x1="1.6088" y1="-2.8632" x2="2.2912" y2="-2.5382" layer="31" rot="R90"/>
<rectangle x1="2.3465" y1="-2.132" x2="3.0549" y2="-1.768" layer="29" rot="R180"/>
<rectangle x1="2.3595" y1="-2.1125" x2="3.0419" y2="-1.7875" layer="31" rot="R180"/>
<rectangle x1="2.3465" y1="-1.482" x2="3.0549" y2="-1.118" layer="29" rot="R180"/>
<rectangle x1="2.3595" y1="-1.4625" x2="3.0419" y2="-1.1375" layer="31" rot="R180"/>
<rectangle x1="2.3465" y1="-0.832" x2="3.0549" y2="-0.468" layer="29" rot="R180"/>
<rectangle x1="2.3595" y1="-0.8125" x2="3.0419" y2="-0.4875" layer="31" rot="R180"/>
<rectangle x1="2.3465" y1="-0.182" x2="3.0549" y2="0.182" layer="29" rot="R180"/>
<rectangle x1="2.3595" y1="-0.1625" x2="3.0419" y2="0.1625" layer="31" rot="R180"/>
<rectangle x1="2.3465" y1="0.468" x2="3.0549" y2="0.832" layer="29" rot="R180"/>
<rectangle x1="2.3595" y1="0.4875" x2="3.0419" y2="0.8125" layer="31" rot="R180"/>
<rectangle x1="2.3465" y1="1.118" x2="3.0549" y2="1.482" layer="29" rot="R180"/>
<rectangle x1="2.3595" y1="1.1375" x2="3.0419" y2="1.4625" layer="31" rot="R180"/>
<rectangle x1="2.3465" y1="1.768" x2="3.0549" y2="2.132" layer="29" rot="R180"/>
<rectangle x1="2.3595" y1="1.7875" x2="3.0419" y2="2.1125" layer="31" rot="R180"/>
<rectangle x1="1.5958" y1="2.5187" x2="2.3042" y2="2.8827" layer="29" rot="R270"/>
<rectangle x1="1.6088" y1="2.5382" x2="2.2912" y2="2.8632" layer="31" rot="R270"/>
<rectangle x1="0.9458" y1="2.5187" x2="1.6542" y2="2.8827" layer="29" rot="R270"/>
<rectangle x1="0.9588" y1="2.5382" x2="1.6412" y2="2.8632" layer="31" rot="R270"/>
<rectangle x1="0.2958" y1="2.5187" x2="1.0042" y2="2.8827" layer="29" rot="R270"/>
<rectangle x1="0.3088" y1="2.5382" x2="0.9912" y2="2.8632" layer="31" rot="R270"/>
<rectangle x1="-0.3542" y1="2.5187" x2="0.3542" y2="2.8827" layer="29" rot="R270"/>
<rectangle x1="-0.3412" y1="2.5382" x2="0.3412" y2="2.8632" layer="31" rot="R270"/>
<rectangle x1="-1.0042" y1="2.5187" x2="-0.2958" y2="2.8827" layer="29" rot="R270"/>
<rectangle x1="-0.9912" y1="2.5382" x2="-0.3088" y2="2.8632" layer="31" rot="R270"/>
<rectangle x1="-1.6542" y1="2.5187" x2="-0.9458" y2="2.8827" layer="29" rot="R270"/>
<rectangle x1="-1.6412" y1="2.5382" x2="-0.9588" y2="2.8632" layer="31" rot="R270"/>
<rectangle x1="-2.3042" y1="2.5187" x2="-1.5958" y2="2.8827" layer="29" rot="R270"/>
<rectangle x1="-2.2912" y1="2.5382" x2="-1.6088" y2="2.8632" layer="31" rot="R270"/>
<rectangle x1="-1.859" y1="-1.859" x2="1.859" y2="1.859" layer="29"/>
<rectangle x1="-1.7355" y1="-1.7355" x2="1.7355" y2="1.7355" layer="31"/>
</package>
<package name="CD4543_DIL16">
<description>&lt;b&gt;Dual In Line Package&lt;/b&gt;</description>
<wire x1="10.16" y1="2.921" x2="-10.16" y2="2.921" width="0.1524" layer="21"/>
<wire x1="-10.16" y1="-2.921" x2="10.16" y2="-2.921" width="0.1524" layer="21"/>
<wire x1="10.16" y1="2.921" x2="10.16" y2="-2.921" width="0.1524" layer="21"/>
<wire x1="-10.16" y1="2.921" x2="-10.16" y2="1.016" width="0.1524" layer="21"/>
<wire x1="-10.16" y1="-2.921" x2="-10.16" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="-10.16" y1="1.016" x2="-10.16" y2="-1.016" width="0.1524" layer="21" curve="-180"/>
<pad name="1" x="-8.89" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="2" x="-6.35" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="7" x="6.35" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="8" x="8.89" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="3" x="-3.81" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="4" x="-1.27" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="6" x="3.81" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="5" x="1.27" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="9" x="8.89" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="10" x="6.35" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="11" x="3.81" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="12" x="1.27" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="13" x="-1.27" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="14" x="-3.81" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="15" x="-6.35" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="16" x="-8.89" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<text x="-10.541" y="-2.921" size="1.27" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<text x="-7.493" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="CD4543_SO16">
<description>&lt;b&gt;Small Outline package&lt;/b&gt; 150 mil</description>
<wire x1="4.699" y1="1.9558" x2="-4.699" y2="1.9558" width="0.1524" layer="51"/>
<wire x1="4.699" y1="-1.9558" x2="5.08" y2="-1.5748" width="0.1524" layer="21" curve="90"/>
<wire x1="-5.08" y1="1.5748" x2="-4.699" y2="1.9558" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.699" y1="1.9558" x2="5.08" y2="1.5748" width="0.1524" layer="21" curve="-90"/>
<wire x1="-5.08" y1="-1.5748" x2="-4.699" y2="-1.9558" width="0.1524" layer="21" curve="90"/>
<wire x1="-4.699" y1="-1.9558" x2="4.699" y2="-1.9558" width="0.1524" layer="51"/>
<wire x1="5.08" y1="-1.5748" x2="5.08" y2="1.5748" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="1.5748" x2="-5.08" y2="0.508" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="0.508" x2="-5.08" y2="-0.508" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="-0.508" x2="-5.08" y2="-1.5748" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="0.508" x2="-5.08" y2="-0.508" width="0.1524" layer="21" curve="-180"/>
<wire x1="-5.08" y1="-1.6002" x2="5.08" y2="-1.6002" width="0.0508" layer="21"/>
<smd name="1" x="-4.445" y="-3.0734" dx="0.6604" dy="2.032" layer="1"/>
<smd name="16" x="-4.445" y="3.0734" dx="0.6604" dy="2.032" layer="1"/>
<smd name="2" x="-3.175" y="-3.0734" dx="0.6604" dy="2.032" layer="1"/>
<smd name="3" x="-1.905" y="-3.0734" dx="0.6604" dy="2.032" layer="1"/>
<smd name="15" x="-3.175" y="3.0734" dx="0.6604" dy="2.032" layer="1"/>
<smd name="14" x="-1.905" y="3.0734" dx="0.6604" dy="2.032" layer="1"/>
<smd name="4" x="-0.635" y="-3.0734" dx="0.6604" dy="2.032" layer="1"/>
<smd name="13" x="-0.635" y="3.0734" dx="0.6604" dy="2.032" layer="1"/>
<smd name="5" x="0.635" y="-3.0734" dx="0.6604" dy="2.032" layer="1"/>
<smd name="12" x="0.635" y="3.0734" dx="0.6604" dy="2.032" layer="1"/>
<smd name="6" x="1.905" y="-3.0734" dx="0.6604" dy="2.032" layer="1"/>
<smd name="7" x="3.175" y="-3.0734" dx="0.6604" dy="2.032" layer="1"/>
<smd name="11" x="1.905" y="3.0734" dx="0.6604" dy="2.032" layer="1"/>
<smd name="10" x="3.175" y="3.0734" dx="0.6604" dy="2.032" layer="1"/>
<smd name="8" x="4.445" y="-3.0734" dx="0.6604" dy="2.032" layer="1"/>
<smd name="9" x="4.445" y="3.0734" dx="0.6604" dy="2.032" layer="1"/>
<text x="-3.81" y="-0.762" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<text x="-5.461" y="-1.905" size="1.27" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<rectangle x1="-0.889" y1="1.9558" x2="-0.381" y2="3.0988" layer="51"/>
<rectangle x1="-4.699" y1="-3.0988" x2="-4.191" y2="-1.9558" layer="51"/>
<rectangle x1="-3.429" y1="-3.0988" x2="-2.921" y2="-1.9558" layer="51"/>
<rectangle x1="-2.159" y1="-3.0734" x2="-1.651" y2="-1.9304" layer="51"/>
<rectangle x1="-0.889" y1="-3.0988" x2="-0.381" y2="-1.9558" layer="51"/>
<rectangle x1="-2.159" y1="1.9558" x2="-1.651" y2="3.0988" layer="51"/>
<rectangle x1="-3.429" y1="1.9558" x2="-2.921" y2="3.0988" layer="51"/>
<rectangle x1="-4.699" y1="1.9558" x2="-4.191" y2="3.0988" layer="51"/>
<rectangle x1="0.381" y1="-3.0988" x2="0.889" y2="-1.9558" layer="51"/>
<rectangle x1="1.651" y1="-3.0988" x2="2.159" y2="-1.9558" layer="51"/>
<rectangle x1="2.921" y1="-3.0988" x2="3.429" y2="-1.9558" layer="51"/>
<rectangle x1="4.191" y1="-3.0988" x2="4.699" y2="-1.9558" layer="51"/>
<rectangle x1="0.381" y1="1.9558" x2="0.889" y2="3.0988" layer="51"/>
<rectangle x1="1.651" y1="1.9558" x2="2.159" y2="3.0988" layer="51"/>
<rectangle x1="2.921" y1="1.9558" x2="3.429" y2="3.0988" layer="51"/>
<rectangle x1="4.191" y1="1.9558" x2="4.699" y2="3.0988" layer="51"/>
</package>
<package name="74HC238">
<description>&lt;b&gt;Dual In Line Package&lt;/b&gt;</description>
<wire x1="10.16" y1="2.921" x2="-10.16" y2="2.921" width="0.1524" layer="21"/>
<wire x1="-10.16" y1="-2.921" x2="10.16" y2="-2.921" width="0.1524" layer="21"/>
<wire x1="10.16" y1="2.921" x2="10.16" y2="-2.921" width="0.1524" layer="21"/>
<wire x1="-10.16" y1="2.921" x2="-10.16" y2="1.016" width="0.1524" layer="21"/>
<wire x1="-10.16" y1="-2.921" x2="-10.16" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="-10.16" y1="1.016" x2="-10.16" y2="-1.016" width="0.1524" layer="21" curve="-180"/>
<pad name="1" x="-8.89" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="2" x="-6.35" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="7" x="6.35" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="8" x="8.89" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="3" x="-3.81" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="4" x="-1.27" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="6" x="3.81" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="5" x="1.27" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="9" x="8.89" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="10" x="6.35" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="11" x="3.81" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="12" x="1.27" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="13" x="-1.27" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="14" x="-3.81" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="15" x="-6.35" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="16" x="-8.89" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<text x="-10.541" y="-2.921" size="1.27" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<text x="-7.493" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="74HC238D">
<description>&lt;b&gt;Small Outline package&lt;/b&gt; 150 mil</description>
<wire x1="4.699" y1="1.9558" x2="-4.699" y2="1.9558" width="0.1524" layer="21"/>
<wire x1="4.699" y1="-1.9558" x2="5.08" y2="-1.5748" width="0.1524" layer="21" curve="90"/>
<wire x1="-5.08" y1="1.5748" x2="-4.699" y2="1.9558" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.699" y1="1.9558" x2="5.08" y2="1.5748" width="0.1524" layer="21" curve="-90"/>
<wire x1="-5.08" y1="-1.5748" x2="-4.699" y2="-1.9558" width="0.1524" layer="21" curve="90"/>
<wire x1="-4.699" y1="-1.9558" x2="4.699" y2="-1.9558" width="0.1524" layer="21"/>
<wire x1="5.08" y1="-1.5748" x2="5.08" y2="1.5748" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="1.5748" x2="-5.08" y2="0.508" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="0.508" x2="-5.08" y2="-0.508" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="-0.508" x2="-5.08" y2="-1.5748" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="0.508" x2="-5.08" y2="-0.508" width="0.1524" layer="21" curve="-180"/>
<wire x1="-5.08" y1="-1.6002" x2="5.08" y2="-1.6002" width="0.0508" layer="21"/>
<smd name="1" x="-4.445" y="-3.0734" dx="0.6604" dy="2.032" layer="1"/>
<smd name="16" x="-4.445" y="3.0734" dx="0.6604" dy="2.032" layer="1"/>
<smd name="2" x="-3.175" y="-3.0734" dx="0.6604" dy="2.032" layer="1"/>
<smd name="3" x="-1.905" y="-3.0734" dx="0.6604" dy="2.032" layer="1"/>
<smd name="15" x="-3.175" y="3.0734" dx="0.6604" dy="2.032" layer="1"/>
<smd name="14" x="-1.905" y="3.0734" dx="0.6604" dy="2.032" layer="1"/>
<smd name="4" x="-0.635" y="-3.0734" dx="0.6604" dy="2.032" layer="1"/>
<smd name="13" x="-0.635" y="3.0734" dx="0.6604" dy="2.032" layer="1"/>
<smd name="5" x="0.635" y="-3.0734" dx="0.6604" dy="2.032" layer="1"/>
<smd name="12" x="0.635" y="3.0734" dx="0.6604" dy="2.032" layer="1"/>
<smd name="6" x="1.905" y="-3.0734" dx="0.6604" dy="2.032" layer="1"/>
<smd name="7" x="3.175" y="-3.0734" dx="0.6604" dy="2.032" layer="1"/>
<smd name="11" x="1.905" y="3.0734" dx="0.6604" dy="2.032" layer="1"/>
<smd name="10" x="3.175" y="3.0734" dx="0.6604" dy="2.032" layer="1"/>
<smd name="8" x="4.445" y="-3.0734" dx="0.6604" dy="2.032" layer="1"/>
<smd name="9" x="4.445" y="3.0734" dx="0.6604" dy="2.032" layer="1"/>
<text x="-4.064" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<text x="-5.461" y="-1.778" size="1.27" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<rectangle x1="-0.889" y1="1.9558" x2="-0.381" y2="3.0988" layer="51"/>
<rectangle x1="-4.699" y1="-3.0988" x2="-4.191" y2="-1.9558" layer="51"/>
<rectangle x1="-3.429" y1="-3.0988" x2="-2.921" y2="-1.9558" layer="51"/>
<rectangle x1="-2.159" y1="-3.0734" x2="-1.651" y2="-1.9304" layer="51"/>
<rectangle x1="-0.889" y1="-3.0988" x2="-0.381" y2="-1.9558" layer="51"/>
<rectangle x1="-2.159" y1="1.9558" x2="-1.651" y2="3.0988" layer="51"/>
<rectangle x1="-3.429" y1="1.9558" x2="-2.921" y2="3.0988" layer="51"/>
<rectangle x1="-4.699" y1="1.9558" x2="-4.191" y2="3.0988" layer="51"/>
<rectangle x1="0.381" y1="-3.0988" x2="0.889" y2="-1.9558" layer="51"/>
<rectangle x1="1.651" y1="-3.0988" x2="2.159" y2="-1.9558" layer="51"/>
<rectangle x1="2.921" y1="-3.0988" x2="3.429" y2="-1.9558" layer="51"/>
<rectangle x1="4.191" y1="-3.0988" x2="4.699" y2="-1.9558" layer="51"/>
<rectangle x1="0.381" y1="1.9558" x2="0.889" y2="3.0988" layer="51"/>
<rectangle x1="1.651" y1="1.9558" x2="2.159" y2="3.0988" layer="51"/>
<rectangle x1="2.921" y1="1.9558" x2="3.429" y2="3.0988" layer="51"/>
<rectangle x1="4.191" y1="1.9558" x2="4.699" y2="3.0988" layer="51"/>
</package>
<package name="RELAY">
<description>UNLibrary relay</description>
<pad name="NO" x="-6.05" y="6" drill="1.5" rot="R180"/>
<pad name="NC" x="-6.05" y="-6" drill="1.5" rot="R180"/>
<pad name="1" x="6.15" y="-6" drill="1.5" rot="R180"/>
<pad name="2" x="6.15" y="6" drill="1.5" rot="R180"/>
<pad name="C" x="8.15" y="0" drill="1.5" rot="R180"/>
<text x="-2.54" y="5.08" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-6.35" size="1.27" layer="27">&gt;VALUE</text>
<wire x1="-9.55" y1="-7.75" x2="9.55" y2="-7.75" width="0.127" layer="21"/>
<wire x1="9.55" y1="-7.75" x2="9.55" y2="7.75" width="0.127" layer="21"/>
<wire x1="9.55" y1="7.75" x2="-9.55" y2="7.75" width="0.127" layer="21"/>
<wire x1="-9.55" y1="7.75" x2="-9.55" y2="-7.75" width="0.127" layer="21"/>
<wire x1="-6.35" y1="2.54" x2="-6.35" y2="4.445" width="0.127" layer="21"/>
<wire x1="-6.35" y1="-4.445" x2="-6.35" y2="-2.54" width="0.127" layer="21"/>
<wire x1="-2.54" y1="0" x2="-6.35" y2="-2.54" width="0.127" layer="21"/>
<wire x1="5.08" y1="-4.445" x2="5.08" y2="-3.175" width="0.127" layer="21"/>
<wire x1="5.08" y1="-3.175" x2="3.81" y2="-1.905" width="0.127" layer="21" curve="90"/>
<wire x1="3.81" y1="-1.905" x2="3.81" y2="-3.175" width="0.127" layer="21" curve="180"/>
<wire x1="3.81" y1="-3.175" x2="5.08" y2="-1.905" width="0.127" layer="21" curve="90"/>
<wire x1="5.08" y1="-1.905" x2="5.08" y2="-0.635" width="0.127" layer="21"/>
<wire x1="5.08" y1="-0.635" x2="3.81" y2="0.635" width="0.127" layer="21" curve="90"/>
<wire x1="3.81" y1="0.635" x2="3.81" y2="-0.635" width="0.127" layer="21" curve="180"/>
<wire x1="3.81" y1="-0.635" x2="5.08" y2="0.635" width="0.127" layer="21" curve="90"/>
<wire x1="5.08" y1="0.635" x2="5.08" y2="1.905" width="0.127" layer="21"/>
<wire x1="5.08" y1="1.905" x2="3.81" y2="3.175" width="0.127" layer="21" curve="90"/>
<wire x1="3.81" y1="3.175" x2="3.81" y2="1.905" width="0.127" layer="21" curve="180"/>
<wire x1="3.81" y1="1.905" x2="5.08" y2="3.175" width="0.127" layer="21" curve="90"/>
<wire x1="5.08" y1="3.175" x2="5.08" y2="3.81" width="0.127" layer="21"/>
<wire x1="-2.54" y1="0" x2="6.35" y2="0" width="0.127" layer="21"/>
<circle x="-6.35" y="2.54" radius="0.635" width="0.127" layer="21"/>
<circle x="-6.35" y="-2.54" radius="0.635" width="0.127" layer="21"/>
<circle x="5.08" y="3.81" radius="0.635" width="0.127" layer="21"/>
<circle x="6.35" y="0" radius="0.635" width="0.127" layer="21"/>
<circle x="5.08" y="-4.445" radius="0.635" width="0.127" layer="21"/>
</package>
<package name="W237-102">
<description>&lt;b&gt;WAGO SCREW CLAMP&lt;/b&gt;</description>
<wire x1="-3.491" y1="-2.286" x2="-1.484" y2="-0.279" width="0.254" layer="51"/>
<wire x1="1.488" y1="-2.261" x2="3.469" y2="-0.254" width="0.254" layer="51"/>
<wire x1="-4.989" y1="-5.461" x2="4.993" y2="-5.461" width="0.1524" layer="21"/>
<wire x1="4.993" y1="3.734" x2="4.993" y2="3.531" width="0.1524" layer="21"/>
<wire x1="4.993" y1="3.734" x2="-4.989" y2="3.734" width="0.1524" layer="21"/>
<wire x1="-4.989" y1="-5.461" x2="-4.989" y2="-3.073" width="0.1524" layer="21"/>
<wire x1="-4.989" y1="-3.073" x2="-3.389" y2="-3.073" width="0.1524" layer="21"/>
<wire x1="-3.389" y1="-3.073" x2="-1.611" y2="-3.073" width="0.1524" layer="51"/>
<wire x1="-1.611" y1="-3.073" x2="1.615" y2="-3.073" width="0.1524" layer="21"/>
<wire x1="3.393" y1="-3.073" x2="4.993" y2="-3.073" width="0.1524" layer="21"/>
<wire x1="-4.989" y1="-3.073" x2="-4.989" y2="3.531" width="0.1524" layer="21"/>
<wire x1="4.993" y1="-3.073" x2="4.993" y2="-5.461" width="0.1524" layer="21"/>
<wire x1="-4.989" y1="3.531" x2="4.993" y2="3.531" width="0.1524" layer="21"/>
<wire x1="-4.989" y1="3.531" x2="-4.989" y2="3.734" width="0.1524" layer="21"/>
<wire x1="4.993" y1="3.531" x2="4.993" y2="-3.073" width="0.1524" layer="21"/>
<wire x1="1.615" y1="-3.073" x2="3.393" y2="-3.073" width="0.1524" layer="51"/>
<circle x="-2.5" y="-1.27" radius="1.4986" width="0.1524" layer="51"/>
<circle x="-2.5" y="2.2098" radius="0.508" width="0.1524" layer="21"/>
<circle x="2.5038" y="-1.27" radius="1.4986" width="0.1524" layer="51"/>
<circle x="2.5038" y="2.2098" radius="0.508" width="0.1524" layer="21"/>
<pad name="1" x="-2.5" y="-1.27" drill="1.1938" shape="long" rot="R90"/>
<pad name="2" x="2.5" y="-1.27" drill="1.1938" shape="long" rot="R90"/>
<text x="-5.04" y="-7.62" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<text x="-3.8462" y="-5.0038" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-4.532" y="0.635" size="1.27" layer="21" ratio="10">1</text>
<text x="0.421" y="0.635" size="1.27" layer="21" ratio="10">2</text>
</package>
<package name="1X02">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<wire x1="-1.905" y1="1.27" x2="-0.635" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="0" y2="0.635" width="0.1524" layer="21"/>
<wire x1="0" y1="0.635" x2="0" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="0" y1="-0.635" x2="-0.635" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="0.635" x2="-2.54" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="1.27" x2="-2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="-0.635" x2="-1.905" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="-1.27" x2="-1.905" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="0" y1="0.635" x2="0.635" y2="1.27" width="0.1524" layer="21"/>
<wire x1="0.635" y1="1.27" x2="1.905" y2="1.27" width="0.1524" layer="21"/>
<wire x1="1.905" y1="1.27" x2="2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="2.54" y1="0.635" x2="2.54" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-0.635" x2="1.905" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="1.905" y1="-1.27" x2="0.635" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="0.635" y1="-1.27" x2="0" y2="-0.635" width="0.1524" layer="21"/>
<pad name="1" x="-1.27" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="2" x="1.27" y="0" drill="1.016" shape="long" rot="R90"/>
<text x="-2.6162" y="1.8288" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.54" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.524" y1="-0.254" x2="-1.016" y2="0.254" layer="51"/>
<rectangle x1="1.016" y1="-0.254" x2="1.524" y2="0.254" layer="51"/>
</package>
<package name="1X02/90">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<wire x1="-2.54" y1="-1.905" x2="0" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="0" y1="-1.905" x2="0" y2="0.635" width="0.1524" layer="21"/>
<wire x1="0" y1="0.635" x2="-2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="0.635" x2="-2.54" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="6.985" x2="-1.27" y2="1.27" width="0.762" layer="21"/>
<wire x1="0" y1="-1.905" x2="2.54" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-1.905" x2="2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="2.54" y1="0.635" x2="0" y2="0.635" width="0.1524" layer="21"/>
<wire x1="1.27" y1="6.985" x2="1.27" y2="1.27" width="0.762" layer="21"/>
<pad name="1" x="-1.27" y="-3.81" drill="1.016" shape="long" rot="R90"/>
<pad name="2" x="1.27" y="-3.81" drill="1.016" shape="long" rot="R90"/>
<text x="-3.175" y="-3.81" size="1.27" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<text x="4.445" y="-3.81" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-1.651" y1="0.635" x2="-0.889" y2="1.143" layer="21"/>
<rectangle x1="0.889" y1="0.635" x2="1.651" y2="1.143" layer="21"/>
<rectangle x1="-1.651" y1="-2.921" x2="-0.889" y2="-1.905" layer="21"/>
<rectangle x1="0.889" y1="-2.921" x2="1.651" y2="-1.905" layer="21"/>
</package>
<package name="MICROSD_BOARD_PACKAGE">
<wire x1="42" y1="23" x2="0" y2="23" width="0.127" layer="21"/>
<wire x1="0" y1="23" x2="0" y2="0" width="0.127" layer="21"/>
<wire x1="0" y1="0" x2="42" y2="0" width="0.127" layer="21"/>
<wire x1="42" y1="0" x2="42" y2="23" width="0.127" layer="21"/>
<pad name="VCC" x="3" y="15.31" drill="0.8" diameter="1.905"/>
<pad name="MISO" x="3" y="12.77" drill="0.8" diameter="1.905" rot="R90"/>
<pad name="MOSI" x="3" y="10.23" drill="0.8" diameter="1.905" rot="R90"/>
<pad name="CLK" x="3" y="7.69" drill="0.8" diameter="1.905"/>
<pad name="CS" x="3" y="5.15" drill="0.8" diameter="1.905"/>
<pad name="GND" x="3" y="17.85" drill="0.8" diameter="1.905" rot="R90"/>
<text x="30" y="8.84" size="1.27" layer="21" align="center">CATALEX
MicroSD Card Adapter</text>
<text x="30" y="17.43" size="1.27" layer="21" align="center">&gt;NAME</text>
<text x="4.42" y="4.3" size="1.27" layer="21">CS</text>
<text x="4.42" y="6.84" size="1.27" layer="21">CLK</text>
<text x="4.42" y="9.38" size="1.27" layer="21">MOSI</text>
<text x="4.42" y="11.92" size="1.27" layer="21">MISO</text>
<text x="4.42" y="14.46" size="1.27" layer="21">VCC</text>
<text x="4.42" y="17" size="1.27" layer="21">GND</text>
</package>
</packages>
<symbols>
<symbol name="ESP32_AZ">
<pin name="5V" x="0" y="2.54" visible="pin" length="short"/>
<pin name="CMD" x="0" y="5.08" visible="pin" length="short"/>
<pin name="G10/SD3" x="0" y="7.62" visible="pin" length="short"/>
<pin name="G9/SD2" x="0" y="10.16" visible="pin" length="short"/>
<pin name="G13" x="0" y="12.7" visible="pin" length="short"/>
<pin name="GND1" x="0" y="15.24" visible="pin" length="short"/>
<pin name="G12" x="0" y="17.78" visible="pin" length="short"/>
<pin name="G14" x="0" y="20.32" visible="pin" length="short"/>
<pin name="G27" x="0" y="22.86" visible="pin" length="short"/>
<pin name="G26" x="0" y="25.4" visible="pin" length="short"/>
<pin name="G25" x="0" y="27.94" visible="pin" length="short"/>
<pin name="G33" x="0" y="30.48" visible="pin" length="short"/>
<pin name="G32" x="0" y="33.02" visible="pin" length="short"/>
<pin name="G35" x="0" y="35.56" visible="pin" length="short"/>
<pin name="G34" x="0" y="38.1" visible="pin" length="short"/>
<pin name="G39/SN" x="0" y="40.64" visible="pin" length="short"/>
<pin name="G36/SP" x="0" y="43.18" visible="pin" length="short"/>
<pin name="EN" x="0" y="45.72" visible="pin" length="short"/>
<pin name="3.3V" x="0" y="48.26" visible="pin" length="short"/>
<pin name="G6/CLK" x="30.48" y="2.54" visible="pin" length="short" rot="R180"/>
<pin name="G7/SD0" x="30.48" y="5.08" visible="pin" length="short" rot="R180"/>
<pin name="G8/SD1" x="30.48" y="7.62" visible="pin" length="short" rot="R180"/>
<pin name="G15" x="30.48" y="10.16" visible="pin" length="short" rot="R180"/>
<pin name="G2" x="30.48" y="12.7" visible="pin" length="short" rot="R180"/>
<pin name="G0" x="30.48" y="15.24" visible="pin" length="short" rot="R180"/>
<pin name="G4" x="30.48" y="17.78" visible="pin" length="short" rot="R180"/>
<pin name="G16" x="30.48" y="20.32" visible="pin" length="short" rot="R180"/>
<pin name="G17" x="30.48" y="22.86" visible="pin" length="short" rot="R180"/>
<pin name="G5" x="30.48" y="25.4" visible="pin" length="short" rot="R180"/>
<pin name="G18" x="30.48" y="27.94" visible="pin" length="short" rot="R180"/>
<pin name="G19" x="30.48" y="30.48" visible="pin" length="short" rot="R180"/>
<pin name="GND3" x="30.48" y="33.02" visible="pin" length="short" rot="R180"/>
<pin name="G21/SDA" x="30.48" y="35.56" visible="pin" length="short" rot="R180"/>
<pin name="G3/RXD" x="30.48" y="38.1" visible="pin" length="short" rot="R180"/>
<pin name="G1/TXD" x="30.48" y="40.64" visible="pin" length="short" rot="R180"/>
<pin name="G22/SCL" x="30.48" y="43.18" visible="pin" length="short" rot="R180"/>
<pin name="G23" x="30.48" y="45.72" visible="pin" length="short" rot="R180"/>
<pin name="GND2" x="30.48" y="48.26" visible="pin" length="short" rot="R180"/>
<wire x1="2.54" y1="0" x2="2.54" y2="50.8" width="0.254" layer="94"/>
<wire x1="2.54" y1="50.8" x2="27.94" y2="50.8" width="0.254" layer="94"/>
<wire x1="27.94" y1="50.8" x2="27.94" y2="0" width="0.254" layer="94"/>
<wire x1="27.94" y1="0" x2="2.54" y2="0" width="0.254" layer="94"/>
<text x="11.43" y="-2.54" size="1.778" layer="94">ESP32</text>
</symbol>
<symbol name="7SEG-CA_2.9INCH">
<wire x1="-5.08" y1="7.62" x2="-5.08" y2="-7.62" width="0.4064" layer="94"/>
<wire x1="-5.08" y1="-7.62" x2="5.08" y2="-7.62" width="0.4064" layer="94"/>
<wire x1="5.08" y1="-7.62" x2="5.08" y2="7.62" width="0.4064" layer="94"/>
<wire x1="5.08" y1="7.62" x2="-5.08" y2="7.62" width="0.4064" layer="94"/>
<wire x1="-2.4892" y1="5.5118" x2="2.9972" y2="5.5118" width="0.8128" layer="94"/>
<wire x1="-3.5052" y1="-5.5118" x2="2.0066" y2="-5.5118" width="0.8128" layer="94"/>
<wire x1="-2.9972" y1="4.4958" x2="-3.5052" y2="0.9906" width="0.8128" layer="94"/>
<wire x1="-3.5052" y1="-0.508" x2="-3.9878" y2="-4.4958" width="0.8128" layer="94"/>
<wire x1="3.5052" y1="4.4958" x2="2.9972" y2="0.9906" width="0.8128" layer="94"/>
<wire x1="2.9972" y1="-0.508" x2="2.4892" y2="-4.4958" width="0.8128" layer="94"/>
<wire x1="-2.4892" y1="0" x2="2.0066" y2="0" width="0.8128" layer="94"/>
<circle x="3.81" y="-5.588" radius="0.1016" width="0.8128" layer="94"/>
<text x="-6.4008" y="5.3848" size="1.27" layer="94">a</text>
<text x="-6.4008" y="2.794" size="1.27" layer="94">b</text>
<text x="-6.4008" y="0.4064" size="1.27" layer="94">c</text>
<text x="-6.4008" y="-2.2098" size="1.27" layer="94">d</text>
<text x="-6.4008" y="-4.8006" size="1.27" layer="94">e</text>
<text x="5.4102" y="5.4102" size="1.27" layer="94">f</text>
<text x="5.4102" y="3.2004" size="1.27" layer="94">g</text>
<text x="5.4102" y="0.4064" size="1.27" layer="94">DP</text>
<text x="5.4102" y="-2.2098" size="1.27" layer="94">A1</text>
<text x="-5.08" y="8.255" size="1.778" layer="95">&gt;NAME</text>
<text x="-5.08" y="-10.16" size="1.778" layer="96">&gt;VALUE</text>
<text x="5.4102" y="-4.8006" size="1.27" layer="94">A2</text>
<pin name="C" x="-7.62" y="0" visible="off" length="short" direction="pas"/>
<pin name="B" x="-7.62" y="2.54" visible="off" length="short" direction="pas"/>
<pin name="A" x="-7.62" y="5.08" visible="off" length="short" direction="pas"/>
<pin name="D" x="-7.62" y="-2.54" visible="off" length="short" direction="pas"/>
<pin name="E" x="-7.62" y="-5.08" visible="off" length="short" direction="pas"/>
<pin name="CA1" x="7.62" y="-2.54" visible="off" length="short" direction="pas" rot="R180"/>
<pin name="DP" x="7.62" y="0" visible="off" length="short" direction="pas" rot="R180"/>
<pin name="G" x="7.62" y="2.54" visible="off" length="short" direction="pas" rot="R180"/>
<pin name="F" x="7.62" y="5.08" visible="off" length="short" direction="pas" rot="R180"/>
<pin name="CA2" x="7.62" y="-5.08" visible="off" length="short" direction="pas" rot="R180"/>
</symbol>
<symbol name="7SEG-CA">
<wire x1="-5.08" y1="7.62" x2="-5.08" y2="-7.62" width="0.4064" layer="94"/>
<wire x1="-5.08" y1="-7.62" x2="5.08" y2="-7.62" width="0.4064" layer="94"/>
<wire x1="5.08" y1="-7.62" x2="5.08" y2="7.62" width="0.4064" layer="94"/>
<wire x1="5.08" y1="7.62" x2="-5.08" y2="7.62" width="0.4064" layer="94"/>
<wire x1="-2.4892" y1="5.5118" x2="2.9972" y2="5.5118" width="0.8128" layer="94"/>
<wire x1="-3.5052" y1="-5.5118" x2="2.0066" y2="-5.5118" width="0.8128" layer="94"/>
<wire x1="-2.9972" y1="4.4958" x2="-3.5052" y2="0.9906" width="0.8128" layer="94"/>
<wire x1="-3.5052" y1="-0.508" x2="-3.9878" y2="-4.4958" width="0.8128" layer="94"/>
<wire x1="3.5052" y1="4.4958" x2="2.9972" y2="0.9906" width="0.8128" layer="94"/>
<wire x1="2.9972" y1="-0.508" x2="2.4892" y2="-4.4958" width="0.8128" layer="94"/>
<wire x1="-2.4892" y1="0" x2="2.0066" y2="0" width="0.8128" layer="94"/>
<circle x="3.81" y="-5.588" radius="0.1016" width="0.8128" layer="94"/>
<text x="-6.4008" y="5.3848" size="1.27" layer="94">a</text>
<text x="-6.4008" y="2.794" size="1.27" layer="94">b</text>
<text x="-6.4008" y="0.4064" size="1.27" layer="94">c</text>
<text x="-6.4008" y="-2.2098" size="1.27" layer="94">d</text>
<text x="-6.4008" y="-4.8006" size="1.27" layer="94">e</text>
<text x="5.4102" y="5.4102" size="1.27" layer="94">f</text>
<text x="5.4102" y="3.2004" size="1.27" layer="94">g</text>
<text x="5.4102" y="0.4064" size="1.27" layer="94">P</text>
<text x="5.4102" y="-2.2098" size="1.27" layer="94">A</text>
<text x="-5.08" y="8.255" size="1.778" layer="95">&gt;NAME</text>
<text x="-5.08" y="-10.16" size="1.778" layer="96">&gt;VALUE</text>
<text x="5.4102" y="-4.8006" size="1.27" layer="94">A</text>
<pin name="C" x="-7.62" y="0" visible="off" length="short" direction="pas"/>
<pin name="B" x="-7.62" y="2.54" visible="off" length="short" direction="pas"/>
<pin name="A" x="-7.62" y="5.08" visible="off" length="short" direction="pas"/>
<pin name="D" x="-7.62" y="-2.54" visible="off" length="short" direction="pas"/>
<pin name="E" x="-7.62" y="-5.08" visible="off" length="short" direction="pas"/>
<pin name="CA1" x="7.62" y="-2.54" visible="off" length="short" direction="pas" rot="R180"/>
<pin name="DP" x="7.62" y="0" visible="off" length="short" direction="pas" rot="R180"/>
<pin name="G" x="7.62" y="2.54" visible="off" length="short" direction="pas" rot="R180"/>
<pin name="F" x="7.62" y="5.08" visible="off" length="short" direction="pas" rot="R180"/>
<pin name="CA2" x="7.62" y="-5.08" visible="off" length="short" direction="pas" rot="R180"/>
</symbol>
<symbol name="MCP23017">
<wire x1="-10.16" y1="22.86" x2="10.16" y2="22.86" width="0.254" layer="94"/>
<wire x1="10.16" y1="22.86" x2="10.16" y2="-22.86" width="0.254" layer="94"/>
<wire x1="10.16" y1="-22.86" x2="-10.16" y2="-22.86" width="0.254" layer="94"/>
<wire x1="-10.16" y1="-22.86" x2="-10.16" y2="22.86" width="0.254" layer="94"/>
<text x="-10.16" y="24.13" size="1.778" layer="95">&gt;NAME</text>
<text x="-10.16" y="-25.4" size="1.778" layer="96">&gt;VALUE</text>
<pin name="SCL" x="-12.7" y="-2.54" length="short" direction="in"/>
<pin name="SDA" x="-12.7" y="-5.08" length="short"/>
<pin name="A0" x="-12.7" y="-10.16" length="short" direction="in"/>
<pin name="A1" x="-12.7" y="-12.7" length="short" direction="in"/>
<pin name="A2" x="-12.7" y="-15.24" length="short" direction="in"/>
<pin name="!RESET" x="-12.7" y="15.24" length="short" direction="in"/>
<pin name="INTA" x="-12.7" y="10.16" length="short" direction="out"/>
<pin name="INTB" x="-12.7" y="7.62" length="short" direction="out"/>
<pin name="GPB0" x="12.7" y="-2.54" length="short" rot="R180"/>
<pin name="GPB1" x="12.7" y="-5.08" length="short" rot="R180"/>
<pin name="GPB2" x="12.7" y="-7.62" length="short" rot="R180"/>
<pin name="GPB3" x="12.7" y="-10.16" length="short" rot="R180"/>
<pin name="GPB4" x="12.7" y="-12.7" length="short" rot="R180"/>
<pin name="GPB5" x="12.7" y="-15.24" length="short" rot="R180"/>
<pin name="GPB6" x="12.7" y="-17.78" length="short" rot="R180"/>
<pin name="GPB7" x="12.7" y="-20.32" length="short" rot="R180"/>
<pin name="GPA0" x="12.7" y="20.32" length="short" rot="R180"/>
<pin name="GPA1" x="12.7" y="17.78" length="short" rot="R180"/>
<pin name="GPA2" x="12.7" y="15.24" length="short" rot="R180"/>
<pin name="GPA3" x="12.7" y="12.7" length="short" rot="R180"/>
<pin name="GPA4" x="12.7" y="10.16" length="short" rot="R180"/>
<pin name="GPA5" x="12.7" y="7.62" length="short" rot="R180"/>
<pin name="GPA6" x="12.7" y="5.08" length="short" rot="R180"/>
<pin name="GPA7" x="12.7" y="2.54" length="short" rot="R180"/>
<pin name="VDD" x="-12.7" y="20.32" length="short" direction="pwr"/>
<pin name="VSS" x="-12.7" y="-20.32" length="short" direction="pwr"/>
</symbol>
<symbol name="CD4543">
<wire x1="-7.62" y1="-12.7" x2="7.62" y2="-12.7" width="0.4064" layer="94"/>
<wire x1="7.62" y1="-12.7" x2="7.62" y2="10.16" width="0.4064" layer="94"/>
<wire x1="7.62" y1="10.16" x2="-7.62" y2="10.16" width="0.4064" layer="94"/>
<wire x1="-7.62" y1="10.16" x2="-7.62" y2="-12.7" width="0.4064" layer="94"/>
<text x="-7.62" y="10.795" size="1.778" layer="95">&gt;NAME</text>
<text x="-7.62" y="-15.24" size="1.778" layer="96">&gt;VALUE</text>
<pin name="LD" x="-12.7" y="7.62" length="middle" direction="in"/>
<pin name="IC" x="-12.7" y="2.54" length="middle" direction="in"/>
<pin name="IB" x="-12.7" y="5.08" length="middle" direction="in"/>
<pin name="ID" x="-12.7" y="0" length="middle" direction="in"/>
<pin name="IA" x="-12.7" y="-2.54" length="middle" direction="in"/>
<pin name="PH" x="-12.7" y="-5.08" length="middle" direction="in"/>
<pin name="BI" x="-12.7" y="-7.62" length="middle" direction="in"/>
<pin name="A" x="12.7" y="-10.16" length="middle" direction="out" rot="R180"/>
<pin name="B" x="12.7" y="-7.62" length="middle" direction="out" rot="R180"/>
<pin name="C" x="12.7" y="-5.08" length="middle" direction="out" rot="R180"/>
<pin name="D" x="12.7" y="-2.54" length="middle" direction="out" rot="R180"/>
<pin name="E" x="12.7" y="0" length="middle" direction="out" rot="R180"/>
<pin name="G" x="12.7" y="2.54" length="middle" direction="out" rot="R180"/>
<pin name="F" x="12.7" y="5.08" length="middle" direction="out" rot="R180"/>
<pin name="VSS" x="-12.7" y="-10.16" length="middle" direction="in"/>
<pin name="VDD" x="12.7" y="7.62" length="middle" direction="out" rot="R180"/>
</symbol>
<symbol name="74HC238">
<wire x1="-10.16" y1="-12.7" x2="5.08" y2="-12.7" width="0.4064" layer="94"/>
<wire x1="5.08" y1="-12.7" x2="5.08" y2="10.16" width="0.4064" layer="94"/>
<wire x1="5.08" y1="10.16" x2="-10.16" y2="10.16" width="0.4064" layer="94"/>
<wire x1="-10.16" y1="10.16" x2="-10.16" y2="-12.7" width="0.4064" layer="94"/>
<text x="-10.16" y="10.795" size="1.778" layer="95">&gt;NAME</text>
<text x="-10.16" y="-15.24" size="1.778" layer="96">&gt;VALUE</text>
<pin name="Y1" x="10.16" y="2.54" length="middle" direction="out" function="dot" rot="R180"/>
<pin name="Y2" x="10.16" y="0" length="middle" direction="out" function="dot" rot="R180"/>
<pin name="Y3" x="10.16" y="-2.54" length="middle" direction="out" function="dot" rot="R180"/>
<pin name="Y4" x="10.16" y="-5.08" length="middle" direction="out" function="dot" rot="R180"/>
<pin name="Y5" x="10.16" y="-7.62" length="middle" direction="out" function="dot" rot="R180"/>
<pin name="Y6" x="10.16" y="-10.16" length="middle" direction="out" function="dot" rot="R180"/>
<pin name="Y7" x="-15.24" y="-7.62" length="middle" direction="out" function="dot"/>
<pin name="E3" x="-15.24" y="-5.08" length="middle" direction="in" function="dot"/>
<pin name="E2" x="-15.24" y="-2.54" length="middle" direction="in" function="dot"/>
<pin name="E1" x="-15.24" y="0" length="middle" direction="in"/>
<pin name="A2" x="-15.24" y="2.54" length="middle" direction="in"/>
<pin name="A1" x="-15.24" y="5.08" length="middle" direction="in"/>
<pin name="Y0" x="10.16" y="5.08" length="middle" direction="out" function="dot" rot="R180"/>
<pin name="A0" x="-15.24" y="7.62" length="middle" direction="in"/>
<pin name="VCC" x="10.16" y="7.62" length="middle" direction="in" rot="R180"/>
<pin name="GND" x="-15.24" y="-10.16" length="middle" direction="in"/>
</symbol>
<symbol name="RELAY_POWER">
<wire x1="-3.81" y1="-1.905" x2="-1.905" y2="-1.905" width="0.254" layer="94"/>
<wire x1="3.81" y1="-1.905" x2="3.81" y2="1.905" width="0.254" layer="94"/>
<wire x1="3.81" y1="1.905" x2="1.905" y2="1.905" width="0.254" layer="94"/>
<wire x1="-3.81" y1="1.905" x2="-3.81" y2="-1.905" width="0.254" layer="94"/>
<wire x1="0" y1="-2.54" x2="0" y2="-1.905" width="0.1524" layer="94"/>
<wire x1="0" y1="-1.905" x2="3.81" y2="-1.905" width="0.254" layer="94"/>
<wire x1="0" y1="2.54" x2="0" y2="1.905" width="0.1524" layer="94"/>
<wire x1="0" y1="1.905" x2="-3.81" y2="1.905" width="0.254" layer="94"/>
<wire x1="-1.905" y1="-1.905" x2="1.905" y2="1.905" width="0.1524" layer="94"/>
<wire x1="-1.905" y1="-1.905" x2="0" y2="-1.905" width="0.254" layer="94"/>
<wire x1="1.905" y1="1.905" x2="0" y2="1.905" width="0.254" layer="94"/>
<text x="1.27" y="2.921" size="1.778" layer="96">&gt;VALUE</text>
<text x="1.27" y="5.08" size="1.778" layer="95">&gt;PART</text>
<pin name="2" x="0" y="-5.08" visible="pad" length="short" direction="pas" rot="R90"/>
<pin name="1" x="0" y="5.08" visible="pad" length="short" direction="pas" rot="R270"/>
</symbol>
<symbol name="RELAY_SWITCH">
<wire x1="3.175" y1="5.08" x2="1.905" y2="5.08" width="0.254" layer="94"/>
<wire x1="-3.175" y1="5.08" x2="-1.905" y2="5.08" width="0.254" layer="94"/>
<wire x1="0" y1="1.27" x2="2.54" y2="5.715" width="0.254" layer="94"/>
<wire x1="0" y1="1.27" x2="0" y2="0" width="0.254" layer="94"/>
<circle x="0" y="1.27" radius="0.127" width="0.4064" layer="94"/>
<text x="2.54" y="0" size="1.778" layer="95">&gt;PART</text>
<pin name="O" x="5.08" y="5.08" visible="pad" length="short" direction="pas" rot="R180"/>
<pin name="S" x="-5.08" y="5.08" visible="pad" length="short" direction="pas"/>
<pin name="P" x="0" y="-2.54" visible="pad" length="short" direction="pas" rot="R90"/>
</symbol>
<symbol name="KL">
<circle x="1.27" y="0" radius="1.27" width="0.254" layer="94"/>
<text x="0" y="0.889" size="1.778" layer="95" rot="R180">&gt;NAME</text>
<pin name="KL" x="5.08" y="0" visible="off" length="short" direction="pas" rot="R180"/>
</symbol>
<symbol name="KL+V">
<circle x="1.27" y="0" radius="1.27" width="0.254" layer="94"/>
<text x="-2.54" y="-3.683" size="1.778" layer="96">&gt;VALUE</text>
<text x="0" y="0.889" size="1.778" layer="95" rot="R180">&gt;NAME</text>
<pin name="KL" x="5.08" y="0" visible="off" length="short" direction="pas" rot="R180"/>
</symbol>
<symbol name="PINHD2">
<wire x1="-6.35" y1="-2.54" x2="1.27" y2="-2.54" width="0.4064" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="1.27" y2="5.08" width="0.4064" layer="94"/>
<wire x1="1.27" y1="5.08" x2="-6.35" y2="5.08" width="0.4064" layer="94"/>
<wire x1="-6.35" y1="5.08" x2="-6.35" y2="-2.54" width="0.4064" layer="94"/>
<text x="-6.35" y="5.715" size="1.778" layer="95">&gt;NAME</text>
<text x="-6.35" y="-5.08" size="1.778" layer="96">&gt;VALUE</text>
<pin name="1" x="-2.54" y="2.54" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="2" x="-2.54" y="0" visible="pad" length="short" direction="pas" function="dot"/>
</symbol>
<symbol name="MICROSD_BOARD_SYMBOL">
<wire x1="-7.62" y1="11.43" x2="25.4" y2="11.43" width="0.254" layer="94"/>
<wire x1="25.4" y1="11.43" x2="25.4" y2="-3.81" width="0.254" layer="94"/>
<wire x1="25.4" y1="-3.81" x2="-7.62" y2="-3.81" width="0.254" layer="94"/>
<wire x1="-7.62" y1="-3.81" x2="-7.62" y2="11.43" width="0.254" layer="94"/>
<pin name="CS" x="-10.16" y="10.16" visible="pin" length="short"/>
<pin name="CLK" x="-10.16" y="7.62" visible="pin" length="short"/>
<pin name="MOSI" x="-10.16" y="5.08" visible="pin" length="short"/>
<pin name="MISO" x="-10.16" y="2.54" visible="pin" length="short"/>
<pin name="VCC" x="-10.16" y="0" visible="pin" length="short"/>
<pin name="GND" x="-10.16" y="-2.54" visible="pin" length="short"/>
<text x="7.62" y="12.7" size="1.778" layer="94" align="center">&gt;NAME</text>
<text x="10.16" y="-5.08" size="1.778" layer="94" align="center">MicroSD Card Adapter</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="ESP32_AZ">
<gates>
<gate name="G$1" symbol="ESP32_AZ" x="-15.24" y="-25.4"/>
</gates>
<devices>
<device name="" package="ESP32_AZ">
<connects>
<connect gate="G$1" pin="3.3V" pad="3.3V"/>
<connect gate="G$1" pin="5V" pad="5V"/>
<connect gate="G$1" pin="CMD" pad="GND-V5"/>
<connect gate="G$1" pin="EN" pad="EN"/>
<connect gate="G$1" pin="G0" pad="G0"/>
<connect gate="G$1" pin="G1/TXD" pad="TXD"/>
<connect gate="G$1" pin="G10/SD3" pad="SD3"/>
<connect gate="G$1" pin="G12" pad="G12"/>
<connect gate="G$1" pin="G13" pad="G13"/>
<connect gate="G$1" pin="G14" pad="G14"/>
<connect gate="G$1" pin="G15" pad="G15"/>
<connect gate="G$1" pin="G16" pad="G16"/>
<connect gate="G$1" pin="G17" pad="G17"/>
<connect gate="G$1" pin="G18" pad="G18"/>
<connect gate="G$1" pin="G19" pad="G19"/>
<connect gate="G$1" pin="G2" pad="G2"/>
<connect gate="G$1" pin="G21/SDA" pad="G21"/>
<connect gate="G$1" pin="G22/SCL" pad="G22"/>
<connect gate="G$1" pin="G23" pad="G23"/>
<connect gate="G$1" pin="G25" pad="G25"/>
<connect gate="G$1" pin="G26" pad="G26"/>
<connect gate="G$1" pin="G27" pad="G27"/>
<connect gate="G$1" pin="G3/RXD" pad="RXD"/>
<connect gate="G$1" pin="G32" pad="G32"/>
<connect gate="G$1" pin="G33" pad="G33"/>
<connect gate="G$1" pin="G34" pad="G34"/>
<connect gate="G$1" pin="G35" pad="G35"/>
<connect gate="G$1" pin="G36/SP" pad="SP"/>
<connect gate="G$1" pin="G39/SN" pad="SN"/>
<connect gate="G$1" pin="G4" pad="G4"/>
<connect gate="G$1" pin="G5" pad="G5"/>
<connect gate="G$1" pin="G6/CLK" pad="CLK"/>
<connect gate="G$1" pin="G7/SD0" pad="SD0"/>
<connect gate="G$1" pin="G8/SD1" pad="SD1"/>
<connect gate="G$1" pin="G9/SD2" pad="SD2"/>
<connect gate="G$1" pin="GND1" pad="GND1"/>
<connect gate="G$1" pin="GND2" pad="GND2"/>
<connect gate="G$1" pin="GND3" pad="GND3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="7SEG-CA_2.9INCH" prefix="D" uservalue="yes">
<description>7-segment &lt;b&gt;DISPLAY&lt;/b&gt;&lt;p&gt;
single anode, 13 mm</description>
<gates>
<gate name="G$1" symbol="7SEG-CA_2.9INCH" x="0" y="0"/>
</gates>
<devices>
<device name="" package="7SEG-CA_2.9INCH">
<connects>
<connect gate="G$1" pin="A" pad="7"/>
<connect gate="G$1" pin="B" pad="6"/>
<connect gate="G$1" pin="C" pad="4"/>
<connect gate="G$1" pin="CA1" pad="1"/>
<connect gate="G$1" pin="CA2" pad="5"/>
<connect gate="G$1" pin="D" pad="3"/>
<connect gate="G$1" pin="DP" pad="8"/>
<connect gate="G$1" pin="E" pad="2"/>
<connect gate="G$1" pin="F" pad="9"/>
<connect gate="G$1" pin="G" pad="10"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="7SEG-CA" prefix="D" uservalue="yes">
<description>7-segment &lt;b&gt;DISPLAY&lt;/b&gt;&lt;p&gt;
single anode, 13 mm</description>
<gates>
<gate name="G$1" symbol="7SEG-CA" x="0" y="0"/>
</gates>
<devices>
<device name="" package="7SEG-13">
<connects>
<connect gate="G$1" pin="A" pad="7"/>
<connect gate="G$1" pin="B" pad="6"/>
<connect gate="G$1" pin="C" pad="4"/>
<connect gate="G$1" pin="CA1" pad="3"/>
<connect gate="G$1" pin="CA2" pad="8"/>
<connect gate="G$1" pin="D" pad="2"/>
<connect gate="G$1" pin="DP" pad="5"/>
<connect gate="G$1" pin="E" pad="1"/>
<connect gate="G$1" pin="F" pad="9"/>
<connect gate="G$1" pin="G" pad="10"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="MCP23017" prefix="IC">
<description>&lt;b&gt;http://ww1.microchip.com/downloads/en/DeviceDoc/21952a.pdf&lt;/b&gt;&lt;p&gt;
Source: http://ww1.microchip.com/downloads/en/DeviceDoc/21952a.pdf</description>
<gates>
<gate name="G$1" symbol="MCP23017" x="0" y="0"/>
</gates>
<devices>
<device name="SP" package="DIL28-3">
<connects>
<connect gate="G$1" pin="!RESET" pad="18"/>
<connect gate="G$1" pin="A0" pad="15"/>
<connect gate="G$1" pin="A1" pad="16"/>
<connect gate="G$1" pin="A2" pad="17"/>
<connect gate="G$1" pin="GPA0" pad="21"/>
<connect gate="G$1" pin="GPA1" pad="22"/>
<connect gate="G$1" pin="GPA2" pad="23"/>
<connect gate="G$1" pin="GPA3" pad="24"/>
<connect gate="G$1" pin="GPA4" pad="25"/>
<connect gate="G$1" pin="GPA5" pad="26"/>
<connect gate="G$1" pin="GPA6" pad="27"/>
<connect gate="G$1" pin="GPA7" pad="28"/>
<connect gate="G$1" pin="GPB0" pad="1"/>
<connect gate="G$1" pin="GPB1" pad="2"/>
<connect gate="G$1" pin="GPB2" pad="3"/>
<connect gate="G$1" pin="GPB3" pad="4"/>
<connect gate="G$1" pin="GPB4" pad="5"/>
<connect gate="G$1" pin="GPB5" pad="6"/>
<connect gate="G$1" pin="GPB6" pad="7"/>
<connect gate="G$1" pin="GPB7" pad="8"/>
<connect gate="G$1" pin="INTA" pad="20"/>
<connect gate="G$1" pin="INTB" pad="19"/>
<connect gate="G$1" pin="SCL" pad="12"/>
<connect gate="G$1" pin="SDA" pad="13"/>
<connect gate="G$1" pin="VDD" pad="9"/>
<connect gate="G$1" pin="VSS" pad="10"/>
</connects>
<technologies>
<technology name="">
<attribute name="MF" value="MICROCHIP" constant="no"/>
<attribute name="MPN" value="MCP23017-E/SP" constant="no"/>
<attribute name="OC_FARNELL" value="1332088" constant="no"/>
<attribute name="OC_NEWARK" value="31K2959" constant="no"/>
</technology>
</technologies>
</device>
<device name="SO" package="SO28W">
<connects>
<connect gate="G$1" pin="!RESET" pad="18"/>
<connect gate="G$1" pin="A0" pad="15"/>
<connect gate="G$1" pin="A1" pad="16"/>
<connect gate="G$1" pin="A2" pad="17"/>
<connect gate="G$1" pin="GPA0" pad="21"/>
<connect gate="G$1" pin="GPA1" pad="22"/>
<connect gate="G$1" pin="GPA2" pad="23"/>
<connect gate="G$1" pin="GPA3" pad="24"/>
<connect gate="G$1" pin="GPA4" pad="25"/>
<connect gate="G$1" pin="GPA5" pad="26"/>
<connect gate="G$1" pin="GPA6" pad="27"/>
<connect gate="G$1" pin="GPA7" pad="28"/>
<connect gate="G$1" pin="GPB0" pad="1"/>
<connect gate="G$1" pin="GPB1" pad="2"/>
<connect gate="G$1" pin="GPB2" pad="3"/>
<connect gate="G$1" pin="GPB3" pad="4"/>
<connect gate="G$1" pin="GPB4" pad="5"/>
<connect gate="G$1" pin="GPB5" pad="6"/>
<connect gate="G$1" pin="GPB6" pad="7"/>
<connect gate="G$1" pin="GPB7" pad="8"/>
<connect gate="G$1" pin="INTA" pad="20"/>
<connect gate="G$1" pin="INTB" pad="19"/>
<connect gate="G$1" pin="SCL" pad="12"/>
<connect gate="G$1" pin="SDA" pad="13"/>
<connect gate="G$1" pin="VDD" pad="9"/>
<connect gate="G$1" pin="VSS" pad="10"/>
</connects>
<technologies>
<technology name="">
<attribute name="MF" value="MICROCHIP" constant="no"/>
<attribute name="MPN" value="MCP23017-E/SO" constant="no"/>
<attribute name="OC_FARNELL" value="1332087" constant="no"/>
<attribute name="OC_NEWARK" value="31K2958" constant="no"/>
</technology>
</technologies>
</device>
<device name="SS" package="SSOP28">
<connects>
<connect gate="G$1" pin="!RESET" pad="18"/>
<connect gate="G$1" pin="A0" pad="15"/>
<connect gate="G$1" pin="A1" pad="16"/>
<connect gate="G$1" pin="A2" pad="17"/>
<connect gate="G$1" pin="GPA0" pad="21"/>
<connect gate="G$1" pin="GPA1" pad="22"/>
<connect gate="G$1" pin="GPA2" pad="23"/>
<connect gate="G$1" pin="GPA3" pad="24"/>
<connect gate="G$1" pin="GPA4" pad="25"/>
<connect gate="G$1" pin="GPA5" pad="26"/>
<connect gate="G$1" pin="GPA6" pad="27"/>
<connect gate="G$1" pin="GPA7" pad="28"/>
<connect gate="G$1" pin="GPB0" pad="1"/>
<connect gate="G$1" pin="GPB1" pad="2"/>
<connect gate="G$1" pin="GPB2" pad="3"/>
<connect gate="G$1" pin="GPB3" pad="4"/>
<connect gate="G$1" pin="GPB4" pad="5"/>
<connect gate="G$1" pin="GPB5" pad="6"/>
<connect gate="G$1" pin="GPB6" pad="7"/>
<connect gate="G$1" pin="GPB7" pad="8"/>
<connect gate="G$1" pin="INTA" pad="20"/>
<connect gate="G$1" pin="INTB" pad="19"/>
<connect gate="G$1" pin="SCL" pad="12"/>
<connect gate="G$1" pin="SDA" pad="13"/>
<connect gate="G$1" pin="VDD" pad="9"/>
<connect gate="G$1" pin="VSS" pad="10"/>
</connects>
<technologies>
<technology name="">
<attribute name="MF" value="MICROCHIP" constant="no"/>
<attribute name="MPN" value="MCP23017-E/SS" constant="no"/>
<attribute name="OC_FARNELL" value="1467674" constant="no"/>
<attribute name="OC_NEWARK" value="31K2960" constant="no"/>
</technology>
</technologies>
</device>
<device name="ML" package="QFN28-ML_6X6MM">
<connects>
<connect gate="G$1" pin="!RESET" pad="14"/>
<connect gate="G$1" pin="A0" pad="11"/>
<connect gate="G$1" pin="A1" pad="12"/>
<connect gate="G$1" pin="A2" pad="13"/>
<connect gate="G$1" pin="GPA0" pad="17"/>
<connect gate="G$1" pin="GPA1" pad="18"/>
<connect gate="G$1" pin="GPA2" pad="19"/>
<connect gate="G$1" pin="GPA3" pad="20"/>
<connect gate="G$1" pin="GPA4" pad="21"/>
<connect gate="G$1" pin="GPA5" pad="22"/>
<connect gate="G$1" pin="GPA6" pad="23"/>
<connect gate="G$1" pin="GPA7" pad="24"/>
<connect gate="G$1" pin="GPB0" pad="25"/>
<connect gate="G$1" pin="GPB1" pad="26"/>
<connect gate="G$1" pin="GPB2" pad="27"/>
<connect gate="G$1" pin="GPB3" pad="28"/>
<connect gate="G$1" pin="GPB4" pad="1"/>
<connect gate="G$1" pin="GPB5" pad="2"/>
<connect gate="G$1" pin="GPB6" pad="3"/>
<connect gate="G$1" pin="GPB7" pad="4"/>
<connect gate="G$1" pin="INTA" pad="16"/>
<connect gate="G$1" pin="INTB" pad="15"/>
<connect gate="G$1" pin="SCL" pad="8"/>
<connect gate="G$1" pin="SDA" pad="9"/>
<connect gate="G$1" pin="VDD" pad="5"/>
<connect gate="G$1" pin="VSS" pad="6"/>
</connects>
<technologies>
<technology name="">
<attribute name="MF" value="MICROCHIP" constant="no"/>
<attribute name="MPN" value="MCP23017-E/ML" constant="no"/>
<attribute name="OC_FARNELL" value="unknown" constant="no"/>
<attribute name="OC_NEWARK" value="31K2957" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="CD4543" prefix="IC">
<description>BCD to 7-segment &lt;b&gt;LCD DECODER&lt;/b&gt;</description>
<gates>
<gate name="A" symbol="CD4543" x="20.32" y="0"/>
</gates>
<devices>
<device name="N" package="CD4543_DIL16">
<connects>
<connect gate="A" pin="A" pad="9"/>
<connect gate="A" pin="B" pad="10"/>
<connect gate="A" pin="BI" pad="7"/>
<connect gate="A" pin="C" pad="11"/>
<connect gate="A" pin="D" pad="12"/>
<connect gate="A" pin="E" pad="13"/>
<connect gate="A" pin="F" pad="15"/>
<connect gate="A" pin="G" pad="14"/>
<connect gate="A" pin="IA" pad="5"/>
<connect gate="A" pin="IB" pad="3"/>
<connect gate="A" pin="IC" pad="2"/>
<connect gate="A" pin="ID" pad="4"/>
<connect gate="A" pin="LD" pad="1"/>
<connect gate="A" pin="PH" pad="6"/>
<connect gate="A" pin="VDD" pad="16"/>
<connect gate="A" pin="VSS" pad="8"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="D" package="CD4543_SO16">
<connects>
<connect gate="A" pin="A" pad="9"/>
<connect gate="A" pin="B" pad="10"/>
<connect gate="A" pin="BI" pad="7"/>
<connect gate="A" pin="C" pad="11"/>
<connect gate="A" pin="D" pad="12"/>
<connect gate="A" pin="E" pad="13"/>
<connect gate="A" pin="F" pad="15"/>
<connect gate="A" pin="G" pad="14"/>
<connect gate="A" pin="IA" pad="5"/>
<connect gate="A" pin="IB" pad="3"/>
<connect gate="A" pin="IC" pad="2"/>
<connect gate="A" pin="ID" pad="4"/>
<connect gate="A" pin="LD" pad="1"/>
<connect gate="A" pin="PH" pad="6"/>
<connect gate="A" pin="VDD" pad="16"/>
<connect gate="A" pin="VSS" pad="8"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="74HC238" prefix="IC">
<description>3 to 8 line &lt;B&gt;DECODER/DEMULTIPLEXER&lt;/B&gt;</description>
<gates>
<gate name="A" symbol="74HC238" x="2.54" y="0"/>
</gates>
<devices>
<device name="" package="74HC238">
<connects>
<connect gate="A" pin="A0" pad="1"/>
<connect gate="A" pin="A1" pad="2"/>
<connect gate="A" pin="A2" pad="3"/>
<connect gate="A" pin="E1" pad="4"/>
<connect gate="A" pin="E2" pad="5"/>
<connect gate="A" pin="E3" pad="6"/>
<connect gate="A" pin="GND" pad="8"/>
<connect gate="A" pin="VCC" pad="16"/>
<connect gate="A" pin="Y0" pad="15"/>
<connect gate="A" pin="Y1" pad="14"/>
<connect gate="A" pin="Y2" pad="13"/>
<connect gate="A" pin="Y3" pad="12"/>
<connect gate="A" pin="Y4" pad="11"/>
<connect gate="A" pin="Y5" pad="10"/>
<connect gate="A" pin="Y6" pad="9"/>
<connect gate="A" pin="Y7" pad="7"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="D" package="74HC238D">
<connects>
<connect gate="A" pin="A0" pad="1"/>
<connect gate="A" pin="A1" pad="2"/>
<connect gate="A" pin="A2" pad="3"/>
<connect gate="A" pin="E1" pad="4"/>
<connect gate="A" pin="E2" pad="5"/>
<connect gate="A" pin="E3" pad="6"/>
<connect gate="A" pin="GND" pad="8"/>
<connect gate="A" pin="VCC" pad="16"/>
<connect gate="A" pin="Y0" pad="15"/>
<connect gate="A" pin="Y1" pad="14"/>
<connect gate="A" pin="Y2" pad="13"/>
<connect gate="A" pin="Y3" pad="12"/>
<connect gate="A" pin="Y4" pad="11"/>
<connect gate="A" pin="Y5" pad="10"/>
<connect gate="A" pin="Y6" pad="9"/>
<connect gate="A" pin="Y7" pad="7"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="RELAY" prefix="K">
<description>UNLibrary relay</description>
<gates>
<gate name="1" symbol="RELAY_POWER" x="-7.62" y="0" addlevel="must"/>
<gate name="2" symbol="RELAY_SWITCH" x="10.16" y="0" addlevel="always"/>
</gates>
<devices>
<device name="" package="RELAY">
<connects>
<connect gate="1" pin="1" pad="1"/>
<connect gate="1" pin="2" pad="2"/>
<connect gate="2" pin="O" pad="NC"/>
<connect gate="2" pin="P" pad="C"/>
<connect gate="2" pin="S" pad="NO"/>
</connects>
<technologies>
<technology name="">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OC_FARNELL" value="unknown" constant="no"/>
<attribute name="OC_NEWARK" value="unknown" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="2PIN_BLOCK" prefix="X" uservalue="yes">
<description>&lt;b&gt;WAGO SCREW CLAMP&lt;/b&gt;</description>
<gates>
<gate name="-1" symbol="KL" x="0" y="5.08" addlevel="always"/>
<gate name="-2" symbol="KL+V" x="0" y="0" addlevel="always"/>
</gates>
<devices>
<device name="" package="W237-102">
<connects>
<connect gate="-1" pin="KL" pad="1"/>
<connect gate="-2" pin="KL" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="237-102" constant="no"/>
<attribute name="OC_FARNELL" value="unknown" constant="no"/>
<attribute name="OC_NEWARK" value="70K9898" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="PINHD-1X2" prefix="JP" uservalue="yes">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="PINHD2" x="0" y="0"/>
</gates>
<devices>
<device name="" package="1X02">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="/90" package="1X02/90">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="MICROSD_BOARD">
<gates>
<gate name="G$1" symbol="MICROSD_BOARD_SYMBOL" x="-7.62" y="-2.54"/>
</gates>
<devices>
<device name="" package="MICROSD_BOARD_PACKAGE">
<connects>
<connect gate="G$1" pin="CLK" pad="CLK"/>
<connect gate="G$1" pin="CS" pad="CS"/>
<connect gate="G$1" pin="GND" pad="GND"/>
<connect gate="G$1" pin="MISO" pad="MISO"/>
<connect gate="G$1" pin="MOSI" pad="MOSI"/>
<connect gate="G$1" pin="VCC" pad="VCC"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="led">
<description>&lt;b&gt;LEDs&lt;/b&gt;&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;&lt;br&gt;
Extended by Federico Battaglin &lt;author&gt;&amp;lt;federico.rd@fdpinternational.com&amp;gt;&lt;/author&gt; with DUOLED</description>
<packages>
<package name="1206">
<description>&lt;b&gt;CHICAGO MINIATURE LAMP, INC.&lt;/b&gt;&lt;p&gt;
7022X Series SMT LEDs 1206 Package Size</description>
<wire x1="1.55" y1="-0.75" x2="-1.55" y2="-0.75" width="0.1016" layer="51"/>
<wire x1="-1.55" y1="-0.75" x2="-1.55" y2="0.75" width="0.1016" layer="51"/>
<wire x1="-1.55" y1="0.75" x2="1.55" y2="0.75" width="0.1016" layer="51"/>
<wire x1="1.55" y1="0.75" x2="1.55" y2="-0.75" width="0.1016" layer="51"/>
<wire x1="-0.55" y1="-0.5" x2="0.55" y2="-0.5" width="0.1016" layer="21" curve="95.452622"/>
<wire x1="-0.55" y1="-0.5" x2="-0.55" y2="0.5" width="0.1016" layer="51" curve="-84.547378"/>
<wire x1="-0.55" y1="0.5" x2="0.55" y2="0.5" width="0.1016" layer="21" curve="-95.452622"/>
<wire x1="0.55" y1="0.5" x2="0.55" y2="-0.5" width="0.1016" layer="51" curve="-84.547378"/>
<smd name="A" x="-1.422" y="0" dx="1.6" dy="1.803" layer="1"/>
<smd name="C" x="1.422" y="0" dx="1.6" dy="1.803" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.1" y1="-0.1" x2="0.1" y2="0.1" layer="21"/>
<rectangle x1="0.45" y1="-0.7" x2="0.8" y2="-0.45" layer="51"/>
<rectangle x1="0.8" y1="-0.7" x2="0.9" y2="0.5" layer="51"/>
<rectangle x1="0.8" y1="0.55" x2="0.9" y2="0.7" layer="51"/>
<rectangle x1="-0.9" y1="-0.7" x2="-0.8" y2="0.5" layer="51"/>
<rectangle x1="-0.9" y1="0.55" x2="-0.8" y2="0.7" layer="51"/>
<rectangle x1="0.45" y1="-0.7" x2="0.6" y2="-0.45" layer="21"/>
</package>
<package name="LD260">
<description>&lt;B&gt;LED&lt;/B&gt;&lt;p&gt;
5 mm, square, Siemens</description>
<wire x1="-1.27" y1="-1.27" x2="0" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="0" y1="-1.27" x2="1.27" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="1.27" y1="1.27" x2="0" y2="1.27" width="0.1524" layer="21"/>
<wire x1="0" y1="1.27" x2="-1.27" y2="1.27" width="0.1524" layer="21"/>
<wire x1="1.27" y1="-1.27" x2="1.27" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="1.27" y1="1.27" x2="1.27" y2="0.889" width="0.1524" layer="21"/>
<wire x1="1.27" y1="0.889" x2="1.27" y2="0" width="0.1524" layer="51"/>
<wire x1="1.27" y1="0" x2="1.27" y2="-0.889" width="0.1524" layer="51"/>
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="0.889" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-1.27" x2="-1.27" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-0.889" x2="-1.27" y2="0" width="0.1524" layer="51"/>
<wire x1="-1.27" y1="0" x2="-1.27" y2="0.889" width="0.1524" layer="51"/>
<wire x1="0" y1="1.27" x2="0.9917" y2="0.7934" width="0.1524" layer="21" curve="-51.33923"/>
<wire x1="-0.9917" y1="0.7934" x2="0" y2="1.27" width="0.1524" layer="21" curve="-51.33923"/>
<wire x1="0" y1="-1.27" x2="0.9917" y2="-0.7934" width="0.1524" layer="21" curve="51.33923"/>
<wire x1="-0.9917" y1="-0.7934" x2="0" y2="-1.27" width="0.1524" layer="21" curve="51.33923"/>
<wire x1="0.9558" y1="-0.8363" x2="1.27" y2="0" width="0.1524" layer="51" curve="41.185419"/>
<wire x1="0.9756" y1="0.813" x2="1.2699" y2="0" width="0.1524" layer="51" curve="-39.806332"/>
<wire x1="-1.27" y1="0" x2="-0.9643" y2="-0.8265" width="0.1524" layer="51" curve="40.600331"/>
<wire x1="-1.27" y1="0" x2="-0.9643" y2="0.8265" width="0.1524" layer="51" curve="-40.600331"/>
<wire x1="-0.889" y1="0" x2="0" y2="0.889" width="0.1524" layer="51" curve="-90"/>
<wire x1="-0.508" y1="0" x2="0" y2="0.508" width="0.1524" layer="51" curve="-90"/>
<wire x1="0" y1="-0.508" x2="0.508" y2="0" width="0.1524" layer="21" curve="90"/>
<wire x1="0" y1="-0.889" x2="0.889" y2="0" width="0.1524" layer="51" curve="90"/>
<pad name="A" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="K" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-1.2954" y="1.4732" size="1.016" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-2.4892" size="1.016" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="1.27" y1="-0.635" x2="2.032" y2="0.635" layer="51"/>
<rectangle x1="1.905" y1="-0.635" x2="2.032" y2="0.635" layer="21"/>
</package>
<package name="LED2X5">
<description>&lt;B&gt;LED&lt;/B&gt;&lt;p&gt;
2 x 5 mm, rectangle</description>
<wire x1="-2.54" y1="-1.27" x2="2.54" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="2.54" y1="1.27" x2="2.54" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="2.54" y1="1.27" x2="-2.54" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="-1.27" x2="-2.54" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="0" x2="0.508" y2="0" width="0.1524" layer="51"/>
<wire x1="-0.508" y1="0.381" x2="-0.508" y2="-0.381" width="0.1524" layer="51"/>
<wire x1="-0.508" y1="0.381" x2="0.508" y2="0" width="0.1524" layer="51"/>
<wire x1="0.508" y1="0" x2="1.778" y2="0" width="0.1524" layer="51"/>
<wire x1="0.508" y1="0" x2="-0.508" y2="-0.381" width="0.1524" layer="51"/>
<wire x1="0.508" y1="0.381" x2="0.508" y2="0" width="0.1524" layer="51"/>
<wire x1="0.508" y1="0" x2="0.508" y2="-0.381" width="0.1524" layer="51"/>
<wire x1="0.889" y1="-0.254" x2="1.143" y2="-0.762" width="0.1524" layer="51"/>
<wire x1="1.143" y1="-0.762" x2="1.143" y2="-0.508" width="0.1524" layer="51"/>
<wire x1="1.143" y1="-0.762" x2="0.9398" y2="-0.6096" width="0.1524" layer="51"/>
<wire x1="0.9398" y1="-0.6096" x2="1.143" y2="-0.508" width="0.1524" layer="51"/>
<wire x1="1.397" y1="-0.254" x2="1.651" y2="-0.762" width="0.1524" layer="51"/>
<wire x1="1.651" y1="-0.762" x2="1.651" y2="-0.508" width="0.1524" layer="51"/>
<wire x1="1.651" y1="-0.762" x2="1.4478" y2="-0.6096" width="0.1524" layer="51"/>
<wire x1="1.4478" y1="-0.6096" x2="1.651" y2="-0.508" width="0.1524" layer="51"/>
<pad name="A" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="K" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.54" y="1.397" size="1.016" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.54" y="-2.413" size="1.016" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="2.159" y1="-1.27" x2="2.413" y2="1.27" layer="21"/>
</package>
<package name="LED3MM">
<description>&lt;B&gt;LED&lt;/B&gt;&lt;p&gt;
3 mm, round</description>
<wire x1="1.5748" y1="-1.27" x2="1.5748" y2="1.27" width="0.254" layer="51"/>
<wire x1="-1.524" y1="0" x2="-1.1708" y2="0.9756" width="0.1524" layer="51" curve="-39.80361"/>
<wire x1="-1.524" y1="0" x2="-1.1391" y2="-1.0125" width="0.1524" layer="51" curve="41.633208"/>
<wire x1="1.1571" y1="0.9918" x2="1.524" y2="0" width="0.1524" layer="51" curve="-40.601165"/>
<wire x1="1.1708" y1="-0.9756" x2="1.524" y2="0" width="0.1524" layer="51" curve="39.80361"/>
<wire x1="0" y1="1.524" x2="1.2401" y2="0.8858" width="0.1524" layer="21" curve="-54.461337"/>
<wire x1="-1.2192" y1="0.9144" x2="0" y2="1.524" width="0.1524" layer="21" curve="-53.130102"/>
<wire x1="0" y1="-1.524" x2="1.203" y2="-0.9356" width="0.1524" layer="21" curve="52.126876"/>
<wire x1="-1.203" y1="-0.9356" x2="0" y2="-1.524" width="0.1524" layer="21" curve="52.126876"/>
<wire x1="-0.635" y1="0" x2="0" y2="0.635" width="0.1524" layer="51" curve="-90"/>
<wire x1="-1.016" y1="0" x2="0" y2="1.016" width="0.1524" layer="51" curve="-90"/>
<wire x1="0" y1="-0.635" x2="0.635" y2="0" width="0.1524" layer="51" curve="90"/>
<wire x1="0" y1="-1.016" x2="1.016" y2="0" width="0.1524" layer="51" curve="90"/>
<wire x1="0" y1="2.032" x2="1.561" y2="1.3009" width="0.254" layer="21" curve="-50.193108"/>
<wire x1="-1.7929" y1="0.9562" x2="0" y2="2.032" width="0.254" layer="21" curve="-61.926949"/>
<wire x1="0" y1="-2.032" x2="1.5512" y2="-1.3126" width="0.254" layer="21" curve="49.763022"/>
<wire x1="-1.7643" y1="-1.0082" x2="0" y2="-2.032" width="0.254" layer="21" curve="60.255215"/>
<wire x1="-2.032" y1="0" x2="-1.7891" y2="0.9634" width="0.254" layer="51" curve="-28.301701"/>
<wire x1="-2.032" y1="0" x2="-1.7306" y2="-1.065" width="0.254" layer="51" curve="31.60822"/>
<pad name="A" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="K" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="1.905" y="0.381" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="1.905" y="-1.651" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="LED5MM">
<description>&lt;B&gt;LED&lt;/B&gt;&lt;p&gt;
5 mm, round</description>
<wire x1="2.54" y1="-1.905" x2="2.54" y2="1.905" width="0.2032" layer="21"/>
<wire x1="2.54" y1="-1.905" x2="2.54" y2="1.905" width="0.254" layer="21" curve="-286.260205"/>
<wire x1="-1.143" y1="0" x2="0" y2="1.143" width="0.1524" layer="51" curve="-90"/>
<wire x1="0" y1="-1.143" x2="1.143" y2="0" width="0.1524" layer="51" curve="90"/>
<wire x1="-1.651" y1="0" x2="0" y2="1.651" width="0.1524" layer="51" curve="-90"/>
<wire x1="0" y1="-1.651" x2="1.651" y2="0" width="0.1524" layer="51" curve="90"/>
<wire x1="-2.159" y1="0" x2="0" y2="2.159" width="0.1524" layer="51" curve="-90"/>
<wire x1="0" y1="-2.159" x2="2.159" y2="0" width="0.1524" layer="51" curve="90"/>
<circle x="0" y="0" radius="2.54" width="0.1524" layer="21"/>
<pad name="A" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="K" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="3.175" y="0.5334" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="3.2004" y="-1.8034" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="LSU260">
<description>&lt;B&gt;LED&lt;/B&gt;&lt;p&gt;
1 mm, round, Siemens</description>
<wire x1="0" y1="-0.508" x2="-1.143" y2="-0.508" width="0.1524" layer="51"/>
<wire x1="-1.143" y1="-0.508" x2="-1.143" y2="-0.254" width="0.1524" layer="51"/>
<wire x1="-1.143" y1="0.508" x2="0" y2="0.508" width="0.1524" layer="51"/>
<wire x1="-1.143" y1="-0.254" x2="-1.397" y2="-0.254" width="0.1524" layer="51"/>
<wire x1="-1.143" y1="-0.254" x2="-1.143" y2="0.254" width="0.1524" layer="51"/>
<wire x1="-1.397" y1="-0.254" x2="-1.397" y2="0.254" width="0.1524" layer="51"/>
<wire x1="-1.397" y1="0.254" x2="-1.143" y2="0.254" width="0.1524" layer="51"/>
<wire x1="-1.143" y1="0.254" x2="-1.143" y2="0.508" width="0.1524" layer="51"/>
<wire x1="0.508" y1="-0.254" x2="1.397" y2="-0.254" width="0.1524" layer="51"/>
<wire x1="1.397" y1="-0.254" x2="1.397" y2="0.254" width="0.1524" layer="51"/>
<wire x1="1.397" y1="0.254" x2="0.508" y2="0.254" width="0.1524" layer="51"/>
<wire x1="0.381" y1="-0.381" x2="0.254" y2="-0.508" width="0.1524" layer="21"/>
<wire x1="0.254" y1="-0.508" x2="-0.254" y2="-0.508" width="0.1524" layer="21"/>
<wire x1="-0.381" y1="-0.381" x2="-0.254" y2="-0.508" width="0.1524" layer="21"/>
<wire x1="0.381" y1="0.381" x2="0.254" y2="0.508" width="0.1524" layer="21"/>
<wire x1="0.254" y1="0.508" x2="-0.254" y2="0.508" width="0.1524" layer="21"/>
<wire x1="-0.381" y1="0.381" x2="-0.254" y2="0.508" width="0.1524" layer="21"/>
<wire x1="0" y1="-0.254" x2="0.254" y2="0" width="0.1524" layer="21" curve="90"/>
<wire x1="-0.254" y1="0" x2="0" y2="0.254" width="0.1524" layer="21" curve="-90"/>
<wire x1="0.381" y1="-0.381" x2="0.381" y2="0.381" width="0.1524" layer="21" curve="90"/>
<circle x="0" y="0" radius="0.508" width="0.1524" layer="51"/>
<pad name="A" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="K" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-1.2954" y="0.8382" size="1.016" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-1.8542" size="1.016" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-1.397" y1="-0.254" x2="-1.143" y2="0.254" layer="51"/>
<rectangle x1="0.508" y1="-0.254" x2="1.397" y2="0.254" layer="51"/>
</package>
<package name="LZR181">
<description>&lt;B&gt;LED BLOCK&lt;/B&gt;&lt;p&gt;
1 LED, Siemens</description>
<wire x1="-1.27" y1="-1.27" x2="1.27" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="1.27" y1="-1.27" x2="1.27" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="1.27" y1="1.27" x2="1.27" y2="0.889" width="0.1524" layer="21"/>
<wire x1="1.27" y1="0.889" x2="1.27" y2="-0.889" width="0.1524" layer="51"/>
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="0.889" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-1.27" x2="-1.27" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-0.889" x2="-1.27" y2="0.889" width="0.1524" layer="51"/>
<wire x1="-0.889" y1="0" x2="0" y2="0.889" width="0.1524" layer="51" curve="-90"/>
<wire x1="-0.508" y1="0" x2="0" y2="0.508" width="0.1524" layer="51" curve="-90"/>
<wire x1="0" y1="-0.508" x2="0.508" y2="0" width="0.1524" layer="21" curve="90"/>
<wire x1="0" y1="-0.889" x2="0.889" y2="0" width="0.1524" layer="51" curve="90"/>
<wire x1="-0.8678" y1="0.7439" x2="0" y2="1.143" width="0.1524" layer="21" curve="-49.396139"/>
<wire x1="0" y1="1.143" x2="0.8678" y2="0.7439" width="0.1524" layer="21" curve="-49.396139"/>
<wire x1="-0.8678" y1="-0.7439" x2="0" y2="-1.143" width="0.1524" layer="21" curve="49.396139"/>
<wire x1="0" y1="-1.143" x2="0.8678" y2="-0.7439" width="0.1524" layer="21" curve="49.396139"/>
<wire x1="0.8678" y1="0.7439" x2="1.143" y2="0" width="0.1524" layer="51" curve="-40.604135"/>
<wire x1="0.8678" y1="-0.7439" x2="1.143" y2="0" width="0.1524" layer="51" curve="40.604135"/>
<wire x1="-1.143" y1="0" x2="-0.8678" y2="0.7439" width="0.1524" layer="51" curve="-40.604135"/>
<wire x1="-1.143" y1="0" x2="-0.8678" y2="-0.7439" width="0.1524" layer="51" curve="40.604135"/>
<wire x1="-1.27" y1="1.27" x2="1.27" y2="1.27" width="0.1524" layer="21"/>
<pad name="A" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="K" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-1.2954" y="1.4732" size="1.016" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-2.4892" size="1.016" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="1.27" y1="-0.889" x2="1.524" y2="0.254" layer="51"/>
<rectangle x1="-1.524" y1="-0.254" x2="-1.27" y2="0.254" layer="51"/>
</package>
<package name="Q62902-B152">
<description>&lt;b&gt;LED HOLDER&lt;/b&gt;&lt;p&gt;
Siemens</description>
<wire x1="-2.9718" y1="-1.8542" x2="-2.9718" y2="-0.254" width="0.1524" layer="21"/>
<wire x1="-2.9718" y1="-0.254" x2="-2.9718" y2="0.254" width="0.1524" layer="21"/>
<wire x1="-2.9718" y1="0.254" x2="-2.9718" y2="1.8542" width="0.1524" layer="21"/>
<wire x1="2.9718" y1="-1.8542" x2="-2.1082" y2="-1.8542" width="0.1524" layer="21"/>
<wire x1="-2.1082" y1="-1.8542" x2="-2.54" y2="-1.8542" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="1.8542" x2="-2.1082" y2="1.8542" width="0.1524" layer="21"/>
<wire x1="-2.1082" y1="1.8542" x2="2.9718" y2="1.8542" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="-1.8542" x2="-2.54" y2="1.8542" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="-1.8542" x2="-2.9718" y2="-1.8542" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="1.8542" x2="-2.9718" y2="1.8542" width="0.1524" layer="21"/>
<wire x1="-2.9718" y1="0.254" x2="-2.9718" y2="-0.254" width="0.1524" layer="21" curve="180"/>
<wire x1="-1.1486" y1="0.8814" x2="0" y2="1.4478" width="0.1524" layer="21" curve="-52.498642"/>
<wire x1="0" y1="1.4478" x2="1.1476" y2="0.8827" width="0.1524" layer="21" curve="-52.433716"/>
<wire x1="-1.1351" y1="-0.8987" x2="0" y2="-1.4478" width="0.1524" layer="21" curve="51.629985"/>
<wire x1="0" y1="-1.4478" x2="1.1305" y2="-0.9044" width="0.1524" layer="21" curve="51.339172"/>
<wire x1="1.1281" y1="-0.9074" x2="1.4478" y2="0" width="0.1524" layer="51" curve="38.811177"/>
<wire x1="1.1401" y1="0.8923" x2="1.4478" y2="0" width="0.1524" layer="51" curve="-38.048073"/>
<wire x1="-1.4478" y1="0" x2="-1.1305" y2="-0.9044" width="0.1524" layer="51" curve="38.659064"/>
<wire x1="-1.4478" y1="0" x2="-1.1456" y2="0.8853" width="0.1524" layer="51" curve="-37.696376"/>
<wire x1="0" y1="1.7018" x2="1.4674" y2="0.8618" width="0.1524" layer="21" curve="-59.573488"/>
<wire x1="-1.4618" y1="0.8714" x2="0" y2="1.7018" width="0.1524" layer="21" curve="-59.200638"/>
<wire x1="0" y1="-1.7018" x2="1.4571" y2="-0.8793" width="0.1524" layer="21" curve="58.891781"/>
<wire x1="-1.4571" y1="-0.8793" x2="0" y2="-1.7018" width="0.1524" layer="21" curve="58.891781"/>
<wire x1="-1.7018" y1="0" x2="-1.4447" y2="0.8995" width="0.1524" layer="51" curve="-31.907626"/>
<wire x1="-1.7018" y1="0" x2="-1.4502" y2="-0.8905" width="0.1524" layer="51" curve="31.551992"/>
<wire x1="1.4521" y1="0.8874" x2="1.7018" y2="0" width="0.1524" layer="51" curve="-31.429586"/>
<wire x1="1.4459" y1="-0.8975" x2="1.7018" y2="0" width="0.1524" layer="51" curve="31.828757"/>
<wire x1="-2.1082" y1="1.8542" x2="-2.1082" y2="-1.8542" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="0" x2="0" y2="0.635" width="0.1524" layer="21" curve="-90"/>
<wire x1="-1.016" y1="0" x2="0" y2="1.016" width="0.1524" layer="51" curve="-90"/>
<wire x1="0" y1="-0.635" x2="0.635" y2="0" width="0.1524" layer="51" curve="90"/>
<wire x1="0.0539" y1="-1.0699" x2="1.0699" y2="-0.0539" width="0.1524" layer="51" curve="90"/>
<wire x1="2.9718" y1="1.8542" x2="2.9718" y2="-1.8542" width="0.1524" layer="21"/>
<pad name="K" x="-1.27" y="0" drill="0.8128" shape="long" rot="R90"/>
<pad name="A" x="1.27" y="0" drill="0.8128" shape="long" rot="R90"/>
<text x="-1.905" y="2.286" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.905" y="-3.556" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="Q62902-B153">
<description>&lt;b&gt;LED HOLDER&lt;/b&gt;&lt;p&gt;
Siemens</description>
<wire x1="-5.5118" y1="-3.5052" x2="-5.5118" y2="-0.254" width="0.1524" layer="21"/>
<wire x1="-5.5118" y1="-0.254" x2="-5.5118" y2="0.254" width="0.1524" layer="21"/>
<wire x1="-5.5118" y1="0.254" x2="-5.5118" y2="3.5052" width="0.1524" layer="21"/>
<wire x1="5.5118" y1="-3.5052" x2="-4.6482" y2="-3.5052" width="0.1524" layer="21"/>
<wire x1="-4.6482" y1="-3.5052" x2="-5.08" y2="-3.5052" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="3.5052" x2="-4.6482" y2="3.5052" width="0.1524" layer="21"/>
<wire x1="-4.6482" y1="3.5052" x2="5.5118" y2="3.5052" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="-3.5052" x2="-5.08" y2="3.5052" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="-3.5052" x2="-5.5118" y2="-3.5052" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="3.5052" x2="-5.5118" y2="3.5052" width="0.1524" layer="21"/>
<wire x1="-5.5118" y1="0.254" x2="-5.5118" y2="-0.254" width="0.1524" layer="21" curve="180"/>
<wire x1="-4.6482" y1="3.5052" x2="-4.6482" y2="-3.5052" width="0.1524" layer="21"/>
<wire x1="5.5118" y1="3.5052" x2="5.5118" y2="-3.5052" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="1.905" x2="-2.54" y2="-1.905" width="0.254" layer="21"/>
<wire x1="0" y1="-1.143" x2="1.143" y2="0" width="0.1524" layer="51" curve="90"/>
<wire x1="-1.143" y1="0" x2="0" y2="1.143" width="0.1524" layer="51" curve="-90"/>
<wire x1="0" y1="-1.651" x2="1.651" y2="0" width="0.1524" layer="51" curve="90"/>
<wire x1="-1.651" y1="0" x2="0" y2="1.651" width="0.1524" layer="51" curve="-90"/>
<wire x1="0" y1="-2.159" x2="2.159" y2="0" width="0.1524" layer="51" curve="90"/>
<wire x1="-2.2129" y1="0.0539" x2="-0.0539" y2="2.2129" width="0.1524" layer="51" curve="-90.010616"/>
<circle x="0" y="0" radius="2.54" width="0.1524" layer="21"/>
<circle x="0" y="0" radius="3.175" width="0.254" layer="21"/>
<pad name="A" x="1.27" y="0" drill="0.8128" shape="long" rot="R90"/>
<pad name="K" x="-1.27" y="0" drill="0.8128" shape="long" rot="R90"/>
<text x="-4.191" y="3.937" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-4.318" y="-5.08" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="Q62902-B155">
<description>&lt;b&gt;LED HOLDER&lt;/b&gt;&lt;p&gt;
Siemens</description>
<wire x1="-1.27" y1="-3.048" x2="-1.27" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="10.033" y1="3.048" x2="2.921" y2="3.048" width="0.1524" layer="21"/>
<wire x1="10.033" y1="3.048" x2="10.033" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-3.048" x2="2.921" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="2.921" y1="-3.048" x2="2.921" y2="3.048" width="0.1524" layer="21"/>
<wire x1="2.921" y1="-3.048" x2="10.033" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="2.921" y1="3.048" x2="-1.27" y2="3.048" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="2.54" x2="-5.207" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="2.54" x2="-1.27" y2="3.048" width="0.1524" layer="21"/>
<wire x1="-5.207" y1="-2.54" x2="-1.27" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-2.54" x2="-1.27" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-5.207" y1="2.54" x2="-5.207" y2="-2.54" width="0.1524" layer="21" curve="180"/>
<wire x1="-6.985" y1="0.635" x2="-6.985" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-6.096" y1="1.397" x2="-6.096" y2="-1.397" width="0.1524" layer="21"/>
<wire x1="-5.207" y1="1.905" x2="-5.207" y2="-1.905" width="0.1524" layer="21"/>
<pad name="K" x="7.62" y="1.27" drill="0.8128" shape="long"/>
<pad name="A" x="7.62" y="-1.27" drill="0.8128" shape="long"/>
<text x="3.302" y="-2.794" size="1.016" layer="21" ratio="14">A+</text>
<text x="3.302" y="1.778" size="1.016" layer="21" ratio="14">K-</text>
<text x="11.684" y="-2.794" size="1.27" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<text x="0.635" y="-4.445" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="2.921" y1="1.016" x2="6.731" y2="1.524" layer="21"/>
<rectangle x1="2.921" y1="-1.524" x2="6.731" y2="-1.016" layer="21"/>
<hole x="0" y="0" drill="0.8128"/>
</package>
<package name="Q62902-B156">
<description>&lt;b&gt;LED HOLDER&lt;/b&gt;&lt;p&gt;
Siemens</description>
<wire x1="2.54" y1="-1.905" x2="2.54" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-1.143" y1="0" x2="0" y2="1.143" width="0.1524" layer="51" curve="-90"/>
<wire x1="0" y1="-1.143" x2="1.143" y2="0" width="0.1524" layer="51" curve="90"/>
<wire x1="-1.651" y1="0" x2="0" y2="1.651" width="0.1524" layer="51" curve="-90"/>
<wire x1="0" y1="-1.651" x2="1.651" y2="0" width="0.1524" layer="51" curve="90"/>
<wire x1="-2.159" y1="0" x2="0" y2="2.159" width="0.1524" layer="51" curve="-90"/>
<wire x1="0.0539" y1="-2.2129" x2="2.2129" y2="-0.0539" width="0.1524" layer="51" curve="90.005308"/>
<wire x1="2.54" y1="3.81" x2="3.81" y2="2.54" width="0.1524" layer="21"/>
<wire x1="2.54" y1="3.81" x2="-3.81" y2="3.81" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="-3.81" x2="-3.81" y2="3.81" width="0.1524" layer="21"/>
<wire x1="3.81" y1="2.54" x2="3.81" y2="-3.81" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="-3.81" x2="-2.54" y2="-3.81" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="-3.302" x2="-2.54" y2="-3.81" width="0.1524" layer="21"/>
<wire x1="3.81" y1="-3.81" x2="2.54" y2="-3.81" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-3.302" x2="2.54" y2="-3.81" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-3.302" x2="-2.54" y2="-3.302" width="0.1524" layer="21"/>
<circle x="0" y="0" radius="2.54" width="0.1524" layer="21"/>
<circle x="0" y="0" radius="3.175" width="0.254" layer="21"/>
<pad name="A" x="-1.27" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="K" x="1.27" y="0" drill="1.016" shape="long" rot="R90"/>
<text x="-3.81" y="4.0894" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.7846" y="-5.3594" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<text x="-3.556" y="-3.302" size="1.016" layer="21" ratio="14">+</text>
<text x="2.794" y="-3.302" size="1.016" layer="21" ratio="14">-</text>
</package>
<package name="SFH480">
<description>&lt;B&gt;IR LED&lt;/B&gt;&lt;p&gt;
infrared emitting diode, Infineon
TO-18, lead spacing 2.54 mm, cathode marking&lt;p&gt;
Inifineon</description>
<wire x1="-2.159" y1="1.524" x2="-2.794" y2="2.159" width="0.1524" layer="21"/>
<wire x1="-2.794" y1="2.159" x2="-2.159" y2="2.794" width="0.1524" layer="21"/>
<wire x1="-1.524" y1="2.159" x2="-2.159" y2="2.794" width="0.1524" layer="21"/>
<wire x1="0" y1="1.778" x2="1.5358" y2="0.8959" width="0.1524" layer="21" curve="-59.743278"/>
<wire x1="-1.5358" y1="0.8959" x2="0" y2="1.778" width="0.1524" layer="21" curve="-59.743278"/>
<wire x1="-1.5358" y1="-0.8959" x2="0" y2="-1.778" width="0.1524" layer="21" curve="59.743278"/>
<wire x1="0" y1="-1.778" x2="1.5358" y2="-0.8959" width="0.1524" layer="21" curve="59.743278"/>
<wire x1="1.5142" y1="0.9318" x2="1.778" y2="0" width="0.1524" layer="51" curve="-31.606487"/>
<wire x1="1.5" y1="-0.9546" x2="1.778" y2="0" width="0.1524" layer="51" curve="32.472615"/>
<wire x1="-1.778" y1="0" x2="-1.5142" y2="-0.9318" width="0.1524" layer="51" curve="31.606487"/>
<wire x1="-1.778" y1="0" x2="-1.5" y2="0.9546" width="0.1524" layer="51" curve="-32.472615"/>
<wire x1="-0.635" y1="0" x2="0" y2="0.635" width="0.1524" layer="51" curve="-90"/>
<wire x1="-1.016" y1="0" x2="0" y2="1.016" width="0.1524" layer="51" curve="-90"/>
<wire x1="0" y1="-0.635" x2="0.635" y2="0" width="0.1524" layer="51" curve="90"/>
<wire x1="0.0539" y1="-1.0699" x2="1.0699" y2="-0.0539" width="0.1524" layer="51" curve="90"/>
<circle x="0" y="0" radius="2.667" width="0.1524" layer="21"/>
<circle x="0" y="0" radius="2.413" width="0.254" layer="21"/>
<pad name="K" x="-1.27" y="0" drill="0.8128" shape="long" rot="R90"/>
<pad name="A" x="1.27" y="0" drill="0.8128" shape="long" rot="R90"/>
<text x="-1.27" y="3.048" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-4.318" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="SFH482">
<description>&lt;B&gt;IR LED&lt;/B&gt;&lt;p&gt;
infrared emitting diode, Infineon
TO-18, lead spacing 2.54 mm, cathode marking&lt;p&gt;
Inifineon</description>
<wire x1="-2.159" y1="1.524" x2="-2.794" y2="2.159" width="0.1524" layer="21"/>
<wire x1="-2.794" y1="2.159" x2="-2.159" y2="2.794" width="0.1524" layer="21"/>
<wire x1="-1.524" y1="2.159" x2="-2.159" y2="2.794" width="0.1524" layer="21"/>
<wire x1="0" y1="1.778" x2="1.5358" y2="0.8959" width="0.1524" layer="21" curve="-59.743278"/>
<wire x1="-1.5358" y1="0.8959" x2="0" y2="1.778" width="0.1524" layer="21" curve="-59.743278"/>
<wire x1="-1.5358" y1="-0.8959" x2="0" y2="-1.778" width="0.1524" layer="21" curve="59.743278"/>
<wire x1="0" y1="-1.778" x2="1.5358" y2="-0.8959" width="0.1524" layer="21" curve="59.743278"/>
<wire x1="1.5142" y1="0.9318" x2="1.778" y2="0" width="0.1524" layer="51" curve="-31.606487"/>
<wire x1="1.5" y1="-0.9546" x2="1.778" y2="0" width="0.1524" layer="51" curve="32.472615"/>
<wire x1="-1.778" y1="0" x2="-1.5142" y2="-0.9318" width="0.1524" layer="51" curve="31.606487"/>
<wire x1="-1.778" y1="0" x2="-1.5" y2="0.9546" width="0.1524" layer="51" curve="-32.472615"/>
<wire x1="-0.635" y1="0" x2="0" y2="0.635" width="0.1524" layer="51" curve="-90"/>
<wire x1="-1.016" y1="0" x2="0" y2="1.016" width="0.1524" layer="51" curve="-90"/>
<wire x1="0" y1="-0.635" x2="0.635" y2="0" width="0.1524" layer="51" curve="90"/>
<wire x1="0.0539" y1="-1.0699" x2="1.0699" y2="-0.0539" width="0.1524" layer="51" curve="90"/>
<circle x="0" y="0" radius="2.667" width="0.1524" layer="21"/>
<circle x="0" y="0" radius="2.413" width="0.254" layer="21"/>
<pad name="K" x="1.27" y="0" drill="0.8128" shape="long" rot="R90"/>
<pad name="A" x="-1.27" y="0" drill="0.8128" shape="long" rot="R90"/>
<text x="-1.27" y="3.048" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-4.318" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="U57X32">
<description>&lt;B&gt;LED&lt;/B&gt;&lt;p&gt;
rectangle, 5.7 x 3.2 mm</description>
<wire x1="-3.175" y1="1.905" x2="3.175" y2="1.905" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-1.905" x2="3.175" y2="1.905" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-1.905" x2="-3.175" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="1.905" x2="-3.175" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-2.667" y1="1.397" x2="2.667" y2="1.397" width="0.1524" layer="21"/>
<wire x1="2.667" y1="-1.397" x2="2.667" y2="1.397" width="0.1524" layer="21"/>
<wire x1="2.667" y1="-1.397" x2="-2.667" y2="-1.397" width="0.1524" layer="21"/>
<wire x1="-2.667" y1="1.397" x2="-2.667" y2="-1.397" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="1.016" x2="2.54" y2="1.016" width="0.1524" layer="51"/>
<wire x1="2.286" y1="1.27" x2="2.286" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-2.54" y1="0.508" x2="2.54" y2="0.508" width="0.1524" layer="51"/>
<wire x1="-2.54" y1="0" x2="2.54" y2="0" width="0.1524" layer="51"/>
<wire x1="-2.54" y1="-0.508" x2="2.54" y2="-0.508" width="0.1524" layer="51"/>
<wire x1="-2.54" y1="-1.016" x2="2.54" y2="-1.016" width="0.1524" layer="51"/>
<wire x1="-2.286" y1="1.27" x2="-2.286" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-1.778" y1="1.27" x2="-1.778" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-0.762" y1="1.27" x2="-0.762" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-0.254" y1="1.27" x2="-0.254" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="0.254" y1="1.27" x2="0.254" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="0.762" y1="1.27" x2="0.762" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="1.27" y1="1.27" x2="1.27" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="1.778" y1="1.27" x2="1.778" y2="-1.27" width="0.1524" layer="51"/>
<pad name="A" x="-1.27" y="0" drill="0.8128" shape="long" rot="R90"/>
<pad name="K" x="1.27" y="0" drill="0.8128" shape="long" rot="R90"/>
<text x="3.683" y="0.254" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="3.683" y="-1.524" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="IRL80A">
<description>&lt;B&gt;IR LED&lt;/B&gt;&lt;p&gt;
IR transmitter Siemens</description>
<wire x1="0.889" y1="2.286" x2="0.889" y2="1.778" width="0.1524" layer="21"/>
<wire x1="0.889" y1="1.778" x2="0.889" y2="0.762" width="0.1524" layer="51"/>
<wire x1="0.889" y1="0.762" x2="0.889" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="0.889" y1="-0.635" x2="0.889" y2="-1.778" width="0.1524" layer="51"/>
<wire x1="0.889" y1="-1.778" x2="0.889" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="0.889" y1="-2.286" x2="-0.889" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="-0.889" y1="2.286" x2="-0.889" y2="1.778" width="0.1524" layer="21"/>
<wire x1="-0.889" y1="1.778" x2="-0.889" y2="0.762" width="0.1524" layer="51"/>
<wire x1="-0.889" y1="0.762" x2="-0.889" y2="-0.762" width="0.1524" layer="21"/>
<wire x1="-0.889" y1="-0.762" x2="-0.889" y2="-1.778" width="0.1524" layer="51"/>
<wire x1="-0.889" y1="-1.778" x2="-0.889" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="-0.889" y1="2.286" x2="0.889" y2="2.286" width="0.1524" layer="21"/>
<wire x1="-0.889" y1="-0.762" x2="-0.889" y2="0.762" width="0.1524" layer="21" curve="-180"/>
<wire x1="-1.397" y1="0.254" x2="-1.397" y2="-0.254" width="0.0508" layer="21"/>
<wire x1="-1.143" y1="0.508" x2="-1.143" y2="-0.508" width="0.0508" layer="21"/>
<pad name="K" x="0" y="1.27" drill="0.8128" shape="octagon"/>
<pad name="A" x="0" y="-1.27" drill="0.8128" shape="octagon"/>
<text x="1.27" y="0.381" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="1.27" y="-1.651" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="P-LCC-2">
<description>&lt;b&gt;TOPLED® High-optical Power LED (HOP)&lt;/b&gt;&lt;p&gt;
Source: http://www.osram.convergy.de/ ... ls_t675.pdf</description>
<wire x1="-1.4" y1="-1.05" x2="-1.4" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="-1.4" y1="-1.6" x2="-1.1" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="-1.1" y1="-1.6" x2="-0.85" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="-0.85" y1="-1.6" x2="1.1" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="1.1" y1="-1.6" x2="1.4" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="1.4" y1="-1.6" x2="1.4" y2="1.6" width="0.2032" layer="51"/>
<wire x1="1.4" y1="1.6" x2="1.1" y2="1.6" width="0.2032" layer="51"/>
<wire x1="1.1" y1="1.6" x2="-1.1" y2="1.6" width="0.2032" layer="51"/>
<wire x1="-1.1" y1="1.6" x2="-1.4" y2="1.6" width="0.2032" layer="51"/>
<wire x1="-1.1" y1="1.6" x2="-1.1" y2="1.8" width="0.1016" layer="51"/>
<wire x1="-1.1" y1="1.8" x2="1.1" y2="1.8" width="0.1016" layer="51"/>
<wire x1="1.1" y1="1.8" x2="1.1" y2="1.6" width="0.1016" layer="51"/>
<wire x1="-1.1" y1="-1.6" x2="-1.1" y2="-1.8" width="0.1016" layer="51"/>
<wire x1="-1.1" y1="-1.8" x2="1.1" y2="-1.8" width="0.1016" layer="51"/>
<wire x1="1.1" y1="-1.8" x2="1.1" y2="-1.6" width="0.1016" layer="51"/>
<wire x1="-0.85" y1="-1.6" x2="-1.4" y2="-1.05" width="0.2032" layer="51"/>
<wire x1="-1.4" y1="1.6" x2="-1.4" y2="-1.05" width="0.2032" layer="51"/>
<circle x="0" y="0" radius="1.1" width="0.2032" layer="51"/>
<smd name="C" x="0" y="-2.75" dx="4" dy="4" layer="1" stop="no" cream="no"/>
<smd name="A" x="0" y="2.75" dx="4" dy="4" layer="1" stop="no" cream="no"/>
<text x="-2.54" y="-1.905" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="3.81" y="-1.905" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<text x="-0.635" y="2.54" size="1.27" layer="21">A</text>
<text x="-0.635" y="-3.81" size="1.27" layer="21">C</text>
<rectangle x1="-1.3" y1="-2.25" x2="1.3" y2="-0.75" layer="31"/>
<rectangle x1="-1.3" y1="0.75" x2="1.3" y2="2.25" layer="31"/>
<rectangle x1="-0.25" y1="-0.25" x2="0.25" y2="0.25" layer="21"/>
<rectangle x1="-1.4" y1="0.65" x2="1.4" y2="2.35" layer="29"/>
<rectangle x1="-1.4" y1="-2.35" x2="1.4" y2="-0.65" layer="29"/>
</package>
<package name="OSRAM-MINI-TOP-LED">
<description>&lt;b&gt;BLUE LINETM Hyper Mini TOPLED® Hyper-Bright LED&lt;/b&gt;&lt;p&gt;
Source: http://www.osram.convergy.de/ ... LB M676.pdf</description>
<wire x1="-0.6" y1="0.9" x2="-0.6" y2="-0.7" width="0.1016" layer="51"/>
<wire x1="-0.45" y1="-0.9" x2="-0.4" y2="-0.9" width="0.1016" layer="51"/>
<wire x1="-0.4" y1="-0.9" x2="0.6" y2="-0.9" width="0.1016" layer="51"/>
<wire x1="0.6" y1="-0.9" x2="0.6" y2="0.9" width="0.1016" layer="51"/>
<wire x1="0.6" y1="0.9" x2="-0.6" y2="0.9" width="0.1016" layer="51"/>
<wire x1="-0.45" y1="0.95" x2="-0.45" y2="1.1" width="0.1016" layer="51"/>
<wire x1="-0.45" y1="1.1" x2="0.45" y2="1.1" width="0.1016" layer="51"/>
<wire x1="0.45" y1="1.1" x2="0.45" y2="0.95" width="0.1016" layer="51"/>
<wire x1="-0.6" y1="-0.7" x2="-0.4" y2="-0.9" width="0.1016" layer="51"/>
<wire x1="-0.45" y1="-0.9" x2="-0.45" y2="-1.1" width="0.1016" layer="51"/>
<wire x1="-0.45" y1="-1.1" x2="0.45" y2="-1.1" width="0.1016" layer="51"/>
<wire x1="0.45" y1="-1.1" x2="0.45" y2="-0.95" width="0.1016" layer="51"/>
<smd name="A" x="0" y="2.6" dx="4" dy="4" layer="1" stop="no" cream="no"/>
<smd name="C" x="0" y="-2.6" dx="4" dy="4" layer="1" stop="no" cream="no"/>
<text x="-0.635" y="1.905" size="1.27" layer="21">A</text>
<text x="-0.635" y="-3.175" size="1.27" layer="21">C</text>
<text x="-2.54" y="-1.905" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="3.81" y="-1.905" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-0.5" y1="0.6" x2="0.5" y2="1.4" layer="29"/>
<rectangle x1="-0.5" y1="-1.4" x2="0.5" y2="-0.6" layer="29"/>
<rectangle x1="-0.15" y1="-0.6" x2="0.15" y2="-0.3" layer="51"/>
<rectangle x1="-0.45" y1="0.65" x2="0.45" y2="1.35" layer="31"/>
<rectangle x1="-0.45" y1="-1.35" x2="0.45" y2="-0.65" layer="31"/>
</package>
<package name="OSRAM-SIDELED">
<description>&lt;b&gt;Super SIDELED® High-Current LED&lt;/b&gt;&lt;p&gt;
LG A672, LP A672 &lt;br&gt;
Source: http://www.osram.convergy.de/ ... LG_LP_A672.pdf (2004.05.13)</description>
<wire x1="-1.85" y1="-2.05" x2="-1.85" y2="-0.75" width="0.1016" layer="51"/>
<wire x1="-1.85" y1="-0.75" x2="-1.7" y2="-0.75" width="0.1016" layer="51"/>
<wire x1="-1.7" y1="-0.75" x2="-1.7" y2="0.75" width="0.1016" layer="51"/>
<wire x1="-1.7" y1="0.75" x2="-1.85" y2="0.75" width="0.1016" layer="51"/>
<wire x1="-1.85" y1="0.75" x2="-1.85" y2="2.05" width="0.1016" layer="51"/>
<wire x1="-1.85" y1="2.05" x2="0.9" y2="2.05" width="0.1016" layer="51"/>
<wire x1="0.9" y1="2.05" x2="0.9" y2="-2.05" width="0.1016" layer="51"/>
<wire x1="0.9" y1="-2.05" x2="-1.85" y2="-2.05" width="0.1016" layer="51"/>
<wire x1="0.9" y1="-2.05" x2="1.05" y2="-2.05" width="0.1016" layer="51"/>
<wire x1="1.05" y1="-2.05" x2="1.85" y2="-1.85" width="0.1016" layer="51"/>
<wire x1="1.85" y1="-1.85" x2="1.85" y2="1.85" width="0.1016" layer="51"/>
<wire x1="1.85" y1="1.85" x2="1.05" y2="2.05" width="0.1016" layer="51"/>
<wire x1="1.05" y1="2.05" x2="0.9" y2="2.05" width="0.1016" layer="51"/>
<wire x1="1.05" y1="2.05" x2="1.05" y2="-2.05" width="0.1016" layer="51"/>
<wire x1="-0.55" y1="-0.9" x2="-0.55" y2="0.9" width="0.1016" layer="51" curve="-167.319617"/>
<wire x1="-0.55" y1="-0.9" x2="0.85" y2="-1.2" width="0.1016" layer="51" style="shortdash"/>
<wire x1="-0.55" y1="0.9" x2="0.85" y2="1.2" width="0.1016" layer="51" style="shortdash"/>
<smd name="C" x="0" y="-2.5" dx="4" dy="4" layer="1" stop="no" cream="no"/>
<smd name="A" x="0" y="2.5" dx="4" dy="4" layer="1" stop="no" cream="no"/>
<text x="0.635" y="-3.175" size="1.27" layer="21" rot="R90">C</text>
<text x="0.635" y="2.54" size="1.27" layer="21" rot="R90">A</text>
<text x="-2.54" y="-2.54" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="3.81" y="-2.54" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-2.1" y1="-2.2" x2="2.1" y2="-0.4" layer="29"/>
<rectangle x1="-2.1" y1="0.4" x2="2.1" y2="2.2" layer="29"/>
<rectangle x1="-1.9" y1="-2.1" x2="1.9" y2="-0.6" layer="31"/>
<rectangle x1="-1.9" y1="0.6" x2="1.9" y2="2.1" layer="31"/>
<rectangle x1="-1.85" y1="-2.05" x2="-0.7" y2="-1" layer="51"/>
</package>
<package name="SMART-LED">
<description>&lt;b&gt;SmartLEDTM Hyper-Bright LED&lt;/b&gt;&lt;p&gt;
Source: http://www.osram.convergy.de/ ... LA_LO_LS_LY L896.pdf</description>
<wire x1="-0.35" y1="0.6" x2="0.35" y2="0.6" width="0.1016" layer="51" style="shortdash"/>
<wire x1="0.35" y1="0.6" x2="0.35" y2="-0.6" width="0.1016" layer="21" style="shortdash"/>
<wire x1="0.35" y1="-0.6" x2="0.15" y2="-0.6" width="0.1016" layer="51" style="shortdash"/>
<wire x1="0.15" y1="-0.6" x2="-0.35" y2="-0.6" width="0.1016" layer="51" style="shortdash"/>
<wire x1="-0.35" y1="-0.6" x2="-0.35" y2="0.6" width="0.1016" layer="21" style="shortdash"/>
<wire x1="0.35" y1="-0.4" x2="0.15" y2="-0.6" width="0.1016" layer="51" style="shortdash"/>
<smd name="A" x="0" y="0.725" dx="0.35" dy="0.35" layer="1"/>
<smd name="B" x="0" y="-0.725" dx="0.35" dy="0.35" layer="1"/>
<text x="-0.635" y="-0.635" size="1.016" layer="25" rot="R90">&gt;NAME</text>
<text x="1.905" y="-0.635" size="1.016" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-0.15" y1="-0.35" x2="0.15" y2="-0.05" layer="21"/>
<rectangle x1="-0.15" y1="0.6" x2="0.15" y2="0.85" layer="51"/>
<rectangle x1="-0.15" y1="-0.85" x2="0.15" y2="-0.6" layer="51"/>
</package>
<package name="P-LCC-2-TOPLED-RG">
<description>&lt;b&gt;Hyper TOPLED® RG Hyper-Bright LED&lt;/b&gt;&lt;p&gt;
Source: http://www.osram.convergy.de/ ... LA_LO_LS_LY T776.pdf</description>
<wire x1="-1.4" y1="-1.05" x2="-1.4" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="-1.4" y1="-1.6" x2="-1.1" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="-1.1" y1="-1.6" x2="-0.85" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="-0.85" y1="-1.6" x2="1.1" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="1.1" y1="-1.6" x2="1.4" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="1.4" y1="-1.6" x2="1.4" y2="1.6" width="0.2032" layer="51"/>
<wire x1="1.4" y1="1.6" x2="1.1" y2="1.6" width="0.2032" layer="51"/>
<wire x1="1.1" y1="1.6" x2="-1.1" y2="1.6" width="0.2032" layer="51"/>
<wire x1="-1.1" y1="1.6" x2="-1.4" y2="1.6" width="0.2032" layer="51"/>
<wire x1="-1.1" y1="1.6" x2="-1.1" y2="2.45" width="0.1016" layer="51"/>
<wire x1="1.1" y1="2.45" x2="1.1" y2="1.6" width="0.1016" layer="51"/>
<wire x1="-1.1" y1="-1.6" x2="-1.1" y2="-2.45" width="0.1016" layer="51"/>
<wire x1="1.1" y1="-2.45" x2="1.1" y2="-1.6" width="0.1016" layer="51"/>
<wire x1="-0.85" y1="-1.6" x2="-1.4" y2="-1.05" width="0.2032" layer="51"/>
<wire x1="-1.4" y1="1.6" x2="-1.4" y2="-1.05" width="0.2032" layer="51"/>
<circle x="0" y="0" radius="1.1" width="0.2032" layer="21"/>
<smd name="C" x="0" y="-3.5" dx="4" dy="4" layer="1" stop="no" cream="no"/>
<smd name="A" x="0" y="3.5" dx="4" dy="4" layer="1" stop="no" cream="no"/>
<text x="-2.54" y="-1.905" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="3.81" y="-1.905" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<text x="-0.635" y="3.29" size="1.27" layer="21">A</text>
<text x="-0.635" y="-4.56" size="1.27" layer="21">C</text>
<rectangle x1="-1.3" y1="-3" x2="1.3" y2="-1.5" layer="31"/>
<rectangle x1="-1.3" y1="1.5" x2="1.3" y2="3" layer="31"/>
<rectangle x1="-0.25" y1="-0.25" x2="0.25" y2="0.25" layer="21"/>
<rectangle x1="-1.15" y1="2.4" x2="1.15" y2="2.7" layer="51"/>
<rectangle x1="-1.15" y1="-2.7" x2="1.15" y2="-2.4" layer="51"/>
<rectangle x1="-1.5" y1="1.5" x2="1.5" y2="3.2" layer="29"/>
<rectangle x1="-1.5" y1="-3.2" x2="1.5" y2="-1.5" layer="29"/>
<hole x="0" y="0" drill="2.8"/>
</package>
<package name="MICRO-SIDELED">
<description>&lt;b&gt;Hyper Micro SIDELED®&lt;/b&gt;&lt;p&gt;
Source: http://www.osram.convergy.de/ ... LA_LO_LS_LY Y876.pdf</description>
<wire x1="0.65" y1="1.1" x2="-0.1" y2="1.1" width="0.1016" layer="51"/>
<wire x1="-0.1" y1="1.1" x2="-0.35" y2="1" width="0.1016" layer="51"/>
<wire x1="-0.35" y1="1" x2="-0.35" y2="-0.9" width="0.1016" layer="21"/>
<wire x1="-0.35" y1="-0.9" x2="-0.1" y2="-1.1" width="0.1016" layer="51"/>
<wire x1="-0.1" y1="-1.1" x2="0.65" y2="-1.1" width="0.1016" layer="51"/>
<wire x1="0.65" y1="-1.1" x2="0.65" y2="1.1" width="0.1016" layer="21"/>
<wire x1="0.6" y1="0.9" x2="0.25" y2="0.7" width="0.0508" layer="21"/>
<wire x1="0.25" y1="0.7" x2="0.25" y2="-0.7" width="0.0508" layer="21"/>
<wire x1="0.25" y1="-0.7" x2="0.6" y2="-0.9" width="0.0508" layer="21"/>
<smd name="A" x="0" y="1.95" dx="1.6" dy="1.6" layer="1" stop="no" cream="no"/>
<smd name="C" x="0" y="-1.95" dx="1.6" dy="1.6" layer="1" stop="no" cream="no"/>
<text x="-1.27" y="-1.905" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="2.54" y="-1.905" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-0.4" y1="1.1" x2="0.4" y2="1.8" layer="29"/>
<rectangle x1="-0.4" y1="-1.8" x2="0.4" y2="-1.1" layer="29"/>
<rectangle x1="-0.35" y1="-1.75" x2="0.35" y2="-1.15" layer="31"/>
<rectangle x1="-0.35" y1="1.15" x2="0.35" y2="1.75" layer="31"/>
<rectangle x1="-0.125" y1="1.125" x2="0.125" y2="1.75" layer="51"/>
<rectangle x1="-0.125" y1="-1.75" x2="0.125" y2="-1.125" layer="51"/>
</package>
<package name="P-LCC-4">
<description>&lt;b&gt;Power TOPLED®&lt;/b&gt;&lt;p&gt;
Source: http://www.osram.convergy.de/ ... LA_LO_LA_LY E67B.pdf</description>
<wire x1="-1.4" y1="-1.05" x2="-1.4" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="-1.4" y1="-1.6" x2="-1" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="-1" y1="-1.6" x2="-0.85" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="-0.85" y1="-1.6" x2="1" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="1" y1="-1.6" x2="1.4" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="1.4" y1="-1.6" x2="1.4" y2="1.6" width="0.2032" layer="51"/>
<wire x1="1.4" y1="1.6" x2="1.1" y2="1.6" width="0.2032" layer="51"/>
<wire x1="1.1" y1="1.6" x2="-1" y2="1.6" width="0.2032" layer="51"/>
<wire x1="-1" y1="1.6" x2="-1.4" y2="1.6" width="0.2032" layer="51"/>
<wire x1="-1" y1="1.6" x2="-1" y2="1.8" width="0.1016" layer="51"/>
<wire x1="-1" y1="1.8" x2="-0.5" y2="1.8" width="0.1016" layer="51"/>
<wire x1="-0.5" y1="1.8" x2="-0.5" y2="1.65" width="0.1016" layer="51"/>
<wire x1="0.5" y1="1.65" x2="0.5" y2="1.8" width="0.1016" layer="51"/>
<wire x1="0.5" y1="1.8" x2="1.1" y2="1.8" width="0.1016" layer="51"/>
<wire x1="1.1" y1="1.8" x2="1.1" y2="1.6" width="0.1016" layer="51"/>
<wire x1="-1" y1="-1.6" x2="-1" y2="-1.8" width="0.1016" layer="51"/>
<wire x1="-1" y1="-1.8" x2="-0.5" y2="-1.8" width="0.1016" layer="51"/>
<wire x1="-0.5" y1="-1.8" x2="-0.5" y2="-1.65" width="0.1016" layer="51"/>
<wire x1="0.5" y1="-1.65" x2="0.5" y2="-1.8" width="0.1016" layer="51"/>
<wire x1="0.5" y1="-1.8" x2="1" y2="-1.8" width="0.1016" layer="51"/>
<wire x1="1" y1="-1.8" x2="1" y2="-1.6" width="0.1016" layer="51"/>
<wire x1="-0.85" y1="-1.6" x2="-1.4" y2="-1.05" width="0.2032" layer="51"/>
<wire x1="-1.4" y1="1.6" x2="-1.4" y2="-1.05" width="0.2032" layer="51"/>
<circle x="0" y="0" radius="1.1" width="0.2032" layer="51"/>
<smd name="A" x="-2" y="3.15" dx="3.3" dy="4.8" layer="1" stop="no" cream="no"/>
<smd name="C@3" x="2" y="3.15" dx="3.3" dy="4.8" layer="1" stop="no" cream="no"/>
<smd name="C@4" x="2" y="-3.15" dx="3.3" dy="4.8" layer="1" stop="no" cream="no"/>
<smd name="C@1" x="-2" y="-3.15" dx="3.3" dy="4.8" layer="1" stop="no" cream="no"/>
<text x="-3.81" y="-2.54" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="5.08" y="-2.54" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<text x="-1.905" y="-3.81" size="1.27" layer="21">C</text>
<text x="-1.905" y="2.54" size="1.27" layer="21">A</text>
<text x="1.27" y="2.54" size="1.27" layer="21">C</text>
<text x="1.27" y="-3.81" size="1.27" layer="21">C</text>
<rectangle x1="-1.15" y1="0.75" x2="-0.35" y2="1.85" layer="29"/>
<rectangle x1="0.35" y1="0.75" x2="1.15" y2="1.85" layer="29"/>
<rectangle x1="0.35" y1="-1.85" x2="1.15" y2="-0.75" layer="29"/>
<rectangle x1="-1.15" y1="-1.85" x2="-0.35" y2="-0.75" layer="29"/>
<rectangle x1="-1.1" y1="-1.8" x2="-0.4" y2="-0.8" layer="31"/>
<rectangle x1="0.4" y1="-1.8" x2="1.1" y2="-0.8" layer="31"/>
<rectangle x1="0.4" y1="0.8" x2="1.1" y2="1.8" layer="31"/>
<rectangle x1="-1.1" y1="0.8" x2="-0.4" y2="1.8" layer="31"/>
<rectangle x1="-0.2" y1="-0.2" x2="0.2" y2="0.2" layer="21"/>
</package>
<package name="CHIP-LED0603">
<description>&lt;b&gt;Hyper CHIPLED Hyper-Bright LED&lt;/b&gt;&lt;p&gt;
LB Q993&lt;br&gt;
Source: http://www.osram.convergy.de/ ... Lb_q993.pdf</description>
<wire x1="-0.4" y1="0.45" x2="-0.4" y2="-0.45" width="0.1016" layer="51"/>
<wire x1="0.4" y1="0.45" x2="0.4" y2="-0.45" width="0.1016" layer="51"/>
<smd name="C" x="0" y="0.75" dx="0.8" dy="0.8" layer="1"/>
<smd name="A" x="0" y="-0.75" dx="0.8" dy="0.8" layer="1"/>
<text x="-0.635" y="-0.635" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="1.905" y="-0.635" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-0.45" y1="0.45" x2="0.45" y2="0.85" layer="51"/>
<rectangle x1="-0.45" y1="-0.85" x2="0.45" y2="-0.45" layer="51"/>
<rectangle x1="-0.45" y1="0" x2="-0.3" y2="0.3" layer="21"/>
<rectangle x1="0.3" y1="0" x2="0.45" y2="0.3" layer="21"/>
<rectangle x1="-0.15" y1="0" x2="0.15" y2="0.3" layer="21"/>
</package>
<package name="CHIP-LED0805">
<description>&lt;b&gt;Hyper CHIPLED Hyper-Bright LED&lt;/b&gt;&lt;p&gt;
LB R99A&lt;br&gt;
Source: http://www.osram.convergy.de/ ... lb_r99a.pdf</description>
<wire x1="-0.625" y1="0.45" x2="-0.625" y2="-0.45" width="0.1016" layer="51"/>
<wire x1="0.625" y1="0.45" x2="0.625" y2="-0.475" width="0.1016" layer="51"/>
<smd name="C" x="0" y="1.05" dx="1.2" dy="1.2" layer="1"/>
<smd name="A" x="0" y="-1.05" dx="1.2" dy="1.2" layer="1"/>
<text x="-1.27" y="-1.27" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="2.54" y="-1.27" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-0.675" y1="0" x2="-0.525" y2="0.3" layer="21"/>
<rectangle x1="0.525" y1="0" x2="0.675" y2="0.3" layer="21"/>
<rectangle x1="-0.15" y1="0" x2="0.15" y2="0.3" layer="21"/>
<rectangle x1="-0.675" y1="0.45" x2="0.675" y2="1.05" layer="51"/>
<rectangle x1="-0.675" y1="-1.05" x2="0.675" y2="-0.45" layer="51"/>
</package>
<package name="MINI-TOPLED-SANTANA">
<description>&lt;b&gt;Mini TOPLED Santana®&lt;/b&gt;&lt;p&gt;
Source: http://www.osram.convergy.de/ ... LG M470.pdf</description>
<wire x1="0.7" y1="-1" x2="0.35" y2="-1" width="0.1016" layer="21"/>
<wire x1="0.35" y1="-1" x2="-0.7" y2="-1" width="0.1016" layer="21"/>
<wire x1="-0.7" y1="-1" x2="-0.7" y2="1" width="0.1016" layer="21"/>
<wire x1="-0.7" y1="1" x2="0.7" y2="1" width="0.1016" layer="21"/>
<wire x1="0.7" y1="1" x2="0.7" y2="-0.65" width="0.1016" layer="21"/>
<wire x1="0.7" y1="-0.65" x2="0.7" y2="-1" width="0.1016" layer="21"/>
<wire x1="0.45" y1="-0.7" x2="-0.45" y2="-0.7" width="0.1016" layer="21"/>
<wire x1="-0.45" y1="-0.7" x2="-0.45" y2="0.7" width="0.1016" layer="21"/>
<wire x1="-0.45" y1="0.7" x2="0.45" y2="0.7" width="0.1016" layer="21"/>
<wire x1="0.45" y1="0.7" x2="0.45" y2="-0.7" width="0.1016" layer="21"/>
<wire x1="0.7" y1="-0.65" x2="0.35" y2="-1" width="0.1016" layer="21"/>
<smd name="C" x="0" y="-2.2" dx="1.6" dy="1.6" layer="1" stop="no" cream="no"/>
<smd name="A" x="0" y="2.2" dx="1.6" dy="1.6" layer="1" stop="no" cream="no"/>
<text x="-1.27" y="-1.905" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="2.54" y="-1.905" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-0.55" y1="1.5" x2="0.55" y2="2.1" layer="29"/>
<rectangle x1="-0.55" y1="-2.1" x2="0.55" y2="-1.5" layer="29"/>
<rectangle x1="-0.5" y1="-2.05" x2="0.5" y2="-1.55" layer="31"/>
<rectangle x1="-0.5" y1="1.55" x2="0.5" y2="2.05" layer="31"/>
<rectangle x1="-0.2" y1="-0.4" x2="0.15" y2="-0.05" layer="21"/>
<rectangle x1="-0.5" y1="-2.1" x2="0.5" y2="-1.4" layer="51"/>
<rectangle x1="-0.5" y1="1.4" x2="0.5" y2="2.05" layer="51"/>
<rectangle x1="-0.5" y1="1" x2="0.5" y2="1.4" layer="21"/>
<rectangle x1="-0.5" y1="-1.4" x2="0.5" y2="-1.05" layer="21"/>
<hole x="0" y="0" drill="2.7"/>
</package>
<package name="CHIPLED_0805">
<description>&lt;b&gt;CHIPLED&lt;/b&gt;&lt;p&gt;
Source: http://www.osram.convergy.de/ ... LG_R971.pdf</description>
<wire x1="-0.35" y1="0.925" x2="0.35" y2="0.925" width="0.1016" layer="51" curve="162.394521"/>
<wire x1="-0.35" y1="-0.925" x2="0.35" y2="-0.925" width="0.1016" layer="51" curve="-162.394521"/>
<wire x1="0.575" y1="0.525" x2="0.575" y2="-0.525" width="0.1016" layer="51"/>
<wire x1="-0.575" y1="-0.5" x2="-0.575" y2="0.925" width="0.1016" layer="51"/>
<circle x="-0.45" y="0.85" radius="0.103" width="0.1016" layer="51"/>
<smd name="C" x="0" y="1.05" dx="1.2" dy="1.2" layer="1"/>
<smd name="A" x="0" y="-1.05" dx="1.2" dy="1.2" layer="1"/>
<text x="-1.27" y="-1.27" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="2.54" y="-1.27" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="0.3" y1="0.5" x2="0.625" y2="1" layer="51"/>
<rectangle x1="-0.325" y1="0.5" x2="-0.175" y2="0.75" layer="51"/>
<rectangle x1="0.175" y1="0.5" x2="0.325" y2="0.75" layer="51"/>
<rectangle x1="-0.2" y1="0.5" x2="0.2" y2="0.675" layer="51"/>
<rectangle x1="0.3" y1="-1" x2="0.625" y2="-0.5" layer="51"/>
<rectangle x1="-0.625" y1="-1" x2="-0.3" y2="-0.5" layer="51"/>
<rectangle x1="0.175" y1="-0.75" x2="0.325" y2="-0.5" layer="51"/>
<rectangle x1="-0.325" y1="-0.75" x2="-0.175" y2="-0.5" layer="51"/>
<rectangle x1="-0.2" y1="-0.675" x2="0.2" y2="-0.5" layer="51"/>
<rectangle x1="-0.1" y1="0" x2="0.1" y2="0.2" layer="21"/>
<rectangle x1="-0.6" y1="0.5" x2="-0.3" y2="0.8" layer="51"/>
<rectangle x1="-0.625" y1="0.925" x2="-0.3" y2="1" layer="51"/>
</package>
<package name="CHIPLED_1206">
<description>&lt;b&gt;CHIPLED&lt;/b&gt;&lt;p&gt;
Source: http://www.osram.convergy.de/ ... LG_LY N971.pdf</description>
<wire x1="-0.4" y1="1.6" x2="0.4" y2="1.6" width="0.1016" layer="51" curve="172.619069"/>
<wire x1="-0.8" y1="-0.95" x2="-0.8" y2="0.95" width="0.1016" layer="51"/>
<wire x1="0.8" y1="0.95" x2="0.8" y2="-0.95" width="0.1016" layer="51"/>
<circle x="-0.55" y="1.425" radius="0.1" width="0.1016" layer="51"/>
<smd name="C" x="0" y="1.75" dx="1.5" dy="1.5" layer="1"/>
<smd name="A" x="0" y="-1.75" dx="1.5" dy="1.5" layer="1"/>
<text x="-1.27" y="-1.27" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="2.54" y="-1.27" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-0.85" y1="1.525" x2="-0.35" y2="1.65" layer="51"/>
<rectangle x1="-0.85" y1="1.225" x2="-0.625" y2="1.55" layer="51"/>
<rectangle x1="-0.45" y1="1.225" x2="-0.325" y2="1.45" layer="51"/>
<rectangle x1="-0.65" y1="1.225" x2="-0.225" y2="1.35" layer="51"/>
<rectangle x1="0.35" y1="1.3" x2="0.85" y2="1.65" layer="51"/>
<rectangle x1="0.25" y1="1.225" x2="0.85" y2="1.35" layer="51"/>
<rectangle x1="-0.85" y1="0.95" x2="0.85" y2="1.25" layer="51"/>
<rectangle x1="-0.85" y1="-1.65" x2="0.85" y2="-0.95" layer="51"/>
<rectangle x1="-0.85" y1="0.35" x2="-0.525" y2="0.775" layer="21"/>
<rectangle x1="0.525" y1="0.35" x2="0.85" y2="0.775" layer="21"/>
<rectangle x1="-0.175" y1="0" x2="0.175" y2="0.35" layer="21"/>
</package>
<package name="CHIPLED_0603">
<description>&lt;b&gt;CHIPLED&lt;/b&gt;&lt;p&gt;
Source: http://www.osram.convergy.de/ ... LG_LY Q971.pdf</description>
<wire x1="-0.3" y1="0.8" x2="0.3" y2="0.8" width="0.1016" layer="51" curve="170.055574"/>
<wire x1="-0.275" y1="-0.825" x2="0.275" y2="-0.825" width="0.0508" layer="51" curve="-180"/>
<wire x1="-0.4" y1="0.375" x2="-0.4" y2="-0.35" width="0.1016" layer="51"/>
<wire x1="0.4" y1="0.35" x2="0.4" y2="-0.35" width="0.1016" layer="51"/>
<circle x="-0.35" y="0.625" radius="0.075" width="0.0508" layer="51"/>
<smd name="C" x="0" y="0.75" dx="0.8" dy="0.8" layer="1"/>
<smd name="A" x="0" y="-0.75" dx="0.8" dy="0.8" layer="1"/>
<text x="-0.635" y="-1.27" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="1.905" y="-1.27" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-0.45" y1="0.7" x2="-0.25" y2="0.85" layer="51"/>
<rectangle x1="-0.275" y1="0.55" x2="-0.225" y2="0.6" layer="51"/>
<rectangle x1="-0.45" y1="0.35" x2="-0.4" y2="0.725" layer="51"/>
<rectangle x1="0.25" y1="0.55" x2="0.45" y2="0.85" layer="51"/>
<rectangle x1="-0.45" y1="0.35" x2="0.45" y2="0.575" layer="51"/>
<rectangle x1="-0.45" y1="-0.85" x2="-0.25" y2="-0.35" layer="51"/>
<rectangle x1="0.25" y1="-0.85" x2="0.45" y2="-0.35" layer="51"/>
<rectangle x1="-0.275" y1="-0.575" x2="0.275" y2="-0.35" layer="51"/>
<rectangle x1="-0.275" y1="-0.65" x2="-0.175" y2="-0.55" layer="51"/>
<rectangle x1="0.175" y1="-0.65" x2="0.275" y2="-0.55" layer="51"/>
<rectangle x1="-0.125" y1="0" x2="0.125" y2="0.25" layer="21"/>
</package>
<package name="CHIPLED-0603-TTW">
<description>&lt;b&gt;CHIPLED-0603&lt;/b&gt;&lt;p&gt;
Recommended Solder Pad useable for SmartLEDTM and Chipled - Package 0603&lt;br&gt;
Package able to withstand TTW-soldering heat&lt;br&gt;
Package suitable for TTW-soldering&lt;br&gt;
Source: http://www.osram.convergy.de/ ... LO_LS_LY L89K.pdf</description>
<wire x1="-0.3" y1="0.8" x2="0.3" y2="0.8" width="0.1016" layer="51" curve="170.055574"/>
<wire x1="-0.275" y1="-0.825" x2="0.275" y2="-0.825" width="0.0508" layer="51" curve="-180"/>
<wire x1="-0.4" y1="0.375" x2="-0.4" y2="-0.35" width="0.1016" layer="51"/>
<wire x1="0.4" y1="0.35" x2="0.4" y2="-0.35" width="0.1016" layer="51"/>
<circle x="-0.35" y="0.625" radius="0.075" width="0.0508" layer="51"/>
<smd name="C" x="0" y="0.875" dx="0.8" dy="0.5" layer="1" stop="no" cream="no"/>
<smd name="A" x="0" y="-0.875" dx="0.8" dy="0.5" layer="1" stop="no" cream="no"/>
<smd name="A@1" x="0" y="-0.5" dx="0.35" dy="0.35" layer="1"/>
<smd name="C@1" x="0" y="0.5" dx="0.35" dy="0.35" layer="1"/>
<text x="-0.635" y="-1.27" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="1.905" y="-1.27" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-0.45" y1="0.7" x2="-0.25" y2="0.85" layer="51"/>
<rectangle x1="-0.275" y1="0.55" x2="-0.225" y2="0.6" layer="51"/>
<rectangle x1="-0.45" y1="0.35" x2="-0.4" y2="0.725" layer="51"/>
<rectangle x1="0.25" y1="0.55" x2="0.45" y2="0.85" layer="51"/>
<rectangle x1="-0.45" y1="0.35" x2="0.45" y2="0.575" layer="51"/>
<rectangle x1="-0.45" y1="-0.85" x2="-0.25" y2="-0.35" layer="51"/>
<rectangle x1="0.25" y1="-0.85" x2="0.45" y2="-0.35" layer="51"/>
<rectangle x1="-0.275" y1="-0.575" x2="0.275" y2="-0.35" layer="51"/>
<rectangle x1="-0.275" y1="-0.65" x2="-0.175" y2="-0.55" layer="51"/>
<rectangle x1="0.175" y1="-0.65" x2="0.275" y2="-0.55" layer="51"/>
<rectangle x1="-0.125" y1="0" x2="0.125" y2="0.25" layer="21"/>
<rectangle x1="-0.175" y1="0.325" x2="0.175" y2="0.7" layer="29"/>
<rectangle x1="-0.4" y1="0.625" x2="0.4" y2="1.125" layer="29"/>
<rectangle x1="-0.4" y1="-1.125" x2="0.4" y2="-0.625" layer="29"/>
<rectangle x1="-0.175" y1="-0.675" x2="0.175" y2="-0.325" layer="29"/>
</package>
<package name="SMARTLED-TTW">
<description>&lt;b&gt;SmartLED TTW&lt;/b&gt;&lt;p&gt;
Recommended Solder Pad useable for SmartLEDTM and Chipled - Package 0603&lt;br&gt;
Package able to withstand TTW-soldering heat&lt;br&gt;
Package suitable for TTW-soldering&lt;br&gt;
Source: http://www.osram.convergy.de/ ... LO_LS_LY L89K.pdf</description>
<wire x1="-0.35" y1="0.6" x2="0.35" y2="0.6" width="0.1016" layer="51" style="shortdash"/>
<wire x1="0.35" y1="0.6" x2="0.35" y2="-0.6" width="0.1016" layer="21" style="shortdash"/>
<wire x1="0.35" y1="-0.6" x2="0.15" y2="-0.6" width="0.1016" layer="51" style="shortdash"/>
<wire x1="0.15" y1="-0.6" x2="-0.35" y2="-0.6" width="0.1016" layer="51" style="shortdash"/>
<wire x1="-0.35" y1="-0.6" x2="-0.35" y2="0.6" width="0.1016" layer="21" style="shortdash"/>
<wire x1="0.35" y1="-0.4" x2="0.15" y2="-0.6" width="0.1016" layer="51" style="shortdash"/>
<smd name="C" x="0" y="0.875" dx="0.8" dy="0.5" layer="1" stop="no" cream="no"/>
<smd name="A" x="0" y="-0.875" dx="0.8" dy="0.5" layer="1" stop="no" cream="no"/>
<smd name="A@1" x="0" y="-0.5" dx="0.35" dy="0.35" layer="1" stop="no" cream="no"/>
<smd name="C@1" x="0" y="0.5" dx="0.35" dy="0.35" layer="1" stop="no" cream="no"/>
<text x="-0.635" y="-1.27" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="1.905" y="-1.27" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-0.175" y1="0.325" x2="0.175" y2="0.7" layer="29"/>
<rectangle x1="-0.15" y1="-0.35" x2="0.15" y2="-0.05" layer="21"/>
<rectangle x1="-0.15" y1="0.6" x2="0.15" y2="0.85" layer="51"/>
<rectangle x1="-0.15" y1="-0.85" x2="0.15" y2="-0.6" layer="51"/>
<rectangle x1="-0.225" y1="0.3" x2="0.225" y2="0.975" layer="31"/>
<rectangle x1="-0.175" y1="-0.7" x2="0.175" y2="-0.325" layer="29" rot="R180"/>
<rectangle x1="-0.225" y1="-0.975" x2="0.225" y2="-0.3" layer="31" rot="R180"/>
</package>
<package name="LUMILED+">
<description>&lt;b&gt;Lumileds Lighting. LUXEON®&lt;/b&gt; with cool pad&lt;p&gt;
Source: K2.pdf</description>
<wire x1="-3.575" y1="2.3375" x2="-2.3375" y2="3.575" width="0.2032" layer="21"/>
<wire x1="-2.3375" y1="3.575" x2="2.3375" y2="3.575" width="0.2032" layer="21"/>
<wire x1="3.575" y1="2.3375" x2="3.575" y2="-3.575" width="0.2032" layer="21"/>
<wire x1="3.575" y1="-3.575" x2="-2.3375" y2="-3.575" width="0.2032" layer="21"/>
<wire x1="-2.3375" y1="-3.575" x2="-2.5" y2="-3.4125" width="0.2032" layer="21"/>
<wire x1="-2.5" y1="-3.4125" x2="-3.4125" y2="-2.5" width="0.2032" layer="21" curve="167.429893"/>
<wire x1="-3.4125" y1="-2.5" x2="-3.575" y2="-2.3375" width="0.2032" layer="21"/>
<wire x1="-3.575" y1="-2.3375" x2="-3.575" y2="2.3375" width="0.2032" layer="21"/>
<wire x1="2.3375" y1="3.575" x2="2.5" y2="3.4125" width="0.2032" layer="21"/>
<wire x1="2.5" y1="3.4125" x2="3.4125" y2="2.5" width="0.2032" layer="21" curve="167.429893"/>
<wire x1="3.4125" y1="2.5" x2="3.575" y2="2.3375" width="0.2032" layer="21"/>
<wire x1="-1.725" y1="2.225" x2="-1.0625" y2="2.5625" width="0.2032" layer="21" curve="-255.44999"/>
<wire x1="1.725" y1="-2.225" x2="1.0625" y2="-2.5625" width="0.2032" layer="21" curve="-255.44999"/>
<circle x="0" y="0" radius="2.725" width="0.2032" layer="51"/>
<smd name="1NC" x="-5.2" y="1.15" dx="2.9" dy="1.7" layer="1"/>
<smd name="2+" x="-5.2" y="-1.15" dx="2.9" dy="1.7" layer="1"/>
<smd name="3NC" x="5.2" y="-1.15" dx="2.9" dy="1.7" layer="1" rot="R180"/>
<smd name="4-" x="5.2" y="1.15" dx="2.9" dy="1.7" layer="1" rot="R180"/>
<text x="-3.175" y="3.81" size="1.27" layer="25">&gt;NAME</text>
<text x="-3.175" y="-5.08" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-5.975" y1="0.575" x2="-3.625" y2="1.6" layer="51"/>
<rectangle x1="-5.975" y1="-1.6" x2="-3.625" y2="-0.575" layer="51"/>
<rectangle x1="3.625" y1="-1.6" x2="5.975" y2="-0.575" layer="51" rot="R180"/>
<rectangle x1="3.625" y1="0.575" x2="5.975" y2="1.6" layer="51" rot="R180"/>
<polygon width="0.4064" layer="1">
<vertex x="2.3383" y="1.35"/>
<vertex x="0" y="2.7"/>
<vertex x="-2.3383" y="1.35"/>
<vertex x="-2.3383" y="-1.35"/>
<vertex x="0" y="-2.7"/>
<vertex x="2.3383" y="-1.35"/>
</polygon>
<polygon width="0.4064" layer="29">
<vertex x="2.3383" y="1.35"/>
<vertex x="0" y="2.7"/>
<vertex x="-2.3383" y="1.35"/>
<vertex x="-2.3383" y="-1.35"/>
<vertex x="0" y="-2.7"/>
<vertex x="2.3383" y="-1.35"/>
</polygon>
<polygon width="0.4064" layer="31">
<vertex x="2.3383" y="1.35"/>
<vertex x="0" y="2.7"/>
<vertex x="-2.3383" y="1.35"/>
<vertex x="-2.3383" y="-1.35"/>
<vertex x="0" y="-2.7"/>
<vertex x="2.3383" y="-1.35"/>
</polygon>
</package>
<package name="LUMILED">
<description>&lt;b&gt;Lumileds Lighting. LUXEON®&lt;/b&gt; without cool pad&lt;p&gt;
Source: K2.pdf</description>
<wire x1="-3.575" y1="2.3375" x2="-2.3375" y2="3.575" width="0.2032" layer="21"/>
<wire x1="-2.3375" y1="3.575" x2="2.3375" y2="3.575" width="0.2032" layer="21"/>
<wire x1="3.575" y1="2.3375" x2="3.575" y2="-3.575" width="0.2032" layer="21"/>
<wire x1="3.575" y1="-3.575" x2="-2.3375" y2="-3.575" width="0.2032" layer="21"/>
<wire x1="-2.3375" y1="-3.575" x2="-2.5" y2="-3.4125" width="0.2032" layer="21"/>
<wire x1="-2.5" y1="-3.4125" x2="-3.4125" y2="-2.5" width="0.2032" layer="21" curve="167.429893"/>
<wire x1="-3.4125" y1="-2.5" x2="-3.575" y2="-2.3375" width="0.2032" layer="21"/>
<wire x1="-3.575" y1="-2.3375" x2="-3.575" y2="2.3375" width="0.2032" layer="21"/>
<wire x1="2.3375" y1="3.575" x2="2.5" y2="3.4125" width="0.2032" layer="21"/>
<wire x1="2.5" y1="3.4125" x2="3.4125" y2="2.5" width="0.2032" layer="21" curve="167.429893"/>
<wire x1="3.4125" y1="2.5" x2="3.575" y2="2.3375" width="0.2032" layer="21"/>
<wire x1="-1.725" y1="2.225" x2="-1.0625" y2="2.5625" width="0.2032" layer="21" curve="-255.44999"/>
<wire x1="1.725" y1="-2.225" x2="1.0625" y2="-2.5625" width="0.2032" layer="21" curve="-255.44999"/>
<circle x="0" y="0" radius="2.725" width="0.2032" layer="51"/>
<smd name="1NC" x="-5.2" y="1.15" dx="2.9" dy="1.7" layer="1"/>
<smd name="2+" x="-5.2" y="-1.15" dx="2.9" dy="1.7" layer="1"/>
<smd name="3NC" x="5.2" y="-1.15" dx="2.9" dy="1.7" layer="1" rot="R180"/>
<smd name="4-" x="5.2" y="1.15" dx="2.9" dy="1.7" layer="1" rot="R180"/>
<text x="-3.175" y="3.81" size="1.27" layer="25">&gt;NAME</text>
<text x="-3.175" y="-5.08" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-5.975" y1="0.575" x2="-3.625" y2="1.6" layer="51"/>
<rectangle x1="-5.975" y1="-1.6" x2="-3.625" y2="-0.575" layer="51"/>
<rectangle x1="3.625" y1="-1.6" x2="5.975" y2="-0.575" layer="51" rot="R180"/>
<rectangle x1="3.625" y1="0.575" x2="5.975" y2="1.6" layer="51" rot="R180"/>
<polygon width="0.4064" layer="29">
<vertex x="2.3383" y="1.35"/>
<vertex x="0" y="2.7"/>
<vertex x="-2.3383" y="1.35"/>
<vertex x="-2.3383" y="-1.35"/>
<vertex x="0" y="-2.7"/>
<vertex x="2.3383" y="-1.35"/>
</polygon>
<polygon width="0.4064" layer="31">
<vertex x="2.3383" y="1.35"/>
<vertex x="0" y="2.7"/>
<vertex x="-2.3383" y="1.35"/>
<vertex x="-2.3383" y="-1.35"/>
<vertex x="0" y="-2.7"/>
<vertex x="2.3383" y="-1.35"/>
</polygon>
</package>
<package name="LED10MM">
<description>&lt;B&gt;LED&lt;/B&gt;&lt;p&gt;
10 mm, round</description>
<wire x1="5.08" y1="-2.54" x2="5.08" y2="2.54" width="0.254" layer="21" curve="-306.869898"/>
<wire x1="4.445" y1="0" x2="0" y2="-4.445" width="0.127" layer="21" curve="-90"/>
<wire x1="3.81" y1="0" x2="0" y2="-3.81" width="0.127" layer="21" curve="-90"/>
<wire x1="3.175" y1="0" x2="0" y2="-3.175" width="0.127" layer="21" curve="-90"/>
<wire x1="2.54" y1="0" x2="0" y2="-2.54" width="0.127" layer="21" curve="-90"/>
<wire x1="-4.445" y1="0" x2="0" y2="4.445" width="0.127" layer="21" curve="-90"/>
<wire x1="-3.81" y1="0" x2="0" y2="3.81" width="0.127" layer="21" curve="-90"/>
<wire x1="-3.175" y1="0" x2="0" y2="3.175" width="0.127" layer="21" curve="-90"/>
<wire x1="-2.54" y1="0" x2="0" y2="2.54" width="0.127" layer="21" curve="-90"/>
<wire x1="5.08" y1="2.54" x2="5.08" y2="-2.54" width="0.254" layer="21"/>
<circle x="0" y="0" radius="5.08" width="0.127" layer="21"/>
<pad name="K" x="1.27" y="0" drill="0.8128" diameter="1.6764" shape="square"/>
<pad name="A" x="-1.27" y="0" drill="0.8128" diameter="1.6764" shape="octagon"/>
<text x="6.35" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="6.35" y="-1.27" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="KA-3528ASYC">
<description>&lt;b&gt;SURFACE MOUNT LED LAMP&lt;/b&gt; 3.5x2.8mm&lt;p&gt;
Source: http://www.kingbright.com/manager/upload/pdf/KA-3528ASYC(Ver1189474662.1)</description>
<wire x1="-1.55" y1="1.35" x2="1.55" y2="1.35" width="0.1016" layer="21"/>
<wire x1="1.55" y1="1.35" x2="1.55" y2="-1.35" width="0.1016" layer="51"/>
<wire x1="1.55" y1="-1.35" x2="-1.55" y2="-1.35" width="0.1016" layer="21"/>
<wire x1="-1.55" y1="-1.35" x2="-1.55" y2="1.35" width="0.1016" layer="51"/>
<wire x1="-0.65" y1="0.95" x2="0.65" y2="0.95" width="0.1016" layer="21" curve="-68.40813"/>
<wire x1="0.65" y1="-0.95" x2="-0.65" y2="-0.95" width="0.1016" layer="21" curve="-68.40813"/>
<circle x="0" y="0" radius="1.15" width="0.1016" layer="51"/>
<smd name="A" x="-1.55" y="0" dx="1.5" dy="2.2" layer="1"/>
<smd name="C" x="1.55" y="0" dx="1.5" dy="2.2" layer="1"/>
<text x="-1.905" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.75" y1="0.6" x2="-1.6" y2="1.1" layer="51"/>
<rectangle x1="-1.75" y1="-1.1" x2="-1.6" y2="-0.6" layer="51"/>
<rectangle x1="1.6" y1="-1.1" x2="1.75" y2="-0.6" layer="51" rot="R180"/>
<rectangle x1="1.6" y1="0.6" x2="1.75" y2="1.1" layer="51" rot="R180"/>
<polygon width="0.1016" layer="51">
<vertex x="1.55" y="-1.35"/>
<vertex x="1.55" y="-0.625"/>
<vertex x="0.825" y="-1.35"/>
</polygon>
<polygon width="0.1016" layer="21">
<vertex x="1.55" y="-1.35"/>
<vertex x="1.55" y="-1.175"/>
<vertex x="1" y="-1.175"/>
<vertex x="0.825" y="-1.35"/>
</polygon>
</package>
<package name="SML0805">
<description>&lt;b&gt;SML0805-2CW-TR (0805 PROFILE)&lt;/b&gt; COOL WHITE&lt;p&gt;
Source: http://www.ledtronics.com/ds/smd-0603/Dstr0093.pdf</description>
<wire x1="-0.95" y1="-0.55" x2="0.95" y2="-0.55" width="0.1016" layer="51"/>
<wire x1="0.95" y1="-0.55" x2="0.95" y2="0.55" width="0.1016" layer="51"/>
<wire x1="0.95" y1="0.55" x2="-0.95" y2="0.55" width="0.1016" layer="51"/>
<wire x1="-0.95" y1="0.55" x2="-0.95" y2="-0.55" width="0.1016" layer="51"/>
<wire x1="-0.175" y1="-0.025" x2="0" y2="0.15" width="0.0634" layer="21"/>
<wire x1="0" y1="0.15" x2="0.15" y2="0" width="0.0634" layer="21"/>
<wire x1="0.15" y1="0" x2="-0.025" y2="-0.175" width="0.0634" layer="21"/>
<wire x1="-0.025" y1="-0.175" x2="-0.175" y2="-0.025" width="0.0634" layer="21"/>
<circle x="-0.275" y="0.4" radius="0.125" width="0" layer="21"/>
<smd name="C" x="-1.05" y="0" dx="1.2" dy="1.2" layer="1"/>
<smd name="A" x="1.05" y="0" dx="1.2" dy="1.2" layer="1"/>
<text x="-1.5" y="1" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.5" y="-2" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="SML1206">
<description>&lt;b&gt;SML10XXKH-TR (HIGH INTENSITY) LED&lt;/b&gt;&lt;p&gt;
&lt;table&gt;
&lt;tr&gt;&lt;td&gt;SML10R3KH-TR&lt;/td&gt;&lt;td&gt;ULTRA RED&lt;/td&gt;&lt;/tr&gt;
&lt;tr&gt;&lt;td&gt;SML10E3KH-TR&lt;/td&gt;&lt;td&gt;SUPER REDSUPER BLUE&lt;/td&gt;&lt;/tr&gt;
&lt;tr&gt;&lt;td&gt;SML10O3KH-TR&lt;/td&gt;&lt;td&gt;SUPER ORANGE&lt;/td&gt;&lt;/tr&gt;
&lt;tr&gt;&lt;td&gt;SML10PY3KH-TR&lt;/td&gt;&lt;td&gt;PURE YELLOW&lt;/td&gt;&lt;/tr&gt;
&lt;tr&gt;&lt;td&gt;SML10OY3KH-TR&lt;/td&gt;&lt;td&gt;ULTRA YELLOW&lt;/td&gt;&lt;/tr&gt;
&lt;tr&gt;&lt;td&gt;SML10AG3KH-TR&lt;/td&gt;&lt;td&gt;AQUA GREEN&lt;/td&gt;&lt;/tr&gt;
&lt;tr&gt;&lt;td&gt;SML10BG3KH-TR&lt;/td&gt;&lt;td&gt;BLUE GREEN&lt;/td&gt;&lt;/tr&gt;
&lt;tr&gt;&lt;td&gt;SML10PB1KH-TR&lt;/td&gt;&lt;td&gt;SUPER BLUE&lt;/td&gt;&lt;/tr&gt;
&lt;tr&gt;&lt;td&gt;SML10CW1KH-TR&lt;/td&gt;&lt;td&gt;WHITE&lt;/td&gt;&lt;/tr&gt;
&lt;/table&gt;

Source: http://www.ledtronics.com/ds/smd-1206/dstr0094.PDF</description>
<wire x1="-1.5" y1="0.5" x2="-1.5" y2="-0.5" width="0.2032" layer="51" curve="-180"/>
<wire x1="1.5" y1="-0.5" x2="1.5" y2="0.5" width="0.2032" layer="51" curve="-180"/>
<wire x1="-1.55" y1="0.75" x2="1.55" y2="0.75" width="0.1016" layer="51"/>
<wire x1="1.55" y1="-0.75" x2="-1.55" y2="-0.75" width="0.1016" layer="51"/>
<circle x="-0.725" y="0.525" radius="0.125" width="0" layer="21"/>
<smd name="C" x="-1.75" y="0" dx="1.5" dy="1.5" layer="1"/>
<smd name="A" x="1.75" y="0" dx="1.5" dy="1.5" layer="1"/>
<text x="-1.5" y="1" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.5" y="-2.5" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.6" y1="0.4" x2="-1.15" y2="0.8" layer="51"/>
<rectangle x1="-1.6" y1="-0.8" x2="-1.15" y2="-0.4" layer="51"/>
<rectangle x1="-1.175" y1="-0.6" x2="-1" y2="-0.275" layer="51"/>
<rectangle x1="1.15" y1="-0.8" x2="1.6" y2="-0.4" layer="51" rot="R180"/>
<rectangle x1="1.15" y1="0.4" x2="1.6" y2="0.8" layer="51" rot="R180"/>
<rectangle x1="1" y1="0.275" x2="1.175" y2="0.6" layer="51" rot="R180"/>
<rectangle x1="-0.1" y1="-0.1" x2="0.1" y2="0.1" layer="21"/>
</package>
<package name="SML0603">
<description>&lt;b&gt;SML0603-XXX (HIGH INTENSITY) LED&lt;/b&gt;&lt;p&gt;
&lt;table&gt;
&lt;tr&gt;&lt;td&gt;AG3K&lt;/td&gt;&lt;td&gt;AQUA GREEN&lt;/td&gt;&lt;/tr&gt;
&lt;tr&gt;&lt;td&gt;B1K&lt;/td&gt;&lt;td&gt;SUPER BLUE&lt;/td&gt;&lt;/tr&gt;
&lt;tr&gt;&lt;td&gt;R1K&lt;/td&gt;&lt;td&gt;SUPER RED&lt;/td&gt;&lt;/tr&gt;
&lt;tr&gt;&lt;td&gt;R3K&lt;/td&gt;&lt;td&gt;ULTRA RED&lt;/td&gt;&lt;/tr&gt;
&lt;tr&gt;&lt;td&gt;O3K&lt;/td&gt;&lt;td&gt;SUPER ORANGE&lt;/td&gt;&lt;/tr&gt;
&lt;tr&gt;&lt;td&gt;O3KH&lt;/td&gt;&lt;td&gt;SOFT ORANGE&lt;/td&gt;&lt;/tr&gt;
&lt;tr&gt;&lt;td&gt;Y3KH&lt;/td&gt;&lt;td&gt;SUPER YELLOW&lt;/td&gt;&lt;/tr&gt;
&lt;tr&gt;&lt;td&gt;Y3K&lt;/td&gt;&lt;td&gt;SUPER YELLOW&lt;/td&gt;&lt;/tr&gt;
&lt;tr&gt;&lt;td&gt;2CW&lt;/td&gt;&lt;td&gt;WHITE&lt;/td&gt;&lt;/tr&gt;
&lt;/table&gt;
Source: http://www.ledtronics.com/ds/smd-0603/Dstr0092.pdf</description>
<wire x1="-0.75" y1="0.35" x2="0.75" y2="0.35" width="0.1016" layer="51"/>
<wire x1="0.75" y1="0.35" x2="0.75" y2="-0.35" width="0.1016" layer="51"/>
<wire x1="0.75" y1="-0.35" x2="-0.75" y2="-0.35" width="0.1016" layer="51"/>
<wire x1="-0.75" y1="-0.35" x2="-0.75" y2="0.35" width="0.1016" layer="51"/>
<wire x1="-0.45" y1="0.3" x2="-0.45" y2="-0.3" width="0.1016" layer="51"/>
<wire x1="0.45" y1="0.3" x2="0.45" y2="-0.3" width="0.1016" layer="51"/>
<wire x1="-0.2" y1="0.35" x2="0.2" y2="0.35" width="0.1016" layer="21"/>
<wire x1="-0.2" y1="-0.35" x2="0.2" y2="-0.35" width="0.1016" layer="21"/>
<smd name="C" x="-0.75" y="0" dx="0.8" dy="0.8" layer="1"/>
<smd name="A" x="0.75" y="0" dx="0.8" dy="0.8" layer="1"/>
<text x="-1" y="1" size="1.27" layer="25">&gt;NAME</text>
<text x="-1" y="-2" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.4" y1="0.175" x2="0" y2="0.4" layer="51"/>
<rectangle x1="-0.25" y1="0.175" x2="0" y2="0.4" layer="21"/>
</package>
</packages>
<symbols>
<symbol name="LED">
<wire x1="1.27" y1="0" x2="0" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0" y1="-2.54" x2="-1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="0" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0" y1="-2.54" x2="-1.27" y2="-2.54" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="-2.032" y1="-0.762" x2="-3.429" y2="-2.159" width="0.1524" layer="94"/>
<wire x1="-1.905" y1="-1.905" x2="-3.302" y2="-3.302" width="0.1524" layer="94"/>
<text x="3.556" y="-4.572" size="1.778" layer="95" rot="R90">&gt;NAME</text>
<text x="5.715" y="-4.572" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="C" x="0" y="-5.08" visible="off" length="short" direction="pas" rot="R90"/>
<pin name="A" x="0" y="2.54" visible="off" length="short" direction="pas" rot="R270"/>
<polygon width="0.1524" layer="94">
<vertex x="-3.429" y="-2.159"/>
<vertex x="-3.048" y="-1.27"/>
<vertex x="-2.54" y="-1.778"/>
</polygon>
<polygon width="0.1524" layer="94">
<vertex x="-3.302" y="-3.302"/>
<vertex x="-2.921" y="-2.413"/>
<vertex x="-2.413" y="-2.921"/>
</polygon>
</symbol>
</symbols>
<devicesets>
<deviceset name="LED" prefix="LED" uservalue="yes">
<description>&lt;b&gt;LED&lt;/b&gt;&lt;p&gt;
&lt;u&gt;OSRAM&lt;/u&gt;:&lt;br&gt;

- &lt;u&gt;CHIPLED&lt;/u&gt;&lt;br&gt;
LG R971, LG N971, LY N971, LG Q971, LY Q971, LO R971, LY R971
LH N974, LH R974&lt;br&gt;
LS Q976, LO Q976, LY Q976&lt;br&gt;
LO Q996&lt;br&gt;

- &lt;u&gt;Hyper CHIPLED&lt;/u&gt;&lt;br&gt;
LW Q18S&lt;br&gt;
LB Q993, LB Q99A, LB R99A&lt;br&gt;

- &lt;u&gt;SideLED&lt;/u&gt;&lt;br&gt;
LS A670, LO A670, LY A670, LG A670, LP A670&lt;br&gt;
LB A673, LV A673, LT A673, LW A673&lt;br&gt;
LH A674&lt;br&gt;
LY A675&lt;br&gt;
LS A676, LA A676, LO A676, LY A676, LW A676&lt;br&gt;
LS A679, LY A679, LG A679&lt;br&gt;

-  &lt;u&gt;Hyper Micro SIDELED®&lt;/u&gt;&lt;br&gt;
LS Y876, LA Y876, LO Y876, LY Y876&lt;br&gt;
LT Y87S&lt;br&gt;

- &lt;u&gt;SmartLED&lt;/u&gt;&lt;br&gt;
LW L88C, LW L88S&lt;br&gt;
LB L89C, LB L89S, LG L890&lt;br&gt;
LS L89K, LO L89K, LY L89K&lt;br&gt;
LS L896, LA L896, LO L896, LY L896&lt;br&gt;

- &lt;u&gt;TOPLED&lt;/u&gt;&lt;br&gt;
LS T670, LO T670, LY T670, LG T670, LP T670&lt;br&gt;
LSG T670, LSP T670, LSY T670, LOP T670, LYG T670&lt;br&gt;
LG T671, LOG T671, LSG T671&lt;br&gt;
LB T673, LV T673, LT T673, LW T673&lt;br&gt;
LH T674&lt;br&gt;
LS T676, LA T676, LO T676, LY T676, LB T676, LH T676, LSB T676, LW T676&lt;br&gt;
LB T67C, LV T67C, LT T67C, LS T67K, LO T67K, LY T67K, LW E67C&lt;br&gt;
LS E67B, LA E67B, LO E67B, LY E67B, LB E67C, LV E67C, LT E67C&lt;br&gt;
LW T67C&lt;br&gt;
LS T679, LY T679, LG T679&lt;br&gt;
LS T770, LO T770, LY T770, LG T770, LP T770&lt;br&gt;
LB T773, LV T773, LT T773, LW T773&lt;br&gt;
LH T774&lt;br&gt;
LS E675, LA E675, LY E675, LS T675&lt;br&gt;
LS T776, LA T776, LO T776, LY T776, LB T776&lt;br&gt;
LHGB T686&lt;br&gt;
LT T68C, LB T68C&lt;br&gt;

- &lt;u&gt;Hyper Mini TOPLED®&lt;/u&gt;&lt;br&gt;
LB M676&lt;br&gt;

- &lt;u&gt;Mini TOPLED Santana®&lt;/u&gt;&lt;br&gt;
LG M470&lt;br&gt;
LS M47K, LO M47K, LY M47K
&lt;p&gt;
Source: http://www.osram.convergy.de&lt;p&gt;

&lt;u&gt;LUXEON:&lt;/u&gt;&lt;br&gt;
- &lt;u&gt;LUMILED®&lt;/u&gt;&lt;br&gt;
LXK2-PW12-R00, LXK2-PW12-S00, LXK2-PW14-U00, LXK2-PW14-V00&lt;br&gt;
LXK2-PM12-R00, LXK2-PM12-S00, LXK2-PM14-U00&lt;br&gt;
LXK2-PE12-Q00, LXK2-PE12-R00, LXK2-PE12-S00, LXK2-PE14-T00, LXK2-PE14-U00&lt;br&gt;
LXK2-PB12-K00, LXK2-PB12-L00, LXK2-PB12-M00, LXK2-PB14-N00, LXK2-PB14-P00, LXK2-PB14-Q00&lt;br&gt;
LXK2-PR12-L00, LXK2-PR12-M00, LXK2-PR14-Q00, LXK2-PR14-R00&lt;br&gt;
LXK2-PD12-Q00, LXK2-PD12-R00, LXK2-PD12-S00&lt;br&gt;
LXK2-PH12-R00, LXK2-PH12-S00&lt;br&gt;
LXK2-PL12-P00, LXK2-PL12-Q00, LXK2-PL12-R00
&lt;p&gt;
Source: www.luxeon.com&lt;p&gt;

&lt;u&gt;KINGBRIGHT:&lt;/U&gt;&lt;p&gt;
KA-3528ASYC&lt;br&gt;
Source: www.kingbright.com</description>
<gates>
<gate name="G$1" symbol="LED" x="0" y="0"/>
</gates>
<devices>
<device name="SMT1206" package="1206">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="LD260" package="LD260">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SQR2X5" package="LED2X5">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="3MM" package="LED3MM">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="5MM" package="LED5MM">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="LSU260" package="LSU260">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="LZR181" package="LZR181">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="B152" package="Q62902-B152">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="B153" package="Q62902-B153">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="B155" package="Q62902-B155">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="B156" package="Q62902-B156">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SFH480" package="SFH480">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SFH482" package="SFH482">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SQR5.7X3.2" package="U57X32">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="IRL80A" package="IRL80A">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="P-LCC-2" package="P-LCC-2">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="MINI-TOP" package="OSRAM-MINI-TOP-LED">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SIDELED" package="OSRAM-SIDELED">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SMART-LED" package="SMART-LED">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="B"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="P-LCC-2-BACK" package="P-LCC-2-TOPLED-RG">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="MICRO-SIDELED" package="MICRO-SIDELED">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="P-LCC-4" package="P-LCC-4">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C@4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="CHIP-LED0603" package="CHIP-LED0603">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="CHIP-LED0805" package="CHIP-LED0805">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="TOPLED-SANTANA" package="MINI-TOPLED-SANTANA">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="CHIPLED_0805" package="CHIPLED_0805">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="CHIPLED_1206" package="CHIPLED_1206">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="CHIPLED_0603" package="CHIPLED_0603">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="CHIPLED-0603-TTW" package="CHIPLED-0603-TTW">
<connects>
<connect gate="G$1" pin="A" pad="A@1"/>
<connect gate="G$1" pin="C" pad="C@1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="" package="SMARTLED-TTW">
<connects>
<connect gate="G$1" pin="A" pad="A@1"/>
<connect gate="G$1" pin="C" pad="C@1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-LUMILED+" package="LUMILED+">
<connects>
<connect gate="G$1" pin="A" pad="2+"/>
<connect gate="G$1" pin="C" pad="4-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-LUMILED" package="LUMILED">
<connects>
<connect gate="G$1" pin="A" pad="2+"/>
<connect gate="G$1" pin="C" pad="4-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="10MM" package="LED10MM">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="KA-3528ASYC" package="KA-3528ASYC">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SML0805" package="SML0805">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SML1206" package="SML1206">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SML0603" package="SML0603">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="uln-udn">
<description>&lt;b&gt;Driver Arrays&lt;/b&gt;&lt;p&gt;
ULN and UDN Series&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
<package name="DIL16">
<description>&lt;b&gt;Dual In Line Package&lt;/b&gt;</description>
<wire x1="10.16" y1="2.921" x2="-10.16" y2="2.921" width="0.1524" layer="21"/>
<wire x1="-10.16" y1="-2.921" x2="10.16" y2="-2.921" width="0.1524" layer="21"/>
<wire x1="10.16" y1="2.921" x2="10.16" y2="-2.921" width="0.1524" layer="21"/>
<wire x1="-10.16" y1="2.921" x2="-10.16" y2="1.016" width="0.1524" layer="21"/>
<wire x1="-10.16" y1="-2.921" x2="-10.16" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="-10.16" y1="1.016" x2="-10.16" y2="-1.016" width="0.1524" layer="21" curve="-180"/>
<pad name="1" x="-8.89" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="2" x="-6.35" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="7" x="6.35" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="8" x="8.89" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="3" x="-3.81" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="4" x="-1.27" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="6" x="3.81" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="5" x="1.27" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="9" x="8.89" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="10" x="6.35" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="11" x="3.81" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="12" x="1.27" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="13" x="-1.27" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="14" x="-3.81" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="15" x="-6.35" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="16" x="-8.89" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<text x="-10.541" y="-2.921" size="1.27" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<text x="-7.493" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="SO16">
<description>&lt;b&gt;Small Outline Package&lt;/b&gt;</description>
<wire x1="4.699" y1="1.9558" x2="-4.699" y2="1.9558" width="0.1524" layer="21"/>
<wire x1="4.699" y1="-1.9558" x2="5.08" y2="-1.5748" width="0.1524" layer="21" curve="90"/>
<wire x1="-5.08" y1="1.5748" x2="-4.699" y2="1.9558" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.699" y1="1.9558" x2="5.08" y2="1.5748" width="0.1524" layer="21" curve="-90"/>
<wire x1="-5.08" y1="-1.5748" x2="-4.699" y2="-1.9558" width="0.1524" layer="21" curve="90"/>
<wire x1="-4.699" y1="-1.9558" x2="4.699" y2="-1.9558" width="0.1524" layer="21"/>
<wire x1="5.08" y1="-1.5748" x2="5.08" y2="1.5748" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="1.5748" x2="-5.08" y2="-1.5748" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="0.508" x2="-5.08" y2="-0.508" width="0.1524" layer="21" curve="-180"/>
<wire x1="-5.08" y1="-1.6002" x2="5.08" y2="-1.6002" width="0.0508" layer="21"/>
<smd name="1" x="-4.445" y="-3.0734" dx="0.6604" dy="2.032" layer="1"/>
<smd name="16" x="-4.445" y="3.0734" dx="0.6604" dy="2.032" layer="1"/>
<smd name="2" x="-3.175" y="-3.0734" dx="0.6604" dy="2.032" layer="1"/>
<smd name="3" x="-1.905" y="-3.0734" dx="0.6604" dy="2.032" layer="1"/>
<smd name="15" x="-3.175" y="3.0734" dx="0.6604" dy="2.032" layer="1"/>
<smd name="14" x="-1.905" y="3.0734" dx="0.6604" dy="2.032" layer="1"/>
<smd name="4" x="-0.635" y="-3.0734" dx="0.6604" dy="2.032" layer="1"/>
<smd name="13" x="-0.635" y="3.0734" dx="0.6604" dy="2.032" layer="1"/>
<smd name="5" x="0.635" y="-3.0734" dx="0.6604" dy="2.032" layer="1"/>
<smd name="12" x="0.635" y="3.0734" dx="0.6604" dy="2.032" layer="1"/>
<smd name="6" x="1.905" y="-3.0734" dx="0.6604" dy="2.032" layer="1"/>
<smd name="7" x="3.175" y="-3.0734" dx="0.6604" dy="2.032" layer="1"/>
<smd name="11" x="1.905" y="3.0734" dx="0.6604" dy="2.032" layer="1"/>
<smd name="10" x="3.175" y="3.0734" dx="0.6604" dy="2.032" layer="1"/>
<smd name="8" x="4.445" y="-3.0734" dx="0.6604" dy="2.032" layer="1"/>
<smd name="9" x="4.445" y="3.0734" dx="0.6604" dy="2.032" layer="1"/>
<text x="-4.064" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<text x="-5.461" y="-1.778" size="1.27" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<rectangle x1="-0.889" y1="1.9558" x2="-0.381" y2="3.0988" layer="51"/>
<rectangle x1="-4.699" y1="-3.0988" x2="-4.191" y2="-1.9558" layer="51"/>
<rectangle x1="-3.429" y1="-3.0988" x2="-2.921" y2="-1.9558" layer="51"/>
<rectangle x1="-2.159" y1="-3.0734" x2="-1.651" y2="-1.9304" layer="51"/>
<rectangle x1="-0.889" y1="-3.0988" x2="-0.381" y2="-1.9558" layer="51"/>
<rectangle x1="-2.159" y1="1.9558" x2="-1.651" y2="3.0988" layer="51"/>
<rectangle x1="-3.429" y1="1.9558" x2="-2.921" y2="3.0988" layer="51"/>
<rectangle x1="-4.699" y1="1.9558" x2="-4.191" y2="3.0988" layer="51"/>
<rectangle x1="0.381" y1="-3.0988" x2="0.889" y2="-1.9558" layer="51"/>
<rectangle x1="1.651" y1="-3.0988" x2="2.159" y2="-1.9558" layer="51"/>
<rectangle x1="2.921" y1="-3.0988" x2="3.429" y2="-1.9558" layer="51"/>
<rectangle x1="4.191" y1="-3.0988" x2="4.699" y2="-1.9558" layer="51"/>
<rectangle x1="0.381" y1="1.9558" x2="0.889" y2="3.0988" layer="51"/>
<rectangle x1="1.651" y1="1.9558" x2="2.159" y2="3.0988" layer="51"/>
<rectangle x1="2.921" y1="1.9558" x2="3.429" y2="3.0988" layer="51"/>
<rectangle x1="4.191" y1="1.9558" x2="4.699" y2="3.0988" layer="51"/>
</package>
</packages>
<symbols>
<symbol name="2001A">
<wire x1="-7.62" y1="10.16" x2="7.62" y2="10.16" width="0.4064" layer="94"/>
<wire x1="7.62" y1="-12.7" x2="7.62" y2="10.16" width="0.4064" layer="94"/>
<wire x1="7.62" y1="-12.7" x2="-7.62" y2="-12.7" width="0.4064" layer="94"/>
<wire x1="-7.62" y1="10.16" x2="-7.62" y2="-12.7" width="0.4064" layer="94"/>
<text x="-7.62" y="10.922" size="1.778" layer="95">&gt;NAME</text>
<text x="-7.62" y="-15.24" size="1.778" layer="96">&gt;VALUE</text>
<pin name="I1" x="-12.7" y="7.62" length="middle" direction="in"/>
<pin name="I2" x="-12.7" y="5.08" length="middle" direction="in"/>
<pin name="I3" x="-12.7" y="2.54" length="middle" direction="in"/>
<pin name="I4" x="-12.7" y="0" length="middle" direction="in"/>
<pin name="I5" x="-12.7" y="-2.54" length="middle" direction="in"/>
<pin name="I6" x="-12.7" y="-5.08" length="middle" direction="in"/>
<pin name="I7" x="-12.7" y="-7.62" length="middle" direction="in"/>
<pin name="O1" x="12.7" y="7.62" length="middle" direction="oc" rot="R180"/>
<pin name="O2" x="12.7" y="5.08" length="middle" direction="oc" rot="R180"/>
<pin name="O3" x="12.7" y="2.54" length="middle" direction="oc" rot="R180"/>
<pin name="O4" x="12.7" y="0" length="middle" direction="oc" rot="R180"/>
<pin name="O5" x="12.7" y="-2.54" length="middle" direction="oc" rot="R180"/>
<pin name="O6" x="12.7" y="-5.08" length="middle" direction="oc" rot="R180"/>
<pin name="O7" x="12.7" y="-7.62" length="middle" direction="oc" rot="R180"/>
<pin name="CD+" x="12.7" y="-10.16" length="middle" direction="pas" rot="R180"/>
<pin name="GND" x="-12.7" y="-10.16" length="middle" direction="pwr"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="ULN2003A" prefix="IC">
<description>&lt;b&gt;DRIVER ARRAY&lt;/b&gt;</description>
<gates>
<gate name="A" symbol="2001A" x="0" y="0"/>
</gates>
<devices>
<device name="N" package="DIL16">
<connects>
<connect gate="A" pin="CD+" pad="9"/>
<connect gate="A" pin="GND" pad="8"/>
<connect gate="A" pin="I1" pad="1"/>
<connect gate="A" pin="I2" pad="2"/>
<connect gate="A" pin="I3" pad="3"/>
<connect gate="A" pin="I4" pad="4"/>
<connect gate="A" pin="I5" pad="5"/>
<connect gate="A" pin="I6" pad="6"/>
<connect gate="A" pin="I7" pad="7"/>
<connect gate="A" pin="O1" pad="16"/>
<connect gate="A" pin="O2" pad="15"/>
<connect gate="A" pin="O3" pad="14"/>
<connect gate="A" pin="O4" pad="13"/>
<connect gate="A" pin="O5" pad="12"/>
<connect gate="A" pin="O6" pad="11"/>
<connect gate="A" pin="O7" pad="10"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="D" package="SO16">
<connects>
<connect gate="A" pin="CD+" pad="9"/>
<connect gate="A" pin="GND" pad="8"/>
<connect gate="A" pin="I1" pad="1"/>
<connect gate="A" pin="I2" pad="2"/>
<connect gate="A" pin="I3" pad="3"/>
<connect gate="A" pin="I4" pad="4"/>
<connect gate="A" pin="I5" pad="5"/>
<connect gate="A" pin="I6" pad="6"/>
<connect gate="A" pin="I7" pad="7"/>
<connect gate="A" pin="O1" pad="16"/>
<connect gate="A" pin="O2" pad="15"/>
<connect gate="A" pin="O3" pad="14"/>
<connect gate="A" pin="O4" pad="13"/>
<connect gate="A" pin="O5" pad="12"/>
<connect gate="A" pin="O6" pad="11"/>
<connect gate="A" pin="O7" pad="10"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="resistor">
<description>&lt;b&gt;Resistors, Capacitors, Inductors&lt;/b&gt;&lt;p&gt;
Based on the previous libraries:
&lt;ul&gt;
&lt;li&gt;r.lbr
&lt;li&gt;cap.lbr 
&lt;li&gt;cap-fe.lbr
&lt;li&gt;captant.lbr
&lt;li&gt;polcap.lbr
&lt;li&gt;ipc-smd.lbr
&lt;/ul&gt;
All SMD packages are defined according to the IPC specifications and  CECC&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;&lt;p&gt;
&lt;p&gt;
for Electrolyt Capacitors see also :&lt;p&gt;
www.bccomponents.com &lt;p&gt;
www.panasonic.com&lt;p&gt;
www.kemet.com&lt;p&gt;
&lt;p&gt;
for trimmer refence see : &lt;u&gt;www.electrospec-inc.com/cross_references/trimpotcrossref.asp&lt;/u&gt;&lt;p&gt;

&lt;map name="nav_main"&gt;
&lt;area shape="rect" coords="0,1,140,23" href="../military_specs.asp" title=""&gt;
&lt;area shape="rect" coords="0,24,140,51" href="../about.asp" title=""&gt;
&lt;area shape="rect" coords="1,52,140,77" href="../rfq.asp" title=""&gt;
&lt;area shape="rect" coords="0,78,139,103" href="../products.asp" title=""&gt;
&lt;area shape="rect" coords="1,102,138,128" href="../excess_inventory.asp" title=""&gt;
&lt;area shape="rect" coords="1,129,138,150" href="../edge.asp" title=""&gt;
&lt;area shape="rect" coords="1,151,139,178" href="../industry_links.asp" title=""&gt;
&lt;area shape="rect" coords="0,179,139,201" href="../comments.asp" title=""&gt;
&lt;area shape="rect" coords="1,203,138,231" href="../directory.asp" title=""&gt;
&lt;area shape="default" nohref&gt;
&lt;/map&gt;

&lt;html&gt;

&lt;title&gt;&lt;/title&gt;

 &lt;LINK REL="StyleSheet" TYPE="text/css" HREF="style-sheet.css"&gt;

&lt;body bgcolor="#ffffff" text="#000000" marginwidth="0" marginheight="0" topmargin="0" leftmargin="0"&gt;
&lt;table border=0 cellspacing=0 cellpadding=0 width="100%" cellpaddding=0 height="55%"&gt;
&lt;tr valign="top"&gt;

&lt;/td&gt;
&lt;! &lt;td width="10"&gt;&amp;nbsp;&lt;/td&gt;
&lt;td width="90%"&gt;

&lt;b&gt;&lt;font color="#0000FF" size="4"&gt;TRIM-POT CROSS REFERENCE&lt;/font&gt;&lt;/b&gt;
&lt;P&gt;
&lt;TABLE BORDER=0 CELLSPACING=1 CELLPADDING=2&gt;
  &lt;TR&gt;
    &lt;TD COLSPAN=8&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;RECTANGULAR MULTI-TURN&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;B&gt;
      &lt;FONT SIZE=3 FACE=ARIAL color="#FF0000"&gt;BOURNS&lt;/FONT&gt;
      &lt;/B&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;B&gt;
      &lt;FONT SIZE=3 FACE=ARIAL color="#FF0000"&gt;BI&amp;nbsp;TECH&lt;/FONT&gt;
      &lt;/B&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;B&gt;
      &lt;FONT SIZE=3 FACE=ARIAL color="#FF0000"&gt;DALE-VISHAY&lt;/FONT&gt;
      &lt;/B&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;B&gt;
      &lt;FONT SIZE=3 FACE=ARIAL color="#FF0000"&gt;PHILIPS/MEPCO&lt;/FONT&gt;
      &lt;/B&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;B&gt;
      &lt;FONT SIZE=3 FACE=ARIAL color="#FF0000"&gt;MURATA&lt;/FONT&gt;
      &lt;/B&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;B&gt;
      &lt;FONT SIZE=3 FACE=ARIAL color="#FF0000"&gt;PANASONIC&lt;/FONT&gt;
      &lt;/B&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;B&gt;
      &lt;FONT SIZE=3 FACE=ARIAL color="#FF0000"&gt;SPECTROL&lt;/FONT&gt;
      &lt;/B&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;B&gt;
      &lt;FONT SIZE=3 FACE=ARIAL color="#FF0000"&gt;MILSPEC&lt;/FONT&gt;
      &lt;/B&gt;
    &lt;/TD&gt;&lt;TD&gt;&amp;nbsp;&lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3 &gt;
      3005P&lt;BR&gt;
      3006P&lt;BR&gt;
      3006W&lt;BR&gt;
      3006Y&lt;BR&gt;
      3009P&lt;BR&gt;
      3009W&lt;BR&gt;
      3009Y&lt;BR&gt;
      3057J&lt;BR&gt;
      3057L&lt;BR&gt;
      3057P&lt;BR&gt;
      3057Y&lt;BR&gt;
      3059J&lt;BR&gt;
      3059L&lt;BR&gt;
      3059P&lt;BR&gt;
      3059Y&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      89P&lt;BR&gt;
      89W&lt;BR&gt;
      89X&lt;BR&gt;
      89PH&lt;BR&gt;
      76P&lt;BR&gt;
      89XH&lt;BR&gt;
      78SLT&lt;BR&gt;
      78L&amp;nbsp;ALT&lt;BR&gt;
      56P&amp;nbsp;ALT&lt;BR&gt;
      78P&amp;nbsp;ALT&lt;BR&gt;
      T8S&lt;BR&gt;
      78L&lt;BR&gt;
      56P&lt;BR&gt;
      78P&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      T18/784&lt;BR&gt;
      783&lt;BR&gt;
      781&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      2199&lt;BR&gt;
      1697/1897&lt;BR&gt;
      1680/1880&lt;BR&gt;
      2187&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      8035EKP/CT20/RJ-20P&lt;BR&gt;
      -&lt;BR&gt;
      RJ-20X&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      1211L&lt;BR&gt;
      8012EKQ&amp;nbsp;ALT&lt;BR&gt;
      8012EKR&amp;nbsp;ALT&lt;BR&gt;
      1211P&lt;BR&gt;
      8012EKJ&lt;BR&gt;
      8012EKL&lt;BR&gt;
      8012EKQ&lt;BR&gt;
      8012EKR&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      2101P&lt;BR&gt;
      2101W&lt;BR&gt;
      2101Y&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      2102L&lt;BR&gt;
      2102S&lt;BR&gt;
      2102Y&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      EVMCOG&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      43P&lt;BR&gt;
      43W&lt;BR&gt;
      43Y&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      40L&lt;BR&gt;
      40P&lt;BR&gt;
      40Y&lt;BR&gt;
      70Y-T602&lt;BR&gt;
      70L&lt;BR&gt;
      70P&lt;BR&gt;
      70Y&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      RT/RTR12&lt;BR&gt;
      RT/RTR12&lt;BR&gt;
      RT/RTR12&lt;BR&gt;
      -&lt;BR&gt;
      RJ/RJR12&lt;BR&gt;
      RJ/RJR12&lt;BR&gt;
      RJ/RJR12&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD COLSPAN=8&gt;&amp;nbsp;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD COLSPAN=8&gt;
      &lt;FONT SIZE=4 FACE=ARIAL&gt;&lt;B&gt;SQUARE MULTI-TURN&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
   &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;BOURN&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;BI&amp;nbsp;TECH&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;DALE-VISHAY&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;PHILIPS/MEPCO&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;MURATA&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;PANASONIC&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;SPECTROL&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;MILSPEC&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      3250L&lt;BR&gt;
      3250P&lt;BR&gt;
      3250W&lt;BR&gt;
      3250X&lt;BR&gt;
      3252P&lt;BR&gt;
      3252W&lt;BR&gt;
      3252X&lt;BR&gt;
      3260P&lt;BR&gt;
      3260W&lt;BR&gt;
      3260X&lt;BR&gt;
      3262P&lt;BR&gt;
      3262W&lt;BR&gt;
      3262X&lt;BR&gt;
      3266P&lt;BR&gt;
      3266W&lt;BR&gt;
      3266X&lt;BR&gt;
      3290H&lt;BR&gt;
      3290P&lt;BR&gt;
      3290W&lt;BR&gt;
      3292P&lt;BR&gt;
      3292W&lt;BR&gt;
      3292X&lt;BR&gt;
      3296P&lt;BR&gt;
      3296W&lt;BR&gt;
      3296X&lt;BR&gt;
      3296Y&lt;BR&gt;
      3296Z&lt;BR&gt;
      3299P&lt;BR&gt;
      3299W&lt;BR&gt;
      3299X&lt;BR&gt;
      3299Y&lt;BR&gt;
      3299Z&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      66P&amp;nbsp;ALT&lt;BR&gt;
      66W&amp;nbsp;ALT&lt;BR&gt;
      66X&amp;nbsp;ALT&lt;BR&gt;
      66P&amp;nbsp;ALT&lt;BR&gt;
      66W&amp;nbsp;ALT&lt;BR&gt;
      66X&amp;nbsp;ALT&lt;BR&gt;
      -&lt;BR&gt;
      64W&amp;nbsp;ALT&lt;BR&gt;
      -&lt;BR&gt;
      64P&amp;nbsp;ALT&lt;BR&gt;
      64W&amp;nbsp;ALT&lt;BR&gt;
      64X&amp;nbsp;ALT&lt;BR&gt;
      64P&lt;BR&gt;
      64W&lt;BR&gt;
      64X&lt;BR&gt;
      66X&amp;nbsp;ALT&lt;BR&gt;
      66P&amp;nbsp;ALT&lt;BR&gt;
      66W&amp;nbsp;ALT&lt;BR&gt;
      66P&lt;BR&gt;
      66W&lt;BR&gt;
      66X&lt;BR&gt;
      67P&lt;BR&gt;
      67W&lt;BR&gt;
      67X&lt;BR&gt;
      67Y&lt;BR&gt;
      67Z&lt;BR&gt;
      68P&lt;BR&gt;
      68W&lt;BR&gt;
      68X&lt;BR&gt;
      67Y&amp;nbsp;ALT&lt;BR&gt;
      67Z&amp;nbsp;ALT&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      5050&lt;BR&gt;
      5091&lt;BR&gt;
      5080&lt;BR&gt;
      5087&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      T63YB&lt;BR&gt;
      T63XB&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      5887&lt;BR&gt;
      5891&lt;BR&gt;
      5880&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      T93Z&lt;BR&gt;
      T93YA&lt;BR&gt;
      T93XA&lt;BR&gt;
      T93YB&lt;BR&gt;
      T93XB&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      8026EKP&lt;BR&gt;
      8026EKW&lt;BR&gt;
      8026EKM&lt;BR&gt;
      8026EKP&lt;BR&gt;
      8026EKB&lt;BR&gt;
      8026EKM&lt;BR&gt;
      1309X&lt;BR&gt;
      1309P&lt;BR&gt;
      1309W&lt;BR&gt;
      8024EKP&lt;BR&gt;
      8024EKW&lt;BR&gt;
      8024EKN&lt;BR&gt;
      RJ-9P/CT9P&lt;BR&gt;
      RJ-9W&lt;BR&gt;
      RJ-9X&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      3103P&lt;BR&gt;
      3103Y&lt;BR&gt;
      3103Z&lt;BR&gt;
      3103P&lt;BR&gt;
      3103Y&lt;BR&gt;
      3103Z&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      3105P/3106P&lt;BR&gt;
      3105W/3106W&lt;BR&gt;
      3105X/3106X&lt;BR&gt;
      3105Y/3106Y&lt;BR&gt;
      3105Z/3105Z&lt;BR&gt;
      3102P&lt;BR&gt;
      3102W&lt;BR&gt;
      3102X&lt;BR&gt;
      3102Y&lt;BR&gt;
      3102Z&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      EVMCBG&lt;BR&gt;
      EVMCCG&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      55-1-X&lt;BR&gt;
      55-4-X&lt;BR&gt;
      55-3-X&lt;BR&gt;
      55-2-X&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      50-2-X&lt;BR&gt;
      50-4-X&lt;BR&gt;
      50-3-X&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      64P&lt;BR&gt;
      64W&lt;BR&gt;
      64X&lt;BR&gt;
      64Y&lt;BR&gt;
      64Z&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      RT/RTR22&lt;BR&gt;
      RT/RTR22&lt;BR&gt;
      RT/RTR22&lt;BR&gt;
      RT/RTR22&lt;BR&gt;
      RJ/RJR22&lt;BR&gt;
      RJ/RJR22&lt;BR&gt;
      RJ/RJR22&lt;BR&gt;
      RT/RTR26&lt;BR&gt;
      RT/RTR26&lt;BR&gt;
      RT/RTR26&lt;BR&gt;
      RJ/RJR26&lt;BR&gt;
      RJ/RJR26&lt;BR&gt;
      RJ/RJR26&lt;BR&gt;
      RJ/RJR26&lt;BR&gt;
      RJ/RJR26&lt;BR&gt;
      RJ/RJR26&lt;BR&gt;
      RT/RTR24&lt;BR&gt;
      RT/RTR24&lt;BR&gt;
      RT/RTR24&lt;BR&gt;
      RJ/RJR24&lt;BR&gt;
      RJ/RJR24&lt;BR&gt;
      RJ/RJR24&lt;BR&gt;
      RJ/RJR24&lt;BR&gt;
      RJ/RJR24&lt;BR&gt;
      RJ/RJR24&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD COLSPAN=8&gt;&amp;nbsp;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD COLSPAN=8&gt;
      &lt;FONT SIZE=4 FACE=ARIAL&gt;&lt;B&gt;SINGLE TURN&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;BOURN&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;BI&amp;nbsp;TECH&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;DALE-VISHAY&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;PHILIPS/MEPCO&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;MURATA&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;PANASONIC&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;SPECTROL&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;MILSPEC&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      3323P&lt;BR&gt;
      3323S&lt;BR&gt;
      3323W&lt;BR&gt;
      3329H&lt;BR&gt;
      3329P&lt;BR&gt;
      3329W&lt;BR&gt;
      3339H&lt;BR&gt;
      3339P&lt;BR&gt;
      3339W&lt;BR&gt;
      3352E&lt;BR&gt;
      3352H&lt;BR&gt;
      3352K&lt;BR&gt;
      3352P&lt;BR&gt;
      3352T&lt;BR&gt;
      3352V&lt;BR&gt;
      3352W&lt;BR&gt;
      3362H&lt;BR&gt;
      3362M&lt;BR&gt;
      3362P&lt;BR&gt;
      3362R&lt;BR&gt;
      3362S&lt;BR&gt;
      3362U&lt;BR&gt;
      3362W&lt;BR&gt;
      3362X&lt;BR&gt;
      3386B&lt;BR&gt;
      3386C&lt;BR&gt;
      3386F&lt;BR&gt;
      3386H&lt;BR&gt;
      3386K&lt;BR&gt;
      3386M&lt;BR&gt;
      3386P&lt;BR&gt;
      3386S&lt;BR&gt;
      3386W&lt;BR&gt;
      3386X&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      25P&lt;BR&gt;
      25S&lt;BR&gt;
      25RX&lt;BR&gt;
      82P&lt;BR&gt;
      82M&lt;BR&gt;
      82PA&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      91E&lt;BR&gt;
      91X&lt;BR&gt;
      91T&lt;BR&gt;
      91B&lt;BR&gt;
      91A&lt;BR&gt;
      91V&lt;BR&gt;
      91W&lt;BR&gt;
      25W&lt;BR&gt;
      25V&lt;BR&gt;
      25P&lt;BR&gt;
      -&lt;BR&gt;
      25S&lt;BR&gt;
      25U&lt;BR&gt;
      25RX&lt;BR&gt;
      25X&lt;BR&gt;
      72XW&lt;BR&gt;
      72XL&lt;BR&gt;
      72PM&lt;BR&gt;
      72RX&lt;BR&gt;
      -&lt;BR&gt;
      72PX&lt;BR&gt;
      72P&lt;BR&gt;
      72RXW&lt;BR&gt;
      72RXL&lt;BR&gt;
      72X&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      T7YB&lt;BR&gt;
      T7YA&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      TXD&lt;BR&gt;
      TYA&lt;BR&gt;
      TYP&lt;BR&gt;
      -&lt;BR&gt;
      TYD&lt;BR&gt;
      TX&lt;BR&gt;
      -&lt;BR&gt;
      150SX&lt;BR&gt;
      100SX&lt;BR&gt;
      102T&lt;BR&gt;
      101S&lt;BR&gt;
      190T&lt;BR&gt;
      150TX&lt;BR&gt;
      101&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      101SX&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      ET6P&lt;BR&gt;
      ET6S&lt;BR&gt;
      ET6X&lt;BR&gt;
      RJ-6W/8014EMW&lt;BR&gt;
      RJ-6P/8014EMP&lt;BR&gt;
      RJ-6X/8014EMX&lt;BR&gt;
      TM7W&lt;BR&gt;
      TM7P&lt;BR&gt;
      TM7X&lt;BR&gt;
      -&lt;BR&gt;
      8017SMS&lt;BR&gt;
      -&lt;BR&gt;
      8017SMB&lt;BR&gt;
      8017SMA&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      CT-6W&lt;BR&gt;
      CT-6H&lt;BR&gt;
      CT-6P&lt;BR&gt;
      CT-6R&lt;BR&gt;
      -&lt;BR&gt;
      CT-6V&lt;BR&gt;
      CT-6X&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      8038EKV&lt;BR&gt;
      -&lt;BR&gt;
      8038EKX&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      8038EKP&lt;BR&gt;
      8038EKZ&lt;BR&gt;
      8038EKW&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      3321H&lt;BR&gt;
      3321P&lt;BR&gt;
      3321N&lt;BR&gt;
      1102H&lt;BR&gt;
      1102P&lt;BR&gt;
      1102T&lt;BR&gt;
      RVA0911V304A&lt;BR&gt;
      -&lt;BR&gt;
      RVA0911H413A&lt;BR&gt;
      RVG0707V100A&lt;BR&gt;
      RVA0607V(H)306A&lt;BR&gt;
      RVA1214H213A&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      3104B&lt;BR&gt;
      3104C&lt;BR&gt;
      3104F&lt;BR&gt;
      3104H&lt;BR&gt;
      -&lt;BR&gt;
      3104M&lt;BR&gt;
      3104P&lt;BR&gt;
      3104S&lt;BR&gt;
      3104W&lt;BR&gt;
      3104X&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      EVMQ0G&lt;BR&gt;
      EVMQIG&lt;BR&gt;
      EVMQ3G&lt;BR&gt;
      EVMS0G&lt;BR&gt;
      EVMQ0G&lt;BR&gt;
      EVMG0G&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      EVMK4GA00B&lt;BR&gt;
      EVM30GA00B&lt;BR&gt;
      EVMK0GA00B&lt;BR&gt;
      EVM38GA00B&lt;BR&gt;
      EVMB6&lt;BR&gt;
      EVLQ0&lt;BR&gt;
      -&lt;BR&gt;
      EVMMSG&lt;BR&gt;
      EVMMBG&lt;BR&gt;
      EVMMAG&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      EVMMCS&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      EVMM1&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      EVMM0&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      EVMM3&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      62-3-1&lt;BR&gt;
      62-1-2&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      67R&lt;BR&gt;
      -&lt;BR&gt;
      67P&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      67X&lt;BR&gt;
      63V&lt;BR&gt;
      63S&lt;BR&gt;
      63M&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      63H&lt;BR&gt;
      63P&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      63X&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      RJ/RJR50&lt;BR&gt;
      RJ/RJR50&lt;BR&gt;
      RJ/RJR50&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
&lt;/TABLE&gt;
&lt;P&gt;&amp;nbsp;&lt;P&gt;
&lt;TABLE BORDER=0 CELLSPACING=1 CELLPADDING=3&gt;
  &lt;TR&gt;
    &lt;TD COLSPAN=7&gt;
      &lt;FONT color="#0000FF" SIZE=4 FACE=ARIAL&gt;&lt;B&gt;SMD TRIM-POT CROSS REFERENCE&lt;/B&gt;&lt;/FONT&gt;
      &lt;P&gt;
      &lt;FONT SIZE=4 FACE=ARIAL&gt;&lt;B&gt;MULTI-TURN&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;BOURNS&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;BI&amp;nbsp;TECH&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;DALE-VISHAY&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;PHILIPS/MEPCO&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;PANASONIC&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;TOCOS&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;AUX/KYOCERA&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      3224G&lt;BR&gt;
      3224J&lt;BR&gt;
      3224W&lt;BR&gt;
      3269P&lt;BR&gt;
      3269W&lt;BR&gt;
      3269X&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      44G&lt;BR&gt;
      44J&lt;BR&gt;
      44W&lt;BR&gt;
      84P&lt;BR&gt;
      84W&lt;BR&gt;
      84X&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      ST63Z&lt;BR&gt;
      ST63Y&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      ST5P&lt;BR&gt;
      ST5W&lt;BR&gt;
      ST5X&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD COLSPAN=7&gt;&amp;nbsp;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD COLSPAN=7&gt;
      &lt;FONT SIZE=4 FACE=ARIAL&gt;&lt;B&gt;SINGLE TURN&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;BOURNS&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;BI&amp;nbsp;TECH&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;DALE-VISHAY&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;PHILIPS/MEPCO&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;PANASONIC&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;TOCOS&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;AUX/KYOCERA&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      3314G&lt;BR&gt;
      3314J&lt;BR&gt;
      3364A/B&lt;BR&gt;
      3364C/D&lt;BR&gt;
      3364W/X&lt;BR&gt;
      3313G&lt;BR&gt;
      3313J&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      23B&lt;BR&gt;
      23A&lt;BR&gt;
      21X&lt;BR&gt;
      21W&lt;BR&gt;
      -&lt;BR&gt;
      22B&lt;BR&gt;
      22A&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      ST5YL/ST53YL&lt;BR&gt;
      ST5YJ/5T53YJ&lt;BR&gt;
      ST-23A&lt;BR&gt;
      ST-22B&lt;BR&gt;
      ST-22&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      ST-4B&lt;BR&gt;
      ST-4A&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      ST-3B&lt;BR&gt;
      ST-3A&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      EVM-6YS&lt;BR&gt;
      EVM-1E&lt;BR&gt;
      EVM-1G&lt;BR&gt;
      EVM-1D&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      G4B&lt;BR&gt;
      G4A&lt;BR&gt;
      TR04-3S1&lt;BR&gt;
      TRG04-2S1&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      DVR-43A&lt;BR&gt;
      CVR-42C&lt;BR&gt;
      CVR-42A/C&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
&lt;/TABLE&gt;
&lt;P&gt;
&lt;FONT SIZE=4 FACE=ARIAL&gt;&lt;B&gt;ALT =&amp;nbsp;ALTERNATE&lt;/B&gt;&lt;/FONT&gt;
&lt;P&gt;

&amp;nbsp;
&lt;P&gt;
&lt;/td&gt;
&lt;/tr&gt;
&lt;/table&gt;
&lt;/BODY&gt;&lt;/HTML&gt;</description>
<packages>
<package name="R0402">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="-0.245" y1="0.224" x2="0.245" y2="0.224" width="0.1524" layer="51"/>
<wire x1="0.245" y1="-0.224" x2="-0.245" y2="-0.224" width="0.1524" layer="51"/>
<wire x1="-1.473" y1="0.483" x2="1.473" y2="0.483" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.483" x2="1.473" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.483" x2="-1.473" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.483" x2="-1.473" y2="0.483" width="0.0508" layer="39"/>
<smd name="1" x="-0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<smd name="2" x="0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<text x="-0.635" y="0.635" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-1.905" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.554" y1="-0.3048" x2="-0.254" y2="0.2951" layer="51"/>
<rectangle x1="0.2588" y1="-0.3048" x2="0.5588" y2="0.2951" layer="51"/>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.1999" y2="0.4001" layer="35"/>
</package>
<package name="R0603">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="-0.432" y1="-0.356" x2="0.432" y2="-0.356" width="0.1524" layer="51"/>
<wire x1="0.432" y1="0.356" x2="-0.432" y2="0.356" width="0.1524" layer="51"/>
<wire x1="-1.473" y1="0.983" x2="1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.983" x2="1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.983" x2="-1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.983" x2="-1.473" y2="0.983" width="0.0508" layer="39"/>
<smd name="1" x="-0.85" y="0" dx="1" dy="1.1" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1" dy="1.1" layer="1"/>
<text x="-0.635" y="0.635" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-1.905" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="0.4318" y1="-0.4318" x2="0.8382" y2="0.4318" layer="51"/>
<rectangle x1="-0.8382" y1="-0.4318" x2="-0.4318" y2="0.4318" layer="51"/>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.1999" y2="0.4001" layer="35"/>
</package>
<package name="R0805">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;</description>
<wire x1="-0.41" y1="0.635" x2="0.41" y2="0.635" width="0.1524" layer="51"/>
<wire x1="-0.41" y1="-0.635" x2="0.41" y2="-0.635" width="0.1524" layer="51"/>
<wire x1="-1.973" y1="0.983" x2="1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="0.983" x2="1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="-0.983" x2="-1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.973" y1="-0.983" x2="-1.973" y2="0.983" width="0.0508" layer="39"/>
<smd name="1" x="-0.95" y="0" dx="1.3" dy="1.5" layer="1"/>
<smd name="2" x="0.95" y="0" dx="1.3" dy="1.5" layer="1"/>
<text x="-0.635" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="0.4064" y1="-0.6985" x2="1.0564" y2="0.7015" layer="51"/>
<rectangle x1="-1.0668" y1="-0.6985" x2="-0.4168" y2="0.7015" layer="51"/>
<rectangle x1="-0.1999" y1="-0.5001" x2="0.1999" y2="0.5001" layer="35"/>
</package>
<package name="R0805W">
<description>&lt;b&gt;RESISTOR&lt;/b&gt; wave soldering&lt;p&gt;</description>
<wire x1="-0.41" y1="0.635" x2="0.41" y2="0.635" width="0.1524" layer="51"/>
<wire x1="-0.41" y1="-0.635" x2="0.41" y2="-0.635" width="0.1524" layer="51"/>
<wire x1="-1.973" y1="0.983" x2="1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="0.983" x2="1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="-0.983" x2="-1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.973" y1="-0.983" x2="-1.973" y2="0.983" width="0.0508" layer="39"/>
<smd name="1" x="-1.0525" y="0" dx="1.5" dy="1" layer="1"/>
<smd name="2" x="1.0525" y="0" dx="1.5" dy="1" layer="1"/>
<text x="-0.635" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="0.4064" y1="-0.6985" x2="1.0564" y2="0.7015" layer="51"/>
<rectangle x1="-1.0668" y1="-0.6985" x2="-0.4168" y2="0.7015" layer="51"/>
<rectangle x1="-0.1999" y1="-0.5001" x2="0.1999" y2="0.5001" layer="35"/>
</package>
<package name="R1206">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="0.9525" y1="-0.8128" x2="-0.9652" y2="-0.8128" width="0.1524" layer="51"/>
<wire x1="0.9525" y1="0.8128" x2="-0.9652" y2="0.8128" width="0.1524" layer="51"/>
<wire x1="-2.473" y1="0.983" x2="2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="0.983" x2="2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-0.983" x2="-2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-0.983" x2="-2.473" y2="0.983" width="0.0508" layer="39"/>
<smd name="2" x="1.422" y="0" dx="1.6" dy="1.803" layer="1"/>
<smd name="1" x="-1.422" y="0" dx="1.6" dy="1.803" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.6891" y1="-0.8763" x2="-0.9525" y2="0.8763" layer="51"/>
<rectangle x1="0.9525" y1="-0.8763" x2="1.6891" y2="0.8763" layer="51"/>
<rectangle x1="-0.3" y1="-0.7" x2="0.3" y2="0.7" layer="35"/>
</package>
<package name="R1206W">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
wave soldering</description>
<wire x1="-0.913" y1="0.8" x2="0.888" y2="0.8" width="0.1524" layer="51"/>
<wire x1="-0.913" y1="-0.8" x2="0.888" y2="-0.8" width="0.1524" layer="51"/>
<wire x1="-2.473" y1="0.983" x2="2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="0.983" x2="2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-0.983" x2="-2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-0.983" x2="-2.473" y2="0.983" width="0.0508" layer="39"/>
<smd name="1" x="-1.499" y="0" dx="1.8" dy="1.2" layer="1"/>
<smd name="2" x="1.499" y="0" dx="1.8" dy="1.2" layer="1"/>
<text x="-1.905" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.651" y1="-0.8763" x2="-0.9009" y2="0.8738" layer="51"/>
<rectangle x1="0.889" y1="-0.8763" x2="1.6391" y2="0.8738" layer="51"/>
<rectangle x1="-0.3" y1="-0.7" x2="0.3" y2="0.7" layer="35"/>
</package>
<package name="R1210">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="-0.913" y1="1.219" x2="0.939" y2="1.219" width="0.1524" layer="51"/>
<wire x1="-0.913" y1="-1.219" x2="0.939" y2="-1.219" width="0.1524" layer="51"/>
<wire x1="-2.473" y1="1.483" x2="2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="1.483" x2="2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-1.483" x2="-2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-1.483" x2="-2.473" y2="1.483" width="0.0508" layer="39"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="2.7" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="2.7" layer="1"/>
<text x="-2.54" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.651" y1="-1.3081" x2="-0.9009" y2="1.2918" layer="51"/>
<rectangle x1="0.9144" y1="-1.3081" x2="1.6645" y2="1.2918" layer="51"/>
<rectangle x1="-0.3" y1="-0.8999" x2="0.3" y2="0.8999" layer="35"/>
</package>
<package name="R1210W">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
wave soldering</description>
<wire x1="-0.913" y1="1.219" x2="0.939" y2="1.219" width="0.1524" layer="51"/>
<wire x1="-0.913" y1="-1.219" x2="0.939" y2="-1.219" width="0.1524" layer="51"/>
<wire x1="-2.473" y1="1.483" x2="2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="1.483" x2="2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-1.483" x2="-2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-1.483" x2="-2.473" y2="1.483" width="0.0508" layer="39"/>
<smd name="1" x="-1.499" y="0" dx="1.8" dy="1.8" layer="1"/>
<smd name="2" x="1.499" y="0" dx="1.8" dy="1.8" layer="1"/>
<text x="-2.54" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.651" y1="-1.3081" x2="-0.9009" y2="1.2918" layer="51"/>
<rectangle x1="0.9144" y1="-1.3081" x2="1.6645" y2="1.2918" layer="51"/>
<rectangle x1="-0.3" y1="-0.8001" x2="0.3" y2="0.8001" layer="35"/>
</package>
<package name="R2010">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="-1.662" y1="1.245" x2="1.662" y2="1.245" width="0.1524" layer="51"/>
<wire x1="-1.637" y1="-1.245" x2="1.687" y2="-1.245" width="0.1524" layer="51"/>
<wire x1="-3.473" y1="1.483" x2="3.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="1.483" x2="3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="-1.483" x2="-3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-3.473" y1="-1.483" x2="-3.473" y2="1.483" width="0.0508" layer="39"/>
<smd name="1" x="-2.2" y="0" dx="1.8" dy="2.7" layer="1"/>
<smd name="2" x="2.2" y="0" dx="1.8" dy="2.7" layer="1"/>
<text x="-3.175" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-3.175" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-2.4892" y1="-1.3208" x2="-1.6393" y2="1.3292" layer="51"/>
<rectangle x1="1.651" y1="-1.3208" x2="2.5009" y2="1.3292" layer="51"/>
</package>
<package name="R2010W">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
wave soldering</description>
<wire x1="-1.662" y1="1.245" x2="1.662" y2="1.245" width="0.1524" layer="51"/>
<wire x1="-1.637" y1="-1.245" x2="1.687" y2="-1.245" width="0.1524" layer="51"/>
<wire x1="-3.473" y1="1.483" x2="3.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="1.483" x2="3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="-1.483" x2="-3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-3.473" y1="-1.483" x2="-3.473" y2="1.483" width="0.0508" layer="39"/>
<smd name="1" x="-2.311" y="0" dx="2" dy="1.8" layer="1"/>
<smd name="2" x="2.311" y="0" dx="2" dy="1.8" layer="1"/>
<text x="-2.54" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-2.4892" y1="-1.3208" x2="-1.6393" y2="1.3292" layer="51"/>
<rectangle x1="1.651" y1="-1.3208" x2="2.5009" y2="1.3292" layer="51"/>
</package>
<package name="R2012">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="-0.41" y1="0.635" x2="0.41" y2="0.635" width="0.1524" layer="51"/>
<wire x1="-0.41" y1="-0.635" x2="0.41" y2="-0.635" width="0.1524" layer="51"/>
<wire x1="-1.973" y1="0.983" x2="1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="0.983" x2="1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="-0.983" x2="-1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.973" y1="-0.983" x2="-1.973" y2="0.983" width="0.0508" layer="39"/>
<smd name="1" x="-0.85" y="0" dx="1.3" dy="1.5" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1.3" dy="1.5" layer="1"/>
<text x="-0.635" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="0.4064" y1="-0.6985" x2="1.0564" y2="0.7015" layer="51"/>
<rectangle x1="-1.0668" y1="-0.6985" x2="-0.4168" y2="0.7015" layer="51"/>
<rectangle x1="-0.1001" y1="-0.5999" x2="0.1001" y2="0.5999" layer="35"/>
</package>
<package name="R2012W">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
wave soldering</description>
<wire x1="-0.41" y1="0.635" x2="0.41" y2="0.635" width="0.1524" layer="51"/>
<wire x1="-0.41" y1="-0.635" x2="0.41" y2="-0.635" width="0.1524" layer="51"/>
<wire x1="-1.973" y1="0.983" x2="1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="0.983" x2="1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="-0.983" x2="-1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.973" y1="-0.983" x2="-1.973" y2="0.983" width="0.0508" layer="39"/>
<smd name="1" x="-0.94" y="0" dx="1.5" dy="1" layer="1"/>
<smd name="2" x="0.94" y="0" dx="1.5" dy="1" layer="1"/>
<text x="-0.635" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="0.4064" y1="-0.6985" x2="1.0564" y2="0.7015" layer="51"/>
<rectangle x1="-1.0668" y1="-0.6985" x2="-0.4168" y2="0.7015" layer="51"/>
<rectangle x1="-0.1001" y1="-0.5999" x2="0.1001" y2="0.5999" layer="35"/>
</package>
<package name="R2512">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="-2.362" y1="1.473" x2="2.387" y2="1.473" width="0.1524" layer="51"/>
<wire x1="-2.362" y1="-1.473" x2="2.387" y2="-1.473" width="0.1524" layer="51"/>
<wire x1="-3.973" y1="1.983" x2="3.973" y2="1.983" width="0.0508" layer="39"/>
<wire x1="3.973" y1="1.983" x2="3.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="3.973" y1="-1.983" x2="-3.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="-3.973" y1="-1.983" x2="-3.973" y2="1.983" width="0.0508" layer="39"/>
<smd name="1" x="-2.8" y="0" dx="1.8" dy="3.2" layer="1"/>
<smd name="2" x="2.8" y="0" dx="1.8" dy="3.2" layer="1"/>
<text x="-2.54" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-3.2004" y1="-1.5494" x2="-2.3505" y2="1.5507" layer="51"/>
<rectangle x1="2.3622" y1="-1.5494" x2="3.2121" y2="1.5507" layer="51"/>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
</package>
<package name="R2512W">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
wave soldering</description>
<wire x1="-2.362" y1="1.473" x2="2.387" y2="1.473" width="0.1524" layer="51"/>
<wire x1="-2.362" y1="-1.473" x2="2.387" y2="-1.473" width="0.1524" layer="51"/>
<wire x1="-3.973" y1="1.983" x2="3.973" y2="1.983" width="0.0508" layer="39"/>
<wire x1="3.973" y1="1.983" x2="3.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="3.973" y1="-1.983" x2="-3.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="-3.973" y1="-1.983" x2="-3.973" y2="1.983" width="0.0508" layer="39"/>
<smd name="1" x="-2.896" y="0" dx="2" dy="2.1" layer="1"/>
<smd name="2" x="2.896" y="0" dx="2" dy="2.1" layer="1"/>
<text x="-1.905" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-3.2004" y1="-1.5494" x2="-2.3505" y2="1.5507" layer="51"/>
<rectangle x1="2.3622" y1="-1.5494" x2="3.2121" y2="1.5507" layer="51"/>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
</package>
<package name="R3216">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="-0.913" y1="0.8" x2="0.888" y2="0.8" width="0.1524" layer="51"/>
<wire x1="-0.913" y1="-0.8" x2="0.888" y2="-0.8" width="0.1524" layer="51"/>
<wire x1="-2.473" y1="0.983" x2="2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="0.983" x2="2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-0.983" x2="-2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-0.983" x2="-2.473" y2="0.983" width="0.0508" layer="39"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<text x="-1.905" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.651" y1="-0.8763" x2="-0.9009" y2="0.8738" layer="51"/>
<rectangle x1="0.889" y1="-0.8763" x2="1.6391" y2="0.8738" layer="51"/>
<rectangle x1="-0.3" y1="-0.7" x2="0.3" y2="0.7" layer="35"/>
</package>
<package name="R3216W">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
wave soldering</description>
<wire x1="-0.913" y1="0.8" x2="0.888" y2="0.8" width="0.1524" layer="51"/>
<wire x1="-0.913" y1="-0.8" x2="0.888" y2="-0.8" width="0.1524" layer="51"/>
<wire x1="-2.473" y1="0.983" x2="2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="0.983" x2="2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-0.983" x2="-2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-0.983" x2="-2.473" y2="0.983" width="0.0508" layer="39"/>
<smd name="1" x="-1.499" y="0" dx="1.8" dy="1.2" layer="1"/>
<smd name="2" x="1.499" y="0" dx="1.8" dy="1.2" layer="1"/>
<text x="-1.905" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.651" y1="-0.8763" x2="-0.9009" y2="0.8738" layer="51"/>
<rectangle x1="0.889" y1="-0.8763" x2="1.6391" y2="0.8738" layer="51"/>
<rectangle x1="-0.3" y1="-0.7" x2="0.3" y2="0.7" layer="35"/>
</package>
<package name="R3225">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="-0.913" y1="1.219" x2="0.939" y2="1.219" width="0.1524" layer="51"/>
<wire x1="-0.913" y1="-1.219" x2="0.939" y2="-1.219" width="0.1524" layer="51"/>
<wire x1="-2.473" y1="1.483" x2="2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="1.483" x2="2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-1.483" x2="-2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-1.483" x2="-2.473" y2="1.483" width="0.0508" layer="39"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="2.7" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="2.7" layer="1"/>
<text x="-2.54" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.651" y1="-1.3081" x2="-0.9009" y2="1.2918" layer="51"/>
<rectangle x1="0.9144" y1="-1.3081" x2="1.6645" y2="1.2918" layer="51"/>
<rectangle x1="-0.3" y1="-1" x2="0.3" y2="1" layer="35"/>
</package>
<package name="R3225W">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
wave soldering</description>
<wire x1="-0.913" y1="1.219" x2="0.939" y2="1.219" width="0.1524" layer="51"/>
<wire x1="-0.913" y1="-1.219" x2="0.939" y2="-1.219" width="0.1524" layer="51"/>
<wire x1="-2.473" y1="1.483" x2="2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="1.483" x2="2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-1.483" x2="-2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-1.483" x2="-2.473" y2="1.483" width="0.0508" layer="39"/>
<smd name="1" x="-1.499" y="0" dx="1.8" dy="1.8" layer="1"/>
<smd name="2" x="1.499" y="0" dx="1.8" dy="1.8" layer="1"/>
<text x="-1.905" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.651" y1="-1.3081" x2="-0.9009" y2="1.2918" layer="51"/>
<rectangle x1="0.9144" y1="-1.3081" x2="1.6645" y2="1.2918" layer="51"/>
<rectangle x1="-0.3" y1="-1" x2="0.3" y2="1" layer="35"/>
</package>
<package name="R5025">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="-1.662" y1="1.245" x2="1.662" y2="1.245" width="0.1524" layer="51"/>
<wire x1="-1.637" y1="-1.245" x2="1.687" y2="-1.245" width="0.1524" layer="51"/>
<wire x1="-3.473" y1="1.483" x2="3.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="1.483" x2="3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="-1.483" x2="-3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-3.473" y1="-1.483" x2="-3.473" y2="1.483" width="0.0508" layer="39"/>
<smd name="1" x="-2.2" y="0" dx="1.8" dy="2.7" layer="1"/>
<smd name="2" x="2.2" y="0" dx="1.8" dy="2.7" layer="1"/>
<text x="-3.175" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-3.175" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-2.4892" y1="-1.3208" x2="-1.6393" y2="1.3292" layer="51"/>
<rectangle x1="1.651" y1="-1.3208" x2="2.5009" y2="1.3292" layer="51"/>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
</package>
<package name="R5025W">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
wave soldering</description>
<wire x1="-1.662" y1="1.245" x2="1.662" y2="1.245" width="0.1524" layer="51"/>
<wire x1="-1.637" y1="-1.245" x2="1.687" y2="-1.245" width="0.1524" layer="51"/>
<wire x1="-3.473" y1="1.483" x2="3.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="1.483" x2="3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="-1.483" x2="-3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-3.473" y1="-1.483" x2="-3.473" y2="1.483" width="0.0508" layer="39"/>
<smd name="1" x="-2.311" y="0" dx="2" dy="1.8" layer="1"/>
<smd name="2" x="2.311" y="0" dx="2" dy="1.8" layer="1"/>
<text x="-3.175" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-3.175" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-2.4892" y1="-1.3208" x2="-1.6393" y2="1.3292" layer="51"/>
<rectangle x1="1.651" y1="-1.3208" x2="2.5009" y2="1.3292" layer="51"/>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
</package>
<package name="R6332">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
Source: http://download.siliconexpert.com/pdfs/2005/02/24/Semi_Ap/2/VSH/Resistor/dcrcwfre.pdf</description>
<wire x1="-2.362" y1="1.473" x2="2.387" y2="1.473" width="0.1524" layer="51"/>
<wire x1="-2.362" y1="-1.473" x2="2.387" y2="-1.473" width="0.1524" layer="51"/>
<wire x1="-3.973" y1="1.983" x2="3.973" y2="1.983" width="0.0508" layer="39"/>
<wire x1="3.973" y1="1.983" x2="3.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="3.973" y1="-1.983" x2="-3.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="-3.973" y1="-1.983" x2="-3.973" y2="1.983" width="0.0508" layer="39"/>
<smd name="1" x="-3.1" y="0" dx="1" dy="3.2" layer="1"/>
<smd name="2" x="3.1" y="0" dx="1" dy="3.2" layer="1"/>
<text x="-2.54" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-3.2004" y1="-1.5494" x2="-2.3505" y2="1.5507" layer="51"/>
<rectangle x1="2.3622" y1="-1.5494" x2="3.2121" y2="1.5507" layer="51"/>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
</package>
<package name="R6332W">
<description>&lt;b&gt;RESISTOR&lt;/b&gt; wave soldering&lt;p&gt;
Source: http://download.siliconexpert.com/pdfs/2005/02/24/Semi_Ap/2/VSH/Resistor/dcrcwfre.pdf</description>
<wire x1="-2.362" y1="1.473" x2="2.387" y2="1.473" width="0.1524" layer="51"/>
<wire x1="-2.362" y1="-1.473" x2="2.387" y2="-1.473" width="0.1524" layer="51"/>
<wire x1="-3.973" y1="1.983" x2="3.973" y2="1.983" width="0.0508" layer="39"/>
<wire x1="3.973" y1="1.983" x2="3.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="3.973" y1="-1.983" x2="-3.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="-3.973" y1="-1.983" x2="-3.973" y2="1.983" width="0.0508" layer="39"/>
<smd name="1" x="-3.196" y="0" dx="1.2" dy="3.2" layer="1"/>
<smd name="2" x="3.196" y="0" dx="1.2" dy="3.2" layer="1"/>
<text x="-2.54" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-3.2004" y1="-1.5494" x2="-2.3505" y2="1.5507" layer="51"/>
<rectangle x1="2.3622" y1="-1.5494" x2="3.2121" y2="1.5507" layer="51"/>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
</package>
<package name="M0805">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
MELF 0.10 W</description>
<wire x1="-1.973" y1="0.983" x2="1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="-0.983" x2="-1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.973" y1="-0.983" x2="-1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="0.983" x2="1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="0.7112" y1="0.635" x2="-0.7112" y2="0.635" width="0.1524" layer="51"/>
<wire x1="0.7112" y1="-0.635" x2="-0.7112" y2="-0.635" width="0.1524" layer="51"/>
<smd name="1" x="-0.95" y="0" dx="1.3" dy="1.6" layer="1"/>
<smd name="2" x="0.95" y="0" dx="1.3" dy="1.6" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.0414" y1="-0.7112" x2="-0.6858" y2="0.7112" layer="51"/>
<rectangle x1="0.6858" y1="-0.7112" x2="1.0414" y2="0.7112" layer="51"/>
<rectangle x1="-0.1999" y1="-0.5999" x2="0.1999" y2="0.5999" layer="35"/>
</package>
<package name="M1206">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
MELF 0.25 W</description>
<wire x1="-2.473" y1="1.483" x2="2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-1.483" x2="-2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-1.483" x2="-2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="1.483" x2="2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="1.143" y1="0.8382" x2="-1.143" y2="0.8382" width="0.1524" layer="51"/>
<wire x1="1.143" y1="-0.8382" x2="-1.143" y2="-0.8382" width="0.1524" layer="51"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="2" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="2" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.7018" y1="-0.9144" x2="-1.1176" y2="0.9144" layer="51"/>
<rectangle x1="1.1176" y1="-0.9144" x2="1.7018" y2="0.9144" layer="51"/>
<rectangle x1="-0.3" y1="-0.8001" x2="0.3" y2="0.8001" layer="35"/>
</package>
<package name="M1406">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
MELF 0.12 W</description>
<wire x1="-2.973" y1="0.983" x2="2.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.973" y1="-0.983" x2="-2.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.973" y1="-0.983" x2="-2.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.973" y1="0.983" x2="2.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.3208" y1="0.762" x2="-1.3208" y2="0.762" width="0.1524" layer="51"/>
<wire x1="1.3208" y1="-0.762" x2="-1.3208" y2="-0.762" width="0.1524" layer="51"/>
<smd name="1" x="-1.7" y="0" dx="1.4" dy="1.8" layer="1"/>
<smd name="2" x="1.7" y="0" dx="1.4" dy="1.8" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.8542" y1="-0.8382" x2="-1.2954" y2="0.8382" layer="51"/>
<rectangle x1="1.2954" y1="-0.8382" x2="1.8542" y2="0.8382" layer="51"/>
<rectangle x1="-0.3" y1="-0.7" x2="0.3" y2="0.7" layer="35"/>
</package>
<package name="M2012">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
MELF 0.10 W</description>
<wire x1="-1.973" y1="0.983" x2="1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="-0.983" x2="-1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.973" y1="-0.983" x2="-1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="0.983" x2="1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="0.7112" y1="0.635" x2="-0.7112" y2="0.635" width="0.1524" layer="51"/>
<wire x1="0.7112" y1="-0.635" x2="-0.7112" y2="-0.635" width="0.1524" layer="51"/>
<smd name="1" x="-0.95" y="0" dx="1.3" dy="1.6" layer="1"/>
<smd name="2" x="0.95" y="0" dx="1.3" dy="1.6" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.0414" y1="-0.7112" x2="-0.6858" y2="0.7112" layer="51"/>
<rectangle x1="0.6858" y1="-0.7112" x2="1.0414" y2="0.7112" layer="51"/>
<rectangle x1="-0.1999" y1="-0.5999" x2="0.1999" y2="0.5999" layer="35"/>
</package>
<package name="M2309">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
MELF 0.25 W</description>
<wire x1="-4.473" y1="1.483" x2="4.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="4.473" y1="-1.483" x2="-4.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-4.473" y1="-1.483" x2="-4.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="4.473" y1="1.483" x2="4.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="2.413" y1="1.1684" x2="-2.4384" y2="1.1684" width="0.1524" layer="51"/>
<wire x1="2.413" y1="-1.1684" x2="-2.413" y2="-1.1684" width="0.1524" layer="51"/>
<smd name="1" x="-2.85" y="0" dx="1.5" dy="2.6" layer="1"/>
<smd name="2" x="2.85" y="0" dx="1.5" dy="2.6" layer="1"/>
<text x="-1.905" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-3.048" y1="-1.2446" x2="-2.3876" y2="1.2446" layer="51"/>
<rectangle x1="2.3876" y1="-1.2446" x2="3.048" y2="1.2446" layer="51"/>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
</package>
<package name="M3216">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
MELF 0.25 W</description>
<wire x1="-2.473" y1="1.483" x2="2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-1.483" x2="-2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-1.483" x2="-2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="1.483" x2="2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="1.143" y1="0.8382" x2="-1.143" y2="0.8382" width="0.1524" layer="51"/>
<wire x1="1.143" y1="-0.8382" x2="-1.143" y2="-0.8382" width="0.1524" layer="51"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="2" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="2" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.7018" y1="-0.9144" x2="-1.1176" y2="0.9144" layer="51"/>
<rectangle x1="1.1176" y1="-0.9144" x2="1.7018" y2="0.9144" layer="51"/>
<rectangle x1="-0.3" y1="-0.8001" x2="0.3" y2="0.8001" layer="35"/>
</package>
<package name="M3516">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
MELF 0.12 W</description>
<wire x1="-2.973" y1="0.983" x2="2.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.973" y1="-0.983" x2="-2.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.973" y1="-0.983" x2="-2.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.973" y1="0.983" x2="2.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.3208" y1="0.762" x2="-1.3208" y2="0.762" width="0.1524" layer="51"/>
<wire x1="1.3208" y1="-0.762" x2="-1.3208" y2="-0.762" width="0.1524" layer="51"/>
<smd name="1" x="-1.7" y="0" dx="1.4" dy="1.8" layer="1"/>
<smd name="2" x="1.7" y="0" dx="1.4" dy="1.8" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.8542" y1="-0.8382" x2="-1.2954" y2="0.8382" layer="51"/>
<rectangle x1="1.2954" y1="-0.8382" x2="1.8542" y2="0.8382" layer="51"/>
<rectangle x1="-0.4001" y1="-0.7" x2="0.4001" y2="0.7" layer="35"/>
</package>
<package name="M5923">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
MELF 0.25 W</description>
<wire x1="-4.473" y1="1.483" x2="4.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="4.473" y1="-1.483" x2="-4.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-4.473" y1="-1.483" x2="-4.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="4.473" y1="1.483" x2="4.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="2.413" y1="1.1684" x2="-2.4384" y2="1.1684" width="0.1524" layer="51"/>
<wire x1="2.413" y1="-1.1684" x2="-2.413" y2="-1.1684" width="0.1524" layer="51"/>
<smd name="1" x="-2.85" y="0" dx="1.5" dy="2.6" layer="1"/>
<smd name="2" x="2.85" y="0" dx="1.5" dy="2.6" layer="1"/>
<text x="-1.905" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-3.048" y1="-1.2446" x2="-2.3876" y2="1.2446" layer="51"/>
<rectangle x1="2.3876" y1="-1.2446" x2="3.048" y2="1.2446" layer="51"/>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
</package>
<package name="0204/5">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0204, grid 5 mm</description>
<wire x1="2.54" y1="0" x2="2.032" y2="0" width="0.508" layer="51"/>
<wire x1="-2.54" y1="0" x2="-2.032" y2="0" width="0.508" layer="51"/>
<wire x1="-1.778" y1="0.635" x2="-1.524" y2="0.889" width="0.1524" layer="21" curve="-90"/>
<wire x1="-1.778" y1="-0.635" x2="-1.524" y2="-0.889" width="0.1524" layer="21" curve="90"/>
<wire x1="1.524" y1="-0.889" x2="1.778" y2="-0.635" width="0.1524" layer="21" curve="90"/>
<wire x1="1.524" y1="0.889" x2="1.778" y2="0.635" width="0.1524" layer="21" curve="-90"/>
<wire x1="-1.778" y1="-0.635" x2="-1.778" y2="0.635" width="0.1524" layer="51"/>
<wire x1="-1.524" y1="0.889" x2="-1.27" y2="0.889" width="0.1524" layer="21"/>
<wire x1="-1.143" y1="0.762" x2="-1.27" y2="0.889" width="0.1524" layer="21"/>
<wire x1="-1.524" y1="-0.889" x2="-1.27" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="-1.143" y1="-0.762" x2="-1.27" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="1.143" y1="0.762" x2="1.27" y2="0.889" width="0.1524" layer="21"/>
<wire x1="1.143" y1="0.762" x2="-1.143" y2="0.762" width="0.1524" layer="21"/>
<wire x1="1.143" y1="-0.762" x2="1.27" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="1.143" y1="-0.762" x2="-1.143" y2="-0.762" width="0.1524" layer="21"/>
<wire x1="1.524" y1="0.889" x2="1.27" y2="0.889" width="0.1524" layer="21"/>
<wire x1="1.524" y1="-0.889" x2="1.27" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="1.778" y1="-0.635" x2="1.778" y2="0.635" width="0.1524" layer="51"/>
<pad name="1" x="-2.54" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.0066" y="1.1684" size="0.9906" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.1336" y="-2.3114" size="0.9906" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-2.032" y1="-0.254" x2="-1.778" y2="0.254" layer="51"/>
<rectangle x1="1.778" y1="-0.254" x2="2.032" y2="0.254" layer="51"/>
</package>
<package name="0204/7">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0204, grid 7.5 mm</description>
<wire x1="3.81" y1="0" x2="2.921" y2="0" width="0.508" layer="51"/>
<wire x1="-3.81" y1="0" x2="-2.921" y2="0" width="0.508" layer="51"/>
<wire x1="-2.54" y1="0.762" x2="-2.286" y2="1.016" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.54" y1="-0.762" x2="-2.286" y2="-1.016" width="0.1524" layer="21" curve="90"/>
<wire x1="2.286" y1="-1.016" x2="2.54" y2="-0.762" width="0.1524" layer="21" curve="90"/>
<wire x1="2.286" y1="1.016" x2="2.54" y2="0.762" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.54" y1="-0.762" x2="-2.54" y2="0.762" width="0.1524" layer="21"/>
<wire x1="-2.286" y1="1.016" x2="-1.905" y2="1.016" width="0.1524" layer="21"/>
<wire x1="-1.778" y1="0.889" x2="-1.905" y2="1.016" width="0.1524" layer="21"/>
<wire x1="-2.286" y1="-1.016" x2="-1.905" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="-1.778" y1="-0.889" x2="-1.905" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="1.778" y1="0.889" x2="1.905" y2="1.016" width="0.1524" layer="21"/>
<wire x1="1.778" y1="0.889" x2="-1.778" y2="0.889" width="0.1524" layer="21"/>
<wire x1="1.778" y1="-0.889" x2="1.905" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="1.778" y1="-0.889" x2="-1.778" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="2.286" y1="1.016" x2="1.905" y2="1.016" width="0.1524" layer="21"/>
<wire x1="2.286" y1="-1.016" x2="1.905" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-0.762" x2="2.54" y2="0.762" width="0.1524" layer="21"/>
<pad name="1" x="-3.81" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="3.81" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.54" y="1.2954" size="0.9906" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.6256" y="-0.4826" size="0.9906" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="2.54" y1="-0.254" x2="2.921" y2="0.254" layer="21"/>
<rectangle x1="-2.921" y1="-0.254" x2="-2.54" y2="0.254" layer="21"/>
</package>
<package name="0204V">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0204, grid 2.5 mm</description>
<wire x1="-1.27" y1="0" x2="1.27" y2="0" width="0.508" layer="51"/>
<wire x1="-0.127" y1="0" x2="0.127" y2="0" width="0.508" layer="21"/>
<circle x="-1.27" y="0" radius="0.889" width="0.1524" layer="51"/>
<circle x="-1.27" y="0" radius="0.635" width="0.0508" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.1336" y="1.1684" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.1336" y="-2.3114" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="0207/10">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0207, grid 10 mm</description>
<wire x1="5.08" y1="0" x2="4.064" y2="0" width="0.6096" layer="51"/>
<wire x1="-5.08" y1="0" x2="-4.064" y2="0" width="0.6096" layer="51"/>
<wire x1="-3.175" y1="0.889" x2="-2.921" y2="1.143" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-0.889" x2="-2.921" y2="-1.143" width="0.1524" layer="21" curve="90"/>
<wire x1="2.921" y1="-1.143" x2="3.175" y2="-0.889" width="0.1524" layer="21" curve="90"/>
<wire x1="2.921" y1="1.143" x2="3.175" y2="0.889" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-0.889" x2="-3.175" y2="0.889" width="0.1524" layer="21"/>
<wire x1="-2.921" y1="1.143" x2="-2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="1.016" x2="-2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.921" y1="-1.143" x2="-2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="-1.016" x2="-2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="-2.413" y2="1.016" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="-2.413" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="2.921" y1="1.143" x2="2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.921" y1="-1.143" x2="2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-0.889" x2="3.175" y2="0.889" width="0.1524" layer="21"/>
<pad name="1" x="-5.08" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="5.08" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.048" y="1.524" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.2606" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="3.175" y1="-0.3048" x2="4.0386" y2="0.3048" layer="21"/>
<rectangle x1="-4.0386" y1="-0.3048" x2="-3.175" y2="0.3048" layer="21"/>
</package>
<package name="0207/12">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0207, grid 12 mm</description>
<wire x1="6.35" y1="0" x2="5.334" y2="0" width="0.6096" layer="51"/>
<wire x1="-6.35" y1="0" x2="-5.334" y2="0" width="0.6096" layer="51"/>
<wire x1="-3.175" y1="0.889" x2="-2.921" y2="1.143" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-0.889" x2="-2.921" y2="-1.143" width="0.1524" layer="21" curve="90"/>
<wire x1="2.921" y1="-1.143" x2="3.175" y2="-0.889" width="0.1524" layer="21" curve="90"/>
<wire x1="2.921" y1="1.143" x2="3.175" y2="0.889" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-0.889" x2="-3.175" y2="0.889" width="0.1524" layer="21"/>
<wire x1="-2.921" y1="1.143" x2="-2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="1.016" x2="-2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.921" y1="-1.143" x2="-2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="-1.016" x2="-2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="-2.413" y2="1.016" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="-2.413" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="2.921" y1="1.143" x2="2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.921" y1="-1.143" x2="2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-0.889" x2="3.175" y2="0.889" width="0.1524" layer="21"/>
<wire x1="4.445" y1="0" x2="4.064" y2="0" width="0.6096" layer="21"/>
<wire x1="-4.445" y1="0" x2="-4.064" y2="0" width="0.6096" layer="21"/>
<pad name="1" x="-6.35" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="6.35" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.175" y="1.397" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.286" y="-0.6858" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="3.175" y1="-0.3048" x2="4.0386" y2="0.3048" layer="21"/>
<rectangle x1="-4.0386" y1="-0.3048" x2="-3.175" y2="0.3048" layer="21"/>
<rectangle x1="4.445" y1="-0.3048" x2="5.3086" y2="0.3048" layer="21"/>
<rectangle x1="-5.3086" y1="-0.3048" x2="-4.445" y2="0.3048" layer="21"/>
</package>
<package name="0207/15">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0207, grid 15mm</description>
<wire x1="7.62" y1="0" x2="6.604" y2="0" width="0.6096" layer="51"/>
<wire x1="-7.62" y1="0" x2="-6.604" y2="0" width="0.6096" layer="51"/>
<wire x1="-3.175" y1="0.889" x2="-2.921" y2="1.143" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-0.889" x2="-2.921" y2="-1.143" width="0.1524" layer="21" curve="90"/>
<wire x1="2.921" y1="-1.143" x2="3.175" y2="-0.889" width="0.1524" layer="21" curve="90"/>
<wire x1="2.921" y1="1.143" x2="3.175" y2="0.889" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-0.889" x2="-3.175" y2="0.889" width="0.1524" layer="21"/>
<wire x1="-2.921" y1="1.143" x2="-2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="1.016" x2="-2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.921" y1="-1.143" x2="-2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="-1.016" x2="-2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="-2.413" y2="1.016" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="-2.413" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="2.921" y1="1.143" x2="2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.921" y1="-1.143" x2="2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-0.889" x2="3.175" y2="0.889" width="0.1524" layer="21"/>
<wire x1="5.715" y1="0" x2="4.064" y2="0" width="0.6096" layer="21"/>
<wire x1="-5.715" y1="0" x2="-4.064" y2="0" width="0.6096" layer="21"/>
<pad name="1" x="-7.62" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="7.62" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.175" y="1.397" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.286" y="-0.6858" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="3.175" y1="-0.3048" x2="4.0386" y2="0.3048" layer="21"/>
<rectangle x1="-4.0386" y1="-0.3048" x2="-3.175" y2="0.3048" layer="21"/>
<rectangle x1="5.715" y1="-0.3048" x2="6.5786" y2="0.3048" layer="21"/>
<rectangle x1="-6.5786" y1="-0.3048" x2="-5.715" y2="0.3048" layer="21"/>
</package>
<package name="0207/2V">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0207, grid 2.5 mm</description>
<wire x1="-1.27" y1="0" x2="-0.381" y2="0" width="0.6096" layer="51"/>
<wire x1="-0.254" y1="0" x2="0.254" y2="0" width="0.6096" layer="21"/>
<wire x1="0.381" y1="0" x2="1.27" y2="0" width="0.6096" layer="51"/>
<circle x="-1.27" y="0" radius="1.27" width="0.1524" layer="21"/>
<circle x="-1.27" y="0" radius="1.016" width="0.1524" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-0.0508" y="1.016" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-0.0508" y="-2.2352" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="0207/5V">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0207, grid 5 mm</description>
<wire x1="-2.54" y1="0" x2="-0.889" y2="0" width="0.6096" layer="51"/>
<wire x1="-0.762" y1="0" x2="0.762" y2="0" width="0.6096" layer="21"/>
<wire x1="0.889" y1="0" x2="2.54" y2="0" width="0.6096" layer="51"/>
<circle x="-2.54" y="0" radius="1.27" width="0.1016" layer="21"/>
<circle x="-2.54" y="0" radius="1.016" width="0.1524" layer="51"/>
<pad name="1" x="-2.54" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="0.8128" shape="octagon"/>
<text x="-1.143" y="0.889" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.143" y="-2.159" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="0207/7">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0207, grid 7.5 mm</description>
<wire x1="-3.81" y1="0" x2="-3.429" y2="0" width="0.6096" layer="51"/>
<wire x1="-3.175" y1="0.889" x2="-2.921" y2="1.143" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-0.889" x2="-2.921" y2="-1.143" width="0.1524" layer="21" curve="90"/>
<wire x1="2.921" y1="-1.143" x2="3.175" y2="-0.889" width="0.1524" layer="21" curve="90"/>
<wire x1="2.921" y1="1.143" x2="3.175" y2="0.889" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-0.889" x2="-3.175" y2="0.889" width="0.1524" layer="51"/>
<wire x1="-2.921" y1="1.143" x2="-2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="1.016" x2="-2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.921" y1="-1.143" x2="-2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="-1.016" x2="-2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="-2.413" y2="1.016" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="-2.413" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="2.921" y1="1.143" x2="2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.921" y1="-1.143" x2="2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-0.889" x2="3.175" y2="0.889" width="0.1524" layer="51"/>
<wire x1="3.429" y1="0" x2="3.81" y2="0" width="0.6096" layer="51"/>
<pad name="1" x="-3.81" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="3.81" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.54" y="1.397" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.286" y="-0.5588" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-3.429" y1="-0.3048" x2="-3.175" y2="0.3048" layer="51"/>
<rectangle x1="3.175" y1="-0.3048" x2="3.429" y2="0.3048" layer="51"/>
</package>
<package name="0309/10">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0309, grid 10mm</description>
<wire x1="-4.699" y1="0" x2="-5.08" y2="0" width="0.6096" layer="51"/>
<wire x1="-4.318" y1="1.27" x2="-4.064" y2="1.524" width="0.1524" layer="21" curve="-90"/>
<wire x1="-4.318" y1="-1.27" x2="-4.064" y2="-1.524" width="0.1524" layer="21" curve="90"/>
<wire x1="4.064" y1="-1.524" x2="4.318" y2="-1.27" width="0.1524" layer="21" curve="90"/>
<wire x1="4.064" y1="1.524" x2="4.318" y2="1.27" width="0.1524" layer="21" curve="-90"/>
<wire x1="-4.318" y1="-1.27" x2="-4.318" y2="1.27" width="0.1524" layer="51"/>
<wire x1="-4.064" y1="1.524" x2="-3.429" y2="1.524" width="0.1524" layer="21"/>
<wire x1="-3.302" y1="1.397" x2="-3.429" y2="1.524" width="0.1524" layer="21"/>
<wire x1="-4.064" y1="-1.524" x2="-3.429" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="-3.302" y1="-1.397" x2="-3.429" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="3.302" y1="1.397" x2="3.429" y2="1.524" width="0.1524" layer="21"/>
<wire x1="3.302" y1="1.397" x2="-3.302" y2="1.397" width="0.1524" layer="21"/>
<wire x1="3.302" y1="-1.397" x2="3.429" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="3.302" y1="-1.397" x2="-3.302" y2="-1.397" width="0.1524" layer="21"/>
<wire x1="4.064" y1="1.524" x2="3.429" y2="1.524" width="0.1524" layer="21"/>
<wire x1="4.064" y1="-1.524" x2="3.429" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="4.318" y1="-1.27" x2="4.318" y2="1.27" width="0.1524" layer="51"/>
<wire x1="5.08" y1="0" x2="4.699" y2="0" width="0.6096" layer="51"/>
<pad name="1" x="-5.08" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="5.08" y="0" drill="0.8128" shape="octagon"/>
<text x="-4.191" y="1.905" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.175" y="-0.6858" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-4.6228" y1="-0.3048" x2="-4.318" y2="0.3048" layer="51"/>
<rectangle x1="4.318" y1="-0.3048" x2="4.6228" y2="0.3048" layer="51"/>
</package>
<package name="0309/12">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0309, grid 12.5 mm</description>
<wire x1="6.35" y1="0" x2="5.08" y2="0" width="0.6096" layer="51"/>
<wire x1="-6.35" y1="0" x2="-5.08" y2="0" width="0.6096" layer="51"/>
<wire x1="-4.318" y1="1.27" x2="-4.064" y2="1.524" width="0.1524" layer="21" curve="-90"/>
<wire x1="-4.318" y1="-1.27" x2="-4.064" y2="-1.524" width="0.1524" layer="21" curve="90"/>
<wire x1="4.064" y1="-1.524" x2="4.318" y2="-1.27" width="0.1524" layer="21" curve="90"/>
<wire x1="4.064" y1="1.524" x2="4.318" y2="1.27" width="0.1524" layer="21" curve="-90"/>
<wire x1="-4.318" y1="-1.27" x2="-4.318" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-4.064" y1="1.524" x2="-3.429" y2="1.524" width="0.1524" layer="21"/>
<wire x1="-3.302" y1="1.397" x2="-3.429" y2="1.524" width="0.1524" layer="21"/>
<wire x1="-4.064" y1="-1.524" x2="-3.429" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="-3.302" y1="-1.397" x2="-3.429" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="3.302" y1="1.397" x2="3.429" y2="1.524" width="0.1524" layer="21"/>
<wire x1="3.302" y1="1.397" x2="-3.302" y2="1.397" width="0.1524" layer="21"/>
<wire x1="3.302" y1="-1.397" x2="3.429" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="3.302" y1="-1.397" x2="-3.302" y2="-1.397" width="0.1524" layer="21"/>
<wire x1="4.064" y1="1.524" x2="3.429" y2="1.524" width="0.1524" layer="21"/>
<wire x1="4.064" y1="-1.524" x2="3.429" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="4.318" y1="-1.27" x2="4.318" y2="1.27" width="0.1524" layer="21"/>
<pad name="1" x="-6.35" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="6.35" y="0" drill="0.8128" shape="octagon"/>
<text x="-4.191" y="1.905" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.175" y="-0.6858" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="4.318" y1="-0.3048" x2="5.1816" y2="0.3048" layer="21"/>
<rectangle x1="-5.1816" y1="-0.3048" x2="-4.318" y2="0.3048" layer="21"/>
</package>
<package name="0309V">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0309, grid 2.5 mm</description>
<wire x1="1.27" y1="0" x2="0.635" y2="0" width="0.6096" layer="51"/>
<wire x1="-0.635" y1="0" x2="-1.27" y2="0" width="0.6096" layer="51"/>
<circle x="-1.27" y="0" radius="1.524" width="0.1524" layer="21"/>
<circle x="-1.27" y="0" radius="0.762" width="0.1524" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="0.254" y="1.016" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="0.254" y="-2.2098" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="0.254" y1="-0.3048" x2="0.5588" y2="0.3048" layer="51"/>
<rectangle x1="-0.635" y1="-0.3048" x2="-0.3302" y2="0.3048" layer="51"/>
<rectangle x1="-0.3302" y1="-0.3048" x2="0.254" y2="0.3048" layer="21"/>
</package>
<package name="0411/12">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0411, grid 12.5 mm</description>
<wire x1="6.35" y1="0" x2="5.461" y2="0" width="0.762" layer="51"/>
<wire x1="-6.35" y1="0" x2="-5.461" y2="0" width="0.762" layer="51"/>
<wire x1="5.08" y1="-1.651" x2="5.08" y2="1.651" width="0.1524" layer="21"/>
<wire x1="4.699" y1="2.032" x2="5.08" y2="1.651" width="0.1524" layer="21" curve="-90"/>
<wire x1="-5.08" y1="-1.651" x2="-4.699" y2="-2.032" width="0.1524" layer="21" curve="90"/>
<wire x1="4.699" y1="-2.032" x2="5.08" y2="-1.651" width="0.1524" layer="21" curve="90"/>
<wire x1="-5.08" y1="1.651" x2="-4.699" y2="2.032" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.699" y1="2.032" x2="4.064" y2="2.032" width="0.1524" layer="21"/>
<wire x1="3.937" y1="1.905" x2="4.064" y2="2.032" width="0.1524" layer="21"/>
<wire x1="4.699" y1="-2.032" x2="4.064" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="3.937" y1="-1.905" x2="4.064" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="-3.937" y1="1.905" x2="-4.064" y2="2.032" width="0.1524" layer="21"/>
<wire x1="-3.937" y1="1.905" x2="3.937" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-3.937" y1="-1.905" x2="-4.064" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="-3.937" y1="-1.905" x2="3.937" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="1.651" x2="-5.08" y2="-1.651" width="0.1524" layer="21"/>
<wire x1="-4.699" y1="2.032" x2="-4.064" y2="2.032" width="0.1524" layer="21"/>
<wire x1="-4.699" y1="-2.032" x2="-4.064" y2="-2.032" width="0.1524" layer="21"/>
<pad name="1" x="-6.35" y="0" drill="0.9144" shape="octagon"/>
<pad name="2" x="6.35" y="0" drill="0.9144" shape="octagon"/>
<text x="-5.08" y="2.413" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.5814" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-5.3594" y1="-0.381" x2="-5.08" y2="0.381" layer="21"/>
<rectangle x1="5.08" y1="-0.381" x2="5.3594" y2="0.381" layer="21"/>
</package>
<package name="0411/15">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0411, grid 15 mm</description>
<wire x1="5.08" y1="-1.651" x2="5.08" y2="1.651" width="0.1524" layer="21"/>
<wire x1="4.699" y1="2.032" x2="5.08" y2="1.651" width="0.1524" layer="21" curve="-90"/>
<wire x1="-5.08" y1="-1.651" x2="-4.699" y2="-2.032" width="0.1524" layer="21" curve="90"/>
<wire x1="4.699" y1="-2.032" x2="5.08" y2="-1.651" width="0.1524" layer="21" curve="90"/>
<wire x1="-5.08" y1="1.651" x2="-4.699" y2="2.032" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.699" y1="2.032" x2="4.064" y2="2.032" width="0.1524" layer="21"/>
<wire x1="3.937" y1="1.905" x2="4.064" y2="2.032" width="0.1524" layer="21"/>
<wire x1="4.699" y1="-2.032" x2="4.064" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="3.937" y1="-1.905" x2="4.064" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="-3.937" y1="1.905" x2="-4.064" y2="2.032" width="0.1524" layer="21"/>
<wire x1="-3.937" y1="1.905" x2="3.937" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-3.937" y1="-1.905" x2="-4.064" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="-3.937" y1="-1.905" x2="3.937" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="1.651" x2="-5.08" y2="-1.651" width="0.1524" layer="21"/>
<wire x1="-4.699" y1="2.032" x2="-4.064" y2="2.032" width="0.1524" layer="21"/>
<wire x1="-4.699" y1="-2.032" x2="-4.064" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="-7.62" y1="0" x2="-6.35" y2="0" width="0.762" layer="51"/>
<wire x1="6.35" y1="0" x2="7.62" y2="0" width="0.762" layer="51"/>
<pad name="1" x="-7.62" y="0" drill="0.9144" shape="octagon"/>
<pad name="2" x="7.62" y="0" drill="0.9144" shape="octagon"/>
<text x="-5.08" y="2.413" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.5814" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="5.08" y1="-0.381" x2="6.477" y2="0.381" layer="21"/>
<rectangle x1="-6.477" y1="-0.381" x2="-5.08" y2="0.381" layer="21"/>
</package>
<package name="0411V">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0411, grid 3.81 mm</description>
<wire x1="1.27" y1="0" x2="0.3048" y2="0" width="0.762" layer="51"/>
<wire x1="-1.5748" y1="0" x2="-2.54" y2="0" width="0.762" layer="51"/>
<circle x="-2.54" y="0" radius="2.032" width="0.1524" layer="21"/>
<circle x="-2.54" y="0" radius="1.016" width="0.1524" layer="51"/>
<pad name="1" x="-2.54" y="0" drill="0.9144" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.9144" shape="octagon"/>
<text x="-0.508" y="1.143" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-0.5334" y="-2.413" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-1.4732" y1="-0.381" x2="0.2032" y2="0.381" layer="21"/>
</package>
<package name="0414/15">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0414, grid 15 mm</description>
<wire x1="7.62" y1="0" x2="6.604" y2="0" width="0.8128" layer="51"/>
<wire x1="-7.62" y1="0" x2="-6.604" y2="0" width="0.8128" layer="51"/>
<wire x1="-6.096" y1="1.905" x2="-5.842" y2="2.159" width="0.1524" layer="21" curve="-90"/>
<wire x1="-6.096" y1="-1.905" x2="-5.842" y2="-2.159" width="0.1524" layer="21" curve="90"/>
<wire x1="5.842" y1="-2.159" x2="6.096" y2="-1.905" width="0.1524" layer="21" curve="90"/>
<wire x1="5.842" y1="2.159" x2="6.096" y2="1.905" width="0.1524" layer="21" curve="-90"/>
<wire x1="-6.096" y1="-1.905" x2="-6.096" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-5.842" y1="2.159" x2="-4.953" y2="2.159" width="0.1524" layer="21"/>
<wire x1="-4.826" y1="2.032" x2="-4.953" y2="2.159" width="0.1524" layer="21"/>
<wire x1="-5.842" y1="-2.159" x2="-4.953" y2="-2.159" width="0.1524" layer="21"/>
<wire x1="-4.826" y1="-2.032" x2="-4.953" y2="-2.159" width="0.1524" layer="21"/>
<wire x1="4.826" y1="2.032" x2="4.953" y2="2.159" width="0.1524" layer="21"/>
<wire x1="4.826" y1="2.032" x2="-4.826" y2="2.032" width="0.1524" layer="21"/>
<wire x1="4.826" y1="-2.032" x2="4.953" y2="-2.159" width="0.1524" layer="21"/>
<wire x1="4.826" y1="-2.032" x2="-4.826" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="5.842" y1="2.159" x2="4.953" y2="2.159" width="0.1524" layer="21"/>
<wire x1="5.842" y1="-2.159" x2="4.953" y2="-2.159" width="0.1524" layer="21"/>
<wire x1="6.096" y1="-1.905" x2="6.096" y2="1.905" width="0.1524" layer="21"/>
<pad name="1" x="-7.62" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="7.62" y="0" drill="1.016" shape="octagon"/>
<text x="-6.096" y="2.5654" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-4.318" y="-0.5842" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="6.096" y1="-0.4064" x2="6.5024" y2="0.4064" layer="21"/>
<rectangle x1="-6.5024" y1="-0.4064" x2="-6.096" y2="0.4064" layer="21"/>
</package>
<package name="0414V">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0414, grid 5 mm</description>
<wire x1="2.54" y1="0" x2="1.397" y2="0" width="0.8128" layer="51"/>
<wire x1="-2.54" y1="0" x2="-1.397" y2="0" width="0.8128" layer="51"/>
<circle x="-2.54" y="0" radius="2.159" width="0.1524" layer="21"/>
<circle x="-2.54" y="0" radius="1.143" width="0.1524" layer="51"/>
<pad name="1" x="-2.54" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="1.016" shape="octagon"/>
<text x="-0.381" y="1.1684" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-0.381" y="-2.3622" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-1.2954" y1="-0.4064" x2="1.2954" y2="0.4064" layer="21"/>
</package>
<package name="0617/17">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0617, grid 17.5 mm</description>
<wire x1="-8.89" y1="0" x2="-8.636" y2="0" width="0.8128" layer="51"/>
<wire x1="-7.874" y1="3.048" x2="-6.985" y2="3.048" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="2.794" x2="-6.985" y2="3.048" width="0.1524" layer="21"/>
<wire x1="-7.874" y1="-3.048" x2="-6.985" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="-2.794" x2="-6.985" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="6.731" y1="2.794" x2="6.985" y2="3.048" width="0.1524" layer="21"/>
<wire x1="6.731" y1="2.794" x2="-6.731" y2="2.794" width="0.1524" layer="21"/>
<wire x1="6.731" y1="-2.794" x2="6.985" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="6.731" y1="-2.794" x2="-6.731" y2="-2.794" width="0.1524" layer="21"/>
<wire x1="7.874" y1="3.048" x2="6.985" y2="3.048" width="0.1524" layer="21"/>
<wire x1="7.874" y1="-3.048" x2="6.985" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="-8.255" y1="-2.667" x2="-8.255" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="-8.255" y1="1.016" x2="-8.255" y2="-1.016" width="0.1524" layer="51"/>
<wire x1="-8.255" y1="1.016" x2="-8.255" y2="2.667" width="0.1524" layer="21"/>
<wire x1="8.255" y1="-2.667" x2="8.255" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="8.255" y1="1.016" x2="8.255" y2="-1.016" width="0.1524" layer="51"/>
<wire x1="8.255" y1="1.016" x2="8.255" y2="2.667" width="0.1524" layer="21"/>
<wire x1="8.636" y1="0" x2="8.89" y2="0" width="0.8128" layer="51"/>
<wire x1="-8.255" y1="2.667" x2="-7.874" y2="3.048" width="0.1524" layer="21" curve="-90"/>
<wire x1="7.874" y1="3.048" x2="8.255" y2="2.667" width="0.1524" layer="21" curve="-90"/>
<wire x1="-8.255" y1="-2.667" x2="-7.874" y2="-3.048" width="0.1524" layer="21" curve="90"/>
<wire x1="7.874" y1="-3.048" x2="8.255" y2="-2.667" width="0.1524" layer="21" curve="90"/>
<pad name="1" x="-8.89" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="8.89" y="0" drill="1.016" shape="octagon"/>
<text x="-8.128" y="3.4544" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-6.096" y="-0.7112" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-8.5344" y1="-0.4064" x2="-8.2296" y2="0.4064" layer="51"/>
<rectangle x1="8.2296" y1="-0.4064" x2="8.5344" y2="0.4064" layer="51"/>
</package>
<package name="0617/22">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0617, grid 22.5 mm</description>
<wire x1="-10.287" y1="0" x2="-11.43" y2="0" width="0.8128" layer="51"/>
<wire x1="-8.255" y1="-2.667" x2="-8.255" y2="2.667" width="0.1524" layer="21"/>
<wire x1="-7.874" y1="3.048" x2="-6.985" y2="3.048" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="2.794" x2="-6.985" y2="3.048" width="0.1524" layer="21"/>
<wire x1="-7.874" y1="-3.048" x2="-6.985" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="-2.794" x2="-6.985" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="6.731" y1="2.794" x2="6.985" y2="3.048" width="0.1524" layer="21"/>
<wire x1="6.731" y1="2.794" x2="-6.731" y2="2.794" width="0.1524" layer="21"/>
<wire x1="6.731" y1="-2.794" x2="6.985" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="6.731" y1="-2.794" x2="-6.731" y2="-2.794" width="0.1524" layer="21"/>
<wire x1="7.874" y1="3.048" x2="6.985" y2="3.048" width="0.1524" layer="21"/>
<wire x1="7.874" y1="-3.048" x2="6.985" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="8.255" y1="-2.667" x2="8.255" y2="2.667" width="0.1524" layer="21"/>
<wire x1="11.43" y1="0" x2="10.287" y2="0" width="0.8128" layer="51"/>
<wire x1="-8.255" y1="2.667" x2="-7.874" y2="3.048" width="0.1524" layer="21" curve="-90"/>
<wire x1="-8.255" y1="-2.667" x2="-7.874" y2="-3.048" width="0.1524" layer="21" curve="90"/>
<wire x1="7.874" y1="3.048" x2="8.255" y2="2.667" width="0.1524" layer="21" curve="-90"/>
<wire x1="7.874" y1="-3.048" x2="8.255" y2="-2.667" width="0.1524" layer="21" curve="90"/>
<pad name="1" x="-11.43" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="11.43" y="0" drill="1.016" shape="octagon"/>
<text x="-8.255" y="3.4544" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-6.477" y="-0.5842" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-10.1854" y1="-0.4064" x2="-8.255" y2="0.4064" layer="21"/>
<rectangle x1="8.255" y1="-0.4064" x2="10.1854" y2="0.4064" layer="21"/>
</package>
<package name="0617V">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0617, grid 5 mm</description>
<wire x1="-2.54" y1="0" x2="-1.27" y2="0" width="0.8128" layer="51"/>
<wire x1="1.27" y1="0" x2="2.54" y2="0" width="0.8128" layer="51"/>
<circle x="-2.54" y="0" radius="3.048" width="0.1524" layer="21"/>
<circle x="-2.54" y="0" radius="1.143" width="0.1524" layer="51"/>
<pad name="1" x="-2.54" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="1.016" shape="octagon"/>
<text x="0.635" y="1.4224" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="0.635" y="-2.6162" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-1.3208" y1="-0.4064" x2="1.3208" y2="0.4064" layer="21"/>
</package>
<package name="0922/22">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0922, grid 22.5 mm</description>
<wire x1="11.43" y1="0" x2="10.795" y2="0" width="0.8128" layer="51"/>
<wire x1="-11.43" y1="0" x2="-10.795" y2="0" width="0.8128" layer="51"/>
<wire x1="-10.16" y1="-4.191" x2="-10.16" y2="4.191" width="0.1524" layer="21"/>
<wire x1="-9.779" y1="4.572" x2="-8.89" y2="4.572" width="0.1524" layer="21"/>
<wire x1="-8.636" y1="4.318" x2="-8.89" y2="4.572" width="0.1524" layer="21"/>
<wire x1="-9.779" y1="-4.572" x2="-8.89" y2="-4.572" width="0.1524" layer="21"/>
<wire x1="-8.636" y1="-4.318" x2="-8.89" y2="-4.572" width="0.1524" layer="21"/>
<wire x1="8.636" y1="4.318" x2="8.89" y2="4.572" width="0.1524" layer="21"/>
<wire x1="8.636" y1="4.318" x2="-8.636" y2="4.318" width="0.1524" layer="21"/>
<wire x1="8.636" y1="-4.318" x2="8.89" y2="-4.572" width="0.1524" layer="21"/>
<wire x1="8.636" y1="-4.318" x2="-8.636" y2="-4.318" width="0.1524" layer="21"/>
<wire x1="9.779" y1="4.572" x2="8.89" y2="4.572" width="0.1524" layer="21"/>
<wire x1="9.779" y1="-4.572" x2="8.89" y2="-4.572" width="0.1524" layer="21"/>
<wire x1="10.16" y1="-4.191" x2="10.16" y2="4.191" width="0.1524" layer="21"/>
<wire x1="-10.16" y1="-4.191" x2="-9.779" y2="-4.572" width="0.1524" layer="21" curve="90"/>
<wire x1="-10.16" y1="4.191" x2="-9.779" y2="4.572" width="0.1524" layer="21" curve="-90"/>
<wire x1="9.779" y1="-4.572" x2="10.16" y2="-4.191" width="0.1524" layer="21" curve="90"/>
<wire x1="9.779" y1="4.572" x2="10.16" y2="4.191" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-11.43" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="11.43" y="0" drill="1.016" shape="octagon"/>
<text x="-10.16" y="5.1054" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-6.477" y="-0.5842" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-10.7188" y1="-0.4064" x2="-10.16" y2="0.4064" layer="51"/>
<rectangle x1="10.16" y1="-0.4064" x2="10.3124" y2="0.4064" layer="21"/>
<rectangle x1="-10.3124" y1="-0.4064" x2="-10.16" y2="0.4064" layer="21"/>
<rectangle x1="10.16" y1="-0.4064" x2="10.7188" y2="0.4064" layer="51"/>
</package>
<package name="P0613V">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0613, grid 5 mm</description>
<wire x1="2.54" y1="0" x2="1.397" y2="0" width="0.8128" layer="51"/>
<wire x1="-2.54" y1="0" x2="-1.397" y2="0" width="0.8128" layer="51"/>
<circle x="-2.54" y="0" radius="2.286" width="0.1524" layer="21"/>
<circle x="-2.54" y="0" radius="1.143" width="0.1524" layer="51"/>
<pad name="1" x="-2.54" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="1.016" shape="octagon"/>
<text x="-0.254" y="1.143" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-0.254" y="-2.413" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-1.2954" y1="-0.4064" x2="1.3208" y2="0.4064" layer="21"/>
</package>
<package name="P0613/15">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0613, grid 15 mm</description>
<wire x1="7.62" y1="0" x2="6.985" y2="0" width="0.8128" layer="51"/>
<wire x1="-7.62" y1="0" x2="-6.985" y2="0" width="0.8128" layer="51"/>
<wire x1="-6.477" y1="2.032" x2="-6.223" y2="2.286" width="0.1524" layer="21" curve="-90"/>
<wire x1="-6.477" y1="-2.032" x2="-6.223" y2="-2.286" width="0.1524" layer="21" curve="90"/>
<wire x1="6.223" y1="-2.286" x2="6.477" y2="-2.032" width="0.1524" layer="21" curve="90"/>
<wire x1="6.223" y1="2.286" x2="6.477" y2="2.032" width="0.1524" layer="21" curve="-90"/>
<wire x1="-6.223" y1="2.286" x2="-5.334" y2="2.286" width="0.1524" layer="21"/>
<wire x1="-5.207" y1="2.159" x2="-5.334" y2="2.286" width="0.1524" layer="21"/>
<wire x1="-6.223" y1="-2.286" x2="-5.334" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="-5.207" y1="-2.159" x2="-5.334" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="5.207" y1="2.159" x2="5.334" y2="2.286" width="0.1524" layer="21"/>
<wire x1="5.207" y1="2.159" x2="-5.207" y2="2.159" width="0.1524" layer="21"/>
<wire x1="5.207" y1="-2.159" x2="5.334" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="5.207" y1="-2.159" x2="-5.207" y2="-2.159" width="0.1524" layer="21"/>
<wire x1="6.223" y1="2.286" x2="5.334" y2="2.286" width="0.1524" layer="21"/>
<wire x1="6.223" y1="-2.286" x2="5.334" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="6.477" y1="-0.635" x2="6.477" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="6.477" y1="-0.635" x2="6.477" y2="0.635" width="0.1524" layer="51"/>
<wire x1="6.477" y1="2.032" x2="6.477" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-6.477" y1="-2.032" x2="-6.477" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-6.477" y1="0.635" x2="-6.477" y2="-0.635" width="0.1524" layer="51"/>
<wire x1="-6.477" y1="0.635" x2="-6.477" y2="2.032" width="0.1524" layer="21"/>
<pad name="1" x="-7.62" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="7.62" y="0" drill="1.016" shape="octagon"/>
<text x="-6.477" y="2.6924" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-4.318" y="-0.7112" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-7.0358" y1="-0.4064" x2="-6.477" y2="0.4064" layer="51"/>
<rectangle x1="6.477" y1="-0.4064" x2="7.0358" y2="0.4064" layer="51"/>
</package>
<package name="P0817/22">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0817, grid 22.5 mm</description>
<wire x1="-10.414" y1="0" x2="-11.43" y2="0" width="0.8128" layer="51"/>
<wire x1="-8.509" y1="-3.429" x2="-8.509" y2="3.429" width="0.1524" layer="21"/>
<wire x1="-8.128" y1="3.81" x2="-7.239" y2="3.81" width="0.1524" layer="21"/>
<wire x1="-6.985" y1="3.556" x2="-7.239" y2="3.81" width="0.1524" layer="21"/>
<wire x1="-8.128" y1="-3.81" x2="-7.239" y2="-3.81" width="0.1524" layer="21"/>
<wire x1="-6.985" y1="-3.556" x2="-7.239" y2="-3.81" width="0.1524" layer="21"/>
<wire x1="6.985" y1="3.556" x2="7.239" y2="3.81" width="0.1524" layer="21"/>
<wire x1="6.985" y1="3.556" x2="-6.985" y2="3.556" width="0.1524" layer="21"/>
<wire x1="6.985" y1="-3.556" x2="7.239" y2="-3.81" width="0.1524" layer="21"/>
<wire x1="6.985" y1="-3.556" x2="-6.985" y2="-3.556" width="0.1524" layer="21"/>
<wire x1="8.128" y1="3.81" x2="7.239" y2="3.81" width="0.1524" layer="21"/>
<wire x1="8.128" y1="-3.81" x2="7.239" y2="-3.81" width="0.1524" layer="21"/>
<wire x1="8.509" y1="-3.429" x2="8.509" y2="3.429" width="0.1524" layer="21"/>
<wire x1="11.43" y1="0" x2="10.414" y2="0" width="0.8128" layer="51"/>
<wire x1="-8.509" y1="3.429" x2="-8.128" y2="3.81" width="0.1524" layer="21" curve="-90"/>
<wire x1="-8.509" y1="-3.429" x2="-8.128" y2="-3.81" width="0.1524" layer="21" curve="90"/>
<wire x1="8.128" y1="3.81" x2="8.509" y2="3.429" width="0.1524" layer="21" curve="-90"/>
<wire x1="8.128" y1="-3.81" x2="8.509" y2="-3.429" width="0.1524" layer="21" curve="90"/>
<pad name="1" x="-11.43" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="11.43" y="0" drill="1.016" shape="octagon"/>
<text x="-8.382" y="4.2164" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-6.223" y="-0.5842" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<text x="6.604" y="-2.2606" size="1.27" layer="51" ratio="10" rot="R90">0817</text>
<rectangle x1="8.509" y1="-0.4064" x2="10.3124" y2="0.4064" layer="21"/>
<rectangle x1="-10.3124" y1="-0.4064" x2="-8.509" y2="0.4064" layer="21"/>
</package>
<package name="P0817V">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0817, grid 6.35 mm</description>
<wire x1="-3.81" y1="0" x2="-5.08" y2="0" width="0.8128" layer="51"/>
<wire x1="1.27" y1="0" x2="0" y2="0" width="0.8128" layer="51"/>
<circle x="-5.08" y="0" radius="3.81" width="0.1524" layer="21"/>
<circle x="-5.08" y="0" radius="1.27" width="0.1524" layer="51"/>
<pad name="1" x="-5.08" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="1.016" shape="octagon"/>
<text x="-1.016" y="1.27" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.016" y="-2.54" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<text x="-6.858" y="2.032" size="1.016" layer="21" ratio="12">0817</text>
<rectangle x1="-3.81" y1="-0.4064" x2="0" y2="0.4064" layer="21"/>
</package>
<package name="V234/12">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type V234, grid 12.5 mm</description>
<wire x1="-4.953" y1="1.524" x2="-4.699" y2="1.778" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.699" y1="1.778" x2="4.953" y2="1.524" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.699" y1="-1.778" x2="4.953" y2="-1.524" width="0.1524" layer="21" curve="90"/>
<wire x1="-4.953" y1="-1.524" x2="-4.699" y2="-1.778" width="0.1524" layer="21" curve="90"/>
<wire x1="-4.699" y1="1.778" x2="4.699" y2="1.778" width="0.1524" layer="21"/>
<wire x1="-4.953" y1="1.524" x2="-4.953" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="4.699" y1="-1.778" x2="-4.699" y2="-1.778" width="0.1524" layer="21"/>
<wire x1="4.953" y1="1.524" x2="4.953" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="6.35" y1="0" x2="5.461" y2="0" width="0.8128" layer="51"/>
<wire x1="-6.35" y1="0" x2="-5.461" y2="0" width="0.8128" layer="51"/>
<pad name="1" x="-6.35" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="6.35" y="0" drill="1.016" shape="octagon"/>
<text x="-4.953" y="2.159" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.81" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="4.953" y1="-0.4064" x2="5.4102" y2="0.4064" layer="21"/>
<rectangle x1="-5.4102" y1="-0.4064" x2="-4.953" y2="0.4064" layer="21"/>
</package>
<package name="V235/17">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type V235, grid 17.78 mm</description>
<wire x1="-6.731" y1="2.921" x2="6.731" y2="2.921" width="0.1524" layer="21"/>
<wire x1="-7.112" y1="2.54" x2="-7.112" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="6.731" y1="-2.921" x2="-6.731" y2="-2.921" width="0.1524" layer="21"/>
<wire x1="7.112" y1="2.54" x2="7.112" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="8.89" y1="0" x2="7.874" y2="0" width="1.016" layer="51"/>
<wire x1="-7.874" y1="0" x2="-8.89" y2="0" width="1.016" layer="51"/>
<wire x1="-7.112" y1="-2.54" x2="-6.731" y2="-2.921" width="0.1524" layer="21" curve="90"/>
<wire x1="6.731" y1="2.921" x2="7.112" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<wire x1="6.731" y1="-2.921" x2="7.112" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="-7.112" y1="2.54" x2="-6.731" y2="2.921" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-8.89" y="0" drill="1.1938" shape="octagon"/>
<pad name="2" x="8.89" y="0" drill="1.1938" shape="octagon"/>
<text x="-6.858" y="3.302" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.842" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="7.112" y1="-0.508" x2="7.747" y2="0.508" layer="21"/>
<rectangle x1="-7.747" y1="-0.508" x2="-7.112" y2="0.508" layer="21"/>
</package>
<package name="V526-0">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type V526-0, grid 2.5 mm</description>
<wire x1="-2.54" y1="1.016" x2="-2.286" y2="1.27" width="0.1524" layer="21" curve="-90"/>
<wire x1="2.286" y1="1.27" x2="2.54" y2="1.016" width="0.1524" layer="21" curve="-90"/>
<wire x1="2.286" y1="-1.27" x2="2.54" y2="-1.016" width="0.1524" layer="21" curve="90"/>
<wire x1="-2.54" y1="-1.016" x2="-2.286" y2="-1.27" width="0.1524" layer="21" curve="90"/>
<wire x1="2.286" y1="1.27" x2="-2.286" y2="1.27" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-1.016" x2="2.54" y2="1.016" width="0.1524" layer="21"/>
<wire x1="-2.286" y1="-1.27" x2="2.286" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="1.016" x2="-2.54" y2="-1.016" width="0.1524" layer="21"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.413" y="1.651" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.413" y="-2.794" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="MINI_MELF-0102R">
<description>&lt;b&gt;CECC Size RC2211&lt;/b&gt; Reflow Soldering&lt;p&gt;
source Beyschlag</description>
<wire x1="-1" y1="-0.5" x2="1" y2="-0.5" width="0.2032" layer="51"/>
<wire x1="1" y1="-0.5" x2="1" y2="0.5" width="0.2032" layer="51"/>
<wire x1="1" y1="0.5" x2="-1" y2="0.5" width="0.2032" layer="51"/>
<wire x1="-1" y1="0.5" x2="-1" y2="-0.5" width="0.2032" layer="51"/>
<smd name="1" x="-0.9" y="0" dx="0.5" dy="1.3" layer="1"/>
<smd name="2" x="0.9" y="0" dx="0.5" dy="1.3" layer="1"/>
<text x="-1.27" y="0.9525" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.2225" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="MINI_MELF-0102W">
<description>&lt;b&gt;CECC Size RC2211&lt;/b&gt; Wave Soldering&lt;p&gt;
source Beyschlag</description>
<wire x1="-1" y1="-0.5" x2="1" y2="-0.5" width="0.2032" layer="51"/>
<wire x1="1" y1="-0.5" x2="1" y2="0.5" width="0.2032" layer="51"/>
<wire x1="1" y1="0.5" x2="-1" y2="0.5" width="0.2032" layer="51"/>
<wire x1="-1" y1="0.5" x2="-1" y2="-0.5" width="0.2032" layer="51"/>
<smd name="1" x="-0.95" y="0" dx="0.6" dy="1.3" layer="1"/>
<smd name="2" x="0.95" y="0" dx="0.6" dy="1.3" layer="1"/>
<text x="-1.27" y="0.9525" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.2225" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="MINI_MELF-0204R">
<description>&lt;b&gt;CECC Size RC3715&lt;/b&gt; Reflow Soldering&lt;p&gt;
source Beyschlag</description>
<wire x1="-1.7" y1="-0.6" x2="1.7" y2="-0.6" width="0.2032" layer="51"/>
<wire x1="1.7" y1="-0.6" x2="1.7" y2="0.6" width="0.2032" layer="51"/>
<wire x1="1.7" y1="0.6" x2="-1.7" y2="0.6" width="0.2032" layer="51"/>
<wire x1="-1.7" y1="0.6" x2="-1.7" y2="-0.6" width="0.2032" layer="51"/>
<wire x1="0.938" y1="0.6" x2="-0.938" y2="0.6" width="0.2032" layer="21"/>
<wire x1="-0.938" y1="-0.6" x2="0.938" y2="-0.6" width="0.2032" layer="21"/>
<smd name="1" x="-1.5" y="0" dx="0.8" dy="1.6" layer="1"/>
<smd name="2" x="1.5" y="0" dx="0.8" dy="1.6" layer="1"/>
<text x="-1.27" y="0.9525" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.2225" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="MINI_MELF-0204W">
<description>&lt;b&gt;CECC Size RC3715&lt;/b&gt; Wave Soldering&lt;p&gt;
source Beyschlag</description>
<wire x1="-1.7" y1="-0.6" x2="1.7" y2="-0.6" width="0.2032" layer="51"/>
<wire x1="1.7" y1="-0.6" x2="1.7" y2="0.6" width="0.2032" layer="51"/>
<wire x1="1.7" y1="0.6" x2="-1.7" y2="0.6" width="0.2032" layer="51"/>
<wire x1="-1.7" y1="0.6" x2="-1.7" y2="-0.6" width="0.2032" layer="51"/>
<wire x1="0.684" y1="0.6" x2="-0.684" y2="0.6" width="0.2032" layer="21"/>
<wire x1="-0.684" y1="-0.6" x2="0.684" y2="-0.6" width="0.2032" layer="21"/>
<smd name="1" x="-1.5" y="0" dx="1.2" dy="1.6" layer="1"/>
<smd name="2" x="1.5" y="0" dx="1.2" dy="1.6" layer="1"/>
<text x="-1.27" y="0.9525" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.2225" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="MINI_MELF-0207R">
<description>&lt;b&gt;CECC Size RC6123&lt;/b&gt; Reflow Soldering&lt;p&gt;
source Beyschlag</description>
<wire x1="-2.8" y1="-1" x2="2.8" y2="-1" width="0.2032" layer="51"/>
<wire x1="2.8" y1="-1" x2="2.8" y2="1" width="0.2032" layer="51"/>
<wire x1="2.8" y1="1" x2="-2.8" y2="1" width="0.2032" layer="51"/>
<wire x1="-2.8" y1="1" x2="-2.8" y2="-1" width="0.2032" layer="51"/>
<wire x1="1.2125" y1="1" x2="-1.2125" y2="1" width="0.2032" layer="21"/>
<wire x1="-1.2125" y1="-1" x2="1.2125" y2="-1" width="0.2032" layer="21"/>
<smd name="1" x="-2.25" y="0" dx="1.6" dy="2.5" layer="1"/>
<smd name="2" x="2.25" y="0" dx="1.6" dy="2.5" layer="1"/>
<text x="-2.2225" y="1.5875" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.2225" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="MINI_MELF-0207W">
<description>&lt;b&gt;CECC Size RC6123&lt;/b&gt; Wave Soldering&lt;p&gt;
source Beyschlag</description>
<wire x1="-2.8" y1="-1" x2="2.8" y2="-1" width="0.2032" layer="51"/>
<wire x1="2.8" y1="-1" x2="2.8" y2="1" width="0.2032" layer="51"/>
<wire x1="2.8" y1="1" x2="-2.8" y2="1" width="0.2032" layer="51"/>
<wire x1="-2.8" y1="1" x2="-2.8" y2="-1" width="0.2032" layer="51"/>
<wire x1="1.149" y1="1" x2="-1.149" y2="1" width="0.2032" layer="21"/>
<wire x1="-1.149" y1="-1" x2="1.149" y2="-1" width="0.2032" layer="21"/>
<smd name="1" x="-2.6" y="0" dx="2.4" dy="2.5" layer="1"/>
<smd name="2" x="2.6" y="0" dx="2.4" dy="2.5" layer="1"/>
<text x="-2.54" y="1.5875" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="0922V">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0922, grid 7.5 mm</description>
<wire x1="2.54" y1="0" x2="1.397" y2="0" width="0.8128" layer="51"/>
<wire x1="-5.08" y1="0" x2="-3.81" y2="0" width="0.8128" layer="51"/>
<circle x="-5.08" y="0" radius="4.572" width="0.1524" layer="21"/>
<circle x="-5.08" y="0" radius="1.905" width="0.1524" layer="21"/>
<pad name="1" x="-5.08" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="1.016" shape="octagon"/>
<text x="-0.508" y="1.6764" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-0.508" y="-2.9972" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<text x="-6.858" y="2.54" size="1.016" layer="21" ratio="12">0922</text>
<rectangle x1="-3.81" y1="-0.4064" x2="1.3208" y2="0.4064" layer="21"/>
</package>
<package name="RDH/15">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type RDH, grid 15 mm</description>
<wire x1="-7.62" y1="0" x2="-6.858" y2="0" width="0.8128" layer="51"/>
<wire x1="-6.096" y1="3.048" x2="-5.207" y2="3.048" width="0.1524" layer="21"/>
<wire x1="-4.953" y1="2.794" x2="-5.207" y2="3.048" width="0.1524" layer="21"/>
<wire x1="-6.096" y1="-3.048" x2="-5.207" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="-4.953" y1="-2.794" x2="-5.207" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="4.953" y1="2.794" x2="5.207" y2="3.048" width="0.1524" layer="21"/>
<wire x1="4.953" y1="2.794" x2="-4.953" y2="2.794" width="0.1524" layer="21"/>
<wire x1="4.953" y1="-2.794" x2="5.207" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="4.953" y1="-2.794" x2="-4.953" y2="-2.794" width="0.1524" layer="21"/>
<wire x1="6.096" y1="3.048" x2="5.207" y2="3.048" width="0.1524" layer="21"/>
<wire x1="6.096" y1="-3.048" x2="5.207" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="-6.477" y1="-2.667" x2="-6.477" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="-6.477" y1="1.016" x2="-6.477" y2="-1.016" width="0.1524" layer="51"/>
<wire x1="-6.477" y1="1.016" x2="-6.477" y2="2.667" width="0.1524" layer="21"/>
<wire x1="6.477" y1="-2.667" x2="6.477" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="6.477" y1="1.016" x2="6.477" y2="-1.016" width="0.1524" layer="51"/>
<wire x1="6.477" y1="1.016" x2="6.477" y2="2.667" width="0.1524" layer="21"/>
<wire x1="6.858" y1="0" x2="7.62" y2="0" width="0.8128" layer="51"/>
<wire x1="-6.477" y1="2.667" x2="-6.096" y2="3.048" width="0.1524" layer="21" curve="-90"/>
<wire x1="6.096" y1="3.048" x2="6.477" y2="2.667" width="0.1524" layer="21" curve="-90"/>
<wire x1="-6.477" y1="-2.667" x2="-6.096" y2="-3.048" width="0.1524" layer="21" curve="90"/>
<wire x1="6.096" y1="-3.048" x2="6.477" y2="-2.667" width="0.1524" layer="21" curve="90"/>
<pad name="1" x="-7.62" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="7.62" y="0" drill="1.016" shape="octagon"/>
<text x="-6.35" y="3.4544" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-4.318" y="-0.5842" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<text x="4.572" y="-1.7272" size="1.27" layer="51" ratio="10" rot="R90">RDH</text>
<rectangle x1="-6.7564" y1="-0.4064" x2="-6.4516" y2="0.4064" layer="51"/>
<rectangle x1="6.4516" y1="-0.4064" x2="6.7564" y2="0.4064" layer="51"/>
</package>
<package name="MINI_MELF-0102AX">
<description>&lt;b&gt;Mini MELF 0102 Axial&lt;/b&gt;</description>
<circle x="0" y="0" radius="0.6" width="0" layer="51"/>
<circle x="0" y="0" radius="0.6" width="0" layer="52"/>
<smd name="1" x="0" y="0" dx="1.9" dy="1.9" layer="1" roundness="100"/>
<smd name="2" x="0" y="0" dx="1.9" dy="1.9" layer="16" roundness="100"/>
<text x="-1.27" y="0.9525" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.2225" size="1.27" layer="27">&gt;VALUE</text>
<hole x="0" y="0" drill="1.3"/>
</package>
<package name="R0201">
<description>&lt;b&gt;RESISTOR&lt;/b&gt; chip&lt;p&gt;
Source: http://www.vishay.com/docs/20008/dcrcw.pdf</description>
<smd name="1" x="-0.255" y="0" dx="0.28" dy="0.43" layer="1"/>
<smd name="2" x="0.255" y="0" dx="0.28" dy="0.43" layer="1"/>
<text x="-0.635" y="0.635" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-1.905" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.3" y1="-0.15" x2="-0.15" y2="0.15" layer="51"/>
<rectangle x1="0.15" y1="-0.15" x2="0.3" y2="0.15" layer="51"/>
<rectangle x1="-0.15" y1="-0.15" x2="0.15" y2="0.15" layer="21"/>
</package>
<package name="VTA52">
<description>&lt;b&gt;Bulk Metal® Foil Technology&lt;/b&gt;, Tubular Axial Lead Resistors, Meets or Exceeds MIL-R-39005 Requirements&lt;p&gt;
MIL SIZE RBR52&lt;br&gt;
Source: VISHAY .. vta56.pdf</description>
<wire x1="-15.24" y1="0" x2="-13.97" y2="0" width="0.6096" layer="51"/>
<wire x1="12.6225" y1="0.025" x2="12.6225" y2="4.725" width="0.1524" layer="21"/>
<wire x1="12.6225" y1="4.725" x2="-12.6225" y2="4.725" width="0.1524" layer="21"/>
<wire x1="-12.6225" y1="4.725" x2="-12.6225" y2="0.025" width="0.1524" layer="21"/>
<wire x1="-12.6225" y1="0.025" x2="-12.6225" y2="-4.65" width="0.1524" layer="21"/>
<wire x1="-12.6225" y1="-4.65" x2="12.6225" y2="-4.65" width="0.1524" layer="21"/>
<wire x1="12.6225" y1="-4.65" x2="12.6225" y2="0.025" width="0.1524" layer="21"/>
<wire x1="13.97" y1="0" x2="15.24" y2="0" width="0.6096" layer="51"/>
<pad name="1" x="-15.24" y="0" drill="1.1" shape="octagon"/>
<pad name="2" x="15.24" y="0" drill="1.1" shape="octagon"/>
<text x="-3.81" y="5.08" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.175" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-13.97" y1="-0.3048" x2="-12.5675" y2="0.3048" layer="21"/>
<rectangle x1="12.5675" y1="-0.3048" x2="13.97" y2="0.3048" layer="21"/>
</package>
<package name="VTA53">
<description>&lt;b&gt;Bulk Metal® Foil Technology&lt;/b&gt;, Tubular Axial Lead Resistors, Meets or Exceeds MIL-R-39005 Requirements&lt;p&gt;
MIL SIZE RBR53&lt;br&gt;
Source: VISHAY .. vta56.pdf</description>
<wire x1="-12.065" y1="0" x2="-10.795" y2="0" width="0.6096" layer="51"/>
<wire x1="9.8975" y1="0" x2="9.8975" y2="4.7" width="0.1524" layer="21"/>
<wire x1="9.8975" y1="4.7" x2="-9.8975" y2="4.7" width="0.1524" layer="21"/>
<wire x1="-9.8975" y1="4.7" x2="-9.8975" y2="0" width="0.1524" layer="21"/>
<wire x1="-9.8975" y1="0" x2="-9.8975" y2="-4.675" width="0.1524" layer="21"/>
<wire x1="-9.8975" y1="-4.675" x2="9.8975" y2="-4.675" width="0.1524" layer="21"/>
<wire x1="9.8975" y1="-4.675" x2="9.8975" y2="0" width="0.1524" layer="21"/>
<wire x1="10.795" y1="0" x2="12.065" y2="0" width="0.6096" layer="51"/>
<pad name="1" x="-12.065" y="0" drill="1.1" shape="octagon"/>
<pad name="2" x="12.065" y="0" drill="1.1" shape="octagon"/>
<text x="-3.81" y="5.08" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.175" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-10.795" y1="-0.3048" x2="-9.8425" y2="0.3048" layer="21"/>
<rectangle x1="9.8425" y1="-0.3048" x2="10.795" y2="0.3048" layer="21"/>
</package>
<package name="VTA54">
<description>&lt;b&gt;Bulk Metal® Foil Technology&lt;/b&gt;, Tubular Axial Lead Resistors, Meets or Exceeds MIL-R-39005 Requirements&lt;p&gt;
MIL SIZE RBR54&lt;br&gt;
Source: VISHAY .. vta56.pdf</description>
<wire x1="-12.065" y1="0" x2="-10.795" y2="0" width="0.6096" layer="51"/>
<wire x1="9.8975" y1="0" x2="9.8975" y2="3.3" width="0.1524" layer="21"/>
<wire x1="9.8975" y1="3.3" x2="-9.8975" y2="3.3" width="0.1524" layer="21"/>
<wire x1="-9.8975" y1="3.3" x2="-9.8975" y2="0" width="0.1524" layer="21"/>
<wire x1="-9.8975" y1="0" x2="-9.8975" y2="-3.3" width="0.1524" layer="21"/>
<wire x1="-9.8975" y1="-3.3" x2="9.8975" y2="-3.3" width="0.1524" layer="21"/>
<wire x1="9.8975" y1="-3.3" x2="9.8975" y2="0" width="0.1524" layer="21"/>
<wire x1="10.795" y1="0" x2="12.065" y2="0" width="0.6096" layer="51"/>
<pad name="1" x="-12.065" y="0" drill="1.1" shape="octagon"/>
<pad name="2" x="12.065" y="0" drill="1.1" shape="octagon"/>
<text x="-3.81" y="3.81" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.175" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-10.795" y1="-0.3048" x2="-9.8425" y2="0.3048" layer="21"/>
<rectangle x1="9.8425" y1="-0.3048" x2="10.795" y2="0.3048" layer="21"/>
</package>
<package name="VTA55">
<description>&lt;b&gt;Bulk Metal® Foil Technology&lt;/b&gt;, Tubular Axial Lead Resistors, Meets or Exceeds MIL-R-39005 Requirements&lt;p&gt;
MIL SIZE RBR55&lt;br&gt;
Source: VISHAY .. vta56.pdf</description>
<wire x1="-8.255" y1="0" x2="-6.985" y2="0" width="0.6096" layer="51"/>
<wire x1="6.405" y1="0" x2="6.405" y2="3.3" width="0.1524" layer="21"/>
<wire x1="6.405" y1="3.3" x2="-6.405" y2="3.3" width="0.1524" layer="21"/>
<wire x1="-6.405" y1="3.3" x2="-6.405" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.405" y1="0" x2="-6.405" y2="-3.3" width="0.1524" layer="21"/>
<wire x1="-6.405" y1="-3.3" x2="6.405" y2="-3.3" width="0.1524" layer="21"/>
<wire x1="6.405" y1="-3.3" x2="6.405" y2="0" width="0.1524" layer="21"/>
<wire x1="6.985" y1="0" x2="8.255" y2="0" width="0.6096" layer="51"/>
<pad name="1" x="-8.255" y="0" drill="1.1" shape="octagon"/>
<pad name="2" x="8.255" y="0" drill="1.1" shape="octagon"/>
<text x="-3.81" y="3.81" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.175" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-6.985" y1="-0.3048" x2="-6.35" y2="0.3048" layer="21"/>
<rectangle x1="6.35" y1="-0.3048" x2="6.985" y2="0.3048" layer="21"/>
</package>
<package name="VTA56">
<description>&lt;b&gt;Bulk Metal® Foil Technology&lt;/b&gt;, Tubular Axial Lead Resistors, Meets or Exceeds MIL-R-39005 Requirements&lt;p&gt;
MIL SIZE RBR56&lt;br&gt;
Source: VISHAY .. vta56.pdf</description>
<wire x1="-6.35" y1="0" x2="-5.08" y2="0" width="0.6096" layer="51"/>
<wire x1="4.5" y1="0" x2="4.5" y2="3.3" width="0.1524" layer="21"/>
<wire x1="4.5" y1="3.3" x2="-4.5" y2="3.3" width="0.1524" layer="21"/>
<wire x1="-4.5" y1="3.3" x2="-4.5" y2="0" width="0.1524" layer="21"/>
<wire x1="-4.5" y1="0" x2="-4.5" y2="-3.3" width="0.1524" layer="21"/>
<wire x1="-4.5" y1="-3.3" x2="4.5" y2="-3.3" width="0.1524" layer="21"/>
<wire x1="4.5" y1="-3.3" x2="4.5" y2="0" width="0.1524" layer="21"/>
<wire x1="5.08" y1="0" x2="6.35" y2="0" width="0.6096" layer="51"/>
<pad name="1" x="-6.35" y="0" drill="1.1" shape="octagon"/>
<pad name="2" x="6.35" y="0" drill="1.1" shape="octagon"/>
<text x="-3.81" y="3.81" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.175" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-5.08" y1="-0.3048" x2="-4.445" y2="0.3048" layer="21"/>
<rectangle x1="4.445" y1="-0.3048" x2="5.08" y2="0.3048" layer="21"/>
</package>
<package name="VMTA55">
<description>&lt;b&gt;Bulk Metal® Foil Technology&lt;/b&gt;, Tubular Axial Lead Resistors, Meets or Exceeds MIL-R-39005 Requirements&lt;p&gt;
MIL SIZE RNC55&lt;br&gt;
Source: VISHAY .. vta56.pdf</description>
<wire x1="-5.08" y1="0" x2="-4.26" y2="0" width="0.6096" layer="51"/>
<wire x1="3.3375" y1="-1.45" x2="3.3375" y2="1.45" width="0.1524" layer="21"/>
<wire x1="3.3375" y1="1.45" x2="-3.3625" y2="1.45" width="0.1524" layer="21"/>
<wire x1="-3.3625" y1="1.45" x2="-3.3625" y2="-1.45" width="0.1524" layer="21"/>
<wire x1="-3.3625" y1="-1.45" x2="3.3375" y2="-1.45" width="0.1524" layer="21"/>
<wire x1="4.235" y1="0" x2="5.08" y2="0" width="0.6096" layer="51"/>
<pad name="1" x="-5.08" y="0" drill="1.1" shape="octagon"/>
<pad name="2" x="5.08" y="0" drill="1.1" shape="octagon"/>
<text x="-3.175" y="1.905" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.175" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-4.26" y1="-0.3048" x2="-3.3075" y2="0.3048" layer="21"/>
<rectangle x1="3.2825" y1="-0.3048" x2="4.235" y2="0.3048" layer="21"/>
</package>
<package name="VMTB60">
<description>&lt;b&gt;Bulk Metal® Foil Technology&lt;/b&gt;, Tubular Axial Lead Resistors, Meets or Exceeds MIL-R-39005 Requirements&lt;p&gt;
MIL SIZE RNC60&lt;br&gt;
Source: VISHAY .. vta56.pdf</description>
<wire x1="-6.35" y1="0" x2="-5.585" y2="0" width="0.6096" layer="51"/>
<wire x1="4.6875" y1="-1.95" x2="4.6875" y2="1.95" width="0.1524" layer="21"/>
<wire x1="4.6875" y1="1.95" x2="-4.6875" y2="1.95" width="0.1524" layer="21"/>
<wire x1="-4.6875" y1="1.95" x2="-4.6875" y2="-1.95" width="0.1524" layer="21"/>
<wire x1="-4.6875" y1="-1.95" x2="4.6875" y2="-1.95" width="0.1524" layer="21"/>
<wire x1="5.585" y1="0" x2="6.35" y2="0" width="0.6096" layer="51"/>
<pad name="1" x="-6.35" y="0" drill="1.1" shape="octagon"/>
<pad name="2" x="6.35" y="0" drill="1.1" shape="octagon"/>
<text x="-4.445" y="2.54" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-4.445" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-5.585" y1="-0.3048" x2="-4.6325" y2="0.3048" layer="21"/>
<rectangle x1="4.6325" y1="-0.3048" x2="5.585" y2="0.3048" layer="21"/>
</package>
<package name="R4527">
<description>&lt;b&gt;Package 4527&lt;/b&gt;&lt;p&gt;
Source: http://www.vishay.com/docs/31059/wsrhigh.pdf</description>
<wire x1="-5.675" y1="-3.375" x2="5.65" y2="-3.375" width="0.2032" layer="21"/>
<wire x1="5.65" y1="-3.375" x2="5.65" y2="3.375" width="0.2032" layer="51"/>
<wire x1="5.65" y1="3.375" x2="-5.675" y2="3.375" width="0.2032" layer="21"/>
<wire x1="-5.675" y1="3.375" x2="-5.675" y2="-3.375" width="0.2032" layer="51"/>
<smd name="1" x="-4.575" y="0" dx="3.94" dy="5.84" layer="1"/>
<smd name="2" x="4.575" y="0" dx="3.94" dy="5.84" layer="1"/>
<text x="-5.715" y="3.81" size="1.27" layer="25">&gt;NAME</text>
<text x="-5.715" y="-5.08" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="WSC0001">
<description>&lt;b&gt;Wirewound Resistors, Precision Power&lt;/b&gt;&lt;p&gt;
Source: VISHAY wscwsn.pdf</description>
<wire x1="-3.075" y1="1.8" x2="-3.075" y2="-1.8" width="0.2032" layer="51"/>
<wire x1="-3.075" y1="-1.8" x2="3.075" y2="-1.8" width="0.2032" layer="21"/>
<wire x1="3.075" y1="-1.8" x2="3.075" y2="1.8" width="0.2032" layer="51"/>
<wire x1="3.075" y1="1.8" x2="-3.075" y2="1.8" width="0.2032" layer="21"/>
<wire x1="-3.075" y1="1.8" x2="-3.075" y2="1.606" width="0.2032" layer="21"/>
<wire x1="-3.075" y1="-1.606" x2="-3.075" y2="-1.8" width="0.2032" layer="21"/>
<wire x1="3.075" y1="1.606" x2="3.075" y2="1.8" width="0.2032" layer="21"/>
<wire x1="3.075" y1="-1.8" x2="3.075" y2="-1.606" width="0.2032" layer="21"/>
<smd name="1" x="-2.675" y="0" dx="2.29" dy="2.92" layer="1"/>
<smd name="2" x="2.675" y="0" dx="2.29" dy="2.92" layer="1"/>
<text x="-2.544" y="2.229" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.544" y="-3.501" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="WSC0002">
<description>&lt;b&gt;Wirewound Resistors, Precision Power&lt;/b&gt;&lt;p&gt;
Source: VISHAY wscwsn.pdf</description>
<wire x1="-5.55" y1="3.375" x2="-5.55" y2="-3.375" width="0.2032" layer="51"/>
<wire x1="-5.55" y1="-3.375" x2="5.55" y2="-3.375" width="0.2032" layer="21"/>
<wire x1="5.55" y1="-3.375" x2="5.55" y2="3.375" width="0.2032" layer="51"/>
<wire x1="5.55" y1="3.375" x2="-5.55" y2="3.375" width="0.2032" layer="21"/>
<smd name="1" x="-4.575" y="0.025" dx="3.94" dy="5.84" layer="1"/>
<smd name="2" x="4.575" y="0" dx="3.94" dy="5.84" layer="1"/>
<text x="-5.65" y="3.9" size="1.27" layer="25">&gt;NAME</text>
<text x="-5.65" y="-5.15" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="WSC01/2">
<description>&lt;b&gt;Wirewound Resistors, Precision Power&lt;/b&gt;&lt;p&gt;
Source: VISHAY wscwsn.pdf</description>
<wire x1="-2.45" y1="1.475" x2="-2.45" y2="-1.475" width="0.2032" layer="51"/>
<wire x1="-2.45" y1="-1.475" x2="2.45" y2="-1.475" width="0.2032" layer="21"/>
<wire x1="2.45" y1="-1.475" x2="2.45" y2="1.475" width="0.2032" layer="51"/>
<wire x1="2.45" y1="1.475" x2="-2.45" y2="1.475" width="0.2032" layer="21"/>
<wire x1="-2.45" y1="1.475" x2="-2.45" y2="1.106" width="0.2032" layer="21"/>
<wire x1="-2.45" y1="-1.106" x2="-2.45" y2="-1.475" width="0.2032" layer="21"/>
<wire x1="2.45" y1="1.106" x2="2.45" y2="1.475" width="0.2032" layer="21"/>
<wire x1="2.45" y1="-1.475" x2="2.45" y2="-1.106" width="0.2032" layer="21"/>
<smd name="1" x="-2.1" y="0" dx="2.16" dy="1.78" layer="1"/>
<smd name="2" x="2.1" y="0" dx="2.16" dy="1.78" layer="1"/>
<text x="-2.544" y="1.904" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.544" y="-3.176" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="WSC2515">
<description>&lt;b&gt;Wirewound Resistors, Precision Power&lt;/b&gt;&lt;p&gt;
Source: VISHAY wscwsn.pdf</description>
<wire x1="-3.075" y1="1.8" x2="-3.075" y2="-1.8" width="0.2032" layer="51"/>
<wire x1="-3.075" y1="-1.8" x2="3.05" y2="-1.8" width="0.2032" layer="21"/>
<wire x1="3.05" y1="-1.8" x2="3.05" y2="1.8" width="0.2032" layer="51"/>
<wire x1="3.05" y1="1.8" x2="-3.075" y2="1.8" width="0.2032" layer="21"/>
<wire x1="-3.075" y1="1.8" x2="-3.075" y2="1.606" width="0.2032" layer="21"/>
<wire x1="-3.075" y1="-1.606" x2="-3.075" y2="-1.8" width="0.2032" layer="21"/>
<wire x1="3.05" y1="1.606" x2="3.05" y2="1.8" width="0.2032" layer="21"/>
<wire x1="3.05" y1="-1.8" x2="3.05" y2="-1.606" width="0.2032" layer="21"/>
<smd name="1" x="-2.675" y="0" dx="2.29" dy="2.92" layer="1"/>
<smd name="2" x="2.675" y="0" dx="2.29" dy="2.92" layer="1"/>
<text x="-3.2" y="2.15" size="1.27" layer="25">&gt;NAME</text>
<text x="-3.2" y="-3.4" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="WSC4527">
<description>&lt;b&gt;Wirewound Resistors, Precision Power&lt;/b&gt;&lt;p&gt;
Source: VISHAY wscwsn.pdf</description>
<wire x1="-5.675" y1="3.4" x2="-5.675" y2="-3.375" width="0.2032" layer="51"/>
<wire x1="-5.675" y1="-3.375" x2="5.675" y2="-3.375" width="0.2032" layer="21"/>
<wire x1="5.675" y1="-3.375" x2="5.675" y2="3.4" width="0.2032" layer="51"/>
<wire x1="5.675" y1="3.4" x2="-5.675" y2="3.4" width="0.2032" layer="21"/>
<smd name="1" x="-4.575" y="0.025" dx="3.94" dy="5.84" layer="1"/>
<smd name="2" x="4.575" y="0" dx="3.94" dy="5.84" layer="1"/>
<text x="-5.775" y="3.925" size="1.27" layer="25">&gt;NAME</text>
<text x="-5.775" y="-5.15" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="WSC6927">
<description>&lt;b&gt;Wirewound Resistors, Precision Power&lt;/b&gt;&lt;p&gt;
Source: VISHAY wscwsn.pdf</description>
<wire x1="-8.65" y1="3.375" x2="-8.65" y2="-3.375" width="0.2032" layer="51"/>
<wire x1="-8.65" y1="-3.375" x2="8.65" y2="-3.375" width="0.2032" layer="21"/>
<wire x1="8.65" y1="-3.375" x2="8.65" y2="3.375" width="0.2032" layer="51"/>
<wire x1="8.65" y1="3.375" x2="-8.65" y2="3.375" width="0.2032" layer="21"/>
<smd name="1" x="-7.95" y="0.025" dx="3.94" dy="5.97" layer="1"/>
<smd name="2" x="7.95" y="0" dx="3.94" dy="5.97" layer="1"/>
<text x="-8.75" y="3.9" size="1.27" layer="25">&gt;NAME</text>
<text x="-8.75" y="-5.15" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="R1218">
<description>&lt;b&gt;CRCW1218 Thick Film, Rectangular Chip Resistors&lt;/b&gt;&lt;p&gt;
Source: http://www.vishay.com .. dcrcw.pdf</description>
<wire x1="-0.913" y1="-2.219" x2="0.939" y2="-2.219" width="0.1524" layer="51"/>
<wire x1="0.913" y1="2.219" x2="-0.939" y2="2.219" width="0.1524" layer="51"/>
<smd name="1" x="-1.475" y="0" dx="1.05" dy="4.9" layer="1"/>
<smd name="2" x="1.475" y="0" dx="1.05" dy="4.9" layer="1"/>
<text x="-2.54" y="2.54" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.81" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.651" y1="-2.3" x2="-0.9009" y2="2.3" layer="51"/>
<rectangle x1="0.9144" y1="-2.3" x2="1.6645" y2="2.3" layer="51"/>
</package>
<package name="1812X7R">
<description>&lt;b&gt;Chip Monolithic Ceramic Capacitors&lt;/b&gt; Medium Voltage High Capacitance for General Use&lt;p&gt;
Source: http://www.murata.com .. GRM43DR72E224KW01.pdf</description>
<wire x1="-1.1" y1="1.5" x2="1.1" y2="1.5" width="0.2032" layer="51"/>
<wire x1="1.1" y1="-1.5" x2="-1.1" y2="-1.5" width="0.2032" layer="51"/>
<wire x1="-0.6" y1="1.5" x2="0.6" y2="1.5" width="0.2032" layer="21"/>
<wire x1="0.6" y1="-1.5" x2="-0.6" y2="-1.5" width="0.2032" layer="21"/>
<smd name="1" x="-1.425" y="0" dx="0.8" dy="3.5" layer="1"/>
<smd name="2" x="1.425" y="0" dx="0.8" dy="3.5" layer="1" rot="R180"/>
<text x="-1.9456" y="1.9958" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.9456" y="-3.7738" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.4" y1="-1.6" x2="-1.1" y2="1.6" layer="51"/>
<rectangle x1="1.1" y1="-1.6" x2="1.4" y2="1.6" layer="51" rot="R180"/>
</package>
<package name="PRL1632">
<description>&lt;b&gt;PRL1632 are realized as 1W for 3.2 × 1.6mm(1206)&lt;/b&gt;&lt;p&gt;
Source: http://www.mouser.com/ds/2/392/products_18-2245.pdf</description>
<wire x1="0.7275" y1="-1.5228" x2="-0.7277" y2="-1.5228" width="0.1524" layer="51"/>
<wire x1="0.7275" y1="1.5228" x2="-0.7152" y2="1.5228" width="0.1524" layer="51"/>
<smd name="2" x="0.822" y="0" dx="1" dy="3.2" layer="1"/>
<smd name="1" x="-0.822" y="0" dx="1" dy="3.2" layer="1"/>
<text x="-1.4" y="1.8" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.4" y="-3" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.8" y1="-1.6" x2="-0.4" y2="1.6" layer="51"/>
<rectangle x1="0.4" y1="-1.6" x2="0.8" y2="1.6" layer="51"/>
</package>
<package name="R01005">
<smd name="1" x="-0.1625" y="0" dx="0.2" dy="0.25" layer="1"/>
<smd name="2" x="0.1625" y="0" dx="0.2" dy="0.25" layer="1"/>
<text x="-0.4" y="0.3" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.4" y="-1.6" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.2" y1="-0.1" x2="-0.075" y2="0.1" layer="51"/>
<rectangle x1="0.075" y1="-0.1" x2="0.2" y2="0.1" layer="51"/>
<rectangle x1="-0.15" y1="0.05" x2="0.15" y2="0.1" layer="51"/>
<rectangle x1="-0.15" y1="-0.1" x2="0.15" y2="-0.05" layer="51"/>
</package>
</packages>
<symbols>
<symbol name="R-EU-1">
<wire x1="-2.54" y1="-0.889" x2="2.54" y2="-0.889" width="0.254" layer="94"/>
<wire x1="2.54" y1="0.889" x2="-2.54" y2="0.889" width="0.254" layer="94"/>
<wire x1="2.54" y1="-0.889" x2="2.54" y2="0.889" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-0.889" x2="-2.54" y2="0.889" width="0.254" layer="94"/>
<text x="-3.81" y="1.4986" size="1.778" layer="95">&gt;NAME</text>
<text x="-3.81" y="-3.302" size="1.778" layer="96">&gt;VALUE</text>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="R-EU_" prefix="R" uservalue="yes">
<description>&lt;B&gt;RESISTOR&lt;/B&gt;, European symbol</description>
<gates>
<gate name="G$1" symbol="R-EU-1" x="0" y="0"/>
</gates>
<devices>
<device name="R0402" package="R0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R0603" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R0805" package="R0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R0805W" package="R0805W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R1206" package="R1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R1206W" package="R1206W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R1210" package="R1210">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R1210W" package="R1210W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R2010" package="R2010">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R2010W" package="R2010W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R2012" package="R2012">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R2012W" package="R2012W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R2512" package="R2512">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R2512W" package="R2512W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R3216" package="R3216">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R3216W" package="R3216W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R3225" package="R3225">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R3225W" package="R3225W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R5025" package="R5025">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R5025W" package="R5025W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R6332" package="R6332">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R6332W" package="R6332W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="M0805" package="M0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="M1206" package="M1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="M1406" package="M1406">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="M2012" package="M2012">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="M2309" package="M2309">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="M3216" package="M3216">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="M3516" package="M3516">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="M5923" package="M5923">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0204/5" package="0204/5">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0204/7" package="0204/7">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0204/2V" package="0204V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0207/10" package="0207/10">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0207/12" package="0207/12">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0207/15" package="0207/15">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0207/2V" package="0207/2V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0207/5V" package="0207/5V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0207/7" package="0207/7">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0309/10" package="0309/10">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0309/12" package="0309/12">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0309/V" package="0309V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0411/12" package="0411/12">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0411/15" package="0411/15">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0411/3V" package="0411V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0414/15" package="0414/15">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0414/5V" package="0414V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0617/17" package="0617/17">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0617/22" package="0617/22">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0617/5V" package="0617V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0922/22" package="0922/22">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0613/5V" package="P0613V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0613/15" package="P0613/15">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0817/22" package="P0817/22">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0817/7V" package="P0817V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="V234/12" package="V234/12">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="V235/17" package="V235/17">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="V526-0" package="V526-0">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="MELF0102R" package="MINI_MELF-0102R">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="MELF0102W" package="MINI_MELF-0102W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="MELF0204R" package="MINI_MELF-0204R">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="MELF0204W" package="MINI_MELF-0204W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="MELF0207R" package="MINI_MELF-0207R">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="MELF0207W" package="MINI_MELF-0207W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0922V" package="0922V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="RDH/15" package="RDH/15">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="MELF0102AX" package="MINI_MELF-0102AX">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R0201" package="R0201">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="VTA52" package="VTA52">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="VTA53" package="VTA53">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="VTA54" package="VTA54">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="VTA55" package="VTA55">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="VTA56" package="VTA56">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="VMTA55" package="VMTA55">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="VMTB60" package="VMTB60">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R4527" package="R4527">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="WSC0001" package="WSC0001">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="WSC0002" package="WSC0002">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="WSC01/2" package="WSC01/2">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="WSC2515" package="WSC2515">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="WSC4527" package="WSC4527">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="WSC6927" package="WSC6927">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R1218" package="R1218">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1812X7R" package="1812X7R">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="PRL1632" package="PRL1632">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="01005" package="R01005">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="supply1">
<description>&lt;b&gt;Supply Symbols&lt;/b&gt;&lt;p&gt;
 GND, VCC, 0V, +5V, -5V, etc.&lt;p&gt;
 Please keep in mind, that these devices are necessary for the
 automatic wiring of the supply signals.&lt;p&gt;
 The pin name defined in the symbol is identical to the net which is to be wired automatically.&lt;p&gt;
 In this library the device names are the same as the pin names of the symbols, therefore the correct signal names appear next to the supply symbols in the schematic.&lt;p&gt;
 &lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="+3V3">
<wire x1="1.27" y1="-1.905" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="-1.905" width="0.254" layer="94"/>
<text x="-2.54" y="-5.08" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="+3V3" x="0" y="-2.54" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
<symbol name="GND">
<wire x1="-1.905" y1="0" x2="1.905" y2="0" width="0.254" layer="94"/>
<text x="-2.54" y="-2.54" size="1.778" layer="96">&gt;VALUE</text>
<pin name="GND" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
</symbol>
<symbol name="+12V">
<wire x1="1.27" y1="-1.905" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="-1.905" width="0.254" layer="94"/>
<wire x1="1.27" y1="-0.635" x2="0" y2="1.27" width="0.254" layer="94"/>
<wire x1="0" y1="1.27" x2="-1.27" y2="-0.635" width="0.254" layer="94"/>
<text x="-2.54" y="-5.08" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="+12V" x="0" y="-2.54" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
<symbol name="+5V">
<wire x1="1.27" y1="-1.905" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="-1.905" width="0.254" layer="94"/>
<text x="-2.54" y="-5.08" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="+5V" x="0" y="-2.54" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="+3V3" prefix="+3V3">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="+3V3" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="GND" prefix="GND">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="GND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="+12V" prefix="P+">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="+12V" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="+5V" prefix="P+">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="+5V" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="transistor">
<description>&lt;b&gt;Transistors&lt;/b&gt;&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
<package name="TO18">
<description>&lt;b&gt;TO 18&lt;/b&gt;</description>
<wire x1="0.9289" y1="2.227" x2="2.227" y2="0.9289" width="0.0508" layer="51" curve="-44.7172"/>
<wire x1="0.9289" y1="-2.227" x2="2.227" y2="-0.9289" width="0.0508" layer="51" curve="44.7172"/>
<wire x1="-3.937" y1="-0.508" x2="-3.937" y2="0.508" width="0.127" layer="21"/>
<wire x1="-2.227" y1="-0.9289" x2="0.929" y2="2.2271" width="0.0508" layer="21" curve="-135.281"/>
<wire x1="-2.227" y1="-0.9289" x2="-0.9289" y2="-2.227" width="0.0508" layer="51" curve="44.7172"/>
<wire x1="-3.937" y1="-0.508" x2="-2.8765" y2="-0.508" width="0.127" layer="21"/>
<wire x1="-0.9289" y1="-2.227" x2="0.9289" y2="-2.227" width="0.0508" layer="21" curve="45.2828"/>
<wire x1="-3.937" y1="0.508" x2="-2.8765" y2="0.508" width="0.127" layer="21"/>
<wire x1="2.227" y1="-0.9289" x2="2.227" y2="0.9289" width="0.0508" layer="21" curve="45.2828"/>
<circle x="0" y="0" radius="2.921" width="0.127" layer="21"/>
<pad name="1" x="-1.27" y="-1.27" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="-1.27" drill="0.8128" shape="octagon"/>
<pad name="3" x="1.27" y="1.27" drill="0.8128" shape="octagon"/>
<text x="3.175" y="0" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="3.175" y="-1.905" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<text x="-1.905" y="-1.27" size="1.27" layer="51" ratio="10">1</text>
<text x="0.635" y="-1.27" size="1.27" layer="51" ratio="10">2</text>
<text x="0" y="0.635" size="1.27" layer="51" ratio="10">3</text>
</package>
<package name="TO3">
<description>&lt;b&gt;TO-3&lt;/b&gt;</description>
<wire x1="6.3754" y1="-10.9982" x2="17.3736" y2="-4.5212" width="0.1524" layer="21"/>
<wire x1="5.969" y1="11.2014" x2="17.3736" y2="4.5212" width="0.1524" layer="21"/>
<wire x1="-6.35" y1="10.9982" x2="-17.272" y2="4.572" width="0.1524" layer="21"/>
<wire x1="-5.9436" y1="-11.2268" x2="-17.2466" y2="-4.5974" width="0.1524" layer="21"/>
<wire x1="17.3366" y1="4.545" x2="19.812" y2="0" width="0.1524" layer="21" curve="-57.1487"/>
<wire x1="17.3366" y1="-4.545" x2="19.812" y2="0" width="0.1524" layer="21" curve="57.1487"/>
<wire x1="-19.812" y1="0" x2="-17.0927" y2="4.6935" width="0.1524" layer="21" curve="-60.1731"/>
<wire x1="-19.812" y1="0" x2="-17.1555" y2="-4.657" width="0.1524" layer="21" curve="59.4042"/>
<wire x1="0" y1="-12.7" x2="6.3718" y2="-10.9859" width="0.1524" layer="21" curve="30.1136"/>
<wire x1="-5.9765" y1="-11.2059" x2="0" y2="-12.7" width="0.1524" layer="21" curve="28.0726"/>
<wire x1="-6.3718" y1="10.9859" x2="0" y2="12.7" width="0.1524" layer="21" curve="-30.1136"/>
<wire x1="0" y1="12.7" x2="5.9765" y2="11.2059" width="0.1524" layer="21" curve="-28.0726"/>
<circle x="0" y="0" radius="9.3726" width="0.1524" layer="21"/>
<circle x="0" y="0" radius="11.684" width="0.0508" layer="21"/>
<circle x="15.113" y="0" radius="2.159" width="0.1524" layer="51"/>
<circle x="-15.113" y="0" radius="2.159" width="0.1524" layer="51"/>
<pad name="E" x="-1.778" y="-5.461" drill="1.1938" diameter="3.1496" shape="long"/>
<pad name="B" x="-1.778" y="5.461" drill="1.1938" diameter="3.1496" shape="long"/>
<pad name="C" x="15.113" y="0" drill="4.1148" diameter="6.477"/>
<pad name="C/" x="-15.113" y="0" drill="4.1148" diameter="6.477"/>
<text x="-5.08" y="1.27" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
<text x="15.24" y="-4.953" size="1.27" layer="51" ratio="10" rot="R90">C</text>
<text x="-3.81" y="-6.223" size="1.27" layer="51" ratio="10" rot="R90">E</text>
<text x="-3.81" y="4.572" size="1.27" layer="51" ratio="10" rot="R90">B</text>
</package>
<package name="TO18-">
<description>&lt;b&gt;TO-18&lt;/b&gt;</description>
<wire x1="-2.2098" y1="-0.9692" x2="2.2098" y2="-0.9692" width="0.0508" layer="21" curve="132.636"/>
<wire x1="-0.9692" y1="2.2098" x2="0.9692" y2="2.2098" width="0.0508" layer="51" curve="-47.3637"/>
<wire x1="-2.2098" y1="0.9692" x2="-2.2098" y2="-0.9692" width="0.0508" layer="51" curve="47.3637"/>
<wire x1="-2.2098" y1="0.9692" x2="-0.9692" y2="2.2098" width="0.0508" layer="21" curve="-42.6363"/>
<wire x1="2.2098" y1="-0.9692" x2="2.2098" y2="0.9692" width="0.0508" layer="51" curve="47.3637"/>
<wire x1="1.649" y1="-2.411" x2="2.413" y2="-3.175" width="0.1524" layer="21"/>
<wire x1="2.411" y1="-1.649" x2="3.175" y2="-2.413" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-3.175" x2="3.175" y2="-2.413" width="0.1524" layer="21"/>
<wire x1="0.9692" y1="2.2098" x2="2.2098" y2="0.9692" width="0.0508" layer="21" curve="-42.6363"/>
<circle x="0" y="0" radius="2.921" width="0.1524" layer="21"/>
<pad name="1" x="1.905" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="0" y="1.905" drill="0.8128" shape="octagon"/>
<pad name="3" x="-1.905" y="0" drill="0.8128" shape="octagon"/>
<text x="3.302" y="0.381" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="3.302" y="-1.27" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<text x="1.016" y="-1.143" size="1.27" layer="51" ratio="10">1</text>
<text x="-0.508" y="0.635" size="1.27" layer="51" ratio="10">2</text>
<text x="-1.905" y="-1.27" size="1.27" layer="51" ratio="10">3</text>
</package>
<package name="TO202">
<description>&lt;b&gt;TO 202 horizontal&lt;/b&gt;</description>
<wire x1="-5.207" y1="-1.27" x2="-5.207" y2="7.3406" width="0.127" layer="21"/>
<wire x1="-3.5052" y1="7.3406" x2="-3.5052" y2="8.8138" width="0.127" layer="21"/>
<wire x1="-3.5052" y1="8.8138" x2="-5.207" y2="8.8138" width="0.127" layer="21"/>
<wire x1="5.207" y1="8.8138" x2="3.5052" y2="8.8138" width="0.127" layer="21"/>
<wire x1="3.5052" y1="8.8138" x2="3.5052" y2="7.3406" width="0.127" layer="21"/>
<wire x1="5.2578" y1="-1.27" x2="5.2578" y2="7.3406" width="0.127" layer="21"/>
<wire x1="-5.207" y1="7.3406" x2="-3.5052" y2="7.3406" width="0.127" layer="21"/>
<wire x1="3.8354" y1="-1.27" x2="3.8354" y2="7.3406" width="0.127" layer="21"/>
<wire x1="-5.207" y1="-1.27" x2="3.8354" y2="-1.27" width="0.127" layer="21"/>
<wire x1="5.207" y1="21.717" x2="5.207" y2="8.8138" width="0.127" layer="21"/>
<wire x1="5.207" y1="21.717" x2="-5.207" y2="21.717" width="0.127" layer="21"/>
<wire x1="-5.207" y1="21.717" x2="-5.207" y2="8.8138" width="0.127" layer="21"/>
<wire x1="-3.5052" y1="7.3406" x2="3.5052" y2="7.3406" width="0.127" layer="21"/>
<wire x1="3.8354" y1="-1.27" x2="5.2578" y2="-1.27" width="0.127" layer="21"/>
<wire x1="3.8354" y1="7.3406" x2="5.2578" y2="7.3406" width="0.127" layer="21"/>
<wire x1="3.5052" y1="7.3406" x2="3.8354" y2="7.3406" width="0.127" layer="21"/>
<wire x1="-4.953" y1="-1.016" x2="4.953" y2="-1.016" width="0.0508" layer="21"/>
<wire x1="4.953" y1="7.112" x2="4.953" y2="-1.016" width="0.0508" layer="21"/>
<wire x1="4.953" y1="7.112" x2="-4.953" y2="7.112" width="0.0508" layer="21"/>
<wire x1="-4.953" y1="-1.016" x2="-4.953" y2="7.112" width="0.0508" layer="21"/>
<circle x="-3.7592" y="-0.2286" radius="0.4318" width="0" layer="21"/>
<circle x="0" y="17.78" radius="1.651" width="0.127" layer="21"/>
<circle x="0" y="17.78" radius="4.191" width="0" layer="42"/>
<circle x="0" y="17.78" radius="4.191" width="0" layer="43"/>
<pad name="1" x="-2.54" y="-5.08" drill="1.1176" shape="long" rot="R90"/>
<pad name="2" x="0" y="-5.08" drill="1.1176" shape="long" rot="R90"/>
<pad name="3" x="2.54" y="-5.08" drill="1.1176" shape="long" rot="R90"/>
<text x="-2.54" y="4.318" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-4.445" y="2.159" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<text x="-2.54" y="-0.635" size="1.27" layer="21" ratio="10">1</text>
<text x="-0.635" y="-0.635" size="1.27" layer="21" ratio="10">2</text>
<text x="1.905" y="-0.635" size="1.27" layer="21" ratio="10">3</text>
<rectangle x1="-0.635" y1="-2.54" x2="0.635" y2="-1.27" layer="21"/>
<rectangle x1="1.905" y1="-2.54" x2="3.175" y2="-1.27" layer="21"/>
<rectangle x1="-3.175" y1="-2.54" x2="-1.905" y2="-1.27" layer="21"/>
<rectangle x1="2.159" y1="-3.429" x2="2.921" y2="-2.54" layer="21"/>
<rectangle x1="2.159" y1="-5.08" x2="2.921" y2="-3.429" layer="51"/>
<rectangle x1="-0.381" y1="-3.429" x2="0.381" y2="-2.54" layer="21"/>
<rectangle x1="-2.921" y1="-3.429" x2="-2.159" y2="-2.54" layer="21"/>
<rectangle x1="-0.381" y1="-5.08" x2="0.381" y2="-3.429" layer="51"/>
<rectangle x1="-2.921" y1="-5.08" x2="-2.159" y2="-3.429" layer="51"/>
<hole x="0" y="17.78" drill="3.302"/>
</package>
<package name="TO202V">
<description>&lt;b&gt;TO 202 vertical&lt;/b&gt;</description>
<wire x1="-4.826" y1="-4.7498" x2="2.794" y2="-4.7498" width="0.127" layer="21"/>
<wire x1="-4.826" y1="-0.127" x2="4.8006" y2="-0.127" width="0.127" layer="21"/>
<wire x1="2.794" y1="-4.7498" x2="4.8006" y2="-2.794" width="0.127" layer="21"/>
<wire x1="-4.826" y1="-4.7498" x2="-4.826" y2="-0.127" width="0.127" layer="21"/>
<wire x1="4.8006" y1="-0.127" x2="4.8006" y2="-2.794" width="0.127" layer="21"/>
<circle x="-4.064" y="-3.9878" radius="0.4318" width="0.127" layer="21"/>
<pad name="1" x="-2.54" y="-2.54" drill="1.1176" shape="long" rot="R90"/>
<pad name="2" x="0" y="-2.54" drill="1.1176" shape="long" rot="R90"/>
<pad name="3" x="2.54" y="-2.54" drill="1.1176" shape="long" rot="R90"/>
<text x="-4.445" y="-6.985" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-4.445" y="-8.89" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<text x="-3.175" y="-1.905" size="1.27" layer="51" ratio="10">1</text>
<text x="-0.635" y="-1.905" size="1.27" layer="51" ratio="10">2</text>
<text x="1.905" y="-1.905" size="1.27" layer="51" ratio="10">3</text>
<rectangle x1="3.429" y1="-2.794" x2="4.826" y2="-2.286" layer="21"/>
<rectangle x1="0.889" y1="-2.794" x2="1.651" y2="-2.286" layer="21"/>
<rectangle x1="-1.651" y1="-2.794" x2="-0.889" y2="-2.286" layer="21"/>
<rectangle x1="-4.826" y1="-2.794" x2="-3.429" y2="-2.286" layer="21"/>
<rectangle x1="-3.429" y1="-2.794" x2="-1.651" y2="-2.286" layer="51"/>
<rectangle x1="-0.889" y1="-2.794" x2="0.889" y2="-2.286" layer="51"/>
<rectangle x1="1.651" y1="-2.794" x2="3.429" y2="-2.286" layer="51"/>
</package>
<package name="SOT93">
<description>SOT-93&lt;p&gt;
grid 5.45 mm</description>
<wire x1="-7.62" y1="-6.35" x2="7.62" y2="-6.35" width="0.1524" layer="21"/>
<wire x1="7.62" y1="6.223" x2="7.62" y2="-6.35" width="0.1524" layer="21"/>
<wire x1="7.62" y1="6.223" x2="6.985" y2="6.223" width="0.1524" layer="21"/>
<wire x1="-7.62" y1="-6.35" x2="-7.62" y2="6.223" width="0.1524" layer="21"/>
<wire x1="-6.985" y1="6.223" x2="-6.985" y2="6.985" width="0.1524" layer="21"/>
<wire x1="-6.985" y1="6.223" x2="-7.62" y2="6.223" width="0.1524" layer="21"/>
<wire x1="-6.985" y1="6.985" x2="-7.62" y2="6.985" width="0.1524" layer="21"/>
<wire x1="-7.62" y1="6.985" x2="-7.62" y2="9.525" width="0.1524" layer="21"/>
<wire x1="-7.62" y1="9.525" x2="-3.1242" y2="13.3096" width="0.1524" layer="21"/>
<wire x1="6.985" y1="6.223" x2="6.985" y2="6.985" width="0.1524" layer="21"/>
<wire x1="6.985" y1="6.223" x2="-6.985" y2="6.223" width="0.1524" layer="21"/>
<wire x1="6.985" y1="6.985" x2="7.62" y2="6.985" width="0.1524" layer="21"/>
<wire x1="7.62" y1="6.985" x2="7.62" y2="9.525" width="0.1524" layer="21"/>
<wire x1="-6.985" y1="-5.715" x2="6.985" y2="-5.715" width="0.0508" layer="21"/>
<wire x1="6.985" y1="5.715" x2="6.985" y2="-5.715" width="0.0508" layer="21"/>
<wire x1="6.985" y1="5.715" x2="-6.985" y2="5.715" width="0.0508" layer="21"/>
<wire x1="-6.985" y1="-5.715" x2="-6.985" y2="5.715" width="0.0508" layer="21"/>
<wire x1="0" y1="14.605" x2="3.1431" y2="13.3031" width="0.1524" layer="21" curve="-45.0001"/>
<wire x1="-3.1431" y1="13.3031" x2="0" y2="14.605" width="0.1524" layer="21" curve="-45.0001"/>
<wire x1="7.62" y1="9.525" x2="3.1242" y2="13.335" width="0.1524" layer="21"/>
<wire x1="6.985" y1="-5.715" x2="7.62" y2="-6.35" width="0.0508" layer="51"/>
<wire x1="6.985" y1="5.715" x2="7.62" y2="6.35" width="0.0508" layer="51"/>
<wire x1="-6.985" y1="5.715" x2="-7.62" y2="6.35" width="0.0508" layer="51"/>
<wire x1="-6.985" y1="-5.715" x2="-7.62" y2="-6.35" width="0.0508" layer="51"/>
<circle x="0" y="10.16" radius="2.159" width="0.1524" layer="21"/>
<circle x="0" y="10.16" radius="5.08" width="0" layer="42"/>
<circle x="0" y="10.16" radius="5.08" width="0" layer="43"/>
<pad name="B" x="-5.461" y="-10.16" drill="1.397" shape="long" rot="R90"/>
<pad name="C" x="0" y="-10.16" drill="1.397" shape="long" rot="R90"/>
<pad name="E" x="5.461" y="-10.16" drill="1.397" shape="long" rot="R90"/>
<text x="-5.08" y="2.54" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="0" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
<text x="-6.477" y="6.604" size="1.016" layer="21" ratio="10">A20,3mm</text>
<rectangle x1="-6.096" y1="-7.366" x2="-4.318" y2="-6.35" layer="21"/>
<rectangle x1="4.318" y1="-7.366" x2="6.096" y2="-6.35" layer="21"/>
<rectangle x1="-1.143" y1="-7.366" x2="1.143" y2="-6.35" layer="21"/>
<rectangle x1="-6.096" y1="-8.128" x2="-4.826" y2="-6.35" layer="21"/>
<rectangle x1="-0.635" y1="-8.128" x2="0.635" y2="-6.35" layer="21"/>
<rectangle x1="4.826" y1="-8.128" x2="6.096" y2="-6.35" layer="21"/>
<rectangle x1="-6.096" y1="-10.287" x2="-4.826" y2="-8.128" layer="51"/>
<rectangle x1="-0.635" y1="-10.287" x2="0.635" y2="-8.128" layer="51"/>
<rectangle x1="4.826" y1="-10.287" x2="6.096" y2="-8.128" layer="51"/>
<hole x="0" y="10.16" drill="4.1148"/>
</package>
<package name="SOT93V">
<description>SOT-93&lt;p&gt;
grid 5.45 mm</description>
<wire x1="7.62" y1="-2.032" x2="3.429" y2="-2.032" width="0.1524" layer="51"/>
<wire x1="3.429" y1="-2.032" x2="2.032" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="2.032" y1="-2.032" x2="-2.032" y2="-2.032" width="0.1524" layer="51"/>
<wire x1="-2.032" y1="-2.032" x2="-3.429" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="-3.429" y1="-2.032" x2="-7.62" y2="-2.032" width="0.1524" layer="51"/>
<wire x1="-7.62" y1="0" x2="7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-7.239" y1="-5.08" x2="7.239" y2="-5.08" width="0.1524" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="-7.62" y1="-2.032" x2="-7.239" y2="-5.08" width="0.1524" layer="21"/>
<wire x1="7.62" y1="0" x2="7.62" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="7.62" y1="-2.032" x2="7.239" y2="-5.08" width="0.1524" layer="21"/>
<pad name="B" x="-5.461" y="-2.54" drill="1.397" shape="long" rot="R90"/>
<pad name="C" x="0" y="-2.54" drill="1.397" shape="long" rot="R90"/>
<pad name="E" x="5.461" y="-2.54" drill="1.397" shape="long" rot="R90"/>
<text x="-1.397" y="-6.858" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<text x="-7.62" y="-6.858" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<rectangle x1="-7.62" y1="-2.032" x2="7.62" y2="0" layer="51"/>
</package>
<package name="TO126">
<description>&lt;b&gt;TO 126 horizontal&lt;/b&gt;</description>
<wire x1="-3.937" y1="-1.27" x2="-3.937" y2="9.144" width="0.127" layer="21"/>
<wire x1="-3.937" y1="9.144" x2="3.937" y2="9.144" width="0.127" layer="21"/>
<wire x1="3.937" y1="9.144" x2="3.937" y2="-1.27" width="0.127" layer="21"/>
<wire x1="-3.937" y1="-1.27" x2="3.937" y2="-1.27" width="0.127" layer="21"/>
<wire x1="1.2393" y1="3.1591" x2="1.9493" y2="3.8858" width="0.127" layer="21" curve="25.6775"/>
<wire x1="0.7687" y1="3.6188" x2="1.2393" y2="3.1591" width="0.127" layer="21"/>
<wire x1="-0.7687" y1="3.6188" x2="-1.2393" y2="3.1591" width="0.127" layer="21"/>
<wire x1="-1.4787" y1="4.3456" x2="-1.9493" y2="3.8858" width="0.127" layer="21"/>
<wire x1="-1.9493" y1="3.8858" x2="-1.2393" y2="3.1591" width="0.127" layer="21" curve="25.6775"/>
<wire x1="0.508" y1="6.6509" x2="0.508" y2="7.3088" width="0.127" layer="21"/>
<wire x1="-0.508" y1="6.6509" x2="-0.508" y2="7.3088" width="0.127" layer="21"/>
<wire x1="-0.508" y1="7.3088" x2="0.508" y2="7.3088" width="0.127" layer="21" curve="-25.6796"/>
<wire x1="1.4787" y1="4.3456" x2="1.9493" y2="3.8858" width="0.127" layer="21"/>
<circle x="-3.1242" y="-0.4826" radius="0.4318" width="0" layer="21"/>
<circle x="0" y="5.08" radius="3.81" width="0" layer="42"/>
<circle x="0" y="5.08" radius="3.81" width="0" layer="43"/>
<circle x="0" y="5.08" radius="1.651" width="0.127" layer="21"/>
<pad name="1" x="-2.286" y="-5.08" drill="1.1176" shape="long" rot="R90"/>
<pad name="2" x="0" y="-5.08" drill="1.1176" shape="long" rot="R90"/>
<pad name="3" x="2.286" y="-5.08" drill="1.1176" shape="long" rot="R90"/>
<text x="-2.54" y="7.62" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.175" y="1.397" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<text x="-2.54" y="-0.889" size="1.27" layer="51" ratio="10">1</text>
<text x="-0.508" y="-0.889" size="1.27" layer="51" ratio="10">2</text>
<text x="1.778" y="-0.889" size="1.27" layer="51" ratio="10">3</text>
<rectangle x1="-0.635" y1="-3.048" x2="0.635" y2="-1.27" layer="21"/>
<rectangle x1="-2.667" y1="-3.429" x2="-1.905" y2="-1.27" layer="21"/>
<rectangle x1="-2.667" y1="-5.207" x2="-1.905" y2="-3.429" layer="51"/>
<rectangle x1="-0.381" y1="-3.429" x2="0.381" y2="-1.27" layer="21"/>
<rectangle x1="-0.381" y1="-5.207" x2="0.381" y2="-3.429" layer="51"/>
<rectangle x1="1.905" y1="-3.429" x2="2.667" y2="-1.27" layer="21"/>
<rectangle x1="1.905" y1="-5.207" x2="2.667" y2="-3.429" layer="51"/>
<hole x="0" y="5.08" drill="3.302"/>
</package>
<package name="TO126V">
<description>&lt;b&gt;TO 126 vertical&lt;/b&gt;</description>
<wire x1="-3.937" y1="-0.127" x2="-3.937" y2="-1.27" width="0.127" layer="21"/>
<wire x1="-3.937" y1="-1.27" x2="-3.683" y2="-2.794" width="0.127" layer="21"/>
<wire x1="3.683" y1="-2.794" x2="3.937" y2="-1.27" width="0.127" layer="21"/>
<wire x1="3.937" y1="-1.27" x2="3.937" y2="-0.127" width="0.127" layer="21"/>
<wire x1="-3.683" y1="-2.794" x2="-2.794" y2="-2.794" width="0.127" layer="21"/>
<wire x1="-2.794" y1="-2.794" x2="-1.778" y2="-2.794" width="0.127" layer="51"/>
<wire x1="-1.778" y1="-2.794" x2="-0.508" y2="-2.794" width="0.127" layer="21"/>
<wire x1="-0.508" y1="-2.794" x2="0.508" y2="-2.794" width="0.127" layer="51"/>
<wire x1="0.508" y1="-2.794" x2="1.778" y2="-2.794" width="0.127" layer="21"/>
<wire x1="1.778" y1="-2.794" x2="2.794" y2="-2.794" width="0.127" layer="51"/>
<wire x1="2.794" y1="-2.794" x2="3.683" y2="-2.794" width="0.127" layer="21"/>
<circle x="-3.175" y="-2.159" radius="0.4064" width="0.127" layer="51"/>
<pad name="1" x="-2.286" y="-1.27" drill="1.016" shape="long" rot="R90"/>
<pad name="2" x="0" y="-1.27" drill="1.016" shape="long" rot="R90"/>
<pad name="3" x="2.286" y="-1.27" drill="1.016" shape="long" rot="R90"/>
<text x="-3.9624" y="-4.5466" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.9878" y="-6.3246" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<text x="-2.54" y="-1.905" size="1.27" layer="51" ratio="10">1</text>
<text x="-0.381" y="-1.905" size="1.27" layer="51" ratio="10">2</text>
<text x="2.032" y="-1.905" size="1.27" layer="51" ratio="10">3</text>
<rectangle x1="-3.937" y1="-0.381" x2="-3.175" y2="0" layer="21"/>
<rectangle x1="-1.397" y1="-0.381" x2="-0.889" y2="0" layer="21"/>
<rectangle x1="0.889" y1="-0.381" x2="1.397" y2="0" layer="21"/>
<rectangle x1="3.175" y1="-0.381" x2="3.937" y2="0" layer="21"/>
<rectangle x1="-3.175" y1="-0.381" x2="-1.397" y2="0" layer="51"/>
<rectangle x1="-0.889" y1="-0.381" x2="0.889" y2="0" layer="51"/>
<rectangle x1="1.397" y1="-0.381" x2="3.175" y2="0" layer="51"/>
</package>
<package name="TO218">
<description>&lt;b&gt;TO 218 horizontal&lt;/b&gt;</description>
<wire x1="-7.62" y1="-6.35" x2="7.62" y2="-6.35" width="0.127" layer="21"/>
<wire x1="7.62" y1="6.223" x2="7.62" y2="0" width="0.127" layer="21"/>
<wire x1="7.62" y1="0" x2="7.62" y2="-3.81" width="0.127" layer="21"/>
<wire x1="7.62" y1="-3.81" x2="7.62" y2="-6.35" width="0.127" layer="21"/>
<wire x1="-7.62" y1="-6.35" x2="-7.62" y2="-3.81" width="0.127" layer="21"/>
<wire x1="-7.62" y1="-3.81" x2="-7.62" y2="0" width="0.127" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="6.223" width="0.127" layer="21"/>
<wire x1="7.62" y1="6.223" x2="-7.62" y2="6.223" width="0.127" layer="21"/>
<wire x1="6.985" y1="5.715" x2="-6.985" y2="5.715" width="0.0508" layer="21"/>
<wire x1="6.985" y1="0.508" x2="6.985" y2="5.715" width="0.0508" layer="21"/>
<wire x1="-6.985" y1="-5.715" x2="6.985" y2="-5.715" width="0.0508" layer="21"/>
<wire x1="7.62" y1="12.065" x2="5.08" y2="14.605" width="0.127" layer="21"/>
<wire x1="7.62" y1="12.065" x2="7.62" y2="6.223" width="0.127" layer="21"/>
<wire x1="5.08" y1="14.605" x2="-5.08" y2="14.605" width="0.127" layer="21"/>
<wire x1="-7.62" y1="12.065" x2="-5.08" y2="14.605" width="0.127" layer="21"/>
<wire x1="-7.62" y1="12.065" x2="-7.62" y2="6.223" width="0.127" layer="21"/>
<wire x1="7.62" y1="0" x2="7.62" y2="-3.81" width="0.127" layer="21" curve="180"/>
<wire x1="6.985" y1="-5.715" x2="6.985" y2="-4.318" width="0.0508" layer="21"/>
<wire x1="6.9522" y1="0.4991" x2="6.9851" y2="-4.3178" width="0.0508" layer="21" curve="149.733"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-3.81" width="0.127" layer="21" curve="-180"/>
<wire x1="-6.985" y1="-5.715" x2="-6.985" y2="-4.318" width="0.0508" layer="21"/>
<wire x1="-6.985" y1="0.508" x2="-6.985" y2="5.715" width="0.0508" layer="21"/>
<wire x1="-6.985" y1="-4.318" x2="-6.985" y2="0.508" width="0.0508" layer="21" curve="150.513"/>
<circle x="0" y="10.16" radius="2.159" width="0.127" layer="21"/>
<circle x="0" y="10.16" radius="5.08" width="0" layer="42"/>
<circle x="0" y="10.16" radius="5.08" width="0" layer="43"/>
<pad name="1" x="-5.461" y="-10.16" drill="1.397" shape="long" rot="R90"/>
<pad name="2" x="0" y="-10.16" drill="1.397" shape="long" rot="R90"/>
<pad name="3" x="5.461" y="-10.16" drill="1.397" shape="long" rot="R90"/>
<text x="-5.08" y="2.54" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="0" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
<text x="-5.08" y="-5.08" size="1.27" layer="21" ratio="10">1</text>
<text x="-0.635" y="-5.08" size="1.27" layer="21" ratio="10">2</text>
<text x="4.445" y="-5.08" size="1.27" layer="21" ratio="10">3</text>
<rectangle x1="-6.096" y1="-7.366" x2="-4.318" y2="-6.35" layer="21"/>
<rectangle x1="4.318" y1="-7.366" x2="6.096" y2="-6.35" layer="21"/>
<rectangle x1="-1.143" y1="-7.366" x2="1.143" y2="-6.35" layer="21"/>
<rectangle x1="-6.096" y1="-8.128" x2="-4.826" y2="-6.35" layer="21"/>
<rectangle x1="-6.096" y1="-10.287" x2="-4.826" y2="-8.128" layer="51"/>
<rectangle x1="-0.635" y1="-8.128" x2="0.635" y2="-6.35" layer="21"/>
<rectangle x1="4.826" y1="-8.128" x2="6.096" y2="-6.35" layer="21"/>
<rectangle x1="-0.635" y1="-10.287" x2="0.635" y2="-8.128" layer="51"/>
<rectangle x1="4.826" y1="-10.287" x2="6.096" y2="-8.128" layer="51"/>
<hole x="0" y="10.16" drill="4.1148"/>
</package>
<package name="TO218V">
<description>&lt;b&gt;TO 218 vertical&lt;/b&gt;</description>
<wire x1="-6.985" y1="-5.08" x2="6.985" y2="-5.08" width="0.127" layer="21"/>
<wire x1="6.985" y1="-5.08" x2="7.366" y2="-4.699" width="0.127" layer="21"/>
<wire x1="7.366" y1="-4.699" x2="7.62" y2="-1.524" width="0.127" layer="21"/>
<wire x1="-6.985" y1="-5.08" x2="-7.366" y2="-4.699" width="0.127" layer="21"/>
<wire x1="-7.366" y1="-4.699" x2="-7.62" y2="-1.524" width="0.127" layer="21"/>
<pad name="1" x="-5.461" y="-3.175" drill="1.397" shape="long" rot="R90"/>
<pad name="2" x="0" y="-3.175" drill="1.397" shape="long" rot="R90"/>
<pad name="3" x="5.461" y="-3.175" drill="1.397" shape="long" rot="R90"/>
<text x="-7.62" y="-6.858" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.016" y="-6.858" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<text x="-6.096" y="-4.445" size="1.27" layer="51" ratio="10">1</text>
<text x="-0.635" y="-4.445" size="1.27" layer="51" ratio="10">2</text>
<text x="4.826" y="-4.445" size="1.27" layer="51" ratio="10">3</text>
<rectangle x1="-7.874" y1="-1.143" x2="7.874" y2="0" layer="21"/>
<rectangle x1="-3.556" y1="-2.032" x2="-1.905" y2="-1.143" layer="21"/>
<rectangle x1="1.905" y1="-2.032" x2="3.556" y2="-1.143" layer="21"/>
<rectangle x1="-7.874" y1="-2.032" x2="-7.239" y2="-1.143" layer="21"/>
<rectangle x1="7.239" y1="-2.032" x2="7.874" y2="-1.143" layer="21"/>
<rectangle x1="3.556" y1="-2.032" x2="7.239" y2="-1.143" layer="51"/>
<rectangle x1="-7.239" y1="-2.032" x2="-3.556" y2="-1.143" layer="51"/>
<rectangle x1="-1.905" y1="-2.032" x2="1.905" y2="-1.143" layer="51"/>
</package>
<package name="TO220">
<description>&lt;b&gt;TO 220 horizontal&lt;/b&gt;</description>
<wire x1="-5.207" y1="-1.27" x2="5.207" y2="-1.27" width="0.127" layer="21"/>
<wire x1="5.207" y1="14.605" x2="-5.207" y2="14.605" width="0.127" layer="21"/>
<wire x1="5.207" y1="-1.27" x2="5.207" y2="11.176" width="0.127" layer="21"/>
<wire x1="5.207" y1="11.176" x2="4.318" y2="11.176" width="0.127" layer="21"/>
<wire x1="4.318" y1="11.176" x2="4.318" y2="12.7" width="0.127" layer="21"/>
<wire x1="4.318" y1="12.7" x2="5.207" y2="12.7" width="0.127" layer="21"/>
<wire x1="5.207" y1="12.7" x2="5.207" y2="14.605" width="0.127" layer="21"/>
<wire x1="-5.207" y1="-1.27" x2="-5.207" y2="11.176" width="0.127" layer="21"/>
<wire x1="-5.207" y1="11.176" x2="-4.318" y2="11.176" width="0.127" layer="21"/>
<wire x1="-4.318" y1="11.176" x2="-4.318" y2="12.7" width="0.127" layer="21"/>
<wire x1="-4.318" y1="12.7" x2="-5.207" y2="12.7" width="0.127" layer="21"/>
<wire x1="-5.207" y1="12.7" x2="-5.207" y2="14.605" width="0.127" layer="21"/>
<wire x1="-4.572" y1="-0.635" x2="4.572" y2="-0.635" width="0.0508" layer="21"/>
<wire x1="4.572" y1="7.62" x2="4.572" y2="-0.635" width="0.0508" layer="21"/>
<wire x1="4.572" y1="7.62" x2="-4.572" y2="7.62" width="0.0508" layer="21"/>
<wire x1="-4.572" y1="-0.635" x2="-4.572" y2="7.62" width="0.0508" layer="21"/>
<circle x="0" y="11.176" radius="1.8034" width="0.127" layer="21"/>
<circle x="0" y="11.176" radius="4.191" width="0" layer="42"/>
<circle x="0" y="11.176" radius="4.191" width="0" layer="43"/>
<pad name="1" x="-2.54" y="-6.35" drill="1.1176" shape="long" rot="R90"/>
<pad name="2" x="0" y="-6.35" drill="1.1176" shape="long" rot="R90"/>
<pad name="3" x="2.54" y="-6.35" drill="1.1176" shape="long" rot="R90"/>
<text x="-3.81" y="5.207" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.937" y="2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
<text x="-4.445" y="7.874" size="0.9906" layer="21" ratio="12">A17,5mm</text>
<text x="-3.175" y="0" size="1.27" layer="51" ratio="10">1</text>
<text x="-0.635" y="0" size="1.27" layer="51" ratio="10">2</text>
<text x="1.905" y="0" size="1.27" layer="51" ratio="10">3</text>
<rectangle x1="2.159" y1="-4.699" x2="2.921" y2="-4.064" layer="21"/>
<rectangle x1="-0.381" y1="-4.699" x2="0.381" y2="-4.064" layer="21"/>
<rectangle x1="-2.921" y1="-4.699" x2="-2.159" y2="-4.064" layer="21"/>
<rectangle x1="-3.175" y1="-4.064" x2="-1.905" y2="-1.27" layer="21"/>
<rectangle x1="-0.635" y1="-4.064" x2="0.635" y2="-1.27" layer="21"/>
<rectangle x1="1.905" y1="-4.064" x2="3.175" y2="-1.27" layer="21"/>
<rectangle x1="-2.921" y1="-6.604" x2="-2.159" y2="-4.699" layer="51"/>
<rectangle x1="-0.381" y1="-6.604" x2="0.381" y2="-4.699" layer="51"/>
<rectangle x1="2.159" y1="-6.604" x2="2.921" y2="-4.699" layer="51"/>
<hole x="0" y="11.176" drill="3.302"/>
</package>
<package name="TO220V">
<description>&lt;b&gt;TO 220 vertical&lt;/b&gt;</description>
<wire x1="5.08" y1="-1.143" x2="4.953" y2="-4.064" width="0.127" layer="21"/>
<wire x1="4.699" y1="-4.318" x2="4.953" y2="-4.064" width="0.127" layer="21"/>
<wire x1="4.699" y1="-4.318" x2="-4.699" y2="-4.318" width="0.127" layer="21"/>
<wire x1="-4.953" y1="-4.064" x2="-4.699" y2="-4.318" width="0.127" layer="21"/>
<wire x1="-4.953" y1="-4.064" x2="-5.08" y2="-1.143" width="0.127" layer="21"/>
<circle x="-4.4958" y="-3.7084" radius="0.254" width="0.127" layer="21"/>
<pad name="1" x="-2.54" y="-2.54" drill="1.016" shape="long" rot="R90"/>
<pad name="2" x="0" y="-2.54" drill="1.016" shape="long" rot="R90"/>
<pad name="3" x="2.54" y="-2.54" drill="1.016" shape="long" rot="R90"/>
<text x="-5.08" y="-6.0452" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-7.62" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-5.334" y1="-0.762" x2="5.334" y2="0" layer="21"/>
<rectangle x1="-5.334" y1="-1.27" x2="-3.429" y2="-0.762" layer="21"/>
<rectangle x1="-1.651" y1="-1.27" x2="-0.889" y2="-0.762" layer="21"/>
<rectangle x1="-3.429" y1="-1.27" x2="-1.651" y2="-0.762" layer="51"/>
<rectangle x1="0.889" y1="-1.27" x2="1.651" y2="-0.762" layer="21"/>
<rectangle x1="3.429" y1="-1.27" x2="5.334" y2="-0.762" layer="21"/>
<rectangle x1="-0.889" y1="-1.27" x2="0.889" y2="-0.762" layer="51"/>
<rectangle x1="1.651" y1="-1.27" x2="3.429" y2="-0.762" layer="51"/>
</package>
<package name="TO225AA">
<description>&lt;b&gt;TO-225&lt;/b&gt;&lt;p&gt;
grid 2.54 mm, vertical</description>
<wire x1="-3.4925" y1="-6.985" x2="-3.4925" y2="3.81" width="0.127" layer="21"/>
<wire x1="-3.4925" y1="3.81" x2="3.4925" y2="3.81" width="0.127" layer="21"/>
<wire x1="3.4925" y1="3.81" x2="3.4925" y2="-6.985" width="0.127" layer="21"/>
<wire x1="-3.4925" y1="-6.985" x2="3.4925" y2="-6.985" width="0.127" layer="21"/>
<wire x1="-2.381" y1="-8.096" x2="-2.381" y2="-9.525" width="0.4064" layer="21"/>
<wire x1="0" y1="-7.779" x2="0" y2="-9.525" width="0.4064" layer="21"/>
<wire x1="2.3813" y1="-8.0963" x2="2.381" y2="-9.525" width="0.4064" layer="21"/>
<wire x1="-2.54" y1="3.175" x2="-2.54" y2="-3.334" width="0.127" layer="51"/>
<wire x1="-2.54" y1="-3.334" x2="2.54" y2="-3.334" width="0.127" layer="51"/>
<wire x1="2.54" y1="-3.334" x2="2.54" y2="3.175" width="0.127" layer="51"/>
<wire x1="2.54" y1="3.175" x2="-2.54" y2="3.175" width="0.127" layer="51"/>
<wire x1="-1.949" y1="1.194" x2="-1.2391" y2="1.9206" width="0.127" layer="21" curve="-25.6749"/>
<wire x1="-0.769" y1="1.461" x2="-1.239" y2="1.921" width="0.127" layer="21"/>
<wire x1="0.769" y1="1.461" x2="1.239" y2="1.921" width="0.127" layer="21"/>
<wire x1="1.479" y1="0.734" x2="1.949" y2="1.194" width="0.127" layer="21"/>
<wire x1="1.239" y1="1.921" x2="1.949" y2="1.1944" width="0.127" layer="21" curve="-25.6741"/>
<wire x1="-0.508" y1="-1.571" x2="-0.508" y2="-2.229" width="0.127" layer="21"/>
<wire x1="0.508" y1="-1.571" x2="0.508" y2="-2.229" width="0.127" layer="21"/>
<wire x1="-0.508" y1="-2.229" x2="0.508" y2="-2.229" width="0.127" layer="21" curve="25.6785"/>
<wire x1="-1.479" y1="0.734" x2="-1.949" y2="1.194" width="0.127" layer="21"/>
<circle x="0" y="0" radius="3.81" width="0" layer="42"/>
<circle x="0" y="0" radius="3.81" width="0" layer="43"/>
<circle x="0" y="0" radius="1.651" width="0.127" layer="21"/>
<pad name="1" x="-2.3813" y="-11.43" drill="0.8128" shape="long" rot="R90"/>
<pad name="2" x="0" y="-11.43" drill="0.8128" shape="long" rot="R90"/>
<pad name="3" x="2.3813" y="-11.43" drill="0.8128" shape="long" rot="R90"/>
<text x="-2.6988" y="-6.6675" size="1.27" layer="51">1</text>
<text x="-0.4763" y="-6.6675" size="1.27" layer="51">2</text>
<text x="1.905" y="-6.6675" size="1.27" layer="51">3</text>
<text x="3.0163" y="5.3975" size="1.27" layer="25" ratio="10" rot="R180">&gt;NAME</text>
<text x="3.4925" y="-3.7783" size="1.27" layer="27" ratio="10" rot="R180">&gt;VALUE</text>
<rectangle x1="-2.6988" y1="-8.255" x2="-2.0638" y2="-6.985" layer="21"/>
<rectangle x1="2.0638" y1="-8.255" x2="2.6988" y2="-6.985" layer="21"/>
<rectangle x1="-0.3175" y1="-7.9375" x2="0.3175" y2="-6.985" layer="21"/>
<hole x="0" y="0" drill="3.302"/>
</package>
<package name="TO39">
<description>&lt;b&gt;TO-39&lt;/b&gt;</description>
<wire x1="-4.0386" y1="-3.5306" x2="-3.5052" y2="-2.9972" width="0.1524" layer="21"/>
<wire x1="-2.9718" y1="-3.5306" x2="-3.5052" y2="-4.064" width="0.1524" layer="21"/>
<wire x1="-3.5052" y1="-4.064" x2="-4.0386" y2="-3.5306" width="0.1524" layer="21"/>
<circle x="0" y="0" radius="4.572" width="0.1524" layer="21"/>
<circle x="0" y="0" radius="3.8608" width="0.0508" layer="21"/>
<pad name="1" x="0" y="-2.54" drill="0.8128" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="0.8128" shape="octagon"/>
<pad name="3" x="0" y="2.54" drill="0.8128" shape="octagon"/>
<text x="-2.54" y="5.08" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.175" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="TO5">
<description>&lt;b&gt;TO 5&lt;/b&gt;</description>
<wire x1="-4.0386" y1="-3.5306" x2="-3.5052" y2="-2.9972" width="0.127" layer="21"/>
<wire x1="-2.9718" y1="-3.5306" x2="-3.5052" y2="-4.064" width="0.127" layer="21"/>
<wire x1="-3.5052" y1="-4.064" x2="-4.0386" y2="-3.5306" width="0.127" layer="21"/>
<circle x="0" y="0" radius="4.572" width="0.127" layer="21"/>
<circle x="0" y="0" radius="3.8608" width="0.0508" layer="21"/>
<pad name="1" x="0" y="-2.54" drill="0.8128" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="0.8128" shape="octagon"/>
<pad name="3" x="0" y="2.54" drill="0.8128" shape="octagon"/>
<text x="-2.54" y="5.08" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.175" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="TO66">
<description>&lt;b&gt;TO-66&lt;/b&gt;</description>
<wire x1="-3.501" y1="-7.895" x2="3.5012" y2="-7.895" width="0.1524" layer="21" curve="47.8332"/>
<wire x1="-3.501" y1="7.895" x2="3.5012" y2="7.895" width="0.1524" layer="21" curve="-47.8332"/>
<wire x1="-13.711" y1="3.3666" x2="-13.711" y2="-3.367" width="0.1524" layer="21" curve="132.168"/>
<wire x1="-3.501" y1="-7.895" x2="-13.711" y2="-3.367" width="0.1524" layer="21"/>
<wire x1="-3.501" y1="7.895" x2="-13.711" y2="3.367" width="0.1524" layer="21"/>
<wire x1="13.711" y1="-3.367" x2="13.711" y2="3.3666" width="0.1524" layer="21" curve="132.168"/>
<wire x1="3.501" y1="-7.895" x2="13.711" y2="-3.367" width="0.1524" layer="21"/>
<wire x1="3.501" y1="7.895" x2="13.711" y2="3.367" width="0.1524" layer="21"/>
<circle x="0" y="0" radius="7.874" width="0.0508" layer="21"/>
<circle x="0" y="0" radius="6.35" width="0.1524" layer="21"/>
<circle x="-12.192" y="0" radius="1.905" width="0.1524" layer="51"/>
<circle x="12.192" y="0" radius="1.905" width="0.1524" layer="51"/>
<pad name="TO66" x="-12.192" y="0" drill="3.302" diameter="6.35"/>
<pad name="3" x="12.192" y="0" drill="3.302" diameter="6.35"/>
<pad name="2" x="-2.54" y="2.54" drill="1.1176" diameter="2.54" shape="long"/>
<pad name="1" x="-2.54" y="-2.54" drill="1.1176" diameter="2.54" shape="long"/>
<text x="8.89" y="-4.445" size="1.27" layer="21" ratio="10">3</text>
<text x="1.27" y="-2.54" size="1.27" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<text x="3.175" y="-3.175" size="1.27" layer="27" ratio="10" rot="R90">&gt;VALUE</text>
<text x="-5.08" y="1.905" size="1.27" layer="21" ratio="10">2</text>
<text x="-5.08" y="-3.175" size="1.27" layer="21" ratio="10">1</text>
</package>
<package name="TO92">
<description>&lt;b&gt;TO 92&lt;/b&gt;</description>
<wire x1="-2.0946" y1="-1.651" x2="-2.6549" y2="-0.254" width="0.127" layer="21" curve="-32.781"/>
<wire x1="-2.6549" y1="-0.254" x2="-0.7863" y2="2.5485" width="0.127" layer="21" curve="-78.3185"/>
<wire x1="0.7863" y1="2.5484" x2="2.0945" y2="-1.651" width="0.127" layer="21" curve="-111.1"/>
<wire x1="-2.0945" y1="-1.651" x2="2.0945" y2="-1.651" width="0.127" layer="21"/>
<wire x1="-2.2537" y1="-0.254" x2="-0.2863" y2="-0.254" width="0.127" layer="51"/>
<wire x1="-2.6549" y1="-0.254" x2="-2.2537" y2="-0.254" width="0.127" layer="21"/>
<wire x1="-0.2863" y1="-0.254" x2="0.2863" y2="-0.254" width="0.127" layer="21"/>
<wire x1="2.2537" y1="-0.254" x2="2.6549" y2="-0.254" width="0.127" layer="21"/>
<wire x1="0.2863" y1="-0.254" x2="2.2537" y2="-0.254" width="0.127" layer="51"/>
<wire x1="-0.7863" y1="2.5485" x2="0.7863" y2="2.5485" width="0.127" layer="51" curve="-34.2936"/>
<pad name="1" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="0" y="1.905" drill="0.8128" shape="octagon"/>
<pad name="3" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="3.175" y="0.635" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="3.175" y="-1.27" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<text x="-0.635" y="0.635" size="1.27" layer="51" ratio="10">2</text>
<text x="-2.159" y="0" size="1.27" layer="51" ratio="10">3</text>
<text x="1.143" y="0" size="1.27" layer="51" ratio="10">1</text>
</package>
<package name="TO92-EBC">
<description>&lt;b&gt;TO-92&lt;/b&gt; Pads In Line E B C from top&lt;p&gt;</description>
<wire x1="-2.095" y1="-2.921" x2="2.095" y2="-2.921" width="0.127" layer="21"/>
<wire x1="-2.413" y1="-0.1341" x2="2.413" y2="-0.1341" width="0.127" layer="21" curve="-129.583"/>
<wire x1="1.136" y1="-1.397" x2="-1.136" y2="-1.397" width="0.127" layer="51"/>
<wire x1="-2.413" y1="-0.1341" x2="-2.664" y2="-1.397" width="0.127" layer="51" curve="27.9376"/>
<wire x1="-2.664" y1="-1.397" x2="-2.413" y2="-2.4059" width="0.127" layer="51" curve="22.4788"/>
<wire x1="-1.404" y1="-1.397" x2="-2.664" y2="-1.397" width="0.127" layer="51"/>
<wire x1="-2.4135" y1="-2.4059" x2="-2.095" y2="-2.921" width="0.127" layer="21" curve="13.0385"/>
<wire x1="-1.136" y1="-1.397" x2="-1.404" y2="-1.397" width="0.127" layer="21"/>
<wire x1="2.413" y1="-2.4059" x2="2.664" y2="-1.397" width="0.127" layer="51" curve="22.4788"/>
<wire x1="2.664" y1="-1.397" x2="2.413" y2="-0.1341" width="0.127" layer="51" curve="27.9376"/>
<wire x1="2.664" y1="-1.397" x2="1.404" y2="-1.397" width="0.127" layer="51"/>
<wire x1="1.404" y1="-1.397" x2="1.136" y2="-1.397" width="0.127" layer="21"/>
<wire x1="2.095" y1="-2.921" x2="2.4247" y2="-2.3818" width="0.127" layer="21" curve="13.6094"/>
<pad name="C" x="2.54" y="-1.27" drill="0.8128" shape="long" rot="R90"/>
<pad name="E" x="-2.54" y="-1.27" drill="0.8128" shape="long" rot="R90"/>
<pad name="B" x="0" y="-1.27" drill="0.8128" shape="long" rot="R90"/>
<text x="-2.54" y="1.778" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.54" y="-4.572" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="TO92-E1">
<description>&lt;b&gt;TO-92&lt;/b&gt; Pads Triangle Reverse</description>
<wire x1="-2.095" y1="-1.651" x2="-0.7869" y2="2.5484" width="0.127" layer="21" curve="-111.098"/>
<wire x1="0.7869" y1="2.5484" x2="2.095" y2="-1.651" width="0.127" layer="21" curve="-111.098"/>
<wire x1="-2.095" y1="-1.651" x2="2.095" y2="-1.651" width="0.127" layer="21"/>
<wire x1="-2.254" y1="-0.254" x2="-0.286" y2="-0.254" width="0.127" layer="51"/>
<wire x1="-2.655" y1="-0.254" x2="-2.254" y2="-0.254" width="0.127" layer="21"/>
<wire x1="-0.286" y1="-0.254" x2="0.286" y2="-0.254" width="0.127" layer="21"/>
<wire x1="2.254" y1="-0.254" x2="2.655" y2="-0.254" width="0.127" layer="21"/>
<wire x1="0.286" y1="-0.254" x2="2.254" y2="-0.254" width="0.127" layer="51"/>
<wire x1="-0.7864" y1="2.5484" x2="0.7864" y2="2.5484" width="0.127" layer="51" curve="-34.299"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="0" y="1.905" drill="0.8128" shape="octagon"/>
<pad name="3" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="3.175" y="0.635" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="3.175" y="-1.27" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="TO92L">
<description>&lt;b&gt;TO-92&lt;/b&gt;&lt;p&gt;
grid 5.08 mm</description>
<wire x1="-1.1359" y1="2.413" x2="-0.127" y2="2.664" width="0.1524" layer="51" curve="-22.4788"/>
<wire x1="-0.127" y1="2.664" x2="1.1359" y2="2.413" width="0.1524" layer="51" curve="-27.9376"/>
<wire x1="-1.1359" y1="-2.413" x2="-0.127" y2="-2.664" width="0.1524" layer="51" curve="22.4788"/>
<wire x1="-0.127" y1="-2.664" x2="1.1359" y2="-2.413" width="0.1524" layer="51" curve="27.9376"/>
<wire x1="-1.651" y1="2.095" x2="-1.651" y2="-2.095" width="0.1524" layer="21"/>
<wire x1="-0.127" y1="-1.404" x2="-0.127" y2="1.404" width="0.1524" layer="21"/>
<wire x1="-0.127" y1="-2.664" x2="-0.127" y2="-1.404" width="0.1524" layer="51"/>
<wire x1="-1.651" y1="-2.095" x2="-1.1118" y2="-2.4247" width="0.1524" layer="21" curve="13.6094"/>
<wire x1="-0.127" y1="1.404" x2="-0.127" y2="2.664" width="0.1524" layer="51"/>
<wire x1="-1.651" y1="2.095" x2="-1.1359" y2="2.4135" width="0.1524" layer="21" curve="-13.0385"/>
<wire x1="2.413" y1="-1.1359" x2="2.413" y2="1.1359" width="0.1524" layer="51" curve="50.4167"/>
<wire x1="1.1359" y1="-2.413" x2="2.413" y2="-1.1359" width="0.1524" layer="21" curve="39.5833"/>
<wire x1="1.1359" y1="2.413" x2="2.413" y2="1.1359" width="0.1524" layer="21" curve="-39.5833"/>
<pad name="1" x="0" y="-2.54" drill="0.8128" shape="long"/>
<pad name="3" x="0" y="2.54" drill="0.8128" shape="long"/>
<pad name="2" x="2.54" y="0" drill="0.8128" shape="long"/>
<text x="-1.524" y="3.556" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.524" y="-4.953" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="TOP3">
<description>&lt;b&gt;TOP 3 horizontal&lt;/b&gt;</description>
<wire x1="-7.874" y1="-1.778" x2="7.874" y2="-1.778" width="0.127" layer="21"/>
<wire x1="7.874" y1="8.763" x2="7.874" y2="-1.778" width="0.127" layer="21"/>
<wire x1="-7.874" y1="-1.778" x2="-7.874" y2="8.763" width="0.127" layer="21"/>
<wire x1="-7.874" y1="8.763" x2="-7.874" y2="11.811" width="0.127" layer="21"/>
<wire x1="-7.874" y1="11.811" x2="-3.1242" y2="15.8496" width="0.127" layer="21"/>
<wire x1="7.874" y1="8.763" x2="-7.874" y2="8.763" width="0.127" layer="21"/>
<wire x1="7.874" y1="8.763" x2="7.874" y2="11.811" width="0.127" layer="21"/>
<wire x1="-7.366" y1="-1.27" x2="7.366" y2="-1.27" width="0.0508" layer="21"/>
<wire x1="7.366" y1="8.255" x2="7.366" y2="-1.27" width="0.0508" layer="21"/>
<wire x1="7.366" y1="8.255" x2="-7.366" y2="8.255" width="0.0508" layer="21"/>
<wire x1="-7.366" y1="-1.27" x2="-7.366" y2="8.255" width="0.0508" layer="21"/>
<wire x1="0" y1="17.145" x2="3.1431" y2="15.8431" width="0.127" layer="21" curve="-45.0001"/>
<wire x1="-3.1431" y1="15.8431" x2="0" y2="17.145" width="0.127" layer="21" curve="-45.0001"/>
<wire x1="7.874" y1="11.811" x2="3.1242" y2="15.875" width="0.127" layer="21"/>
<circle x="0" y="12.7" radius="2.159" width="0.127" layer="21"/>
<circle x="0" y="12.7" radius="5.08" width="0" layer="42"/>
<circle x="0" y="12.7" radius="5.08" width="0" layer="43"/>
<pad name="1" x="-5.588" y="-7.62" drill="1.397" shape="long" rot="R90"/>
<pad name="2" x="0" y="-7.62" drill="1.397" shape="long" rot="R90"/>
<pad name="3" x="5.588" y="-7.62" drill="1.397" shape="long" rot="R90"/>
<text x="-5.08" y="5.08" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
<text x="-7.366" y="9.144" size="0.9906" layer="21" ratio="12">A20,3mm</text>
<text x="-5.715" y="-0.635" size="1.27" layer="51" ratio="10">1</text>
<text x="-0.635" y="-0.635" size="1.27" layer="51" ratio="10">2</text>
<text x="4.445" y="-0.635" size="1.27" layer="51" ratio="10">3</text>
<rectangle x1="-6.223" y1="-3.81" x2="-3.81" y2="-1.778" layer="21"/>
<rectangle x1="-1.397" y1="-3.81" x2="1.397" y2="-1.778" layer="21"/>
<rectangle x1="3.81" y1="-3.81" x2="6.223" y2="-1.778" layer="21"/>
<rectangle x1="-6.223" y1="-5.715" x2="-4.953" y2="-3.81" layer="21"/>
<rectangle x1="-6.223" y1="-7.62" x2="-4.953" y2="-5.715" layer="51"/>
<rectangle x1="-0.635" y1="-5.715" x2="0.635" y2="-3.81" layer="21"/>
<rectangle x1="-0.635" y1="-7.62" x2="0.635" y2="-5.715" layer="51"/>
<rectangle x1="4.953" y1="-5.715" x2="6.223" y2="-3.81" layer="21"/>
<rectangle x1="4.953" y1="-7.62" x2="6.223" y2="-5.715" layer="51"/>
<hole x="0" y="12.7" drill="4.1148"/>
</package>
<package name="TOP3V">
<description>&lt;b&gt;TOP 3 vertical&lt;/b&gt;</description>
<wire x1="6.985" y1="-5.08" x2="7.366" y2="-4.699" width="0.127" layer="21"/>
<wire x1="7.366" y1="-4.699" x2="7.493" y2="-1.524" width="0.127" layer="21"/>
<wire x1="-6.985" y1="-5.08" x2="-7.366" y2="-4.699" width="0.127" layer="21"/>
<wire x1="-7.366" y1="-4.699" x2="-7.493" y2="-1.524" width="0.127" layer="21"/>
<wire x1="6.985" y1="-5.08" x2="6.35" y2="-5.08" width="0.127" layer="21"/>
<wire x1="6.35" y1="-5.08" x2="4.572" y2="-5.08" width="0.127" layer="51"/>
<wire x1="4.572" y1="-5.08" x2="0.889" y2="-5.08" width="0.127" layer="21"/>
<wire x1="0.889" y1="-5.08" x2="-0.889" y2="-5.08" width="0.127" layer="51"/>
<wire x1="-0.889" y1="-5.08" x2="-4.572" y2="-5.08" width="0.127" layer="21"/>
<wire x1="-4.572" y1="-5.08" x2="-6.35" y2="-5.08" width="0.127" layer="51"/>
<wire x1="-6.35" y1="-5.08" x2="-6.985" y2="-5.08" width="0.127" layer="21"/>
<pad name="B" x="-5.461" y="-3.175" drill="1.397" shape="long" rot="R90"/>
<pad name="C" x="0" y="-3.175" drill="1.397" shape="long" rot="R90"/>
<pad name="E" x="5.461" y="-3.175" drill="1.397" shape="long" rot="R90"/>
<text x="-7.62" y="-6.858" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.524" y="-6.858" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<text x="-6.096" y="-4.445" size="1.27" layer="51" ratio="10">B</text>
<text x="-0.635" y="-4.445" size="1.27" layer="51" ratio="10">C</text>
<text x="4.826" y="-4.445" size="1.27" layer="51" ratio="10">E</text>
<rectangle x1="-7.874" y1="-1.143" x2="7.874" y2="0" layer="21"/>
<rectangle x1="-7.874" y1="-1.524" x2="-6.858" y2="-1.143" layer="21"/>
<rectangle x1="-4.064" y1="-1.524" x2="-1.397" y2="-1.143" layer="21"/>
<rectangle x1="1.397" y1="-1.524" x2="4.064" y2="-1.143" layer="21"/>
<rectangle x1="6.858" y1="-1.524" x2="7.874" y2="-1.143" layer="21"/>
<rectangle x1="-6.858" y1="-1.524" x2="-4.064" y2="-1.143" layer="51"/>
<rectangle x1="-1.397" y1="-1.524" x2="1.397" y2="-1.143" layer="51"/>
<rectangle x1="4.064" y1="-1.524" x2="6.858" y2="-1.143" layer="51"/>
</package>
<package name="SOT23-BEC">
<description>TO-236 ITT Intermetall</description>
<wire x1="1.4224" y1="0.6604" x2="1.4224" y2="-0.6604" width="0.127" layer="51"/>
<wire x1="1.4224" y1="-0.6604" x2="-1.4224" y2="-0.6604" width="0.127" layer="51"/>
<wire x1="-1.4224" y1="-0.6604" x2="-1.4224" y2="0.6604" width="0.127" layer="51"/>
<wire x1="-1.4224" y1="0.6604" x2="1.4224" y2="0.6604" width="0.127" layer="51"/>
<smd name="C" x="0" y="1.1" dx="1" dy="1.4" layer="1"/>
<smd name="E" x="0.95" y="-1.1" dx="1" dy="1.4" layer="1"/>
<smd name="B" x="-0.95" y="-1.1" dx="1" dy="1.4" layer="1"/>
<text x="-1.905" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.2286" y1="0.7112" x2="0.2286" y2="1.2954" layer="51"/>
<rectangle x1="0.7112" y1="-1.2954" x2="1.1684" y2="-0.7112" layer="51"/>
<rectangle x1="-1.1684" y1="-1.2954" x2="-0.7112" y2="-0.7112" layer="51"/>
</package>
<package name="SOT23-EBC">
<description>&lt;b&gt;SOT-23 (EBC)&lt;/b&gt;</description>
<wire x1="1.4224" y1="0.6604" x2="1.4224" y2="-0.6604" width="0.127" layer="51"/>
<wire x1="1.4224" y1="-0.6604" x2="-1.4224" y2="-0.6604" width="0.127" layer="51"/>
<wire x1="-1.4224" y1="-0.6604" x2="-1.4224" y2="0.6604" width="0.127" layer="51"/>
<wire x1="-1.4224" y1="0.6604" x2="1.4224" y2="0.6604" width="0.127" layer="51"/>
<smd name="C" x="0" y="1.1" dx="1" dy="1.4" layer="1"/>
<smd name="B" x="0.95" y="-1.1" dx="1" dy="1.4" layer="1"/>
<smd name="E" x="-0.95" y="-1.1" dx="1" dy="1.4" layer="1"/>
<text x="-1.905" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.2286" y1="0.7112" x2="0.2286" y2="1.2954" layer="51"/>
<rectangle x1="0.7112" y1="-1.2954" x2="1.1684" y2="-0.7112" layer="51"/>
<rectangle x1="-1.1684" y1="-1.2954" x2="-0.7112" y2="-0.7112" layer="51"/>
</package>
<package name="TO92-BCE">
<description>&lt;b&gt;TO-92&lt;/b&gt; Pads In Line B C E from top&lt;p&gt;</description>
<wire x1="-2.095" y1="-1.651" x2="2.095" y2="-1.651" width="0.127" layer="21"/>
<wire x1="-2.413" y1="1.1359" x2="2.413" y2="1.1359" width="0.127" layer="21" curve="-129.583"/>
<wire x1="1.136" y1="-0.127" x2="-1.136" y2="-0.127" width="0.127" layer="51"/>
<wire x1="-2.413" y1="1.1359" x2="-2.664" y2="-0.127" width="0.127" layer="51" curve="27.9376"/>
<wire x1="-2.664" y1="-0.127" x2="-2.413" y2="-1.1359" width="0.127" layer="51" curve="22.4788"/>
<wire x1="-1.404" y1="-0.127" x2="-2.664" y2="-0.127" width="0.127" layer="51"/>
<wire x1="-2.4135" y1="-1.1359" x2="-2.095" y2="-1.651" width="0.127" layer="21" curve="13.0385"/>
<wire x1="-1.136" y1="-0.127" x2="-1.404" y2="-0.127" width="0.127" layer="21"/>
<wire x1="2.413" y1="-1.1359" x2="2.664" y2="-0.127" width="0.127" layer="51" curve="22.4788"/>
<wire x1="2.664" y1="-0.127" x2="2.413" y2="1.1359" width="0.127" layer="51" curve="27.9376"/>
<wire x1="2.664" y1="-0.127" x2="1.404" y2="-0.127" width="0.127" layer="51"/>
<wire x1="1.404" y1="-0.127" x2="1.136" y2="-0.127" width="0.127" layer="21"/>
<wire x1="2.095" y1="-1.651" x2="2.4247" y2="-1.1118" width="0.127" layer="21" curve="13.6094"/>
<pad name="C" x="0" y="0" drill="0.8128" shape="long" rot="R90"/>
<pad name="E" x="2.54" y="0" drill="0.8128" shape="long" rot="R90"/>
<pad name="B" x="-2.54" y="0" drill="0.8128" shape="long" rot="R90"/>
<text x="-2.54" y="3.048" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.54" y="-3.302" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="TO92-CBE">
<description>&lt;b&gt;TO-92&lt;/b&gt; Pads In Line  C B E from top&lt;p&gt;</description>
<wire x1="-2.095" y1="-1.651" x2="2.095" y2="-1.651" width="0.127" layer="21"/>
<wire x1="-2.413" y1="1.1359" x2="2.413" y2="1.1359" width="0.127" layer="21" curve="-129.583"/>
<wire x1="1.136" y1="-0.127" x2="-1.136" y2="-0.127" width="0.127" layer="51"/>
<wire x1="-2.413" y1="1.1359" x2="-2.664" y2="-0.127" width="0.127" layer="51" curve="27.9376"/>
<wire x1="-2.664" y1="-0.127" x2="-2.413" y2="-1.1359" width="0.127" layer="51" curve="22.4788"/>
<wire x1="-1.404" y1="-0.127" x2="-2.664" y2="-0.127" width="0.127" layer="51"/>
<wire x1="-2.4135" y1="-1.1359" x2="-2.095" y2="-1.651" width="0.127" layer="21" curve="13.0385"/>
<wire x1="-1.136" y1="-0.127" x2="-1.404" y2="-0.127" width="0.127" layer="21"/>
<wire x1="2.413" y1="-1.1359" x2="2.664" y2="-0.127" width="0.127" layer="51" curve="22.4788"/>
<wire x1="2.664" y1="-0.127" x2="2.413" y2="1.1359" width="0.127" layer="51" curve="27.9376"/>
<wire x1="2.664" y1="-0.127" x2="1.404" y2="-0.127" width="0.127" layer="51"/>
<wire x1="1.404" y1="-0.127" x2="1.136" y2="-0.127" width="0.127" layer="21"/>
<wire x1="2.095" y1="-1.651" x2="2.4247" y2="-1.1118" width="0.127" layer="21" curve="13.6094"/>
<pad name="C" x="-2.54" y="0" drill="0.8128" shape="long" rot="R90"/>
<pad name="E" x="2.54" y="0" drill="0.8128" shape="long" rot="R90"/>
<pad name="B" x="0" y="0" drill="0.8128" shape="long" rot="R90"/>
<text x="-2.54" y="3.048" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.54" y="-3.302" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="TO92-BEC">
<description>&lt;b&gt;TO-92&lt;/b&gt; Pads In Line  B E C from top&lt;p&gt;</description>
<wire x1="-2.095" y1="-1.651" x2="2.095" y2="-1.651" width="0.127" layer="21"/>
<wire x1="-2.413" y1="1.1359" x2="2.413" y2="1.1359" width="0.127" layer="21" curve="-129.583"/>
<wire x1="1.136" y1="-0.127" x2="-1.136" y2="-0.127" width="0.127" layer="51"/>
<wire x1="-2.413" y1="1.1359" x2="-2.664" y2="-0.127" width="0.127" layer="51" curve="27.9376"/>
<wire x1="-2.664" y1="-0.127" x2="-2.413" y2="-1.1359" width="0.127" layer="51" curve="22.4788"/>
<wire x1="-1.404" y1="-0.127" x2="-2.664" y2="-0.127" width="0.127" layer="51"/>
<wire x1="-2.4135" y1="-1.1359" x2="-2.095" y2="-1.651" width="0.127" layer="21" curve="13.0385"/>
<wire x1="-1.136" y1="-0.127" x2="-1.404" y2="-0.127" width="0.127" layer="21"/>
<wire x1="2.413" y1="-1.1359" x2="2.664" y2="-0.127" width="0.127" layer="51" curve="22.4788"/>
<wire x1="2.664" y1="-0.127" x2="2.413" y2="1.1359" width="0.127" layer="51" curve="27.9376"/>
<wire x1="2.664" y1="-0.127" x2="1.404" y2="-0.127" width="0.127" layer="51"/>
<wire x1="1.404" y1="-0.127" x2="1.136" y2="-0.127" width="0.127" layer="21"/>
<wire x1="2.095" y1="-1.651" x2="2.4247" y2="-1.1118" width="0.127" layer="21" curve="13.6094"/>
<pad name="C" x="2.54" y="0" drill="0.8128" shape="long" rot="R90"/>
<pad name="E" x="0" y="0" drill="0.8128" shape="long" rot="R90"/>
<pad name="B" x="-2.54" y="0" drill="0.8128" shape="long" rot="R90"/>
<text x="-2.54" y="3.048" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.54" y="-3.302" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="TO92-CEB">
<description>&lt;b&gt;TO-92&lt;/b&gt; Pads In Line C E B from top&lt;p&gt;</description>
<wire x1="-2.095" y1="-1.651" x2="2.095" y2="-1.651" width="0.127" layer="21"/>
<wire x1="-2.413" y1="1.1359" x2="2.413" y2="1.1359" width="0.127" layer="21" curve="-129.583"/>
<wire x1="1.136" y1="-0.127" x2="-1.136" y2="-0.127" width="0.127" layer="51"/>
<wire x1="-2.413" y1="1.1359" x2="-2.664" y2="-0.127" width="0.127" layer="51" curve="27.9376"/>
<wire x1="-2.664" y1="-0.127" x2="-2.413" y2="-1.1359" width="0.127" layer="51" curve="22.4788"/>
<wire x1="-1.404" y1="-0.127" x2="-2.664" y2="-0.127" width="0.127" layer="51"/>
<wire x1="-2.4135" y1="-1.1359" x2="-2.095" y2="-1.651" width="0.127" layer="21" curve="13.0385"/>
<wire x1="-1.136" y1="-0.127" x2="-1.404" y2="-0.127" width="0.127" layer="21"/>
<wire x1="2.413" y1="-1.1359" x2="2.664" y2="-0.127" width="0.127" layer="51" curve="22.4788"/>
<wire x1="2.664" y1="-0.127" x2="2.413" y2="1.1359" width="0.127" layer="51" curve="27.9376"/>
<wire x1="2.664" y1="-0.127" x2="1.404" y2="-0.127" width="0.127" layer="51"/>
<wire x1="1.404" y1="-0.127" x2="1.136" y2="-0.127" width="0.127" layer="21"/>
<wire x1="2.095" y1="-1.651" x2="2.4247" y2="-1.1118" width="0.127" layer="21" curve="13.6094"/>
<pad name="C" x="-2.54" y="0" drill="0.8128" shape="long" rot="R90"/>
<pad name="E" x="0" y="0" drill="0.8128" shape="long" rot="R90"/>
<pad name="B" x="2.54" y="0" drill="0.8128" shape="long" rot="R90"/>
<text x="-2.54" y="3.048" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.54" y="-3.302" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="SC59-BEC">
<description>SC59 (SOT23) Motorola</description>
<wire x1="1.4224" y1="0.8604" x2="1.4224" y2="-0.8604" width="0.127" layer="51"/>
<wire x1="1.4224" y1="-0.8604" x2="-1.4224" y2="-0.8604" width="0.127" layer="51"/>
<wire x1="-1.4224" y1="-0.8604" x2="-1.4224" y2="0.8604" width="0.127" layer="51"/>
<wire x1="-1.4224" y1="0.8604" x2="1.4224" y2="0.8604" width="0.127" layer="51"/>
<smd name="C" x="0" y="1.2" dx="1" dy="1.4" layer="1"/>
<smd name="E" x="0.95" y="-1.2" dx="1" dy="1.4" layer="1"/>
<smd name="B" x="-0.95" y="-1.2" dx="1" dy="1.4" layer="1"/>
<text x="-1.905" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.2286" y1="0.9112" x2="0.2286" y2="1.4954" layer="51"/>
<rectangle x1="0.7112" y1="-1.4954" x2="1.1684" y2="-0.9112" layer="51"/>
<rectangle x1="-1.1684" y1="-1.4954" x2="-0.7112" y2="-0.9112" layer="51"/>
</package>
<package name="SOT89-BCE">
<description>SOT89 Basis Collector Emitter</description>
<wire x1="2.235" y1="-1.245" x2="-2.235" y2="-1.245" width="0.127" layer="51"/>
<wire x1="2.235" y1="1.219" x2="2.235" y2="-1.245" width="0.127" layer="51"/>
<wire x1="-2.235" y1="-1.245" x2="-2.235" y2="1.219" width="0.127" layer="51"/>
<wire x1="-2.235" y1="1.219" x2="2.235" y2="1.219" width="0.127" layer="51"/>
<wire x1="-0.7874" y1="1.5748" x2="-0.3556" y2="2.0066" width="0.1998" layer="51"/>
<wire x1="-0.3556" y1="2.0066" x2="0.3556" y2="2.0066" width="0.1998" layer="51"/>
<wire x1="0.3556" y1="2.0066" x2="0.7874" y2="1.5748" width="0.1998" layer="51"/>
<wire x1="0.7874" y1="1.5748" x2="0.7874" y2="1.2954" width="0.1998" layer="51"/>
<wire x1="0.7874" y1="1.2954" x2="-0.7874" y2="1.2954" width="0.1998" layer="51"/>
<wire x1="-0.7874" y1="1.2954" x2="-0.7874" y2="1.5748" width="0.1998" layer="51"/>
<smd name="B" x="-1.499" y="-1.981" dx="0.8" dy="1.4" layer="1"/>
<smd name="E" x="1.499" y="-1.981" dx="0.8" dy="1.4" layer="1"/>
<smd name="C" x="0" y="-1.727" dx="0.8" dy="1.9" layer="1" stop="no" cream="no"/>
<text x="-2.54" y="3.175" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.4051" y="-4.3449" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.7272" y1="-2.1082" x2="-1.27" y2="-1.27" layer="51"/>
<rectangle x1="1.27" y1="-2.1082" x2="1.7272" y2="-1.27" layer="51"/>
<rectangle x1="-0.2794" y1="-2.1082" x2="0.2794" y2="-1.27" layer="51"/>
<rectangle x1="-0.4" y1="-2.68" x2="0.4" y2="-1.28" layer="31"/>
<rectangle x1="-0.5" y1="-2.78" x2="0.5" y2="-1.18" layer="29"/>
<polygon width="0.1998" layer="51">
<vertex x="-0.7874" y="1.3208"/>
<vertex x="-0.7874" y="1.5748"/>
<vertex x="-0.3556" y="2.0066"/>
<vertex x="0.3048" y="2.0066"/>
<vertex x="0.3556" y="2.0066"/>
<vertex x="0.7874" y="1.5748"/>
<vertex x="0.7874" y="1.2954"/>
<vertex x="-0.7874" y="1.2954"/>
</polygon>
<polygon width="1.7" layer="29">
<vertex x="-0.275" y="2.025"/>
<vertex x="-0.275" y="-0.15"/>
<vertex x="0.25" y="-0.15"/>
<vertex x="0.25" y="2.025"/>
</polygon>
<polygon width="1.3" layer="31">
<vertex x="-0.25" y="2.05"/>
<vertex x="0.275" y="2.05"/>
<vertex x="0.275" y="-0.125"/>
<vertex x="-0.25" y="-0.125"/>
</polygon>
<polygon width="0.3" layer="1">
<vertex x="0.25" y="-2.05"/>
<vertex x="-0.25" y="-2.05"/>
<vertex x="-0.25" y="-0.75" curve="-90"/>
<vertex x="-0.875" y="-0.125"/>
<vertex x="-0.875" y="2.05" curve="-90"/>
<vertex x="-0.225" y="2.625"/>
<vertex x="0.25" y="2.625" curve="-90"/>
<vertex x="0.875" y="2.025"/>
<vertex x="0.875" y="-0.125" curve="-90"/>
<vertex x="0.25" y="-0.75"/>
</polygon>
</package>
<package name="SOT37">
<description>&lt;b&gt;SOT-37&lt;/b&gt;&lt;p&gt;
Motorola CASE 317A-01 ISSUE B&lt;br&gt;
http://www.ee.siue.edu/~alozows/library/datasheets/BFR96.pdf</description>
<wire x1="5.3086" y1="0.508" x2="5.3086" y2="-0.508" width="0.1524" layer="51"/>
<wire x1="-0.508" y1="-5.3086" x2="0.508" y2="-5.3086" width="0.1524" layer="51"/>
<wire x1="-0.508" y1="6.5024" x2="0.508" y2="6.5024" width="0.1524" layer="51"/>
<wire x1="0.508" y1="-2.2352" x2="0.508" y2="-2.794" width="0.1524" layer="21"/>
<wire x1="0.635" y1="-2.794" x2="0.508" y2="-2.794" width="0.1524" layer="21"/>
<wire x1="0.635" y1="-3.048" x2="0.635" y2="-3.429" width="0.1524" layer="51"/>
<wire x1="0.508" y1="-3.429" x2="0.635" y2="-3.429" width="0.1524" layer="51"/>
<wire x1="0.508" y1="-3.429" x2="0.508" y2="-5.3086" width="0.1524" layer="51"/>
<wire x1="-0.508" y1="-2.2352" x2="-0.508" y2="-2.794" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="-2.794" x2="-0.508" y2="-2.794" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="-3.048" x2="-0.635" y2="-3.429" width="0.1524" layer="51"/>
<wire x1="-0.508" y1="-3.429" x2="-0.635" y2="-3.429" width="0.1524" layer="51"/>
<wire x1="-0.508" y1="-3.429" x2="-0.508" y2="-5.3086" width="0.1524" layer="51"/>
<wire x1="2.2352" y1="-0.508" x2="2.794" y2="-0.508" width="0.1524" layer="21"/>
<wire x1="2.794" y1="-0.635" x2="2.794" y2="-0.508" width="0.1524" layer="21"/>
<wire x1="3.048" y1="-0.635" x2="3.429" y2="-0.635" width="0.1524" layer="51"/>
<wire x1="3.429" y1="-0.508" x2="3.429" y2="-0.635" width="0.1524" layer="51"/>
<wire x1="3.429" y1="-0.508" x2="5.3086" y2="-0.508" width="0.1524" layer="51"/>
<wire x1="2.2098" y1="0.508" x2="2.794" y2="0.508" width="0.1524" layer="21"/>
<wire x1="2.794" y1="0.635" x2="2.794" y2="0.508" width="0.1524" layer="21"/>
<wire x1="3.048" y1="0.635" x2="3.429" y2="0.635" width="0.1524" layer="51"/>
<wire x1="3.429" y1="0.508" x2="3.429" y2="0.635" width="0.1524" layer="51"/>
<wire x1="3.429" y1="0.508" x2="5.3086" y2="0.508" width="0.1524" layer="51"/>
<wire x1="0.508" y1="2.2352" x2="0.508" y2="2.794" width="0.1524" layer="21"/>
<wire x1="0.635" y1="2.794" x2="0.508" y2="2.794" width="0.1524" layer="21"/>
<wire x1="0.635" y1="3.048" x2="0.635" y2="3.429" width="0.1524" layer="51"/>
<wire x1="0.508" y1="3.429" x2="0.635" y2="3.429" width="0.1524" layer="51"/>
<wire x1="0.508" y1="3.429" x2="0.508" y2="6.5024" width="0.1524" layer="51"/>
<wire x1="-0.508" y1="2.2352" x2="-0.508" y2="2.794" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="2.794" x2="-0.508" y2="2.794" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="3.048" x2="-0.635" y2="3.429" width="0.1524" layer="51"/>
<wire x1="-0.508" y1="3.429" x2="-0.635" y2="3.429" width="0.1524" layer="51"/>
<wire x1="-0.508" y1="3.429" x2="-0.508" y2="6.5024" width="0.1524" layer="51"/>
<wire x1="-0.635" y1="-2.794" x2="-0.635" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="0.635" y1="-2.794" x2="0.635" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="3.048" y1="-0.635" x2="2.794" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="3.048" y1="0.635" x2="2.794" y2="0.635" width="0.1524" layer="21"/>
<wire x1="0.635" y1="2.794" x2="0.635" y2="3.048" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="2.794" x2="-0.635" y2="3.048" width="0.1524" layer="21"/>
<circle x="0" y="0" radius="2.286" width="0.1524" layer="21"/>
<circle x="0" y="1.524" radius="0.508" width="0.1524" layer="21"/>
<circle x="0" y="0" radius="3.175" width="0" layer="41"/>
<circle x="0" y="0" radius="3.175" width="0" layer="42"/>
<smd name="3" x="0" y="-5.08" dx="1.9304" dy="3.81" layer="1"/>
<smd name="2" x="5.08" y="0" dx="3.81" dy="1.9304" layer="1"/>
<smd name="1" x="0" y="5.08" dx="1.9304" dy="3.81" layer="1"/>
<text x="4.445" y="1.27" size="1.27" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<text x="6.35" y="1.27" size="1.27" layer="27" ratio="10" rot="R90">&gt;VALUE</text>
<text x="3.175" y="-2.286" size="1.016" layer="21" ratio="10">E</text>
<text x="1.27" y="-4.191" size="1.016" layer="21" ratio="10">B</text>
<text x="1.27" y="3.175" size="1.016" layer="21" ratio="10">C</text>
<text x="-1.524" y="-4.318" size="1.27" layer="47">5,1</text>
<rectangle x1="-0.508" y1="-2.794" x2="0.508" y2="-2.413" layer="21"/>
<rectangle x1="-0.635" y1="-3.048" x2="0.635" y2="-2.794" layer="21"/>
<rectangle x1="-0.635" y1="-3.429" x2="0.635" y2="-3.048" layer="51"/>
<rectangle x1="-0.508" y1="-5.334" x2="0.508" y2="-3.429" layer="51"/>
<rectangle x1="2.413" y1="-0.508" x2="2.794" y2="0.508" layer="21"/>
<rectangle x1="2.794" y1="-0.635" x2="3.048" y2="0.635" layer="21"/>
<rectangle x1="3.048" y1="-0.635" x2="3.429" y2="0.635" layer="51"/>
<rectangle x1="3.429" y1="-0.508" x2="5.334" y2="0.508" layer="51"/>
<rectangle x1="-0.508" y1="3.429" x2="0.508" y2="6.477" layer="51"/>
<rectangle x1="-0.635" y1="3.048" x2="0.635" y2="3.429" layer="51"/>
<rectangle x1="-0.635" y1="2.794" x2="0.635" y2="3.048" layer="21"/>
<rectangle x1="-0.508" y1="2.413" x2="0.508" y2="2.794" layer="21"/>
<hole x="0" y="0" drill="5.08"/>
</package>
<package name="TO92-ECB">
<description>&lt;b&gt;TO-92&lt;/b&gt; Pads In Line E C B from top&lt;p&gt;</description>
<wire x1="-2.095" y1="-1.651" x2="2.095" y2="-1.651" width="0.127" layer="21"/>
<wire x1="-2.413" y1="1.1359" x2="2.413" y2="1.1359" width="0.127" layer="21" curve="-129.583"/>
<wire x1="1.136" y1="-0.127" x2="-1.136" y2="-0.127" width="0.127" layer="51"/>
<wire x1="-2.413" y1="1.1359" x2="-2.664" y2="-0.127" width="0.127" layer="51" curve="27.9407"/>
<wire x1="-2.664" y1="-0.127" x2="-2.413" y2="-1.1359" width="0.127" layer="51" curve="22.4788"/>
<wire x1="-1.404" y1="-0.127" x2="-2.664" y2="-0.127" width="0.127" layer="51"/>
<wire x1="-2.4135" y1="-1.1359" x2="-2.095" y2="-1.651" width="0.127" layer="21" curve="13.0385"/>
<wire x1="-1.136" y1="-0.127" x2="-1.404" y2="-0.127" width="0.127" layer="21"/>
<wire x1="2.413" y1="-1.1359" x2="2.664" y2="-0.127" width="0.127" layer="51" curve="22.4788"/>
<wire x1="2.664" y1="-0.127" x2="2.413" y2="1.1359" width="0.127" layer="51" curve="27.9333"/>
<wire x1="2.664" y1="-0.127" x2="1.404" y2="-0.127" width="0.127" layer="51"/>
<wire x1="1.404" y1="-0.127" x2="1.136" y2="-0.127" width="0.127" layer="21"/>
<wire x1="2.095" y1="-1.651" x2="2.4247" y2="-1.1118" width="0.127" layer="21" curve="13.6094"/>
<pad name="C" x="0" y="0" drill="0.8128" shape="long" rot="R90"/>
<pad name="E" x="-2.54" y="0" drill="0.8128" shape="long" rot="R90"/>
<pad name="B" x="2.54" y="0" drill="0.8128" shape="long" rot="R90"/>
<text x="-2.54" y="3.048" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.54" y="-3.302" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
</packages>
<symbols>
<symbol name="NPN">
<wire x1="2.54" y1="2.54" x2="0.508" y2="1.524" width="0.1524" layer="94"/>
<wire x1="1.778" y1="-1.524" x2="2.54" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-2.54" x2="1.27" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="1.778" y2="-1.524" width="0.1524" layer="94"/>
<wire x1="1.54" y1="-2.04" x2="0.308" y2="-1.424" width="0.1524" layer="94"/>
<wire x1="1.524" y1="-2.413" x2="2.286" y2="-2.413" width="0.254" layer="94"/>
<wire x1="2.286" y1="-2.413" x2="1.778" y2="-1.778" width="0.254" layer="94"/>
<wire x1="1.778" y1="-1.778" x2="1.524" y2="-2.286" width="0.254" layer="94"/>
<wire x1="1.524" y1="-2.286" x2="1.905" y2="-2.286" width="0.254" layer="94"/>
<wire x1="1.905" y1="-2.286" x2="1.778" y2="-2.032" width="0.254" layer="94"/>
<text x="-10.16" y="7.62" size="1.778" layer="95">&gt;NAME</text>
<text x="-10.16" y="5.08" size="1.778" layer="96">&gt;VALUE</text>
<rectangle x1="-0.254" y1="-2.54" x2="0.508" y2="2.54" layer="94"/>
<pin name="B" x="-2.54" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
<pin name="E" x="2.54" y="-5.08" visible="off" length="short" direction="pas" swaplevel="3" rot="R90"/>
<pin name="C" x="2.54" y="5.08" visible="off" length="short" direction="pas" swaplevel="2" rot="R270"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="*-NPN-" prefix="T" uservalue="yes">
<description>NPN Transistror&lt;p&gt;
BF959 corrected 2008.03.06&lt;br&gt;</description>
<gates>
<gate name="G$1" symbol="NPN" x="0" y="0"/>
</gates>
<devices>
<device name="TO3/" package="TO3">
<connects>
<connect gate="G$1" pin="B" pad="B"/>
<connect gate="G$1" pin="C" pad="C"/>
<connect gate="G$1" pin="E" pad="E"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="TO18-EBC" package="TO18">
<connects>
<connect gate="G$1" pin="B" pad="2"/>
<connect gate="G$1" pin="C" pad="3"/>
<connect gate="G$1" pin="E" pad="1"/>
</connects>
<technologies>
<technology name="2N2218"/>
<technology name="2N2222"/>
<technology name="2N2222A"/>
<technology name="2N2369"/>
<technology name="2N2369A"/>
<technology name="2N2484"/>
<technology name="2N2896"/>
<technology name="2N3700"/>
<technology name="2N930"/>
<technology name="BC107A"/>
<technology name="BC107B"/>
<technology name="BC108B"/>
<technology name="BC108C"/>
<technology name="BCY58-IX"/>
<technology name="BCY58-VIII"/>
<technology name="BCY59-VII"/>
<technology name="BCY59-VIII"/>
<technology name="BCY59-X"/>
<technology name="BSS71"/>
<technology name="BSS72"/>
<technology name="BSS73"/>
<technology name="BSX20"/>
</technologies>
</device>
<device name="TO18-" package="TO18-">
<connects>
<connect gate="G$1" pin="B" pad="2"/>
<connect gate="G$1" pin="C" pad="3"/>
<connect gate="G$1" pin="E" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="TO202" package="TO202">
<connects>
<connect gate="G$1" pin="B" pad="3"/>
<connect gate="G$1" pin="C" pad="2"/>
<connect gate="G$1" pin="E" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="TO202V" package="TO202V">
<connects>
<connect gate="G$1" pin="B" pad="3"/>
<connect gate="G$1" pin="C" pad="2"/>
<connect gate="G$1" pin="E" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SOT93" package="SOT93">
<connects>
<connect gate="G$1" pin="B" pad="B"/>
<connect gate="G$1" pin="C" pad="C"/>
<connect gate="G$1" pin="E" pad="E"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SOT93V" package="SOT93V">
<connects>
<connect gate="G$1" pin="B" pad="B"/>
<connect gate="G$1" pin="C" pad="C"/>
<connect gate="G$1" pin="E" pad="E"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="TO126" package="TO126">
<connects>
<connect gate="G$1" pin="B" pad="3"/>
<connect gate="G$1" pin="C" pad="2"/>
<connect gate="G$1" pin="E" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="TO126V" package="TO126V">
<connects>
<connect gate="G$1" pin="B" pad="3"/>
<connect gate="G$1" pin="C" pad="2"/>
<connect gate="G$1" pin="E" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="TO218" package="TO218">
<connects>
<connect gate="G$1" pin="B" pad="1"/>
<connect gate="G$1" pin="C" pad="2"/>
<connect gate="G$1" pin="E" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="TO218V" package="TO218V">
<connects>
<connect gate="G$1" pin="B" pad="1"/>
<connect gate="G$1" pin="C" pad="2"/>
<connect gate="G$1" pin="E" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="TO220" package="TO220">
<connects>
<connect gate="G$1" pin="B" pad="1"/>
<connect gate="G$1" pin="C" pad="2"/>
<connect gate="G$1" pin="E" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="TO220V" package="TO220V">
<connects>
<connect gate="G$1" pin="B" pad="1"/>
<connect gate="G$1" pin="C" pad="2"/>
<connect gate="G$1" pin="E" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="TO225AA" package="TO225AA">
<connects>
<connect gate="G$1" pin="B" pad="1"/>
<connect gate="G$1" pin="C" pad="2"/>
<connect gate="G$1" pin="E" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="TO39-EBC" package="TO39">
<connects>
<connect gate="G$1" pin="B" pad="2"/>
<connect gate="G$1" pin="C" pad="3"/>
<connect gate="G$1" pin="E" pad="1"/>
</connects>
<technologies>
<technology name="2N1613"/>
<technology name="2N1711"/>
<technology name="2N1893"/>
<technology name="2N2102"/>
<technology name="2N2218"/>
<technology name="2N2219"/>
<technology name="2N2219A"/>
<technology name="2N3019"/>
<technology name="2N3020"/>
<technology name="2N3439"/>
<technology name="2N3440"/>
<technology name="BC140-10"/>
<technology name="BC140-16"/>
<technology name="BC141-10"/>
<technology name="BC141-16"/>
<technology name="BFY50"/>
<technology name="BSX45-16"/>
<technology name="BSX46-10"/>
<technology name="BSX46-16"/>
</technologies>
</device>
<device name="TO5" package="TO5">
<connects>
<connect gate="G$1" pin="B" pad="2"/>
<connect gate="G$1" pin="C" pad="3"/>
<connect gate="G$1" pin="E" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="TO66" package="TO66">
<connects>
<connect gate="G$1" pin="B" pad="2"/>
<connect gate="G$1" pin="C" pad="3"/>
<connect gate="G$1" pin="E" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="TO92" package="TO92">
<connects>
<connect gate="G$1" pin="B" pad="2"/>
<connect gate="G$1" pin="C" pad="3"/>
<connect gate="G$1" pin="E" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="TO92-EBC" package="TO92-EBC">
<connects>
<connect gate="G$1" pin="B" pad="B"/>
<connect gate="G$1" pin="C" pad="C"/>
<connect gate="G$1" pin="E" pad="E"/>
</connects>
<technologies>
<technology name=""/>
<technology name="29"/>
<technology name="92"/>
<technology name="BC317"/>
<technology name="BC318"/>
<technology name="BC319"/>
<technology name="MPSA06"/>
<technology name="MPSA13"/>
<technology name="MPSA14"/>
<technology name="MPSA18"/>
<technology name="MPSA28"/>
<technology name="MPSA42"/>
<technology name="MPSA44"/>
</technologies>
</device>
<device name="TO92-E1" package="TO92-E1">
<connects>
<connect gate="G$1" pin="B" pad="2"/>
<connect gate="G$1" pin="C" pad="1"/>
<connect gate="G$1" pin="E" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="TO92L" package="TO92L">
<connects>
<connect gate="G$1" pin="B" pad="2"/>
<connect gate="G$1" pin="C" pad="3"/>
<connect gate="G$1" pin="E" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="TOP3" package="TOP3">
<connects>
<connect gate="G$1" pin="B" pad="1"/>
<connect gate="G$1" pin="C" pad="2"/>
<connect gate="G$1" pin="E" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="TOP3V" package="TOP3V">
<connects>
<connect gate="G$1" pin="B" pad="B"/>
<connect gate="G$1" pin="C" pad="C"/>
<connect gate="G$1" pin="E" pad="E"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SOT23-BEC" package="SOT23-BEC">
<connects>
<connect gate="G$1" pin="B" pad="B"/>
<connect gate="G$1" pin="C" pad="C"/>
<connect gate="G$1" pin="E" pad="E"/>
</connects>
<technologies>
<technology name="BC808"/>
<technology name="BC817"/>
<technology name="BC817-16"/>
<technology name="BC817-16LT1"/>
<technology name="BC817-25"/>
<technology name="BC817-25LT1"/>
<technology name="BC817-40"/>
<technology name="BC817-40LT1"/>
<technology name="BC818"/>
<technology name="BC818-16"/>
<technology name="BC818-25"/>
<technology name="BC818-40"/>
<technology name="BC846"/>
<technology name="BC846A"/>
<technology name="BC846ALT1"/>
<technology name="BC846B"/>
<technology name="BC846BLT1"/>
<technology name="BC847"/>
<technology name="BC847A"/>
<technology name="BC847ALT1"/>
<technology name="BC847B"/>
<technology name="BC847BLT1"/>
<technology name="BC847C"/>
<technology name="BC847CLT1"/>
<technology name="BC848"/>
<technology name="BC848A"/>
<technology name="BC848ALT1"/>
<technology name="BC848B"/>
<technology name="BC848BLT1"/>
<technology name="BC848C"/>
<technology name="BC848CLT1"/>
<technology name="BC849"/>
<technology name="BC850"/>
<technology name="BCF29"/>
<technology name="BCF30"/>
<technology name="BCF32"/>
<technology name="BCF33"/>
<technology name="BCF81"/>
<technology name="BCV71"/>
<technology name="BCV72"/>
<technology name="BCW31"/>
<technology name="BCW32"/>
<technology name="BCW33"/>
<technology name="BCW60"/>
<technology name="BCW71"/>
<technology name="BCW72"/>
<technology name="BCW81"/>
<technology name="BCX19"/>
<technology name="BCX20"/>
<technology name="BCX70"/>
<technology name="BF820"/>
<technology name="BF822"/>
<technology name="BFR53"/>
<technology name="BFR92"/>
<technology name="BFR92A"/>
<technology name="BFR93"/>
<technology name="BFR93A"/>
<technology name="BFS17"/>
<technology name="BFS19"/>
<technology name="BFS20"/>
<technology name="BFT25"/>
<technology name="BSR12"/>
<technology name="BSR13"/>
<technology name="BSR14"/>
<technology name="BSV52LT1"/>
<technology name="MMBT2222ALT1"/>
<technology name="MMBT2369LT1"/>
<technology name="MMBT2484LT1"/>
<technology name="MMBT3904LT1"/>
<technology name="MMBT4401LT1"/>
<technology name="MMBT5551LT1"/>
<technology name="MMBT6429LT1"/>
<technology name="MMBT6517LT1"/>
<technology name="MMBT918LT1"/>
<technology name="MMBTA42LT1"/>
<technology name="MMBTH10LT1"/>
<technology name="MMBTH24LT1"/>
</technologies>
</device>
<device name="SOT23-EBC" package="SOT23-EBC">
<connects>
<connect gate="G$1" pin="B" pad="B"/>
<connect gate="G$1" pin="C" pad="C"/>
<connect gate="G$1" pin="E" pad="E"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="TO92-BCE" package="TO92-BCE">
<connects>
<connect gate="G$1" pin="B" pad="B"/>
<connect gate="G$1" pin="C" pad="C"/>
<connect gate="G$1" pin="E" pad="E"/>
</connects>
<technologies>
<technology name="BF420"/>
<technology name="BF422"/>
</technologies>
</device>
<device name="TO92-CBE" package="TO92-CBE">
<connects>
<connect gate="G$1" pin="B" pad="B"/>
<connect gate="G$1" pin="C" pad="C"/>
<connect gate="G$1" pin="E" pad="E"/>
</connects>
<technologies>
<technology name="2N4124"/>
<technology name="2N5400"/>
<technology name="2N5401"/>
<technology name="2N5550"/>
<technology name="2N5551"/>
<technology name="2N6427"/>
<technology name="2N6517"/>
<technology name="2N6520"/>
<technology name="BC237"/>
<technology name="BC238"/>
<technology name="BC239"/>
<technology name="BC328"/>
<technology name="BC337"/>
<technology name="BC337-16"/>
<technology name="BC337-25"/>
<technology name="BC337-40"/>
<technology name="BC338-16"/>
<technology name="BC338-25"/>
<technology name="BC338-40"/>
<technology name="BC372"/>
<technology name="BC373"/>
<technology name="BC447"/>
<technology name="BC449"/>
<technology name="BC449A"/>
<technology name="BC485"/>
<technology name="BC487"/>
<technology name="BC489"/>
<technology name="BC517"/>
<technology name="BC546"/>
<technology name="BC547"/>
<technology name="BC547A"/>
<technology name="BC547B"/>
<technology name="BC548"/>
<technology name="BC549"/>
<technology name="BC550"/>
<technology name="BC618"/>
<technology name="BF391"/>
<technology name="BF393"/>
<technology name="BF844"/>
<technology name="MPS2222A"/>
<technology name="MPSL51"/>
<technology name="MPSW42"/>
</technologies>
</device>
<device name="TO92-BEC" package="TO92-BEC">
<connects>
<connect gate="G$1" pin="B" pad="B"/>
<connect gate="G$1" pin="C" pad="C"/>
<connect gate="G$1" pin="E" pad="E"/>
</connects>
<technologies>
<technology name="BF199"/>
<technology name="BF224"/>
<technology name="BF240"/>
</technologies>
</device>
<device name="TO92-CEB" package="TO92-CEB">
<connects>
<connect gate="G$1" pin="B" pad="B"/>
<connect gate="G$1" pin="C" pad="C"/>
<connect gate="G$1" pin="E" pad="E"/>
</connects>
<technologies>
<technology name="BF374"/>
<technology name="BF959"/>
<technology name="MPHS10"/>
</technologies>
</device>
<device name="SC59-BEC" package="SC59-BEC">
<connects>
<connect gate="G$1" pin="B" pad="B"/>
<connect gate="G$1" pin="C" pad="C"/>
<connect gate="G$1" pin="E" pad="E"/>
</connects>
<technologies>
<technology name="MMUN2211LT1"/>
<technology name="MMUN2212LT1"/>
<technology name="MMUN2213LT1"/>
<technology name="MMUN2214LT1"/>
</technologies>
</device>
<device name="SOT89-BCE" package="SOT89-BCE">
<connects>
<connect gate="G$1" pin="B" pad="B"/>
<connect gate="G$1" pin="C" pad="C"/>
<connect gate="G$1" pin="E" pad="E"/>
</connects>
<technologies>
<technology name=""/>
<technology name="BC868"/>
<technology name="BCX54"/>
<technology name="BCX55"/>
<technology name="BCX56"/>
<technology name="BF622"/>
</technologies>
</device>
<device name="SOT-37" package="SOT37">
<connects>
<connect gate="G$1" pin="B" pad="3"/>
<connect gate="G$1" pin="C" pad="1"/>
<connect gate="G$1" pin="E" pad="2"/>
</connects>
<technologies>
<technology name="BFR96"/>
</technologies>
</device>
<device name="TO92-ECB" package="TO92-ECB">
<connects>
<connect gate="G$1" pin="B" pad="B"/>
<connect gate="G$1" pin="C" pad="C"/>
<connect gate="G$1" pin="E" pad="E"/>
</connects>
<technologies>
<technology name=""/>
<technology name="BC167"/>
<technology name="BC168"/>
<technology name="BC169"/>
<technology name="BC368"/>
<technology name="BC635"/>
<technology name="BC637"/>
<technology name="BC639"/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
<part name="U$1" library="UNLibrary" deviceset="ESP32_AZ" device=""/>
<part name="HOUR_A" library="UNLibrary" deviceset="7SEG-CA_2.9INCH" device=""/>
<part name="HOUR_B" library="UNLibrary" deviceset="7SEG-CA_2.9INCH" device=""/>
<part name="MIN_A" library="UNLibrary" deviceset="7SEG-CA_2.9INCH" device=""/>
<part name="MIN_B" library="UNLibrary" deviceset="7SEG-CA_2.9INCH" device=""/>
<part name="LED_SUN" library="led" deviceset="LED" device="5MM"/>
<part name="LED_MON" library="led" deviceset="LED" device="5MM"/>
<part name="LED_TUE" library="led" deviceset="LED" device="5MM"/>
<part name="LED_WED" library="led" deviceset="LED" device="5MM"/>
<part name="LED_THU" library="led" deviceset="LED" device="5MM"/>
<part name="LED_FRI" library="led" deviceset="LED" device="5MM"/>
<part name="LED_SAT" library="led" deviceset="LED" device="5MM"/>
<part name="IC1" library="UNLibrary" deviceset="MCP23017" device="SO"/>
<part name="2003_HOUR_A" library="uln-udn" deviceset="ULN2003A" device="D"/>
<part name="R1" library="resistor" deviceset="R-EU_" device="M1206" value="10k"/>
<part name="R2" library="resistor" deviceset="R-EU_" device="M1206" value="10k"/>
<part name="R3" library="resistor" deviceset="R-EU_" device="M1206" value="10k"/>
<part name="R4" library="resistor" deviceset="R-EU_" device="M1206" value="10k"/>
<part name="R5" library="resistor" deviceset="R-EU_" device="M1206" value="10k"/>
<part name="R6" library="resistor" deviceset="R-EU_" device="M1206" value="10k"/>
<part name="+3V1" library="supply1" deviceset="+3V3" device=""/>
<part name="+3V2" library="supply1" deviceset="+3V3" device=""/>
<part name="+3V3" library="supply1" deviceset="+3V3" device=""/>
<part name="4543_HOUR_A" library="UNLibrary" deviceset="CD4543" device="D"/>
<part name="R7" library="resistor" deviceset="R-EU_" device="M1206" value="10k"/>
<part name="GND1" library="supply1" deviceset="GND" device=""/>
<part name="R8" library="resistor" deviceset="R-EU_" device="M1206" value="1k"/>
<part name="R9" library="resistor" deviceset="R-EU_" device="M1206" value="1k"/>
<part name="R10" library="resistor" deviceset="R-EU_" device="M1206" value="1k"/>
<part name="R11" library="resistor" deviceset="R-EU_" device="M1206" value="1k"/>
<part name="R12" library="resistor" deviceset="R-EU_" device="M1206" value="1k"/>
<part name="R13" library="resistor" deviceset="R-EU_" device="M1206" value="470"/>
<part name="R14" library="resistor" deviceset="R-EU_" device="M1206" value="470"/>
<part name="R15" library="resistor" deviceset="R-EU_" device="M1206" value="470"/>
<part name="R16" library="resistor" deviceset="R-EU_" device="M1206" value="470"/>
<part name="R17" library="resistor" deviceset="R-EU_" device="M1206" value="470"/>
<part name="R18" library="resistor" deviceset="R-EU_" device="M1206" value="470"/>
<part name="R19" library="resistor" deviceset="R-EU_" device="M1206" value="470"/>
<part name="2003_HOUR_B" library="uln-udn" deviceset="ULN2003A" device="D"/>
<part name="R20" library="resistor" deviceset="R-EU_" device="M1206" value="10k"/>
<part name="R21" library="resistor" deviceset="R-EU_" device="M1206" value="10k"/>
<part name="R22" library="resistor" deviceset="R-EU_" device="M1206" value="10k"/>
<part name="R23" library="resistor" deviceset="R-EU_" device="M1206" value="10k"/>
<part name="R24" library="resistor" deviceset="R-EU_" device="M1206" value="10k"/>
<part name="R25" library="resistor" deviceset="R-EU_" device="M1206" value="10k"/>
<part name="+3V4" library="supply1" deviceset="+3V3" device=""/>
<part name="4543_HOUR_B" library="UNLibrary" deviceset="CD4543" device="D"/>
<part name="R26" library="resistor" deviceset="R-EU_" device="M1206" value="10k"/>
<part name="R27" library="resistor" deviceset="R-EU_" device="M1206" value="1k"/>
<part name="R28" library="resistor" deviceset="R-EU_" device="M1206" value="1k"/>
<part name="R29" library="resistor" deviceset="R-EU_" device="M1206" value="1k"/>
<part name="R30" library="resistor" deviceset="R-EU_" device="M1206" value="1k"/>
<part name="R31" library="resistor" deviceset="R-EU_" device="M1206" value="1k"/>
<part name="R32" library="resistor" deviceset="R-EU_" device="M1206" value="470"/>
<part name="R33" library="resistor" deviceset="R-EU_" device="M1206" value="470"/>
<part name="R34" library="resistor" deviceset="R-EU_" device="M1206" value="470"/>
<part name="R35" library="resistor" deviceset="R-EU_" device="M1206" value="470"/>
<part name="R36" library="resistor" deviceset="R-EU_" device="M1206" value="470"/>
<part name="R37" library="resistor" deviceset="R-EU_" device="M1206" value="470"/>
<part name="R38" library="resistor" deviceset="R-EU_" device="M1206" value="470"/>
<part name="GND2" library="supply1" deviceset="GND" device=""/>
<part name="2003_MIN_A" library="uln-udn" deviceset="ULN2003A" device="D"/>
<part name="R39" library="resistor" deviceset="R-EU_" device="M1206" value="10k"/>
<part name="R40" library="resistor" deviceset="R-EU_" device="M1206" value="10k"/>
<part name="R41" library="resistor" deviceset="R-EU_" device="M1206" value="10k"/>
<part name="R42" library="resistor" deviceset="R-EU_" device="M1206" value="10k"/>
<part name="R43" library="resistor" deviceset="R-EU_" device="M1206" value="10k"/>
<part name="R44" library="resistor" deviceset="R-EU_" device="M1206" value="10k"/>
<part name="+3V5" library="supply1" deviceset="+3V3" device=""/>
<part name="4543_MIN_A" library="UNLibrary" deviceset="CD4543" device="D"/>
<part name="R45" library="resistor" deviceset="R-EU_" device="M1206" value="10k"/>
<part name="GND3" library="supply1" deviceset="GND" device=""/>
<part name="R46" library="resistor" deviceset="R-EU_" device="M1206" value="1k"/>
<part name="R47" library="resistor" deviceset="R-EU_" device="M1206" value="1k"/>
<part name="R48" library="resistor" deviceset="R-EU_" device="M1206" value="1k"/>
<part name="R49" library="resistor" deviceset="R-EU_" device="M1206" value="1k"/>
<part name="R50" library="resistor" deviceset="R-EU_" device="M1206" value="1k"/>
<part name="R51" library="resistor" deviceset="R-EU_" device="M1206" value="470"/>
<part name="R52" library="resistor" deviceset="R-EU_" device="M1206" value="470"/>
<part name="R53" library="resistor" deviceset="R-EU_" device="M1206" value="470"/>
<part name="R54" library="resistor" deviceset="R-EU_" device="M1206" value="470"/>
<part name="R55" library="resistor" deviceset="R-EU_" device="M1206" value="470"/>
<part name="R56" library="resistor" deviceset="R-EU_" device="M1206" value="470"/>
<part name="R57" library="resistor" deviceset="R-EU_" device="M1206" value="470"/>
<part name="2003_MIN_B" library="uln-udn" deviceset="ULN2003A" device="D"/>
<part name="R58" library="resistor" deviceset="R-EU_" device="M1206" value="10k"/>
<part name="R59" library="resistor" deviceset="R-EU_" device="M1206" value="10k"/>
<part name="R60" library="resistor" deviceset="R-EU_" device="M1206" value="10k"/>
<part name="R61" library="resistor" deviceset="R-EU_" device="M1206" value="10k"/>
<part name="R62" library="resistor" deviceset="R-EU_" device="M1206" value="10k"/>
<part name="R63" library="resistor" deviceset="R-EU_" device="M1206" value="10k"/>
<part name="+3V6" library="supply1" deviceset="+3V3" device=""/>
<part name="4543_MIN_B" library="UNLibrary" deviceset="CD4543" device="D"/>
<part name="R64" library="resistor" deviceset="R-EU_" device="M1206" value="10k"/>
<part name="R65" library="resistor" deviceset="R-EU_" device="M1206" value="1k"/>
<part name="R66" library="resistor" deviceset="R-EU_" device="M1206" value="1k"/>
<part name="R67" library="resistor" deviceset="R-EU_" device="M1206" value="1k"/>
<part name="R68" library="resistor" deviceset="R-EU_" device="M1206" value="1k"/>
<part name="R69" library="resistor" deviceset="R-EU_" device="M1206" value="1k"/>
<part name="R70" library="resistor" deviceset="R-EU_" device="M1206" value="470"/>
<part name="R71" library="resistor" deviceset="R-EU_" device="M1206" value="470"/>
<part name="R72" library="resistor" deviceset="R-EU_" device="M1206" value="470"/>
<part name="R73" library="resistor" deviceset="R-EU_" device="M1206" value="470"/>
<part name="R74" library="resistor" deviceset="R-EU_" device="M1206" value="470"/>
<part name="R75" library="resistor" deviceset="R-EU_" device="M1206" value="470"/>
<part name="R76" library="resistor" deviceset="R-EU_" device="M1206" value="470"/>
<part name="GND4" library="supply1" deviceset="GND" device=""/>
<part name="SEC_A" library="UNLibrary" deviceset="7SEG-CA_2.9INCH" device=""/>
<part name="SEC_B" library="UNLibrary" deviceset="7SEG-CA_2.9INCH" device=""/>
<part name="2003_SEC_A" library="uln-udn" deviceset="ULN2003A" device="D"/>
<part name="R77" library="resistor" deviceset="R-EU_" device="M1206" value="10k"/>
<part name="R78" library="resistor" deviceset="R-EU_" device="M1206" value="10k"/>
<part name="R79" library="resistor" deviceset="R-EU_" device="M1206" value="10k"/>
<part name="R80" library="resistor" deviceset="R-EU_" device="M1206" value="10k"/>
<part name="R81" library="resistor" deviceset="R-EU_" device="M1206" value="10k"/>
<part name="R82" library="resistor" deviceset="R-EU_" device="M1206" value="10k"/>
<part name="+3V7" library="supply1" deviceset="+3V3" device=""/>
<part name="4543_SEC_A" library="UNLibrary" deviceset="CD4543" device="D"/>
<part name="R83" library="resistor" deviceset="R-EU_" device="M1206" value="10k"/>
<part name="GND5" library="supply1" deviceset="GND" device=""/>
<part name="R84" library="resistor" deviceset="R-EU_" device="M1206" value="1k"/>
<part name="R85" library="resistor" deviceset="R-EU_" device="M1206" value="1k"/>
<part name="R86" library="resistor" deviceset="R-EU_" device="M1206" value="1k"/>
<part name="R87" library="resistor" deviceset="R-EU_" device="M1206" value="1k"/>
<part name="R88" library="resistor" deviceset="R-EU_" device="M1206" value="1k"/>
<part name="R89" library="resistor" deviceset="R-EU_" device="M1206" value="470"/>
<part name="R90" library="resistor" deviceset="R-EU_" device="M1206" value="470"/>
<part name="R91" library="resistor" deviceset="R-EU_" device="M1206" value="470"/>
<part name="R92" library="resistor" deviceset="R-EU_" device="M1206" value="470"/>
<part name="R93" library="resistor" deviceset="R-EU_" device="M1206" value="470"/>
<part name="R94" library="resistor" deviceset="R-EU_" device="M1206" value="470"/>
<part name="R95" library="resistor" deviceset="R-EU_" device="M1206" value="470"/>
<part name="2003_SEC_B" library="uln-udn" deviceset="ULN2003A" device="D"/>
<part name="R96" library="resistor" deviceset="R-EU_" device="M1206" value="10k"/>
<part name="R97" library="resistor" deviceset="R-EU_" device="M1206" value="10k"/>
<part name="R98" library="resistor" deviceset="R-EU_" device="M1206" value="10k"/>
<part name="R99" library="resistor" deviceset="R-EU_" device="M1206" value="10k"/>
<part name="R100" library="resistor" deviceset="R-EU_" device="M1206" value="10k"/>
<part name="R101" library="resistor" deviceset="R-EU_" device="M1206" value="10k"/>
<part name="+3V8" library="supply1" deviceset="+3V3" device=""/>
<part name="4543_SEC_B" library="UNLibrary" deviceset="CD4543" device="D"/>
<part name="R102" library="resistor" deviceset="R-EU_" device="M1206" value="10k"/>
<part name="R103" library="resistor" deviceset="R-EU_" device="M1206" value="1k"/>
<part name="R104" library="resistor" deviceset="R-EU_" device="M1206" value="1k"/>
<part name="R105" library="resistor" deviceset="R-EU_" device="M1206" value="1k"/>
<part name="R106" library="resistor" deviceset="R-EU_" device="M1206" value="1k"/>
<part name="R107" library="resistor" deviceset="R-EU_" device="M1206" value="1k"/>
<part name="R108" library="resistor" deviceset="R-EU_" device="M1206" value="470"/>
<part name="R109" library="resistor" deviceset="R-EU_" device="M1206" value="470"/>
<part name="R110" library="resistor" deviceset="R-EU_" device="M1206" value="470"/>
<part name="R111" library="resistor" deviceset="R-EU_" device="M1206" value="470"/>
<part name="R112" library="resistor" deviceset="R-EU_" device="M1206" value="470"/>
<part name="R113" library="resistor" deviceset="R-EU_" device="M1206" value="470"/>
<part name="R114" library="resistor" deviceset="R-EU_" device="M1206" value="470"/>
<part name="GND6" library="supply1" deviceset="GND" device=""/>
<part name="LED_COLON1" library="led" deviceset="LED" device="5MM"/>
<part name="LED_COLON2" library="led" deviceset="LED" device="5MM"/>
<part name="DEMUX_WEEKDAY" library="UNLibrary" deviceset="74HC238" device="D"/>
<part name="R245" library="resistor" deviceset="R-EU_" device="M1206" value="1k"/>
<part name="R246" library="resistor" deviceset="R-EU_" device="M1206" value="1k"/>
<part name="R247" library="resistor" deviceset="R-EU_" device="M1206" value="1k"/>
<part name="R248" library="resistor" deviceset="R-EU_" device="M1206" value="1k"/>
<part name="R249" library="resistor" deviceset="R-EU_" device="M1206" value="1k"/>
<part name="R250" library="resistor" deviceset="R-EU_" device="M1206" value="1k"/>
<part name="R251" library="resistor" deviceset="R-EU_" device="M1206" value="1k"/>
<part name="GND17" library="supply1" deviceset="GND" device=""/>
<part name="+3V39" library="supply1" deviceset="+3V3" device=""/>
<part name="GND18" library="supply1" deviceset="GND" device=""/>
<part name="R252" library="resistor" deviceset="R-EU_" device="M1206" value="100"/>
<part name="R253" library="resistor" deviceset="R-EU_" device="M1206" value="470"/>
<part name="GND19" library="supply1" deviceset="GND" device=""/>
<part name="LED_COLON3" library="led" deviceset="LED" device="5MM"/>
<part name="LED_COLON4" library="led" deviceset="LED" device="5MM"/>
<part name="R254" library="resistor" deviceset="R-EU_" device="M1206" value="470"/>
<part name="GND20" library="supply1" deviceset="GND" device=""/>
<part name="POWER_SWITCH" library="UNLibrary" deviceset="RELAY" device=""/>
<part name="R255" library="resistor" deviceset="R-EU_" device="M1206" value="300"/>
<part name="GND21" library="supply1" deviceset="GND" device=""/>
<part name="P+9" library="supply1" deviceset="+12V" device=""/>
<part name="R256" library="resistor" deviceset="R-EU_" device="M1206" value="4.7k"/>
<part name="R257" library="resistor" deviceset="R-EU_" device="M1206" value="4.7k"/>
<part name="GND22" library="supply1" deviceset="GND" device=""/>
<part name="GND23" library="supply1" deviceset="GND" device=""/>
<part name="P+1" library="supply1" deviceset="+12V" device=""/>
<part name="GND24" library="supply1" deviceset="GND" device=""/>
<part name="12V_POWER_OUT" library="UNLibrary" deviceset="2PIN_BLOCK" device=""/>
<part name="12_POWER_IN" library="UNLibrary" deviceset="PINHD-1X2" device=""/>
<part name="4543_DAY_A" library="UNLibrary" deviceset="CD4543" device="D"/>
<part name="DAY_A" library="UNLibrary" deviceset="7SEG-CA" device=""/>
<part name="2003_DAY_A" library="uln-udn" deviceset="ULN2003A" device="D"/>
<part name="R258" library="resistor" deviceset="R-EU_" device="M1206" value="10k"/>
<part name="R259" library="resistor" deviceset="R-EU_" device="M1206" value="10k"/>
<part name="R260" library="resistor" deviceset="R-EU_" device="M1206" value="10k"/>
<part name="R261" library="resistor" deviceset="R-EU_" device="M1206" value="10k"/>
<part name="R262" library="resistor" deviceset="R-EU_" device="M1206" value="10k"/>
<part name="R263" library="resistor" deviceset="R-EU_" device="M1206" value="10k"/>
<part name="R264" library="resistor" deviceset="R-EU_" device="M1206" value="10k"/>
<part name="R265" library="resistor" deviceset="R-EU_" device="M1206" value="1k"/>
<part name="R266" library="resistor" deviceset="R-EU_" device="M1206" value="1k"/>
<part name="R267" library="resistor" deviceset="R-EU_" device="M1206" value="1k"/>
<part name="R268" library="resistor" deviceset="R-EU_" device="M1206" value="1k"/>
<part name="R269" library="resistor" deviceset="R-EU_" device="M1206" value="1k"/>
<part name="GND25" library="supply1" deviceset="GND" device=""/>
<part name="R270" library="resistor" deviceset="R-EU_" device="M1206" value="2k"/>
<part name="R271" library="resistor" deviceset="R-EU_" device="M1206" value="2k"/>
<part name="R272" library="resistor" deviceset="R-EU_" device="M1206" value="2k"/>
<part name="R273" library="resistor" deviceset="R-EU_" device="M1206" value="2k"/>
<part name="R274" library="resistor" deviceset="R-EU_" device="M1206" value="2k"/>
<part name="R275" library="resistor" deviceset="R-EU_" device="M1206" value="2k"/>
<part name="R276" library="resistor" deviceset="R-EU_" device="M1206" value="2k"/>
<part name="+3V40" library="supply1" deviceset="+3V3" device=""/>
<part name="4543_DAY_B" library="UNLibrary" deviceset="CD4543" device="D"/>
<part name="DAY_B" library="UNLibrary" deviceset="7SEG-CA" device=""/>
<part name="2003_DAY_B" library="uln-udn" deviceset="ULN2003A" device="D"/>
<part name="R115" library="resistor" deviceset="R-EU_" device="M1206" value="10k"/>
<part name="R116" library="resistor" deviceset="R-EU_" device="M1206" value="10k"/>
<part name="R117" library="resistor" deviceset="R-EU_" device="M1206" value="10k"/>
<part name="R118" library="resistor" deviceset="R-EU_" device="M1206" value="10k"/>
<part name="R119" library="resistor" deviceset="R-EU_" device="M1206" value="10k"/>
<part name="R120" library="resistor" deviceset="R-EU_" device="M1206" value="10k"/>
<part name="R121" library="resistor" deviceset="R-EU_" device="M1206" value="10k"/>
<part name="R122" library="resistor" deviceset="R-EU_" device="M1206" value="1k"/>
<part name="R123" library="resistor" deviceset="R-EU_" device="M1206" value="1k"/>
<part name="R124" library="resistor" deviceset="R-EU_" device="M1206" value="1k"/>
<part name="R125" library="resistor" deviceset="R-EU_" device="M1206" value="1k"/>
<part name="R126" library="resistor" deviceset="R-EU_" device="M1206" value="1k"/>
<part name="GND7" library="supply1" deviceset="GND" device=""/>
<part name="R127" library="resistor" deviceset="R-EU_" device="M1206" value="2k"/>
<part name="R128" library="resistor" deviceset="R-EU_" device="M1206" value="2k"/>
<part name="R129" library="resistor" deviceset="R-EU_" device="M1206" value="2k"/>
<part name="R130" library="resistor" deviceset="R-EU_" device="M1206" value="2k"/>
<part name="R131" library="resistor" deviceset="R-EU_" device="M1206" value="2k"/>
<part name="R132" library="resistor" deviceset="R-EU_" device="M1206" value="2k"/>
<part name="R133" library="resistor" deviceset="R-EU_" device="M1206" value="2k"/>
<part name="+3V9" library="supply1" deviceset="+3V3" device=""/>
<part name="4543_MONTH_A" library="UNLibrary" deviceset="CD4543" device="D"/>
<part name="MONTH_A" library="UNLibrary" deviceset="7SEG-CA" device=""/>
<part name="2003_MONTH_A" library="uln-udn" deviceset="ULN2003A" device="D"/>
<part name="R134" library="resistor" deviceset="R-EU_" device="M1206" value="10k"/>
<part name="R135" library="resistor" deviceset="R-EU_" device="M1206" value="10k"/>
<part name="R136" library="resistor" deviceset="R-EU_" device="M1206" value="10k"/>
<part name="R137" library="resistor" deviceset="R-EU_" device="M1206" value="10k"/>
<part name="R138" library="resistor" deviceset="R-EU_" device="M1206" value="10k"/>
<part name="R139" library="resistor" deviceset="R-EU_" device="M1206" value="10k"/>
<part name="R140" library="resistor" deviceset="R-EU_" device="M1206" value="10k"/>
<part name="R141" library="resistor" deviceset="R-EU_" device="M1206" value="1k"/>
<part name="R142" library="resistor" deviceset="R-EU_" device="M1206" value="1k"/>
<part name="R143" library="resistor" deviceset="R-EU_" device="M1206" value="1k"/>
<part name="R144" library="resistor" deviceset="R-EU_" device="M1206" value="1k"/>
<part name="R145" library="resistor" deviceset="R-EU_" device="M1206" value="1k"/>
<part name="GND8" library="supply1" deviceset="GND" device=""/>
<part name="R146" library="resistor" deviceset="R-EU_" device="M1206" value="2k"/>
<part name="R147" library="resistor" deviceset="R-EU_" device="M1206" value="2k"/>
<part name="R148" library="resistor" deviceset="R-EU_" device="M1206" value="2k"/>
<part name="R149" library="resistor" deviceset="R-EU_" device="M1206" value="2k"/>
<part name="R150" library="resistor" deviceset="R-EU_" device="M1206" value="2k"/>
<part name="R151" library="resistor" deviceset="R-EU_" device="M1206" value="2k"/>
<part name="R152" library="resistor" deviceset="R-EU_" device="M1206" value="2k"/>
<part name="+3V10" library="supply1" deviceset="+3V3" device=""/>
<part name="4543_MONTH_B" library="UNLibrary" deviceset="CD4543" device="D"/>
<part name="MONTH_B" library="UNLibrary" deviceset="7SEG-CA" device=""/>
<part name="2003_MONTH_B" library="uln-udn" deviceset="ULN2003A" device="D"/>
<part name="R153" library="resistor" deviceset="R-EU_" device="M1206" value="10k"/>
<part name="R154" library="resistor" deviceset="R-EU_" device="M1206" value="10k"/>
<part name="R155" library="resistor" deviceset="R-EU_" device="M1206" value="10k"/>
<part name="R156" library="resistor" deviceset="R-EU_" device="M1206" value="10k"/>
<part name="R157" library="resistor" deviceset="R-EU_" device="M1206" value="10k"/>
<part name="R158" library="resistor" deviceset="R-EU_" device="M1206" value="10k"/>
<part name="R159" library="resistor" deviceset="R-EU_" device="M1206" value="10k"/>
<part name="R160" library="resistor" deviceset="R-EU_" device="M1206" value="1k"/>
<part name="R161" library="resistor" deviceset="R-EU_" device="M1206" value="1k"/>
<part name="R162" library="resistor" deviceset="R-EU_" device="M1206" value="1k"/>
<part name="R163" library="resistor" deviceset="R-EU_" device="M1206" value="1k"/>
<part name="R164" library="resistor" deviceset="R-EU_" device="M1206" value="1k"/>
<part name="GND9" library="supply1" deviceset="GND" device=""/>
<part name="R165" library="resistor" deviceset="R-EU_" device="M1206" value="2k"/>
<part name="R166" library="resistor" deviceset="R-EU_" device="M1206" value="2k"/>
<part name="R277" library="resistor" deviceset="R-EU_" device="M1206" value="2k"/>
<part name="R278" library="resistor" deviceset="R-EU_" device="M1206" value="2k"/>
<part name="R279" library="resistor" deviceset="R-EU_" device="M1206" value="2k"/>
<part name="R280" library="resistor" deviceset="R-EU_" device="M1206" value="2k"/>
<part name="R281" library="resistor" deviceset="R-EU_" device="M1206" value="2k"/>
<part name="+3V11" library="supply1" deviceset="+3V3" device=""/>
<part name="4543_YEAR_A" library="UNLibrary" deviceset="CD4543" device="D"/>
<part name="YEAR_A" library="UNLibrary" deviceset="7SEG-CA" device=""/>
<part name="2003_YEAR_A" library="uln-udn" deviceset="ULN2003A" device="D"/>
<part name="R282" library="resistor" deviceset="R-EU_" device="M1206" value="10k"/>
<part name="R283" library="resistor" deviceset="R-EU_" device="M1206" value="10k"/>
<part name="R284" library="resistor" deviceset="R-EU_" device="M1206" value="10k"/>
<part name="R285" library="resistor" deviceset="R-EU_" device="M1206" value="10k"/>
<part name="R286" library="resistor" deviceset="R-EU_" device="M1206" value="10k"/>
<part name="R287" library="resistor" deviceset="R-EU_" device="M1206" value="10k"/>
<part name="R288" library="resistor" deviceset="R-EU_" device="M1206" value="10k"/>
<part name="R289" library="resistor" deviceset="R-EU_" device="M1206" value="1k"/>
<part name="R290" library="resistor" deviceset="R-EU_" device="M1206" value="1k"/>
<part name="R291" library="resistor" deviceset="R-EU_" device="M1206" value="1k"/>
<part name="R292" library="resistor" deviceset="R-EU_" device="M1206" value="1k"/>
<part name="R293" library="resistor" deviceset="R-EU_" device="M1206" value="1k"/>
<part name="GND10" library="supply1" deviceset="GND" device=""/>
<part name="R294" library="resistor" deviceset="R-EU_" device="M1206" value="2k"/>
<part name="R295" library="resistor" deviceset="R-EU_" device="M1206" value="2k"/>
<part name="R296" library="resistor" deviceset="R-EU_" device="M1206" value="2k"/>
<part name="R297" library="resistor" deviceset="R-EU_" device="M1206" value="2k"/>
<part name="R298" library="resistor" deviceset="R-EU_" device="M1206" value="2k"/>
<part name="R299" library="resistor" deviceset="R-EU_" device="M1206" value="2k"/>
<part name="R300" library="resistor" deviceset="R-EU_" device="M1206" value="2k"/>
<part name="+3V12" library="supply1" deviceset="+3V3" device=""/>
<part name="4543_YEAR_B" library="UNLibrary" deviceset="CD4543" device="D"/>
<part name="YEAR_B" library="UNLibrary" deviceset="7SEG-CA" device=""/>
<part name="2003_YEAR_B" library="uln-udn" deviceset="ULN2003A" device="D"/>
<part name="R167" library="resistor" deviceset="R-EU_" device="M1206" value="10k"/>
<part name="R168" library="resistor" deviceset="R-EU_" device="M1206" value="10k"/>
<part name="R169" library="resistor" deviceset="R-EU_" device="M1206" value="10k"/>
<part name="R170" library="resistor" deviceset="R-EU_" device="M1206" value="10k"/>
<part name="R171" library="resistor" deviceset="R-EU_" device="M1206" value="10k"/>
<part name="R172" library="resistor" deviceset="R-EU_" device="M1206" value="10k"/>
<part name="R173" library="resistor" deviceset="R-EU_" device="M1206" value="10k"/>
<part name="R174" library="resistor" deviceset="R-EU_" device="M1206" value="1k"/>
<part name="R175" library="resistor" deviceset="R-EU_" device="M1206" value="1k"/>
<part name="R176" library="resistor" deviceset="R-EU_" device="M1206" value="1k"/>
<part name="R177" library="resistor" deviceset="R-EU_" device="M1206" value="1k"/>
<part name="R178" library="resistor" deviceset="R-EU_" device="M1206" value="1k"/>
<part name="GND11" library="supply1" deviceset="GND" device=""/>
<part name="R179" library="resistor" deviceset="R-EU_" device="M1206" value="2k"/>
<part name="R180" library="resistor" deviceset="R-EU_" device="M1206" value="2k"/>
<part name="R181" library="resistor" deviceset="R-EU_" device="M1206" value="2k"/>
<part name="R182" library="resistor" deviceset="R-EU_" device="M1206" value="2k"/>
<part name="R183" library="resistor" deviceset="R-EU_" device="M1206" value="2k"/>
<part name="R184" library="resistor" deviceset="R-EU_" device="M1206" value="2k"/>
<part name="R185" library="resistor" deviceset="R-EU_" device="M1206" value="2k"/>
<part name="+3V13" library="supply1" deviceset="+3V3" device=""/>
<part name="4543_YEAR_C" library="UNLibrary" deviceset="CD4543" device="D"/>
<part name="YEAR_C" library="UNLibrary" deviceset="7SEG-CA" device=""/>
<part name="2003_YEAR_C" library="uln-udn" deviceset="ULN2003A" device="D"/>
<part name="R186" library="resistor" deviceset="R-EU_" device="M1206" value="10k"/>
<part name="R187" library="resistor" deviceset="R-EU_" device="M1206" value="10k"/>
<part name="R188" library="resistor" deviceset="R-EU_" device="M1206" value="10k"/>
<part name="R189" library="resistor" deviceset="R-EU_" device="M1206" value="10k"/>
<part name="R190" library="resistor" deviceset="R-EU_" device="M1206" value="10k"/>
<part name="R191" library="resistor" deviceset="R-EU_" device="M1206" value="10k"/>
<part name="R192" library="resistor" deviceset="R-EU_" device="M1206" value="10k"/>
<part name="R193" library="resistor" deviceset="R-EU_" device="M1206" value="1k"/>
<part name="R194" library="resistor" deviceset="R-EU_" device="M1206" value="1k"/>
<part name="R195" library="resistor" deviceset="R-EU_" device="M1206" value="1k"/>
<part name="R196" library="resistor" deviceset="R-EU_" device="M1206" value="1k"/>
<part name="R197" library="resistor" deviceset="R-EU_" device="M1206" value="1k"/>
<part name="GND12" library="supply1" deviceset="GND" device=""/>
<part name="R198" library="resistor" deviceset="R-EU_" device="M1206" value="2k"/>
<part name="R199" library="resistor" deviceset="R-EU_" device="M1206" value="2k"/>
<part name="R200" library="resistor" deviceset="R-EU_" device="M1206" value="2k"/>
<part name="R201" library="resistor" deviceset="R-EU_" device="M1206" value="2k"/>
<part name="R202" library="resistor" deviceset="R-EU_" device="M1206" value="2k"/>
<part name="R203" library="resistor" deviceset="R-EU_" device="M1206" value="2k"/>
<part name="R204" library="resistor" deviceset="R-EU_" device="M1206" value="2k"/>
<part name="+3V14" library="supply1" deviceset="+3V3" device=""/>
<part name="4543_YEAR_D" library="UNLibrary" deviceset="CD4543" device="D"/>
<part name="YEAR_D" library="UNLibrary" deviceset="7SEG-CA" device=""/>
<part name="2003_YEAR_D" library="uln-udn" deviceset="ULN2003A" device="D"/>
<part name="R205" library="resistor" deviceset="R-EU_" device="M1206" value="10k"/>
<part name="R206" library="resistor" deviceset="R-EU_" device="M1206" value="10k"/>
<part name="R207" library="resistor" deviceset="R-EU_" device="M1206" value="10k"/>
<part name="R208" library="resistor" deviceset="R-EU_" device="M1206" value="10k"/>
<part name="R209" library="resistor" deviceset="R-EU_" device="M1206" value="10k"/>
<part name="R210" library="resistor" deviceset="R-EU_" device="M1206" value="10k"/>
<part name="R211" library="resistor" deviceset="R-EU_" device="M1206" value="10k"/>
<part name="R212" library="resistor" deviceset="R-EU_" device="M1206" value="1k"/>
<part name="R213" library="resistor" deviceset="R-EU_" device="M1206" value="1k"/>
<part name="R214" library="resistor" deviceset="R-EU_" device="M1206" value="1k"/>
<part name="R215" library="resistor" deviceset="R-EU_" device="M1206" value="1k"/>
<part name="R216" library="resistor" deviceset="R-EU_" device="M1206" value="1k"/>
<part name="GND13" library="supply1" deviceset="GND" device=""/>
<part name="R217" library="resistor" deviceset="R-EU_" device="M1206" value="2k"/>
<part name="R218" library="resistor" deviceset="R-EU_" device="M1206" value="2k"/>
<part name="R301" library="resistor" deviceset="R-EU_" device="M1206" value="2k"/>
<part name="R302" library="resistor" deviceset="R-EU_" device="M1206" value="2k"/>
<part name="R303" library="resistor" deviceset="R-EU_" device="M1206" value="2k"/>
<part name="R304" library="resistor" deviceset="R-EU_" device="M1206" value="2k"/>
<part name="R305" library="resistor" deviceset="R-EU_" device="M1206" value="2k"/>
<part name="+3V15" library="supply1" deviceset="+3V3" device=""/>
<part name="4543_TEMP_A" library="UNLibrary" deviceset="CD4543" device="D"/>
<part name="TEMP_A" library="UNLibrary" deviceset="7SEG-CA" device=""/>
<part name="2003_TEMP_A" library="uln-udn" deviceset="ULN2003A" device="D"/>
<part name="R306" library="resistor" deviceset="R-EU_" device="M1206" value="10k"/>
<part name="R307" library="resistor" deviceset="R-EU_" device="M1206" value="10k"/>
<part name="R308" library="resistor" deviceset="R-EU_" device="M1206" value="10k"/>
<part name="R309" library="resistor" deviceset="R-EU_" device="M1206" value="10k"/>
<part name="R310" library="resistor" deviceset="R-EU_" device="M1206" value="10k"/>
<part name="R311" library="resistor" deviceset="R-EU_" device="M1206" value="10k"/>
<part name="R312" library="resistor" deviceset="R-EU_" device="M1206" value="10k"/>
<part name="R313" library="resistor" deviceset="R-EU_" device="M1206" value="1k"/>
<part name="R314" library="resistor" deviceset="R-EU_" device="M1206" value="1k"/>
<part name="R315" library="resistor" deviceset="R-EU_" device="M1206" value="1k"/>
<part name="R316" library="resistor" deviceset="R-EU_" device="M1206" value="1k"/>
<part name="R317" library="resistor" deviceset="R-EU_" device="M1206" value="1k"/>
<part name="GND14" library="supply1" deviceset="GND" device=""/>
<part name="R318" library="resistor" deviceset="R-EU_" device="M1206" value="2k"/>
<part name="R319" library="resistor" deviceset="R-EU_" device="M1206" value="2k"/>
<part name="R320" library="resistor" deviceset="R-EU_" device="M1206" value="2k"/>
<part name="R321" library="resistor" deviceset="R-EU_" device="M1206" value="2k"/>
<part name="R322" library="resistor" deviceset="R-EU_" device="M1206" value="2k"/>
<part name="R323" library="resistor" deviceset="R-EU_" device="M1206" value="2k"/>
<part name="R324" library="resistor" deviceset="R-EU_" device="M1206" value="2k"/>
<part name="+3V16" library="supply1" deviceset="+3V3" device=""/>
<part name="4543_TEMP_B" library="UNLibrary" deviceset="CD4543" device="D"/>
<part name="TEMP_B" library="UNLibrary" deviceset="7SEG-CA" device=""/>
<part name="2003_TEMP_B" library="uln-udn" deviceset="ULN2003A" device="D"/>
<part name="R219" library="resistor" deviceset="R-EU_" device="M1206" value="10k"/>
<part name="R220" library="resistor" deviceset="R-EU_" device="M1206" value="10k"/>
<part name="R221" library="resistor" deviceset="R-EU_" device="M1206" value="10k"/>
<part name="R222" library="resistor" deviceset="R-EU_" device="M1206" value="10k"/>
<part name="R223" library="resistor" deviceset="R-EU_" device="M1206" value="10k"/>
<part name="R224" library="resistor" deviceset="R-EU_" device="M1206" value="10k"/>
<part name="R225" library="resistor" deviceset="R-EU_" device="M1206" value="10k"/>
<part name="R226" library="resistor" deviceset="R-EU_" device="M1206" value="1k"/>
<part name="R227" library="resistor" deviceset="R-EU_" device="M1206" value="1k"/>
<part name="R228" library="resistor" deviceset="R-EU_" device="M1206" value="1k"/>
<part name="R229" library="resistor" deviceset="R-EU_" device="M1206" value="1k"/>
<part name="R230" library="resistor" deviceset="R-EU_" device="M1206" value="1k"/>
<part name="GND15" library="supply1" deviceset="GND" device=""/>
<part name="R231" library="resistor" deviceset="R-EU_" device="M1206" value="2k"/>
<part name="R325" library="resistor" deviceset="R-EU_" device="M1206" value="2k"/>
<part name="R326" library="resistor" deviceset="R-EU_" device="M1206" value="2k"/>
<part name="R327" library="resistor" deviceset="R-EU_" device="M1206" value="2k"/>
<part name="R328" library="resistor" deviceset="R-EU_" device="M1206" value="2k"/>
<part name="R329" library="resistor" deviceset="R-EU_" device="M1206" value="2k"/>
<part name="R330" library="resistor" deviceset="R-EU_" device="M1206" value="2k"/>
<part name="+3V17" library="supply1" deviceset="+3V3" device=""/>
<part name="2003_WEEKDAY" library="uln-udn" deviceset="ULN2003A" device="D"/>
<part name="R232" library="resistor" deviceset="R-EU_" device="M1206" value="400"/>
<part name="R233" library="resistor" deviceset="R-EU_" device="M1206" value="400"/>
<part name="R234" library="resistor" deviceset="R-EU_" device="M1206" value="400"/>
<part name="R235" library="resistor" deviceset="R-EU_" device="M1206" value="400"/>
<part name="R236" library="resistor" deviceset="R-EU_" device="M1206" value="400"/>
<part name="R237" library="resistor" deviceset="R-EU_" device="M1206" value="400"/>
<part name="R238" library="resistor" deviceset="R-EU_" device="M1206" value="400"/>
<part name="R239" library="resistor" deviceset="R-EU_" device="M1206" value="10k"/>
<part name="BUTTON34" library="UNLibrary" deviceset="2PIN_BLOCK" device=""/>
<part name="BUTTON35" library="UNLibrary" deviceset="2PIN_BLOCK" device=""/>
<part name="BUTTON36" library="UNLibrary" deviceset="2PIN_BLOCK" device=""/>
<part name="R240" library="resistor" deviceset="R-EU_" device="M1206" value="10k"/>
<part name="R241" library="resistor" deviceset="R-EU_" device="M1206" value="10k"/>
<part name="T2" library="transistor" deviceset="*-NPN-" device="SOT23-BEC" technology="MMBT2222ALT1" value="2N2222"/>
<part name="U$2" library="UNLibrary" deviceset="MICROSD_BOARD" device=""/>
<part name="GND16" library="supply1" deviceset="GND" device=""/>
<part name="P+2" library="supply1" deviceset="+5V" device=""/>
<part name="P+3" library="supply1" deviceset="+5V" device=""/>
</parts>
<sheets>
<sheet>
<plain>
</plain>
<instances>
<instance part="U$1" gate="G$1" x="53.34" y="33.02"/>
<instance part="HOUR_A" gate="G$1" x="-177.8" y="401.32"/>
<instance part="HOUR_B" gate="G$1" x="-137.16" y="401.32"/>
<instance part="MIN_A" gate="G$1" x="5.08" y="401.32"/>
<instance part="MIN_B" gate="G$1" x="45.72" y="401.32"/>
<instance part="LED_SUN" gate="G$1" x="309.88" y="127"/>
<instance part="LED_MON" gate="G$1" x="322.58" y="127"/>
<instance part="LED_TUE" gate="G$1" x="335.28" y="127"/>
<instance part="LED_WED" gate="G$1" x="350.52" y="127"/>
<instance part="LED_THU" gate="G$1" x="363.22" y="127"/>
<instance part="LED_FRI" gate="G$1" x="375.92" y="127"/>
<instance part="LED_SAT" gate="G$1" x="388.62" y="127"/>
<instance part="IC1" gate="G$1" x="152.4" y="58.42"/>
<instance part="2003_HOUR_A" gate="A" x="-208.28" y="434.34"/>
<instance part="R1" gate="G$1" x="-228.6" y="441.96" smashed="yes">
<attribute name="VALUE" x="-229.87" y="441.198" size="1.778" layer="96"/>
</instance>
<instance part="R2" gate="G$1" x="-228.6" y="439.42" smashed="yes">
<attribute name="VALUE" x="-229.87" y="438.658" size="1.778" layer="96"/>
</instance>
<instance part="R3" gate="G$1" x="-228.6" y="436.88" smashed="yes">
<attribute name="VALUE" x="-229.87" y="436.118" size="1.778" layer="96"/>
</instance>
<instance part="R4" gate="G$1" x="-228.6" y="434.34" smashed="yes">
<attribute name="VALUE" x="-229.87" y="433.578" size="1.778" layer="96"/>
</instance>
<instance part="R5" gate="G$1" x="-228.6" y="431.8" smashed="yes">
<attribute name="VALUE" x="-229.87" y="431.038" size="1.778" layer="96"/>
</instance>
<instance part="R6" gate="G$1" x="-228.6" y="429.26" smashed="yes">
<attribute name="VALUE" x="-229.87" y="428.498" size="1.778" layer="96"/>
</instance>
<instance part="+3V1" gate="G$1" x="-236.22" y="457.2"/>
<instance part="+3V2" gate="G$1" x="40.64" y="96.52"/>
<instance part="+3V3" gate="G$1" x="134.62" y="96.52"/>
<instance part="4543_HOUR_A" gate="A" x="-248.92" y="436.88"/>
<instance part="R7" gate="G$1" x="-228.6" y="426.72" smashed="yes">
<attribute name="VALUE" x="-229.87" y="425.958" size="1.778" layer="96"/>
</instance>
<instance part="GND1" gate="1" x="-266.7" y="406.4"/>
<instance part="R8" gate="G$1" x="-269.24" y="434.34" smashed="yes">
<attribute name="VALUE" x="-270.51" y="433.578" size="1.778" layer="96"/>
</instance>
<instance part="R9" gate="G$1" x="-269.24" y="436.88" smashed="yes">
<attribute name="VALUE" x="-270.51" y="436.118" size="1.778" layer="96"/>
</instance>
<instance part="R10" gate="G$1" x="-269.24" y="439.42" smashed="yes">
<attribute name="VALUE" x="-270.51" y="438.658" size="1.778" layer="96"/>
</instance>
<instance part="R11" gate="G$1" x="-269.24" y="441.96" smashed="yes">
<attribute name="VALUE" x="-270.51" y="441.198" size="1.778" layer="96"/>
</instance>
<instance part="R12" gate="G$1" x="-269.24" y="444.5" smashed="yes">
<attribute name="VALUE" x="-270.51" y="443.738" size="1.778" layer="96"/>
</instance>
<instance part="R13" gate="G$1" x="-187.96" y="441.96" smashed="yes">
<attribute name="VALUE" x="-189.23" y="441.198" size="1.778" layer="96"/>
</instance>
<instance part="R14" gate="G$1" x="-187.96" y="439.42" smashed="yes">
<attribute name="VALUE" x="-189.23" y="438.658" size="1.778" layer="96"/>
</instance>
<instance part="R15" gate="G$1" x="-187.96" y="436.88" smashed="yes">
<attribute name="VALUE" x="-189.23" y="436.118" size="1.778" layer="96"/>
</instance>
<instance part="R16" gate="G$1" x="-187.96" y="434.34" smashed="yes">
<attribute name="VALUE" x="-189.23" y="433.578" size="1.778" layer="96"/>
</instance>
<instance part="R17" gate="G$1" x="-187.96" y="431.8" smashed="yes">
<attribute name="VALUE" x="-189.23" y="431.038" size="1.778" layer="96"/>
</instance>
<instance part="R18" gate="G$1" x="-187.96" y="429.26" smashed="yes">
<attribute name="VALUE" x="-189.23" y="428.498" size="1.778" layer="96"/>
</instance>
<instance part="R19" gate="G$1" x="-187.96" y="426.72" smashed="yes">
<attribute name="VALUE" x="-189.23" y="425.958" size="1.778" layer="96"/>
</instance>
<instance part="2003_HOUR_B" gate="A" x="-220.98" y="500.38"/>
<instance part="R20" gate="G$1" x="-241.3" y="508" smashed="yes">
<attribute name="VALUE" x="-242.57" y="507.238" size="1.778" layer="96"/>
</instance>
<instance part="R21" gate="G$1" x="-241.3" y="505.46" smashed="yes">
<attribute name="VALUE" x="-242.57" y="504.698" size="1.778" layer="96"/>
</instance>
<instance part="R22" gate="G$1" x="-241.3" y="502.92" smashed="yes">
<attribute name="VALUE" x="-242.57" y="502.158" size="1.778" layer="96"/>
</instance>
<instance part="R23" gate="G$1" x="-241.3" y="500.38" smashed="yes">
<attribute name="VALUE" x="-242.57" y="499.618" size="1.778" layer="96"/>
</instance>
<instance part="R24" gate="G$1" x="-241.3" y="497.84" smashed="yes">
<attribute name="VALUE" x="-242.57" y="497.078" size="1.778" layer="96"/>
</instance>
<instance part="R25" gate="G$1" x="-241.3" y="495.3" smashed="yes">
<attribute name="VALUE" x="-242.57" y="494.538" size="1.778" layer="96"/>
</instance>
<instance part="+3V4" gate="G$1" x="-248.92" y="523.24"/>
<instance part="4543_HOUR_B" gate="A" x="-261.62" y="502.92"/>
<instance part="R26" gate="G$1" x="-241.3" y="492.76" smashed="yes">
<attribute name="VALUE" x="-242.57" y="491.998" size="1.778" layer="96"/>
</instance>
<instance part="R27" gate="G$1" x="-281.94" y="500.38" smashed="yes">
<attribute name="VALUE" x="-283.21" y="499.618" size="1.778" layer="96"/>
</instance>
<instance part="R28" gate="G$1" x="-281.94" y="502.92" smashed="yes">
<attribute name="VALUE" x="-283.21" y="502.158" size="1.778" layer="96"/>
</instance>
<instance part="R29" gate="G$1" x="-281.94" y="505.46" smashed="yes">
<attribute name="VALUE" x="-283.21" y="504.698" size="1.778" layer="96"/>
</instance>
<instance part="R30" gate="G$1" x="-281.94" y="508" smashed="yes">
<attribute name="VALUE" x="-283.21" y="507.238" size="1.778" layer="96"/>
</instance>
<instance part="R31" gate="G$1" x="-281.94" y="510.54" smashed="yes">
<attribute name="VALUE" x="-283.21" y="509.778" size="1.778" layer="96"/>
</instance>
<instance part="R32" gate="G$1" x="-200.66" y="508" smashed="yes">
<attribute name="VALUE" x="-201.93" y="507.238" size="1.778" layer="96"/>
</instance>
<instance part="R33" gate="G$1" x="-200.66" y="505.46" smashed="yes">
<attribute name="VALUE" x="-201.93" y="504.698" size="1.778" layer="96"/>
</instance>
<instance part="R34" gate="G$1" x="-200.66" y="502.92" smashed="yes">
<attribute name="VALUE" x="-201.93" y="502.158" size="1.778" layer="96"/>
</instance>
<instance part="R35" gate="G$1" x="-200.66" y="500.38" smashed="yes">
<attribute name="VALUE" x="-201.93" y="499.618" size="1.778" layer="96"/>
</instance>
<instance part="R36" gate="G$1" x="-200.66" y="497.84" smashed="yes">
<attribute name="VALUE" x="-201.93" y="497.078" size="1.778" layer="96"/>
</instance>
<instance part="R37" gate="G$1" x="-200.66" y="495.3" smashed="yes">
<attribute name="VALUE" x="-201.93" y="494.538" size="1.778" layer="96"/>
</instance>
<instance part="R38" gate="G$1" x="-200.66" y="492.76" smashed="yes">
<attribute name="VALUE" x="-201.93" y="491.998" size="1.778" layer="96"/>
</instance>
<instance part="GND2" gate="1" x="-279.4" y="480.06"/>
<instance part="2003_MIN_A" gate="A" x="-25.4" y="434.34"/>
<instance part="R39" gate="G$1" x="-45.72" y="441.96" smashed="yes">
<attribute name="VALUE" x="-46.99" y="441.198" size="1.778" layer="96"/>
</instance>
<instance part="R40" gate="G$1" x="-45.72" y="439.42" smashed="yes">
<attribute name="VALUE" x="-46.99" y="438.658" size="1.778" layer="96"/>
</instance>
<instance part="R41" gate="G$1" x="-45.72" y="436.88" smashed="yes">
<attribute name="VALUE" x="-46.99" y="436.118" size="1.778" layer="96"/>
</instance>
<instance part="R42" gate="G$1" x="-45.72" y="434.34" smashed="yes">
<attribute name="VALUE" x="-46.99" y="433.578" size="1.778" layer="96"/>
</instance>
<instance part="R43" gate="G$1" x="-45.72" y="431.8" smashed="yes">
<attribute name="VALUE" x="-46.99" y="431.038" size="1.778" layer="96"/>
</instance>
<instance part="R44" gate="G$1" x="-45.72" y="429.26" smashed="yes">
<attribute name="VALUE" x="-46.99" y="428.498" size="1.778" layer="96"/>
</instance>
<instance part="+3V5" gate="G$1" x="-53.34" y="457.2"/>
<instance part="4543_MIN_A" gate="A" x="-66.04" y="436.88"/>
<instance part="R45" gate="G$1" x="-45.72" y="426.72" smashed="yes">
<attribute name="VALUE" x="-46.99" y="425.958" size="1.778" layer="96"/>
</instance>
<instance part="GND3" gate="1" x="-83.82" y="406.4"/>
<instance part="R46" gate="G$1" x="-86.36" y="434.34" smashed="yes">
<attribute name="VALUE" x="-87.63" y="433.578" size="1.778" layer="96"/>
</instance>
<instance part="R47" gate="G$1" x="-86.36" y="436.88" smashed="yes">
<attribute name="VALUE" x="-87.63" y="436.118" size="1.778" layer="96"/>
</instance>
<instance part="R48" gate="G$1" x="-86.36" y="439.42" smashed="yes">
<attribute name="VALUE" x="-87.63" y="438.658" size="1.778" layer="96"/>
</instance>
<instance part="R49" gate="G$1" x="-86.36" y="441.96" smashed="yes">
<attribute name="VALUE" x="-87.63" y="441.198" size="1.778" layer="96"/>
</instance>
<instance part="R50" gate="G$1" x="-86.36" y="444.5" smashed="yes">
<attribute name="VALUE" x="-87.63" y="443.738" size="1.778" layer="96"/>
</instance>
<instance part="R51" gate="G$1" x="-5.08" y="441.96" smashed="yes">
<attribute name="VALUE" x="-6.35" y="441.198" size="1.778" layer="96"/>
</instance>
<instance part="R52" gate="G$1" x="-5.08" y="439.42" smashed="yes">
<attribute name="VALUE" x="-6.35" y="438.658" size="1.778" layer="96"/>
</instance>
<instance part="R53" gate="G$1" x="-5.08" y="436.88" smashed="yes">
<attribute name="VALUE" x="-6.35" y="436.118" size="1.778" layer="96"/>
</instance>
<instance part="R54" gate="G$1" x="-5.08" y="434.34" smashed="yes">
<attribute name="VALUE" x="-6.35" y="433.578" size="1.778" layer="96"/>
</instance>
<instance part="R55" gate="G$1" x="-5.08" y="431.8" smashed="yes">
<attribute name="VALUE" x="-6.35" y="431.038" size="1.778" layer="96"/>
</instance>
<instance part="R56" gate="G$1" x="-5.08" y="429.26" smashed="yes">
<attribute name="VALUE" x="-6.35" y="428.498" size="1.778" layer="96"/>
</instance>
<instance part="R57" gate="G$1" x="-5.08" y="426.72" smashed="yes">
<attribute name="VALUE" x="-6.35" y="425.958" size="1.778" layer="96"/>
</instance>
<instance part="2003_MIN_B" gate="A" x="-38.1" y="513.08"/>
<instance part="R58" gate="G$1" x="-58.42" y="520.7" smashed="yes">
<attribute name="VALUE" x="-59.69" y="519.938" size="1.778" layer="96"/>
</instance>
<instance part="R59" gate="G$1" x="-58.42" y="518.16" smashed="yes">
<attribute name="VALUE" x="-59.69" y="517.398" size="1.778" layer="96"/>
</instance>
<instance part="R60" gate="G$1" x="-58.42" y="515.62" smashed="yes">
<attribute name="VALUE" x="-59.69" y="514.858" size="1.778" layer="96"/>
</instance>
<instance part="R61" gate="G$1" x="-58.42" y="513.08" smashed="yes">
<attribute name="VALUE" x="-59.69" y="512.318" size="1.778" layer="96"/>
</instance>
<instance part="R62" gate="G$1" x="-58.42" y="510.54" smashed="yes">
<attribute name="VALUE" x="-59.69" y="509.778" size="1.778" layer="96"/>
</instance>
<instance part="R63" gate="G$1" x="-58.42" y="508" smashed="yes">
<attribute name="VALUE" x="-59.69" y="507.238" size="1.778" layer="96"/>
</instance>
<instance part="+3V6" gate="G$1" x="-66.04" y="535.94"/>
<instance part="4543_MIN_B" gate="A" x="-78.74" y="515.62"/>
<instance part="R64" gate="G$1" x="-58.42" y="505.46" smashed="yes">
<attribute name="VALUE" x="-59.69" y="504.698" size="1.778" layer="96"/>
</instance>
<instance part="R65" gate="G$1" x="-99.06" y="513.08" smashed="yes">
<attribute name="VALUE" x="-100.33" y="512.318" size="1.778" layer="96"/>
</instance>
<instance part="R66" gate="G$1" x="-99.06" y="515.62" smashed="yes">
<attribute name="VALUE" x="-100.33" y="514.858" size="1.778" layer="96"/>
</instance>
<instance part="R67" gate="G$1" x="-99.06" y="518.16" smashed="yes">
<attribute name="VALUE" x="-100.33" y="517.398" size="1.778" layer="96"/>
</instance>
<instance part="R68" gate="G$1" x="-99.06" y="520.7" smashed="yes">
<attribute name="VALUE" x="-100.33" y="519.938" size="1.778" layer="96"/>
</instance>
<instance part="R69" gate="G$1" x="-99.06" y="523.24" smashed="yes">
<attribute name="VALUE" x="-100.33" y="522.478" size="1.778" layer="96"/>
</instance>
<instance part="R70" gate="G$1" x="-17.78" y="520.7" smashed="yes">
<attribute name="VALUE" x="-19.05" y="519.938" size="1.778" layer="96"/>
</instance>
<instance part="R71" gate="G$1" x="-17.78" y="518.16" smashed="yes">
<attribute name="VALUE" x="-19.05" y="517.398" size="1.778" layer="96"/>
</instance>
<instance part="R72" gate="G$1" x="-17.78" y="515.62" smashed="yes">
<attribute name="VALUE" x="-19.05" y="514.858" size="1.778" layer="96"/>
</instance>
<instance part="R73" gate="G$1" x="-17.78" y="513.08" smashed="yes">
<attribute name="VALUE" x="-19.05" y="512.318" size="1.778" layer="96"/>
</instance>
<instance part="R74" gate="G$1" x="-17.78" y="510.54" smashed="yes">
<attribute name="VALUE" x="-19.05" y="509.778" size="1.778" layer="96"/>
</instance>
<instance part="R75" gate="G$1" x="-17.78" y="508" smashed="yes">
<attribute name="VALUE" x="-19.05" y="507.238" size="1.778" layer="96"/>
</instance>
<instance part="R76" gate="G$1" x="-17.78" y="505.46" smashed="yes">
<attribute name="VALUE" x="-19.05" y="504.698" size="1.778" layer="96"/>
</instance>
<instance part="GND4" gate="1" x="-96.52" y="492.76"/>
<instance part="SEC_A" gate="G$1" x="190.5" y="398.78"/>
<instance part="SEC_B" gate="G$1" x="231.14" y="398.78"/>
<instance part="2003_SEC_A" gate="A" x="160.02" y="431.8"/>
<instance part="R77" gate="G$1" x="139.7" y="439.42" smashed="yes">
<attribute name="VALUE" x="138.43" y="438.658" size="1.778" layer="96"/>
</instance>
<instance part="R78" gate="G$1" x="139.7" y="436.88" smashed="yes">
<attribute name="VALUE" x="138.43" y="436.118" size="1.778" layer="96"/>
</instance>
<instance part="R79" gate="G$1" x="139.7" y="434.34" smashed="yes">
<attribute name="VALUE" x="138.43" y="433.578" size="1.778" layer="96"/>
</instance>
<instance part="R80" gate="G$1" x="139.7" y="431.8" smashed="yes">
<attribute name="VALUE" x="138.43" y="431.038" size="1.778" layer="96"/>
</instance>
<instance part="R81" gate="G$1" x="139.7" y="429.26" smashed="yes">
<attribute name="VALUE" x="138.43" y="428.498" size="1.778" layer="96"/>
</instance>
<instance part="R82" gate="G$1" x="139.7" y="426.72" smashed="yes">
<attribute name="VALUE" x="138.43" y="425.958" size="1.778" layer="96"/>
</instance>
<instance part="+3V7" gate="G$1" x="132.08" y="454.66"/>
<instance part="4543_SEC_A" gate="A" x="119.38" y="434.34"/>
<instance part="R83" gate="G$1" x="139.7" y="424.18" smashed="yes">
<attribute name="VALUE" x="138.43" y="423.418" size="1.778" layer="96"/>
</instance>
<instance part="GND5" gate="1" x="101.6" y="403.86"/>
<instance part="R84" gate="G$1" x="99.06" y="431.8" smashed="yes">
<attribute name="VALUE" x="97.79" y="431.038" size="1.778" layer="96"/>
</instance>
<instance part="R85" gate="G$1" x="99.06" y="434.34" smashed="yes">
<attribute name="VALUE" x="97.79" y="433.578" size="1.778" layer="96"/>
</instance>
<instance part="R86" gate="G$1" x="99.06" y="436.88" smashed="yes">
<attribute name="VALUE" x="97.79" y="436.118" size="1.778" layer="96"/>
</instance>
<instance part="R87" gate="G$1" x="99.06" y="439.42" smashed="yes">
<attribute name="VALUE" x="97.79" y="438.658" size="1.778" layer="96"/>
</instance>
<instance part="R88" gate="G$1" x="99.06" y="441.96" smashed="yes">
<attribute name="VALUE" x="97.79" y="441.198" size="1.778" layer="96"/>
</instance>
<instance part="R89" gate="G$1" x="180.34" y="439.42" smashed="yes">
<attribute name="VALUE" x="179.07" y="438.658" size="1.778" layer="96"/>
</instance>
<instance part="R90" gate="G$1" x="180.34" y="436.88" smashed="yes">
<attribute name="VALUE" x="179.07" y="436.118" size="1.778" layer="96"/>
</instance>
<instance part="R91" gate="G$1" x="180.34" y="434.34" smashed="yes">
<attribute name="VALUE" x="179.07" y="433.578" size="1.778" layer="96"/>
</instance>
<instance part="R92" gate="G$1" x="180.34" y="431.8" smashed="yes">
<attribute name="VALUE" x="179.07" y="431.038" size="1.778" layer="96"/>
</instance>
<instance part="R93" gate="G$1" x="180.34" y="429.26" smashed="yes">
<attribute name="VALUE" x="179.07" y="428.498" size="1.778" layer="96"/>
</instance>
<instance part="R94" gate="G$1" x="180.34" y="426.72" smashed="yes">
<attribute name="VALUE" x="179.07" y="425.958" size="1.778" layer="96"/>
</instance>
<instance part="R95" gate="G$1" x="180.34" y="424.18" smashed="yes">
<attribute name="VALUE" x="179.07" y="423.418" size="1.778" layer="96"/>
</instance>
<instance part="2003_SEC_B" gate="A" x="147.32" y="510.54"/>
<instance part="R96" gate="G$1" x="127" y="518.16" smashed="yes">
<attribute name="VALUE" x="125.73" y="517.398" size="1.778" layer="96"/>
</instance>
<instance part="R97" gate="G$1" x="127" y="515.62" smashed="yes">
<attribute name="VALUE" x="125.73" y="514.858" size="1.778" layer="96"/>
</instance>
<instance part="R98" gate="G$1" x="127" y="513.08" smashed="yes">
<attribute name="VALUE" x="125.73" y="512.318" size="1.778" layer="96"/>
</instance>
<instance part="R99" gate="G$1" x="127" y="510.54" smashed="yes">
<attribute name="VALUE" x="125.73" y="509.778" size="1.778" layer="96"/>
</instance>
<instance part="R100" gate="G$1" x="127" y="508" smashed="yes">
<attribute name="VALUE" x="125.73" y="507.238" size="1.778" layer="96"/>
</instance>
<instance part="R101" gate="G$1" x="127" y="505.46" smashed="yes">
<attribute name="VALUE" x="125.73" y="504.698" size="1.778" layer="96"/>
</instance>
<instance part="+3V8" gate="G$1" x="119.38" y="533.4"/>
<instance part="4543_SEC_B" gate="A" x="106.68" y="513.08"/>
<instance part="R102" gate="G$1" x="127" y="502.92" smashed="yes">
<attribute name="VALUE" x="125.73" y="502.158" size="1.778" layer="96"/>
</instance>
<instance part="R103" gate="G$1" x="86.36" y="510.54" smashed="yes">
<attribute name="VALUE" x="85.09" y="509.778" size="1.778" layer="96"/>
</instance>
<instance part="R104" gate="G$1" x="86.36" y="513.08" smashed="yes">
<attribute name="VALUE" x="85.09" y="512.318" size="1.778" layer="96"/>
</instance>
<instance part="R105" gate="G$1" x="86.36" y="515.62" smashed="yes">
<attribute name="VALUE" x="85.09" y="514.858" size="1.778" layer="96"/>
</instance>
<instance part="R106" gate="G$1" x="86.36" y="518.16" smashed="yes">
<attribute name="VALUE" x="85.09" y="517.398" size="1.778" layer="96"/>
</instance>
<instance part="R107" gate="G$1" x="86.36" y="520.7" smashed="yes">
<attribute name="VALUE" x="85.09" y="519.938" size="1.778" layer="96"/>
</instance>
<instance part="R108" gate="G$1" x="167.64" y="518.16" smashed="yes">
<attribute name="VALUE" x="166.37" y="517.398" size="1.778" layer="96"/>
</instance>
<instance part="R109" gate="G$1" x="167.64" y="515.62" smashed="yes">
<attribute name="VALUE" x="166.37" y="514.858" size="1.778" layer="96"/>
</instance>
<instance part="R110" gate="G$1" x="167.64" y="513.08" smashed="yes">
<attribute name="VALUE" x="166.37" y="512.318" size="1.778" layer="96"/>
</instance>
<instance part="R111" gate="G$1" x="167.64" y="510.54" smashed="yes">
<attribute name="VALUE" x="166.37" y="509.778" size="1.778" layer="96"/>
</instance>
<instance part="R112" gate="G$1" x="167.64" y="508" smashed="yes">
<attribute name="VALUE" x="166.37" y="507.238" size="1.778" layer="96"/>
</instance>
<instance part="R113" gate="G$1" x="167.64" y="505.46" smashed="yes">
<attribute name="VALUE" x="166.37" y="504.698" size="1.778" layer="96"/>
</instance>
<instance part="R114" gate="G$1" x="167.64" y="502.92" smashed="yes">
<attribute name="VALUE" x="166.37" y="502.158" size="1.778" layer="96"/>
</instance>
<instance part="GND6" gate="1" x="88.9" y="490.22"/>
<instance part="LED_COLON1" gate="G$1" x="-109.22" y="363.22"/>
<instance part="LED_COLON2" gate="G$1" x="-109.22" y="337.82"/>
<instance part="DEMUX_WEEKDAY" gate="A" x="241.3" y="78.74"/>
<instance part="R245" gate="G$1" x="256.54" y="83.82" smashed="yes">
<attribute name="VALUE" x="255.27" y="83.058" size="1.778" layer="96"/>
</instance>
<instance part="R246" gate="G$1" x="256.54" y="81.28" smashed="yes">
<attribute name="VALUE" x="255.27" y="80.518" size="1.778" layer="96"/>
</instance>
<instance part="R247" gate="G$1" x="256.54" y="78.74" smashed="yes">
<attribute name="VALUE" x="255.27" y="77.978" size="1.778" layer="96"/>
</instance>
<instance part="R248" gate="G$1" x="256.54" y="76.2" smashed="yes">
<attribute name="VALUE" x="255.27" y="75.438" size="1.778" layer="96"/>
</instance>
<instance part="R249" gate="G$1" x="256.54" y="73.66" smashed="yes">
<attribute name="VALUE" x="255.27" y="72.898" size="1.778" layer="96"/>
</instance>
<instance part="R250" gate="G$1" x="256.54" y="71.12" smashed="yes">
<attribute name="VALUE" x="255.27" y="70.358" size="1.778" layer="96"/>
</instance>
<instance part="R251" gate="G$1" x="256.54" y="68.58" smashed="yes">
<attribute name="VALUE" x="255.27" y="67.818" size="1.778" layer="96"/>
</instance>
<instance part="GND17" gate="1" x="259.08" y="30.48"/>
<instance part="+3V39" gate="G$1" x="251.46" y="101.6"/>
<instance part="GND18" gate="1" x="223.52" y="58.42"/>
<instance part="R252" gate="G$1" x="215.9" y="73.66" smashed="yes">
<attribute name="VALUE" x="214.63" y="72.898" size="1.778" layer="96"/>
</instance>
<instance part="R253" gate="G$1" x="-109.22" y="373.38" smashed="yes" rot="R90">
<attribute name="VALUE" x="-108.458" y="372.11" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="GND19" gate="1" x="-109.22" y="320.04"/>
<instance part="LED_COLON3" gate="G$1" x="78.74" y="358.14"/>
<instance part="LED_COLON4" gate="G$1" x="78.74" y="332.74"/>
<instance part="R254" gate="G$1" x="78.74" y="368.3" smashed="yes" rot="R90">
<attribute name="VALUE" x="79.502" y="367.03" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="GND20" gate="1" x="78.74" y="314.96"/>
<instance part="POWER_SWITCH" gate="1" x="208.28" y="5.08"/>
<instance part="POWER_SWITCH" gate="2" x="238.76" y="5.08"/>
<instance part="R255" gate="G$1" x="190.5" y="-12.7" smashed="yes">
<attribute name="VALUE" x="189.23" y="-13.462" size="1.778" layer="96"/>
</instance>
<instance part="GND21" gate="1" x="208.28" y="-30.48"/>
<instance part="P+9" gate="1" x="208.28" y="25.4"/>
<instance part="R256" gate="G$1" x="121.92" y="73.66" smashed="yes" rot="R90">
<attribute name="VALUE" x="122.682" y="72.39" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="R257" gate="G$1" x="116.84" y="73.66" smashed="yes" rot="R90">
<attribute name="VALUE" x="117.602" y="72.39" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="GND22" gate="1" x="134.62" y="27.94"/>
<instance part="GND23" gate="1" x="68.58" y="17.78"/>
<instance part="P+1" gate="1" x="383.54" y="-12.7"/>
<instance part="GND24" gate="1" x="383.54" y="-43.18"/>
<instance part="12V_POWER_OUT" gate="-1" x="340.36" y="-10.16"/>
<instance part="12V_POWER_OUT" gate="-2" x="340.36" y="-15.24"/>
<instance part="12_POWER_IN" gate="G$1" x="365.76" y="-27.94" rot="R180"/>
<instance part="4543_DAY_A" gate="A" x="-408.94" y="243.84"/>
<instance part="DAY_A" gate="G$1" x="-365.76" y="195.58"/>
<instance part="2003_DAY_A" gate="A" x="-368.3" y="241.3"/>
<instance part="R258" gate="G$1" x="-388.62" y="248.92" smashed="yes">
<attribute name="VALUE" x="-389.89" y="248.158" size="1.778" layer="96"/>
</instance>
<instance part="R259" gate="G$1" x="-388.62" y="246.38" smashed="yes">
<attribute name="VALUE" x="-389.89" y="245.618" size="1.778" layer="96"/>
</instance>
<instance part="R260" gate="G$1" x="-388.62" y="243.84" smashed="yes">
<attribute name="VALUE" x="-389.89" y="243.078" size="1.778" layer="96"/>
</instance>
<instance part="R261" gate="G$1" x="-388.62" y="241.3" smashed="yes">
<attribute name="VALUE" x="-389.89" y="240.538" size="1.778" layer="96"/>
</instance>
<instance part="R262" gate="G$1" x="-388.62" y="238.76" smashed="yes">
<attribute name="VALUE" x="-389.89" y="237.998" size="1.778" layer="96"/>
</instance>
<instance part="R263" gate="G$1" x="-388.62" y="236.22" smashed="yes">
<attribute name="VALUE" x="-389.89" y="235.458" size="1.778" layer="96"/>
</instance>
<instance part="R264" gate="G$1" x="-388.62" y="233.68" smashed="yes">
<attribute name="VALUE" x="-389.89" y="232.918" size="1.778" layer="96"/>
</instance>
<instance part="R265" gate="G$1" x="-426.72" y="251.46" smashed="yes">
<attribute name="VALUE" x="-427.99" y="250.698" size="1.778" layer="96"/>
</instance>
<instance part="R266" gate="G$1" x="-426.72" y="248.92" smashed="yes">
<attribute name="VALUE" x="-427.99" y="248.158" size="1.778" layer="96"/>
</instance>
<instance part="R267" gate="G$1" x="-426.72" y="246.38" smashed="yes">
<attribute name="VALUE" x="-427.99" y="245.618" size="1.778" layer="96"/>
</instance>
<instance part="R268" gate="G$1" x="-426.72" y="243.84" smashed="yes">
<attribute name="VALUE" x="-427.99" y="243.078" size="1.778" layer="96"/>
</instance>
<instance part="R269" gate="G$1" x="-426.72" y="241.3" smashed="yes">
<attribute name="VALUE" x="-427.99" y="240.538" size="1.778" layer="96"/>
</instance>
<instance part="GND25" gate="1" x="-424.18" y="223.52"/>
<instance part="R270" gate="G$1" x="-350.52" y="248.92" smashed="yes">
<attribute name="VALUE" x="-351.79" y="248.158" size="1.778" layer="96"/>
</instance>
<instance part="R271" gate="G$1" x="-350.52" y="246.38" smashed="yes">
<attribute name="VALUE" x="-351.79" y="245.618" size="1.778" layer="96"/>
</instance>
<instance part="R272" gate="G$1" x="-350.52" y="243.84" smashed="yes">
<attribute name="VALUE" x="-351.79" y="243.078" size="1.778" layer="96"/>
</instance>
<instance part="R273" gate="G$1" x="-350.52" y="241.3" smashed="yes">
<attribute name="VALUE" x="-351.79" y="240.538" size="1.778" layer="96"/>
</instance>
<instance part="R274" gate="G$1" x="-350.52" y="238.76" smashed="yes">
<attribute name="VALUE" x="-351.79" y="237.998" size="1.778" layer="96"/>
</instance>
<instance part="R275" gate="G$1" x="-350.52" y="236.22" smashed="yes">
<attribute name="VALUE" x="-351.79" y="235.458" size="1.778" layer="96"/>
</instance>
<instance part="R276" gate="G$1" x="-350.52" y="233.68" smashed="yes">
<attribute name="VALUE" x="-351.79" y="232.918" size="1.778" layer="96"/>
</instance>
<instance part="+3V40" gate="G$1" x="-396.24" y="266.7"/>
<instance part="4543_DAY_B" gate="A" x="-284.48" y="243.84"/>
<instance part="DAY_B" gate="G$1" x="-241.3" y="195.58"/>
<instance part="2003_DAY_B" gate="A" x="-243.84" y="241.3"/>
<instance part="R115" gate="G$1" x="-264.16" y="248.92" smashed="yes">
<attribute name="VALUE" x="-265.43" y="248.158" size="1.778" layer="96"/>
</instance>
<instance part="R116" gate="G$1" x="-264.16" y="246.38" smashed="yes">
<attribute name="VALUE" x="-265.43" y="245.618" size="1.778" layer="96"/>
</instance>
<instance part="R117" gate="G$1" x="-264.16" y="243.84" smashed="yes">
<attribute name="VALUE" x="-265.43" y="243.078" size="1.778" layer="96"/>
</instance>
<instance part="R118" gate="G$1" x="-264.16" y="241.3" smashed="yes">
<attribute name="VALUE" x="-265.43" y="240.538" size="1.778" layer="96"/>
</instance>
<instance part="R119" gate="G$1" x="-264.16" y="238.76" smashed="yes">
<attribute name="VALUE" x="-265.43" y="237.998" size="1.778" layer="96"/>
</instance>
<instance part="R120" gate="G$1" x="-264.16" y="236.22" smashed="yes">
<attribute name="VALUE" x="-265.43" y="235.458" size="1.778" layer="96"/>
</instance>
<instance part="R121" gate="G$1" x="-264.16" y="233.68" smashed="yes">
<attribute name="VALUE" x="-265.43" y="232.918" size="1.778" layer="96"/>
</instance>
<instance part="R122" gate="G$1" x="-302.26" y="251.46" smashed="yes">
<attribute name="VALUE" x="-303.53" y="250.698" size="1.778" layer="96"/>
</instance>
<instance part="R123" gate="G$1" x="-302.26" y="248.92" smashed="yes">
<attribute name="VALUE" x="-303.53" y="248.158" size="1.778" layer="96"/>
</instance>
<instance part="R124" gate="G$1" x="-302.26" y="246.38" smashed="yes">
<attribute name="VALUE" x="-303.53" y="245.618" size="1.778" layer="96"/>
</instance>
<instance part="R125" gate="G$1" x="-302.26" y="243.84" smashed="yes">
<attribute name="VALUE" x="-303.53" y="243.078" size="1.778" layer="96"/>
</instance>
<instance part="R126" gate="G$1" x="-302.26" y="241.3" smashed="yes">
<attribute name="VALUE" x="-303.53" y="240.538" size="1.778" layer="96"/>
</instance>
<instance part="GND7" gate="1" x="-299.72" y="223.52"/>
<instance part="R127" gate="G$1" x="-226.06" y="248.92" smashed="yes">
<attribute name="VALUE" x="-227.33" y="248.158" size="1.778" layer="96"/>
</instance>
<instance part="R128" gate="G$1" x="-226.06" y="246.38" smashed="yes">
<attribute name="VALUE" x="-227.33" y="245.618" size="1.778" layer="96"/>
</instance>
<instance part="R129" gate="G$1" x="-226.06" y="243.84" smashed="yes">
<attribute name="VALUE" x="-227.33" y="243.078" size="1.778" layer="96"/>
</instance>
<instance part="R130" gate="G$1" x="-226.06" y="241.3" smashed="yes">
<attribute name="VALUE" x="-227.33" y="240.538" size="1.778" layer="96"/>
</instance>
<instance part="R131" gate="G$1" x="-226.06" y="238.76" smashed="yes">
<attribute name="VALUE" x="-227.33" y="237.998" size="1.778" layer="96"/>
</instance>
<instance part="R132" gate="G$1" x="-226.06" y="236.22" smashed="yes">
<attribute name="VALUE" x="-227.33" y="235.458" size="1.778" layer="96"/>
</instance>
<instance part="R133" gate="G$1" x="-226.06" y="233.68" smashed="yes">
<attribute name="VALUE" x="-227.33" y="232.918" size="1.778" layer="96"/>
</instance>
<instance part="+3V9" gate="G$1" x="-271.78" y="266.7"/>
<instance part="4543_MONTH_A" gate="A" x="-152.4" y="251.46"/>
<instance part="MONTH_A" gate="G$1" x="-109.22" y="203.2"/>
<instance part="2003_MONTH_A" gate="A" x="-111.76" y="248.92"/>
<instance part="R134" gate="G$1" x="-132.08" y="256.54" smashed="yes">
<attribute name="VALUE" x="-133.35" y="255.778" size="1.778" layer="96"/>
</instance>
<instance part="R135" gate="G$1" x="-132.08" y="254" smashed="yes">
<attribute name="VALUE" x="-133.35" y="253.238" size="1.778" layer="96"/>
</instance>
<instance part="R136" gate="G$1" x="-132.08" y="251.46" smashed="yes">
<attribute name="VALUE" x="-133.35" y="250.698" size="1.778" layer="96"/>
</instance>
<instance part="R137" gate="G$1" x="-132.08" y="248.92" smashed="yes">
<attribute name="VALUE" x="-133.35" y="248.158" size="1.778" layer="96"/>
</instance>
<instance part="R138" gate="G$1" x="-132.08" y="246.38" smashed="yes">
<attribute name="VALUE" x="-133.35" y="245.618" size="1.778" layer="96"/>
</instance>
<instance part="R139" gate="G$1" x="-132.08" y="243.84" smashed="yes">
<attribute name="VALUE" x="-133.35" y="243.078" size="1.778" layer="96"/>
</instance>
<instance part="R140" gate="G$1" x="-132.08" y="241.3" smashed="yes">
<attribute name="VALUE" x="-133.35" y="240.538" size="1.778" layer="96"/>
</instance>
<instance part="R141" gate="G$1" x="-170.18" y="259.08" smashed="yes">
<attribute name="VALUE" x="-171.45" y="258.318" size="1.778" layer="96"/>
</instance>
<instance part="R142" gate="G$1" x="-170.18" y="256.54" smashed="yes">
<attribute name="VALUE" x="-171.45" y="255.778" size="1.778" layer="96"/>
</instance>
<instance part="R143" gate="G$1" x="-170.18" y="254" smashed="yes">
<attribute name="VALUE" x="-171.45" y="253.238" size="1.778" layer="96"/>
</instance>
<instance part="R144" gate="G$1" x="-170.18" y="251.46" smashed="yes">
<attribute name="VALUE" x="-171.45" y="250.698" size="1.778" layer="96"/>
</instance>
<instance part="R145" gate="G$1" x="-170.18" y="248.92" smashed="yes">
<attribute name="VALUE" x="-171.45" y="248.158" size="1.778" layer="96"/>
</instance>
<instance part="GND8" gate="1" x="-167.64" y="231.14"/>
<instance part="R146" gate="G$1" x="-93.98" y="256.54" smashed="yes">
<attribute name="VALUE" x="-95.25" y="255.778" size="1.778" layer="96"/>
</instance>
<instance part="R147" gate="G$1" x="-93.98" y="254" smashed="yes">
<attribute name="VALUE" x="-95.25" y="253.238" size="1.778" layer="96"/>
</instance>
<instance part="R148" gate="G$1" x="-93.98" y="251.46" smashed="yes">
<attribute name="VALUE" x="-95.25" y="250.698" size="1.778" layer="96"/>
</instance>
<instance part="R149" gate="G$1" x="-93.98" y="248.92" smashed="yes">
<attribute name="VALUE" x="-95.25" y="248.158" size="1.778" layer="96"/>
</instance>
<instance part="R150" gate="G$1" x="-93.98" y="246.38" smashed="yes">
<attribute name="VALUE" x="-95.25" y="245.618" size="1.778" layer="96"/>
</instance>
<instance part="R151" gate="G$1" x="-93.98" y="243.84" smashed="yes">
<attribute name="VALUE" x="-95.25" y="243.078" size="1.778" layer="96"/>
</instance>
<instance part="R152" gate="G$1" x="-93.98" y="241.3" smashed="yes">
<attribute name="VALUE" x="-95.25" y="240.538" size="1.778" layer="96"/>
</instance>
<instance part="+3V10" gate="G$1" x="-139.7" y="274.32"/>
<instance part="4543_MONTH_B" gate="A" x="-35.56" y="251.46"/>
<instance part="MONTH_B" gate="G$1" x="7.62" y="203.2"/>
<instance part="2003_MONTH_B" gate="A" x="5.08" y="248.92"/>
<instance part="R153" gate="G$1" x="-15.24" y="256.54" smashed="yes">
<attribute name="VALUE" x="-16.51" y="255.778" size="1.778" layer="96"/>
</instance>
<instance part="R154" gate="G$1" x="-15.24" y="254" smashed="yes">
<attribute name="VALUE" x="-16.51" y="253.238" size="1.778" layer="96"/>
</instance>
<instance part="R155" gate="G$1" x="-15.24" y="251.46" smashed="yes">
<attribute name="VALUE" x="-16.51" y="250.698" size="1.778" layer="96"/>
</instance>
<instance part="R156" gate="G$1" x="-15.24" y="248.92" smashed="yes">
<attribute name="VALUE" x="-16.51" y="248.158" size="1.778" layer="96"/>
</instance>
<instance part="R157" gate="G$1" x="-15.24" y="246.38" smashed="yes">
<attribute name="VALUE" x="-16.51" y="245.618" size="1.778" layer="96"/>
</instance>
<instance part="R158" gate="G$1" x="-15.24" y="243.84" smashed="yes">
<attribute name="VALUE" x="-16.51" y="243.078" size="1.778" layer="96"/>
</instance>
<instance part="R159" gate="G$1" x="-15.24" y="241.3" smashed="yes">
<attribute name="VALUE" x="-16.51" y="240.538" size="1.778" layer="96"/>
</instance>
<instance part="R160" gate="G$1" x="-53.34" y="259.08" smashed="yes">
<attribute name="VALUE" x="-54.61" y="258.318" size="1.778" layer="96"/>
</instance>
<instance part="R161" gate="G$1" x="-53.34" y="256.54" smashed="yes">
<attribute name="VALUE" x="-54.61" y="255.778" size="1.778" layer="96"/>
</instance>
<instance part="R162" gate="G$1" x="-53.34" y="254" smashed="yes">
<attribute name="VALUE" x="-54.61" y="253.238" size="1.778" layer="96"/>
</instance>
<instance part="R163" gate="G$1" x="-53.34" y="251.46" smashed="yes">
<attribute name="VALUE" x="-54.61" y="250.698" size="1.778" layer="96"/>
</instance>
<instance part="R164" gate="G$1" x="-53.34" y="248.92" smashed="yes">
<attribute name="VALUE" x="-54.61" y="248.158" size="1.778" layer="96"/>
</instance>
<instance part="GND9" gate="1" x="-50.8" y="231.14"/>
<instance part="R165" gate="G$1" x="22.86" y="256.54" smashed="yes">
<attribute name="VALUE" x="21.59" y="255.778" size="1.778" layer="96"/>
</instance>
<instance part="R166" gate="G$1" x="22.86" y="254" smashed="yes">
<attribute name="VALUE" x="21.59" y="253.238" size="1.778" layer="96"/>
</instance>
<instance part="R277" gate="G$1" x="22.86" y="251.46" smashed="yes">
<attribute name="VALUE" x="21.59" y="250.698" size="1.778" layer="96"/>
</instance>
<instance part="R278" gate="G$1" x="22.86" y="248.92" smashed="yes">
<attribute name="VALUE" x="21.59" y="248.158" size="1.778" layer="96"/>
</instance>
<instance part="R279" gate="G$1" x="22.86" y="246.38" smashed="yes">
<attribute name="VALUE" x="21.59" y="245.618" size="1.778" layer="96"/>
</instance>
<instance part="R280" gate="G$1" x="22.86" y="243.84" smashed="yes">
<attribute name="VALUE" x="21.59" y="243.078" size="1.778" layer="96"/>
</instance>
<instance part="R281" gate="G$1" x="22.86" y="241.3" smashed="yes">
<attribute name="VALUE" x="21.59" y="240.538" size="1.778" layer="96"/>
</instance>
<instance part="+3V11" gate="G$1" x="-22.86" y="274.32"/>
<instance part="4543_YEAR_A" gate="A" x="137.16" y="261.62"/>
<instance part="YEAR_A" gate="G$1" x="180.34" y="213.36"/>
<instance part="2003_YEAR_A" gate="A" x="177.8" y="259.08"/>
<instance part="R282" gate="G$1" x="157.48" y="266.7" smashed="yes">
<attribute name="VALUE" x="156.21" y="265.938" size="1.778" layer="96"/>
</instance>
<instance part="R283" gate="G$1" x="157.48" y="264.16" smashed="yes">
<attribute name="VALUE" x="156.21" y="263.398" size="1.778" layer="96"/>
</instance>
<instance part="R284" gate="G$1" x="157.48" y="261.62" smashed="yes">
<attribute name="VALUE" x="156.21" y="260.858" size="1.778" layer="96"/>
</instance>
<instance part="R285" gate="G$1" x="157.48" y="259.08" smashed="yes">
<attribute name="VALUE" x="156.21" y="258.318" size="1.778" layer="96"/>
</instance>
<instance part="R286" gate="G$1" x="157.48" y="256.54" smashed="yes">
<attribute name="VALUE" x="156.21" y="255.778" size="1.778" layer="96"/>
</instance>
<instance part="R287" gate="G$1" x="157.48" y="254" smashed="yes">
<attribute name="VALUE" x="156.21" y="253.238" size="1.778" layer="96"/>
</instance>
<instance part="R288" gate="G$1" x="157.48" y="251.46" smashed="yes">
<attribute name="VALUE" x="156.21" y="250.698" size="1.778" layer="96"/>
</instance>
<instance part="R289" gate="G$1" x="119.38" y="269.24" smashed="yes">
<attribute name="VALUE" x="118.11" y="268.478" size="1.778" layer="96"/>
</instance>
<instance part="R290" gate="G$1" x="119.38" y="266.7" smashed="yes">
<attribute name="VALUE" x="118.11" y="265.938" size="1.778" layer="96"/>
</instance>
<instance part="R291" gate="G$1" x="119.38" y="264.16" smashed="yes">
<attribute name="VALUE" x="118.11" y="263.398" size="1.778" layer="96"/>
</instance>
<instance part="R292" gate="G$1" x="119.38" y="261.62" smashed="yes">
<attribute name="VALUE" x="118.11" y="260.858" size="1.778" layer="96"/>
</instance>
<instance part="R293" gate="G$1" x="119.38" y="259.08" smashed="yes">
<attribute name="VALUE" x="118.11" y="258.318" size="1.778" layer="96"/>
</instance>
<instance part="GND10" gate="1" x="121.92" y="241.3"/>
<instance part="R294" gate="G$1" x="195.58" y="266.7" smashed="yes">
<attribute name="VALUE" x="194.31" y="265.938" size="1.778" layer="96"/>
</instance>
<instance part="R295" gate="G$1" x="195.58" y="264.16" smashed="yes">
<attribute name="VALUE" x="194.31" y="263.398" size="1.778" layer="96"/>
</instance>
<instance part="R296" gate="G$1" x="195.58" y="261.62" smashed="yes">
<attribute name="VALUE" x="194.31" y="260.858" size="1.778" layer="96"/>
</instance>
<instance part="R297" gate="G$1" x="195.58" y="259.08" smashed="yes">
<attribute name="VALUE" x="194.31" y="258.318" size="1.778" layer="96"/>
</instance>
<instance part="R298" gate="G$1" x="195.58" y="256.54" smashed="yes">
<attribute name="VALUE" x="194.31" y="255.778" size="1.778" layer="96"/>
</instance>
<instance part="R299" gate="G$1" x="195.58" y="254" smashed="yes">
<attribute name="VALUE" x="194.31" y="253.238" size="1.778" layer="96"/>
</instance>
<instance part="R300" gate="G$1" x="195.58" y="251.46" smashed="yes">
<attribute name="VALUE" x="194.31" y="250.698" size="1.778" layer="96"/>
</instance>
<instance part="+3V12" gate="G$1" x="149.86" y="284.48"/>
<instance part="4543_YEAR_B" gate="A" x="264.16" y="261.62"/>
<instance part="YEAR_B" gate="G$1" x="307.34" y="213.36"/>
<instance part="2003_YEAR_B" gate="A" x="304.8" y="259.08"/>
<instance part="R167" gate="G$1" x="284.48" y="266.7" smashed="yes">
<attribute name="VALUE" x="283.21" y="265.938" size="1.778" layer="96"/>
</instance>
<instance part="R168" gate="G$1" x="284.48" y="264.16" smashed="yes">
<attribute name="VALUE" x="283.21" y="263.398" size="1.778" layer="96"/>
</instance>
<instance part="R169" gate="G$1" x="284.48" y="261.62" smashed="yes">
<attribute name="VALUE" x="283.21" y="260.858" size="1.778" layer="96"/>
</instance>
<instance part="R170" gate="G$1" x="284.48" y="259.08" smashed="yes">
<attribute name="VALUE" x="283.21" y="258.318" size="1.778" layer="96"/>
</instance>
<instance part="R171" gate="G$1" x="284.48" y="256.54" smashed="yes">
<attribute name="VALUE" x="283.21" y="255.778" size="1.778" layer="96"/>
</instance>
<instance part="R172" gate="G$1" x="284.48" y="254" smashed="yes">
<attribute name="VALUE" x="283.21" y="253.238" size="1.778" layer="96"/>
</instance>
<instance part="R173" gate="G$1" x="284.48" y="251.46" smashed="yes">
<attribute name="VALUE" x="283.21" y="250.698" size="1.778" layer="96"/>
</instance>
<instance part="R174" gate="G$1" x="246.38" y="269.24" smashed="yes">
<attribute name="VALUE" x="245.11" y="268.478" size="1.778" layer="96"/>
</instance>
<instance part="R175" gate="G$1" x="246.38" y="266.7" smashed="yes">
<attribute name="VALUE" x="245.11" y="265.938" size="1.778" layer="96"/>
</instance>
<instance part="R176" gate="G$1" x="246.38" y="264.16" smashed="yes">
<attribute name="VALUE" x="245.11" y="263.398" size="1.778" layer="96"/>
</instance>
<instance part="R177" gate="G$1" x="246.38" y="261.62" smashed="yes">
<attribute name="VALUE" x="245.11" y="260.858" size="1.778" layer="96"/>
</instance>
<instance part="R178" gate="G$1" x="246.38" y="259.08" smashed="yes">
<attribute name="VALUE" x="245.11" y="258.318" size="1.778" layer="96"/>
</instance>
<instance part="GND11" gate="1" x="248.92" y="241.3"/>
<instance part="R179" gate="G$1" x="322.58" y="266.7" smashed="yes">
<attribute name="VALUE" x="321.31" y="265.938" size="1.778" layer="96"/>
</instance>
<instance part="R180" gate="G$1" x="322.58" y="264.16" smashed="yes">
<attribute name="VALUE" x="321.31" y="263.398" size="1.778" layer="96"/>
</instance>
<instance part="R181" gate="G$1" x="322.58" y="261.62" smashed="yes">
<attribute name="VALUE" x="321.31" y="260.858" size="1.778" layer="96"/>
</instance>
<instance part="R182" gate="G$1" x="322.58" y="259.08" smashed="yes">
<attribute name="VALUE" x="321.31" y="258.318" size="1.778" layer="96"/>
</instance>
<instance part="R183" gate="G$1" x="322.58" y="256.54" smashed="yes">
<attribute name="VALUE" x="321.31" y="255.778" size="1.778" layer="96"/>
</instance>
<instance part="R184" gate="G$1" x="322.58" y="254" smashed="yes">
<attribute name="VALUE" x="321.31" y="253.238" size="1.778" layer="96"/>
</instance>
<instance part="R185" gate="G$1" x="322.58" y="251.46" smashed="yes">
<attribute name="VALUE" x="321.31" y="250.698" size="1.778" layer="96"/>
</instance>
<instance part="+3V13" gate="G$1" x="276.86" y="284.48"/>
<instance part="4543_YEAR_C" gate="A" x="388.62" y="261.62"/>
<instance part="YEAR_C" gate="G$1" x="431.8" y="213.36"/>
<instance part="2003_YEAR_C" gate="A" x="429.26" y="259.08"/>
<instance part="R186" gate="G$1" x="408.94" y="266.7" smashed="yes">
<attribute name="VALUE" x="407.67" y="265.938" size="1.778" layer="96"/>
</instance>
<instance part="R187" gate="G$1" x="408.94" y="264.16" smashed="yes">
<attribute name="VALUE" x="407.67" y="263.398" size="1.778" layer="96"/>
</instance>
<instance part="R188" gate="G$1" x="408.94" y="261.62" smashed="yes">
<attribute name="VALUE" x="407.67" y="260.858" size="1.778" layer="96"/>
</instance>
<instance part="R189" gate="G$1" x="408.94" y="259.08" smashed="yes">
<attribute name="VALUE" x="407.67" y="258.318" size="1.778" layer="96"/>
</instance>
<instance part="R190" gate="G$1" x="408.94" y="256.54" smashed="yes">
<attribute name="VALUE" x="407.67" y="255.778" size="1.778" layer="96"/>
</instance>
<instance part="R191" gate="G$1" x="408.94" y="254" smashed="yes">
<attribute name="VALUE" x="407.67" y="253.238" size="1.778" layer="96"/>
</instance>
<instance part="R192" gate="G$1" x="408.94" y="251.46" smashed="yes">
<attribute name="VALUE" x="407.67" y="250.698" size="1.778" layer="96"/>
</instance>
<instance part="R193" gate="G$1" x="370.84" y="269.24" smashed="yes">
<attribute name="VALUE" x="369.57" y="268.478" size="1.778" layer="96"/>
</instance>
<instance part="R194" gate="G$1" x="370.84" y="266.7" smashed="yes">
<attribute name="VALUE" x="369.57" y="265.938" size="1.778" layer="96"/>
</instance>
<instance part="R195" gate="G$1" x="370.84" y="264.16" smashed="yes">
<attribute name="VALUE" x="369.57" y="263.398" size="1.778" layer="96"/>
</instance>
<instance part="R196" gate="G$1" x="370.84" y="261.62" smashed="yes">
<attribute name="VALUE" x="369.57" y="260.858" size="1.778" layer="96"/>
</instance>
<instance part="R197" gate="G$1" x="370.84" y="259.08" smashed="yes">
<attribute name="VALUE" x="369.57" y="258.318" size="1.778" layer="96"/>
</instance>
<instance part="GND12" gate="1" x="373.38" y="241.3"/>
<instance part="R198" gate="G$1" x="447.04" y="266.7" smashed="yes">
<attribute name="VALUE" x="445.77" y="265.938" size="1.778" layer="96"/>
</instance>
<instance part="R199" gate="G$1" x="447.04" y="264.16" smashed="yes">
<attribute name="VALUE" x="445.77" y="263.398" size="1.778" layer="96"/>
</instance>
<instance part="R200" gate="G$1" x="447.04" y="261.62" smashed="yes">
<attribute name="VALUE" x="445.77" y="260.858" size="1.778" layer="96"/>
</instance>
<instance part="R201" gate="G$1" x="447.04" y="259.08" smashed="yes">
<attribute name="VALUE" x="445.77" y="258.318" size="1.778" layer="96"/>
</instance>
<instance part="R202" gate="G$1" x="447.04" y="256.54" smashed="yes">
<attribute name="VALUE" x="445.77" y="255.778" size="1.778" layer="96"/>
</instance>
<instance part="R203" gate="G$1" x="447.04" y="254" smashed="yes">
<attribute name="VALUE" x="445.77" y="253.238" size="1.778" layer="96"/>
</instance>
<instance part="R204" gate="G$1" x="447.04" y="251.46" smashed="yes">
<attribute name="VALUE" x="445.77" y="250.698" size="1.778" layer="96"/>
</instance>
<instance part="+3V14" gate="G$1" x="401.32" y="284.48"/>
<instance part="4543_YEAR_D" gate="A" x="513.08" y="261.62"/>
<instance part="YEAR_D" gate="G$1" x="556.26" y="213.36"/>
<instance part="2003_YEAR_D" gate="A" x="553.72" y="259.08"/>
<instance part="R205" gate="G$1" x="533.4" y="266.7" smashed="yes">
<attribute name="VALUE" x="532.13" y="265.938" size="1.778" layer="96"/>
</instance>
<instance part="R206" gate="G$1" x="533.4" y="264.16" smashed="yes">
<attribute name="VALUE" x="532.13" y="263.398" size="1.778" layer="96"/>
</instance>
<instance part="R207" gate="G$1" x="533.4" y="261.62" smashed="yes">
<attribute name="VALUE" x="532.13" y="260.858" size="1.778" layer="96"/>
</instance>
<instance part="R208" gate="G$1" x="533.4" y="259.08" smashed="yes">
<attribute name="VALUE" x="532.13" y="258.318" size="1.778" layer="96"/>
</instance>
<instance part="R209" gate="G$1" x="533.4" y="256.54" smashed="yes">
<attribute name="VALUE" x="532.13" y="255.778" size="1.778" layer="96"/>
</instance>
<instance part="R210" gate="G$1" x="533.4" y="254" smashed="yes">
<attribute name="VALUE" x="532.13" y="253.238" size="1.778" layer="96"/>
</instance>
<instance part="R211" gate="G$1" x="533.4" y="251.46" smashed="yes">
<attribute name="VALUE" x="532.13" y="250.698" size="1.778" layer="96"/>
</instance>
<instance part="R212" gate="G$1" x="495.3" y="269.24" smashed="yes">
<attribute name="VALUE" x="494.03" y="268.478" size="1.778" layer="96"/>
</instance>
<instance part="R213" gate="G$1" x="495.3" y="266.7" smashed="yes">
<attribute name="VALUE" x="494.03" y="265.938" size="1.778" layer="96"/>
</instance>
<instance part="R214" gate="G$1" x="495.3" y="264.16" smashed="yes">
<attribute name="VALUE" x="494.03" y="263.398" size="1.778" layer="96"/>
</instance>
<instance part="R215" gate="G$1" x="495.3" y="261.62" smashed="yes">
<attribute name="VALUE" x="494.03" y="260.858" size="1.778" layer="96"/>
</instance>
<instance part="R216" gate="G$1" x="495.3" y="259.08" smashed="yes">
<attribute name="VALUE" x="494.03" y="258.318" size="1.778" layer="96"/>
</instance>
<instance part="GND13" gate="1" x="497.84" y="241.3"/>
<instance part="R217" gate="G$1" x="571.5" y="266.7" smashed="yes">
<attribute name="VALUE" x="570.23" y="265.938" size="1.778" layer="96"/>
</instance>
<instance part="R218" gate="G$1" x="571.5" y="264.16" smashed="yes">
<attribute name="VALUE" x="570.23" y="263.398" size="1.778" layer="96"/>
</instance>
<instance part="R301" gate="G$1" x="571.5" y="261.62" smashed="yes">
<attribute name="VALUE" x="570.23" y="260.858" size="1.778" layer="96"/>
</instance>
<instance part="R302" gate="G$1" x="571.5" y="259.08" smashed="yes">
<attribute name="VALUE" x="570.23" y="258.318" size="1.778" layer="96"/>
</instance>
<instance part="R303" gate="G$1" x="571.5" y="256.54" smashed="yes">
<attribute name="VALUE" x="570.23" y="255.778" size="1.778" layer="96"/>
</instance>
<instance part="R304" gate="G$1" x="571.5" y="254" smashed="yes">
<attribute name="VALUE" x="570.23" y="253.238" size="1.778" layer="96"/>
</instance>
<instance part="R305" gate="G$1" x="571.5" y="251.46" smashed="yes">
<attribute name="VALUE" x="570.23" y="250.698" size="1.778" layer="96"/>
</instance>
<instance part="+3V15" gate="G$1" x="525.78" y="284.48"/>
<instance part="4543_TEMP_A" gate="A" x="701.04" y="259.08"/>
<instance part="TEMP_A" gate="G$1" x="744.22" y="210.82"/>
<instance part="2003_TEMP_A" gate="A" x="741.68" y="256.54"/>
<instance part="R306" gate="G$1" x="721.36" y="264.16" smashed="yes">
<attribute name="VALUE" x="720.09" y="263.398" size="1.778" layer="96"/>
</instance>
<instance part="R307" gate="G$1" x="721.36" y="261.62" smashed="yes">
<attribute name="VALUE" x="720.09" y="260.858" size="1.778" layer="96"/>
</instance>
<instance part="R308" gate="G$1" x="721.36" y="259.08" smashed="yes">
<attribute name="VALUE" x="720.09" y="258.318" size="1.778" layer="96"/>
</instance>
<instance part="R309" gate="G$1" x="721.36" y="256.54" smashed="yes">
<attribute name="VALUE" x="720.09" y="255.778" size="1.778" layer="96"/>
</instance>
<instance part="R310" gate="G$1" x="721.36" y="254" smashed="yes">
<attribute name="VALUE" x="720.09" y="253.238" size="1.778" layer="96"/>
</instance>
<instance part="R311" gate="G$1" x="721.36" y="251.46" smashed="yes">
<attribute name="VALUE" x="720.09" y="250.698" size="1.778" layer="96"/>
</instance>
<instance part="R312" gate="G$1" x="721.36" y="248.92" smashed="yes">
<attribute name="VALUE" x="720.09" y="248.158" size="1.778" layer="96"/>
</instance>
<instance part="R313" gate="G$1" x="683.26" y="266.7" smashed="yes">
<attribute name="VALUE" x="681.99" y="265.938" size="1.778" layer="96"/>
</instance>
<instance part="R314" gate="G$1" x="683.26" y="264.16" smashed="yes">
<attribute name="VALUE" x="681.99" y="263.398" size="1.778" layer="96"/>
</instance>
<instance part="R315" gate="G$1" x="683.26" y="261.62" smashed="yes">
<attribute name="VALUE" x="681.99" y="260.858" size="1.778" layer="96"/>
</instance>
<instance part="R316" gate="G$1" x="683.26" y="259.08" smashed="yes">
<attribute name="VALUE" x="681.99" y="258.318" size="1.778" layer="96"/>
</instance>
<instance part="R317" gate="G$1" x="683.26" y="256.54" smashed="yes">
<attribute name="VALUE" x="681.99" y="255.778" size="1.778" layer="96"/>
</instance>
<instance part="GND14" gate="1" x="685.8" y="238.76"/>
<instance part="R318" gate="G$1" x="759.46" y="264.16" smashed="yes">
<attribute name="VALUE" x="758.19" y="263.398" size="1.778" layer="96"/>
</instance>
<instance part="R319" gate="G$1" x="759.46" y="261.62" smashed="yes">
<attribute name="VALUE" x="758.19" y="260.858" size="1.778" layer="96"/>
</instance>
<instance part="R320" gate="G$1" x="759.46" y="259.08" smashed="yes">
<attribute name="VALUE" x="758.19" y="258.318" size="1.778" layer="96"/>
</instance>
<instance part="R321" gate="G$1" x="759.46" y="256.54" smashed="yes">
<attribute name="VALUE" x="758.19" y="255.778" size="1.778" layer="96"/>
</instance>
<instance part="R322" gate="G$1" x="759.46" y="254" smashed="yes">
<attribute name="VALUE" x="758.19" y="253.238" size="1.778" layer="96"/>
</instance>
<instance part="R323" gate="G$1" x="759.46" y="251.46" smashed="yes">
<attribute name="VALUE" x="758.19" y="250.698" size="1.778" layer="96"/>
</instance>
<instance part="R324" gate="G$1" x="759.46" y="248.92" smashed="yes">
<attribute name="VALUE" x="758.19" y="248.158" size="1.778" layer="96"/>
</instance>
<instance part="+3V16" gate="G$1" x="713.74" y="281.94"/>
<instance part="4543_TEMP_B" gate="A" x="833.12" y="256.54"/>
<instance part="TEMP_B" gate="G$1" x="876.3" y="208.28"/>
<instance part="2003_TEMP_B" gate="A" x="873.76" y="254"/>
<instance part="R219" gate="G$1" x="853.44" y="261.62" smashed="yes">
<attribute name="VALUE" x="852.17" y="260.858" size="1.778" layer="96"/>
</instance>
<instance part="R220" gate="G$1" x="853.44" y="259.08" smashed="yes">
<attribute name="VALUE" x="852.17" y="258.318" size="1.778" layer="96"/>
</instance>
<instance part="R221" gate="G$1" x="853.44" y="256.54" smashed="yes">
<attribute name="VALUE" x="852.17" y="255.778" size="1.778" layer="96"/>
</instance>
<instance part="R222" gate="G$1" x="853.44" y="254" smashed="yes">
<attribute name="VALUE" x="852.17" y="253.238" size="1.778" layer="96"/>
</instance>
<instance part="R223" gate="G$1" x="853.44" y="251.46" smashed="yes">
<attribute name="VALUE" x="852.17" y="250.698" size="1.778" layer="96"/>
</instance>
<instance part="R224" gate="G$1" x="853.44" y="248.92" smashed="yes">
<attribute name="VALUE" x="852.17" y="248.158" size="1.778" layer="96"/>
</instance>
<instance part="R225" gate="G$1" x="853.44" y="246.38" smashed="yes">
<attribute name="VALUE" x="852.17" y="245.618" size="1.778" layer="96"/>
</instance>
<instance part="R226" gate="G$1" x="815.34" y="264.16" smashed="yes">
<attribute name="VALUE" x="814.07" y="263.398" size="1.778" layer="96"/>
</instance>
<instance part="R227" gate="G$1" x="815.34" y="261.62" smashed="yes">
<attribute name="VALUE" x="814.07" y="260.858" size="1.778" layer="96"/>
</instance>
<instance part="R228" gate="G$1" x="815.34" y="259.08" smashed="yes">
<attribute name="VALUE" x="814.07" y="258.318" size="1.778" layer="96"/>
</instance>
<instance part="R229" gate="G$1" x="815.34" y="256.54" smashed="yes">
<attribute name="VALUE" x="814.07" y="255.778" size="1.778" layer="96"/>
</instance>
<instance part="R230" gate="G$1" x="815.34" y="254" smashed="yes">
<attribute name="VALUE" x="814.07" y="253.238" size="1.778" layer="96"/>
</instance>
<instance part="GND15" gate="1" x="817.88" y="236.22"/>
<instance part="R231" gate="G$1" x="891.54" y="261.62" smashed="yes">
<attribute name="VALUE" x="890.27" y="260.858" size="1.778" layer="96"/>
</instance>
<instance part="R325" gate="G$1" x="891.54" y="259.08" smashed="yes">
<attribute name="VALUE" x="890.27" y="258.318" size="1.778" layer="96"/>
</instance>
<instance part="R326" gate="G$1" x="891.54" y="256.54" smashed="yes">
<attribute name="VALUE" x="890.27" y="255.778" size="1.778" layer="96"/>
</instance>
<instance part="R327" gate="G$1" x="891.54" y="254" smashed="yes">
<attribute name="VALUE" x="890.27" y="253.238" size="1.778" layer="96"/>
</instance>
<instance part="R328" gate="G$1" x="891.54" y="251.46" smashed="yes">
<attribute name="VALUE" x="890.27" y="250.698" size="1.778" layer="96"/>
</instance>
<instance part="R329" gate="G$1" x="891.54" y="248.92" smashed="yes">
<attribute name="VALUE" x="890.27" y="248.158" size="1.778" layer="96"/>
</instance>
<instance part="R330" gate="G$1" x="891.54" y="246.38" smashed="yes">
<attribute name="VALUE" x="890.27" y="245.618" size="1.778" layer="96"/>
</instance>
<instance part="+3V17" gate="G$1" x="845.82" y="279.4"/>
<instance part="2003_WEEKDAY" gate="A" x="281.94" y="76.2"/>
<instance part="R232" gate="G$1" x="309.88" y="114.3" smashed="yes" rot="R90">
<attribute name="VALUE" x="310.642" y="113.03" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="R233" gate="G$1" x="322.58" y="114.3" smashed="yes" rot="R90">
<attribute name="VALUE" x="323.342" y="113.03" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="R234" gate="G$1" x="335.28" y="114.3" smashed="yes" rot="R90">
<attribute name="VALUE" x="336.042" y="113.03" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="R235" gate="G$1" x="350.52" y="114.3" smashed="yes" rot="R90">
<attribute name="VALUE" x="351.282" y="113.03" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="R236" gate="G$1" x="363.22" y="114.3" smashed="yes" rot="R90">
<attribute name="VALUE" x="363.982" y="113.03" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="R237" gate="G$1" x="375.92" y="114.3" smashed="yes" rot="R90">
<attribute name="VALUE" x="376.682" y="113.03" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="R238" gate="G$1" x="388.62" y="114.3" smashed="yes" rot="R90">
<attribute name="VALUE" x="389.382" y="113.03" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="R239" gate="G$1" x="7.62" y="78.74" smashed="yes" rot="R180">
<attribute name="VALUE" x="8.89" y="79.502" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="BUTTON34" gate="-1" x="-30.48" y="81.28"/>
<instance part="BUTTON34" gate="-2" x="-30.48" y="76.2"/>
<instance part="BUTTON35" gate="-1" x="-30.48" y="93.98"/>
<instance part="BUTTON35" gate="-2" x="-30.48" y="88.9"/>
<instance part="BUTTON36" gate="-1" x="-30.48" y="106.68"/>
<instance part="BUTTON36" gate="-2" x="-30.48" y="101.6"/>
<instance part="R240" gate="G$1" x="7.62" y="88.9" smashed="yes" rot="R180">
<attribute name="VALUE" x="8.89" y="89.662" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="R241" gate="G$1" x="7.62" y="101.6" smashed="yes" rot="R180">
<attribute name="VALUE" x="8.89" y="102.362" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="T2" gate="G$1" x="205.74" y="-12.7"/>
<instance part="U$2" gate="G$1" x="53.34" y="-10.16"/>
<instance part="GND16" gate="1" x="38.1" y="-33.02"/>
<instance part="P+2" gate="1" x="0" y="12.7"/>
<instance part="P+3" gate="1" x="45.72" y="38.1"/>
</instances>
<busses>
</busses>
<nets>
<net name="+3V3" class="0">
<segment>
<pinref part="+3V2" gate="G$1" pin="+3V3"/>
<pinref part="U$1" gate="G$1" pin="3.3V"/>
<wire x1="40.64" y1="93.98" x2="40.64" y2="81.28" width="0.1524" layer="91"/>
<wire x1="40.64" y1="81.28" x2="53.34" y2="81.28" width="0.1524" layer="91"/>
<wire x1="40.64" y1="81.28" x2="-15.24" y2="81.28" width="0.1524" layer="91"/>
<junction x="40.64" y="81.28"/>
<pinref part="BUTTON34" gate="-1" pin="KL"/>
<pinref part="BUTTON35" gate="-1" pin="KL"/>
<wire x1="-15.24" y1="81.28" x2="-25.4" y2="81.28" width="0.1524" layer="91"/>
<wire x1="-15.24" y1="81.28" x2="-15.24" y2="93.98" width="0.1524" layer="91"/>
<wire x1="-15.24" y1="93.98" x2="-25.4" y2="93.98" width="0.1524" layer="91"/>
<junction x="-15.24" y="81.28"/>
<pinref part="BUTTON36" gate="-1" pin="KL"/>
<wire x1="-15.24" y1="93.98" x2="-15.24" y2="106.68" width="0.1524" layer="91"/>
<wire x1="-15.24" y1="106.68" x2="-25.4" y2="106.68" width="0.1524" layer="91"/>
<junction x="-15.24" y="93.98"/>
</segment>
<segment>
<pinref part="+3V3" gate="G$1" pin="+3V3"/>
<pinref part="IC1" gate="G$1" pin="VDD"/>
<wire x1="134.62" y1="93.98" x2="134.62" y2="78.74" width="0.1524" layer="91"/>
<wire x1="134.62" y1="78.74" x2="139.7" y2="78.74" width="0.1524" layer="91"/>
<pinref part="R256" gate="G$1" pin="2"/>
<wire x1="134.62" y1="78.74" x2="121.92" y2="78.74" width="0.1524" layer="91"/>
<junction x="134.62" y="78.74"/>
<pinref part="R257" gate="G$1" pin="2"/>
<wire x1="121.92" y1="78.74" x2="116.84" y2="78.74" width="0.1524" layer="91"/>
<junction x="121.92" y="78.74"/>
<pinref part="IC1" gate="G$1" pin="!RESET"/>
<wire x1="139.7" y1="73.66" x2="134.62" y2="73.66" width="0.1524" layer="91"/>
<wire x1="134.62" y1="73.66" x2="134.62" y2="78.74" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="4543_HOUR_A" gate="A" pin="VDD"/>
<pinref part="+3V1" gate="G$1" pin="+3V3"/>
<wire x1="-236.22" y1="444.5" x2="-236.22" y2="454.66" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="4543_HOUR_B" gate="A" pin="VDD"/>
<pinref part="+3V4" gate="G$1" pin="+3V3"/>
<wire x1="-248.92" y1="510.54" x2="-248.92" y2="520.7" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="4543_MIN_A" gate="A" pin="VDD"/>
<pinref part="+3V5" gate="G$1" pin="+3V3"/>
<wire x1="-53.34" y1="444.5" x2="-53.34" y2="454.66" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="4543_MIN_B" gate="A" pin="VDD"/>
<pinref part="+3V6" gate="G$1" pin="+3V3"/>
<wire x1="-66.04" y1="523.24" x2="-66.04" y2="533.4" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="4543_SEC_A" gate="A" pin="VDD"/>
<pinref part="+3V7" gate="G$1" pin="+3V3"/>
<wire x1="132.08" y1="441.96" x2="132.08" y2="452.12" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="4543_SEC_B" gate="A" pin="VDD"/>
<pinref part="+3V8" gate="G$1" pin="+3V3"/>
<wire x1="119.38" y1="520.7" x2="119.38" y2="530.86" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="+3V39" gate="G$1" pin="+3V3"/>
<pinref part="DEMUX_WEEKDAY" gate="A" pin="VCC"/>
<wire x1="251.46" y1="99.06" x2="251.46" y2="93.98" width="0.1524" layer="91"/>
<wire x1="251.46" y1="93.98" x2="251.46" y2="86.36" width="0.1524" layer="91"/>
<wire x1="251.46" y1="93.98" x2="203.2" y2="93.98" width="0.1524" layer="91"/>
<wire x1="203.2" y1="93.98" x2="203.2" y2="73.66" width="0.1524" layer="91"/>
<junction x="251.46" y="93.98"/>
<wire x1="203.2" y1="73.66" x2="210.82" y2="73.66" width="0.1524" layer="91"/>
<pinref part="R252" gate="G$1" pin="1"/>
</segment>
<segment>
<pinref part="+3V40" gate="G$1" pin="+3V3"/>
<pinref part="4543_DAY_A" gate="A" pin="VDD"/>
<wire x1="-396.24" y1="264.16" x2="-396.24" y2="251.46" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="+3V9" gate="G$1" pin="+3V3"/>
<pinref part="4543_DAY_B" gate="A" pin="VDD"/>
<wire x1="-271.78" y1="264.16" x2="-271.78" y2="251.46" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="+3V11" gate="G$1" pin="+3V3"/>
<pinref part="4543_MONTH_B" gate="A" pin="VDD"/>
<wire x1="-22.86" y1="271.78" x2="-22.86" y2="259.08" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="+3V12" gate="G$1" pin="+3V3"/>
<pinref part="4543_YEAR_A" gate="A" pin="VDD"/>
<wire x1="149.86" y1="281.94" x2="149.86" y2="269.24" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="+3V13" gate="G$1" pin="+3V3"/>
<pinref part="4543_YEAR_B" gate="A" pin="VDD"/>
<wire x1="276.86" y1="281.94" x2="276.86" y2="269.24" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="+3V14" gate="G$1" pin="+3V3"/>
<pinref part="4543_YEAR_C" gate="A" pin="VDD"/>
<wire x1="401.32" y1="281.94" x2="401.32" y2="269.24" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="+3V15" gate="G$1" pin="+3V3"/>
<pinref part="4543_YEAR_D" gate="A" pin="VDD"/>
<wire x1="525.78" y1="281.94" x2="525.78" y2="269.24" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="+3V16" gate="G$1" pin="+3V3"/>
<pinref part="4543_TEMP_A" gate="A" pin="VDD"/>
<wire x1="713.74" y1="279.4" x2="713.74" y2="266.7" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="+3V17" gate="G$1" pin="+3V3"/>
<pinref part="4543_TEMP_B" gate="A" pin="VDD"/>
<wire x1="845.82" y1="276.86" x2="845.82" y2="264.16" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="4543_MONTH_A" gate="A" pin="VDD"/>
<pinref part="+3V10" gate="G$1" pin="+3V3"/>
<wire x1="-139.7" y1="259.08" x2="-139.7" y2="271.78" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$1" class="0">
<segment>
<pinref part="R1" gate="G$1" pin="1"/>
<pinref part="4543_HOUR_A" gate="A" pin="F"/>
<wire x1="-233.68" y1="441.96" x2="-236.22" y2="441.96" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$2" class="0">
<segment>
<pinref part="R2" gate="G$1" pin="1"/>
<pinref part="4543_HOUR_A" gate="A" pin="G"/>
<wire x1="-233.68" y1="439.42" x2="-236.22" y2="439.42" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$3" class="0">
<segment>
<pinref part="R3" gate="G$1" pin="1"/>
<pinref part="4543_HOUR_A" gate="A" pin="E"/>
<wire x1="-233.68" y1="436.88" x2="-236.22" y2="436.88" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$4" class="0">
<segment>
<pinref part="R4" gate="G$1" pin="1"/>
<pinref part="4543_HOUR_A" gate="A" pin="D"/>
<wire x1="-233.68" y1="434.34" x2="-236.22" y2="434.34" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$5" class="0">
<segment>
<pinref part="R5" gate="G$1" pin="1"/>
<pinref part="4543_HOUR_A" gate="A" pin="C"/>
<wire x1="-233.68" y1="431.8" x2="-236.22" y2="431.8" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$6" class="0">
<segment>
<pinref part="R6" gate="G$1" pin="1"/>
<pinref part="4543_HOUR_A" gate="A" pin="B"/>
<wire x1="-233.68" y1="429.26" x2="-236.22" y2="429.26" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$7" class="0">
<segment>
<pinref part="R7" gate="G$1" pin="1"/>
<pinref part="4543_HOUR_A" gate="A" pin="A"/>
<wire x1="-233.68" y1="426.72" x2="-236.22" y2="426.72" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$8" class="0">
<segment>
<pinref part="R7" gate="G$1" pin="2"/>
<pinref part="2003_HOUR_A" gate="A" pin="I7"/>
<wire x1="-223.52" y1="426.72" x2="-220.98" y2="426.72" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$9" class="0">
<segment>
<pinref part="R6" gate="G$1" pin="2"/>
<pinref part="2003_HOUR_A" gate="A" pin="I6"/>
<wire x1="-223.52" y1="429.26" x2="-220.98" y2="429.26" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$10" class="0">
<segment>
<pinref part="R5" gate="G$1" pin="2"/>
<pinref part="2003_HOUR_A" gate="A" pin="I5"/>
<wire x1="-223.52" y1="431.8" x2="-220.98" y2="431.8" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$11" class="0">
<segment>
<pinref part="R4" gate="G$1" pin="2"/>
<pinref part="2003_HOUR_A" gate="A" pin="I4"/>
<wire x1="-223.52" y1="434.34" x2="-220.98" y2="434.34" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$12" class="0">
<segment>
<pinref part="R3" gate="G$1" pin="2"/>
<pinref part="2003_HOUR_A" gate="A" pin="I3"/>
<wire x1="-223.52" y1="436.88" x2="-220.98" y2="436.88" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$13" class="0">
<segment>
<pinref part="R2" gate="G$1" pin="2"/>
<pinref part="2003_HOUR_A" gate="A" pin="I2"/>
<wire x1="-223.52" y1="439.42" x2="-220.98" y2="439.42" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$14" class="0">
<segment>
<pinref part="R1" gate="G$1" pin="2"/>
<pinref part="2003_HOUR_A" gate="A" pin="I1"/>
<wire x1="-223.52" y1="441.96" x2="-220.98" y2="441.96" width="0.1524" layer="91"/>
</segment>
</net>
<net name="GND" class="0">
<segment>
<pinref part="4543_HOUR_A" gate="A" pin="VSS"/>
<pinref part="GND1" gate="1" pin="GND"/>
<wire x1="-261.62" y1="426.72" x2="-266.7" y2="426.72" width="0.1524" layer="91"/>
<wire x1="-266.7" y1="426.72" x2="-266.7" y2="416.56" width="0.1524" layer="91"/>
<pinref part="4543_HOUR_A" gate="A" pin="BI"/>
<wire x1="-266.7" y1="416.56" x2="-266.7" y2="408.94" width="0.1524" layer="91"/>
<wire x1="-261.62" y1="429.26" x2="-266.7" y2="429.26" width="0.1524" layer="91"/>
<wire x1="-266.7" y1="429.26" x2="-266.7" y2="426.72" width="0.1524" layer="91"/>
<junction x="-266.7" y="426.72"/>
<pinref part="4543_HOUR_A" gate="A" pin="PH"/>
<wire x1="-261.62" y1="431.8" x2="-266.7" y2="431.8" width="0.1524" layer="91"/>
<wire x1="-266.7" y1="431.8" x2="-266.7" y2="429.26" width="0.1524" layer="91"/>
<junction x="-266.7" y="429.26"/>
<pinref part="2003_HOUR_A" gate="A" pin="GND"/>
<wire x1="-220.98" y1="424.18" x2="-223.52" y2="424.18" width="0.1524" layer="91"/>
<wire x1="-223.52" y1="424.18" x2="-223.52" y2="416.56" width="0.1524" layer="91"/>
<wire x1="-223.52" y1="416.56" x2="-266.7" y2="416.56" width="0.1524" layer="91"/>
<junction x="-266.7" y="416.56"/>
</segment>
<segment>
<pinref part="4543_HOUR_B" gate="A" pin="VSS"/>
<wire x1="-274.32" y1="492.76" x2="-279.4" y2="492.76" width="0.1524" layer="91"/>
<wire x1="-279.4" y1="492.76" x2="-279.4" y2="482.6" width="0.1524" layer="91"/>
<pinref part="4543_HOUR_B" gate="A" pin="BI"/>
<wire x1="-274.32" y1="495.3" x2="-279.4" y2="495.3" width="0.1524" layer="91"/>
<wire x1="-279.4" y1="495.3" x2="-279.4" y2="492.76" width="0.1524" layer="91"/>
<junction x="-279.4" y="492.76"/>
<pinref part="4543_HOUR_B" gate="A" pin="PH"/>
<wire x1="-274.32" y1="497.84" x2="-279.4" y2="497.84" width="0.1524" layer="91"/>
<wire x1="-279.4" y1="497.84" x2="-279.4" y2="495.3" width="0.1524" layer="91"/>
<junction x="-279.4" y="495.3"/>
<pinref part="2003_HOUR_B" gate="A" pin="GND"/>
<wire x1="-233.68" y1="490.22" x2="-236.22" y2="490.22" width="0.1524" layer="91"/>
<wire x1="-236.22" y1="490.22" x2="-236.22" y2="482.6" width="0.1524" layer="91"/>
<pinref part="GND2" gate="1" pin="GND"/>
<wire x1="-236.22" y1="482.6" x2="-279.4" y2="482.6" width="0.1524" layer="91"/>
<junction x="-279.4" y="482.6"/>
</segment>
<segment>
<pinref part="4543_MIN_A" gate="A" pin="VSS"/>
<pinref part="GND3" gate="1" pin="GND"/>
<wire x1="-78.74" y1="426.72" x2="-83.82" y2="426.72" width="0.1524" layer="91"/>
<wire x1="-83.82" y1="426.72" x2="-83.82" y2="416.56" width="0.1524" layer="91"/>
<pinref part="4543_MIN_A" gate="A" pin="BI"/>
<wire x1="-83.82" y1="416.56" x2="-83.82" y2="408.94" width="0.1524" layer="91"/>
<wire x1="-78.74" y1="429.26" x2="-83.82" y2="429.26" width="0.1524" layer="91"/>
<wire x1="-83.82" y1="429.26" x2="-83.82" y2="426.72" width="0.1524" layer="91"/>
<junction x="-83.82" y="426.72"/>
<pinref part="4543_MIN_A" gate="A" pin="PH"/>
<wire x1="-78.74" y1="431.8" x2="-83.82" y2="431.8" width="0.1524" layer="91"/>
<wire x1="-83.82" y1="431.8" x2="-83.82" y2="429.26" width="0.1524" layer="91"/>
<junction x="-83.82" y="429.26"/>
<pinref part="2003_MIN_A" gate="A" pin="GND"/>
<wire x1="-38.1" y1="424.18" x2="-40.64" y2="424.18" width="0.1524" layer="91"/>
<wire x1="-40.64" y1="424.18" x2="-40.64" y2="416.56" width="0.1524" layer="91"/>
<wire x1="-40.64" y1="416.56" x2="-83.82" y2="416.56" width="0.1524" layer="91"/>
<junction x="-83.82" y="416.56"/>
</segment>
<segment>
<pinref part="4543_MIN_B" gate="A" pin="VSS"/>
<wire x1="-91.44" y1="505.46" x2="-96.52" y2="505.46" width="0.1524" layer="91"/>
<wire x1="-96.52" y1="505.46" x2="-96.52" y2="495.3" width="0.1524" layer="91"/>
<pinref part="4543_MIN_B" gate="A" pin="BI"/>
<wire x1="-91.44" y1="508" x2="-96.52" y2="508" width="0.1524" layer="91"/>
<wire x1="-96.52" y1="508" x2="-96.52" y2="505.46" width="0.1524" layer="91"/>
<junction x="-96.52" y="505.46"/>
<pinref part="4543_MIN_B" gate="A" pin="PH"/>
<wire x1="-91.44" y1="510.54" x2="-96.52" y2="510.54" width="0.1524" layer="91"/>
<wire x1="-96.52" y1="510.54" x2="-96.52" y2="508" width="0.1524" layer="91"/>
<junction x="-96.52" y="508"/>
<pinref part="2003_MIN_B" gate="A" pin="GND"/>
<wire x1="-50.8" y1="502.92" x2="-53.34" y2="502.92" width="0.1524" layer="91"/>
<wire x1="-53.34" y1="502.92" x2="-53.34" y2="495.3" width="0.1524" layer="91"/>
<pinref part="GND4" gate="1" pin="GND"/>
<wire x1="-53.34" y1="495.3" x2="-96.52" y2="495.3" width="0.1524" layer="91"/>
<junction x="-96.52" y="495.3"/>
</segment>
<segment>
<pinref part="4543_SEC_A" gate="A" pin="VSS"/>
<pinref part="GND5" gate="1" pin="GND"/>
<wire x1="106.68" y1="424.18" x2="101.6" y2="424.18" width="0.1524" layer="91"/>
<wire x1="101.6" y1="424.18" x2="101.6" y2="414.02" width="0.1524" layer="91"/>
<pinref part="4543_SEC_A" gate="A" pin="BI"/>
<wire x1="101.6" y1="414.02" x2="101.6" y2="406.4" width="0.1524" layer="91"/>
<wire x1="106.68" y1="426.72" x2="101.6" y2="426.72" width="0.1524" layer="91"/>
<wire x1="101.6" y1="426.72" x2="101.6" y2="424.18" width="0.1524" layer="91"/>
<junction x="101.6" y="424.18"/>
<pinref part="4543_SEC_A" gate="A" pin="PH"/>
<wire x1="106.68" y1="429.26" x2="101.6" y2="429.26" width="0.1524" layer="91"/>
<wire x1="101.6" y1="429.26" x2="101.6" y2="426.72" width="0.1524" layer="91"/>
<junction x="101.6" y="426.72"/>
<pinref part="2003_SEC_A" gate="A" pin="GND"/>
<wire x1="147.32" y1="421.64" x2="144.78" y2="421.64" width="0.1524" layer="91"/>
<wire x1="144.78" y1="421.64" x2="144.78" y2="414.02" width="0.1524" layer="91"/>
<wire x1="144.78" y1="414.02" x2="101.6" y2="414.02" width="0.1524" layer="91"/>
<junction x="101.6" y="414.02"/>
</segment>
<segment>
<pinref part="4543_SEC_B" gate="A" pin="VSS"/>
<wire x1="93.98" y1="502.92" x2="88.9" y2="502.92" width="0.1524" layer="91"/>
<wire x1="88.9" y1="502.92" x2="88.9" y2="492.76" width="0.1524" layer="91"/>
<pinref part="4543_SEC_B" gate="A" pin="BI"/>
<wire x1="93.98" y1="505.46" x2="88.9" y2="505.46" width="0.1524" layer="91"/>
<wire x1="88.9" y1="505.46" x2="88.9" y2="502.92" width="0.1524" layer="91"/>
<junction x="88.9" y="502.92"/>
<pinref part="4543_SEC_B" gate="A" pin="PH"/>
<wire x1="93.98" y1="508" x2="88.9" y2="508" width="0.1524" layer="91"/>
<wire x1="88.9" y1="508" x2="88.9" y2="505.46" width="0.1524" layer="91"/>
<junction x="88.9" y="505.46"/>
<pinref part="2003_SEC_B" gate="A" pin="GND"/>
<wire x1="134.62" y1="500.38" x2="132.08" y2="500.38" width="0.1524" layer="91"/>
<wire x1="132.08" y1="500.38" x2="132.08" y2="492.76" width="0.1524" layer="91"/>
<pinref part="GND6" gate="1" pin="GND"/>
<wire x1="132.08" y1="492.76" x2="88.9" y2="492.76" width="0.1524" layer="91"/>
<junction x="88.9" y="492.76"/>
</segment>
<segment>
<pinref part="DEMUX_WEEKDAY" gate="A" pin="GND"/>
<wire x1="226.06" y1="68.58" x2="223.52" y2="68.58" width="0.1524" layer="91"/>
<pinref part="GND18" gate="1" pin="GND"/>
<wire x1="223.52" y1="68.58" x2="223.52" y2="60.96" width="0.1524" layer="91"/>
<pinref part="DEMUX_WEEKDAY" gate="A" pin="E1"/>
<wire x1="226.06" y1="78.74" x2="223.52" y2="78.74" width="0.1524" layer="91"/>
<wire x1="223.52" y1="78.74" x2="223.52" y2="76.2" width="0.1524" layer="91"/>
<junction x="223.52" y="68.58"/>
<pinref part="DEMUX_WEEKDAY" gate="A" pin="E2"/>
<wire x1="223.52" y1="76.2" x2="223.52" y2="68.58" width="0.1524" layer="91"/>
<wire x1="226.06" y1="76.2" x2="223.52" y2="76.2" width="0.1524" layer="91"/>
<junction x="223.52" y="76.2"/>
</segment>
<segment>
<pinref part="LED_COLON2" gate="G$1" pin="C"/>
<pinref part="GND19" gate="1" pin="GND"/>
<wire x1="-109.22" y1="332.74" x2="-109.22" y2="322.58" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="LED_COLON4" gate="G$1" pin="C"/>
<pinref part="GND20" gate="1" pin="GND"/>
<wire x1="78.74" y1="327.66" x2="78.74" y2="317.5" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND21" gate="1" pin="GND"/>
<wire x1="208.28" y1="-17.78" x2="208.28" y2="-27.94" width="0.1524" layer="91"/>
<pinref part="T2" gate="G$1" pin="E"/>
</segment>
<segment>
<pinref part="IC1" gate="G$1" pin="A0"/>
<pinref part="GND22" gate="1" pin="GND"/>
<wire x1="139.7" y1="48.26" x2="134.62" y2="48.26" width="0.1524" layer="91"/>
<wire x1="134.62" y1="48.26" x2="134.62" y2="45.72" width="0.1524" layer="91"/>
<pinref part="IC1" gate="G$1" pin="A1"/>
<wire x1="134.62" y1="45.72" x2="134.62" y2="43.18" width="0.1524" layer="91"/>
<wire x1="134.62" y1="43.18" x2="134.62" y2="38.1" width="0.1524" layer="91"/>
<wire x1="134.62" y1="38.1" x2="134.62" y2="30.48" width="0.1524" layer="91"/>
<wire x1="139.7" y1="45.72" x2="134.62" y2="45.72" width="0.1524" layer="91"/>
<junction x="134.62" y="45.72"/>
<pinref part="IC1" gate="G$1" pin="A2"/>
<wire x1="139.7" y1="43.18" x2="134.62" y2="43.18" width="0.1524" layer="91"/>
<junction x="134.62" y="43.18"/>
<pinref part="IC1" gate="G$1" pin="VSS"/>
<wire x1="139.7" y1="38.1" x2="134.62" y2="38.1" width="0.1524" layer="91"/>
<junction x="134.62" y="38.1"/>
</segment>
<segment>
<pinref part="U$1" gate="G$1" pin="GND3"/>
<wire x1="83.82" y1="66.04" x2="101.6" y2="66.04" width="0.1524" layer="91"/>
<wire x1="101.6" y1="66.04" x2="101.6" y2="25.4" width="0.1524" layer="91"/>
<pinref part="GND23" gate="1" pin="GND"/>
<wire x1="101.6" y1="25.4" x2="68.58" y2="25.4" width="0.1524" layer="91"/>
<wire x1="68.58" y1="25.4" x2="68.58" y2="20.32" width="0.1524" layer="91"/>
<pinref part="U$1" gate="G$1" pin="GND1"/>
<wire x1="53.34" y1="48.26" x2="43.18" y2="48.26" width="0.1524" layer="91"/>
<wire x1="43.18" y1="48.26" x2="43.18" y2="30.48" width="0.1524" layer="91"/>
<wire x1="43.18" y1="30.48" x2="43.18" y2="25.4" width="0.1524" layer="91"/>
<wire x1="43.18" y1="25.4" x2="68.58" y2="25.4" width="0.1524" layer="91"/>
<junction x="68.58" y="25.4"/>
<pinref part="U$1" gate="G$1" pin="GND2"/>
<wire x1="83.82" y1="81.28" x2="101.6" y2="81.28" width="0.1524" layer="91"/>
<wire x1="101.6" y1="81.28" x2="101.6" y2="66.04" width="0.1524" layer="91"/>
<junction x="101.6" y="66.04"/>
<pinref part="R239" gate="G$1" pin="1"/>
<wire x1="12.7" y1="78.74" x2="17.78" y2="78.74" width="0.1524" layer="91"/>
<wire x1="17.78" y1="78.74" x2="17.78" y2="30.48" width="0.1524" layer="91"/>
<wire x1="17.78" y1="30.48" x2="43.18" y2="30.48" width="0.1524" layer="91"/>
<junction x="43.18" y="30.48"/>
<pinref part="R240" gate="G$1" pin="1"/>
<wire x1="12.7" y1="88.9" x2="17.78" y2="88.9" width="0.1524" layer="91"/>
<wire x1="17.78" y1="88.9" x2="17.78" y2="78.74" width="0.1524" layer="91"/>
<junction x="17.78" y="78.74"/>
<pinref part="R241" gate="G$1" pin="1"/>
<wire x1="12.7" y1="101.6" x2="17.78" y2="101.6" width="0.1524" layer="91"/>
<wire x1="17.78" y1="101.6" x2="17.78" y2="88.9" width="0.1524" layer="91"/>
<junction x="17.78" y="88.9"/>
</segment>
<segment>
<pinref part="GND24" gate="1" pin="GND"/>
<wire x1="383.54" y1="-30.48" x2="383.54" y2="-38.1" width="0.1524" layer="91"/>
<pinref part="12_POWER_IN" gate="G$1" pin="1"/>
<wire x1="383.54" y1="-38.1" x2="383.54" y2="-40.64" width="0.1524" layer="91"/>
<wire x1="383.54" y1="-30.48" x2="368.3" y2="-30.48" width="0.1524" layer="91"/>
<pinref part="12V_POWER_OUT" gate="-2" pin="KL"/>
<wire x1="383.54" y1="-38.1" x2="345.44" y2="-38.1" width="0.1524" layer="91"/>
<wire x1="345.44" y1="-38.1" x2="345.44" y2="-15.24" width="0.1524" layer="91"/>
<junction x="383.54" y="-38.1"/>
</segment>
<segment>
<pinref part="4543_DAY_A" gate="A" pin="PH"/>
<pinref part="GND25" gate="1" pin="GND"/>
<wire x1="-421.64" y1="238.76" x2="-424.18" y2="238.76" width="0.1524" layer="91"/>
<wire x1="-424.18" y1="238.76" x2="-424.18" y2="236.22" width="0.1524" layer="91"/>
<pinref part="4543_DAY_A" gate="A" pin="BI"/>
<wire x1="-424.18" y1="236.22" x2="-424.18" y2="233.68" width="0.1524" layer="91"/>
<wire x1="-424.18" y1="233.68" x2="-424.18" y2="226.06" width="0.1524" layer="91"/>
<wire x1="-421.64" y1="236.22" x2="-424.18" y2="236.22" width="0.1524" layer="91"/>
<junction x="-424.18" y="236.22"/>
<pinref part="4543_DAY_A" gate="A" pin="VSS"/>
<wire x1="-421.64" y1="233.68" x2="-424.18" y2="233.68" width="0.1524" layer="91"/>
<junction x="-424.18" y="233.68"/>
<pinref part="2003_DAY_A" gate="A" pin="GND"/>
<wire x1="-381" y1="231.14" x2="-391.16" y2="231.14" width="0.1524" layer="91"/>
<wire x1="-391.16" y1="231.14" x2="-391.16" y2="226.06" width="0.1524" layer="91"/>
<wire x1="-391.16" y1="226.06" x2="-424.18" y2="226.06" width="0.1524" layer="91"/>
<junction x="-424.18" y="226.06"/>
</segment>
<segment>
<pinref part="4543_DAY_B" gate="A" pin="PH"/>
<pinref part="GND7" gate="1" pin="GND"/>
<wire x1="-297.18" y1="238.76" x2="-299.72" y2="238.76" width="0.1524" layer="91"/>
<wire x1="-299.72" y1="238.76" x2="-299.72" y2="236.22" width="0.1524" layer="91"/>
<pinref part="4543_DAY_B" gate="A" pin="BI"/>
<wire x1="-299.72" y1="236.22" x2="-299.72" y2="233.68" width="0.1524" layer="91"/>
<wire x1="-299.72" y1="233.68" x2="-299.72" y2="226.06" width="0.1524" layer="91"/>
<wire x1="-297.18" y1="236.22" x2="-299.72" y2="236.22" width="0.1524" layer="91"/>
<junction x="-299.72" y="236.22"/>
<pinref part="4543_DAY_B" gate="A" pin="VSS"/>
<wire x1="-297.18" y1="233.68" x2="-299.72" y2="233.68" width="0.1524" layer="91"/>
<junction x="-299.72" y="233.68"/>
<pinref part="2003_DAY_B" gate="A" pin="GND"/>
<wire x1="-256.54" y1="231.14" x2="-266.7" y2="231.14" width="0.1524" layer="91"/>
<wire x1="-266.7" y1="231.14" x2="-266.7" y2="226.06" width="0.1524" layer="91"/>
<wire x1="-266.7" y1="226.06" x2="-299.72" y2="226.06" width="0.1524" layer="91"/>
<junction x="-299.72" y="226.06"/>
</segment>
<segment>
<pinref part="4543_MONTH_A" gate="A" pin="PH"/>
<pinref part="GND8" gate="1" pin="GND"/>
<wire x1="-165.1" y1="246.38" x2="-167.64" y2="246.38" width="0.1524" layer="91"/>
<wire x1="-167.64" y1="246.38" x2="-167.64" y2="243.84" width="0.1524" layer="91"/>
<pinref part="4543_MONTH_A" gate="A" pin="BI"/>
<wire x1="-167.64" y1="243.84" x2="-167.64" y2="241.3" width="0.1524" layer="91"/>
<wire x1="-167.64" y1="241.3" x2="-167.64" y2="233.68" width="0.1524" layer="91"/>
<wire x1="-165.1" y1="243.84" x2="-167.64" y2="243.84" width="0.1524" layer="91"/>
<junction x="-167.64" y="243.84"/>
<pinref part="4543_MONTH_A" gate="A" pin="VSS"/>
<wire x1="-165.1" y1="241.3" x2="-167.64" y2="241.3" width="0.1524" layer="91"/>
<junction x="-167.64" y="241.3"/>
<pinref part="2003_MONTH_A" gate="A" pin="GND"/>
<wire x1="-124.46" y1="238.76" x2="-134.62" y2="238.76" width="0.1524" layer="91"/>
<wire x1="-134.62" y1="238.76" x2="-134.62" y2="233.68" width="0.1524" layer="91"/>
<wire x1="-134.62" y1="233.68" x2="-167.64" y2="233.68" width="0.1524" layer="91"/>
<junction x="-167.64" y="233.68"/>
</segment>
<segment>
<pinref part="4543_MONTH_B" gate="A" pin="PH"/>
<pinref part="GND9" gate="1" pin="GND"/>
<wire x1="-48.26" y1="246.38" x2="-50.8" y2="246.38" width="0.1524" layer="91"/>
<wire x1="-50.8" y1="246.38" x2="-50.8" y2="243.84" width="0.1524" layer="91"/>
<pinref part="4543_MONTH_B" gate="A" pin="BI"/>
<wire x1="-50.8" y1="243.84" x2="-50.8" y2="241.3" width="0.1524" layer="91"/>
<wire x1="-50.8" y1="241.3" x2="-50.8" y2="233.68" width="0.1524" layer="91"/>
<wire x1="-48.26" y1="243.84" x2="-50.8" y2="243.84" width="0.1524" layer="91"/>
<junction x="-50.8" y="243.84"/>
<pinref part="4543_MONTH_B" gate="A" pin="VSS"/>
<wire x1="-48.26" y1="241.3" x2="-50.8" y2="241.3" width="0.1524" layer="91"/>
<junction x="-50.8" y="241.3"/>
<pinref part="2003_MONTH_B" gate="A" pin="GND"/>
<wire x1="-7.62" y1="238.76" x2="-17.78" y2="238.76" width="0.1524" layer="91"/>
<wire x1="-17.78" y1="238.76" x2="-17.78" y2="233.68" width="0.1524" layer="91"/>
<wire x1="-17.78" y1="233.68" x2="-50.8" y2="233.68" width="0.1524" layer="91"/>
<junction x="-50.8" y="233.68"/>
</segment>
<segment>
<pinref part="4543_YEAR_A" gate="A" pin="PH"/>
<pinref part="GND10" gate="1" pin="GND"/>
<wire x1="124.46" y1="256.54" x2="121.92" y2="256.54" width="0.1524" layer="91"/>
<wire x1="121.92" y1="256.54" x2="121.92" y2="254" width="0.1524" layer="91"/>
<pinref part="4543_YEAR_A" gate="A" pin="BI"/>
<wire x1="121.92" y1="254" x2="121.92" y2="251.46" width="0.1524" layer="91"/>
<wire x1="121.92" y1="251.46" x2="121.92" y2="243.84" width="0.1524" layer="91"/>
<wire x1="124.46" y1="254" x2="121.92" y2="254" width="0.1524" layer="91"/>
<junction x="121.92" y="254"/>
<pinref part="4543_YEAR_A" gate="A" pin="VSS"/>
<wire x1="124.46" y1="251.46" x2="121.92" y2="251.46" width="0.1524" layer="91"/>
<junction x="121.92" y="251.46"/>
<pinref part="2003_YEAR_A" gate="A" pin="GND"/>
<wire x1="165.1" y1="248.92" x2="154.94" y2="248.92" width="0.1524" layer="91"/>
<wire x1="154.94" y1="248.92" x2="154.94" y2="243.84" width="0.1524" layer="91"/>
<wire x1="154.94" y1="243.84" x2="121.92" y2="243.84" width="0.1524" layer="91"/>
<junction x="121.92" y="243.84"/>
</segment>
<segment>
<pinref part="4543_YEAR_B" gate="A" pin="PH"/>
<pinref part="GND11" gate="1" pin="GND"/>
<wire x1="251.46" y1="256.54" x2="248.92" y2="256.54" width="0.1524" layer="91"/>
<wire x1="248.92" y1="256.54" x2="248.92" y2="254" width="0.1524" layer="91"/>
<pinref part="4543_YEAR_B" gate="A" pin="BI"/>
<wire x1="248.92" y1="254" x2="248.92" y2="251.46" width="0.1524" layer="91"/>
<wire x1="248.92" y1="251.46" x2="248.92" y2="243.84" width="0.1524" layer="91"/>
<wire x1="251.46" y1="254" x2="248.92" y2="254" width="0.1524" layer="91"/>
<junction x="248.92" y="254"/>
<pinref part="4543_YEAR_B" gate="A" pin="VSS"/>
<wire x1="251.46" y1="251.46" x2="248.92" y2="251.46" width="0.1524" layer="91"/>
<junction x="248.92" y="251.46"/>
<pinref part="2003_YEAR_B" gate="A" pin="GND"/>
<wire x1="292.1" y1="248.92" x2="281.94" y2="248.92" width="0.1524" layer="91"/>
<wire x1="281.94" y1="248.92" x2="281.94" y2="243.84" width="0.1524" layer="91"/>
<wire x1="281.94" y1="243.84" x2="248.92" y2="243.84" width="0.1524" layer="91"/>
<junction x="248.92" y="243.84"/>
</segment>
<segment>
<pinref part="4543_YEAR_C" gate="A" pin="PH"/>
<pinref part="GND12" gate="1" pin="GND"/>
<wire x1="375.92" y1="256.54" x2="373.38" y2="256.54" width="0.1524" layer="91"/>
<wire x1="373.38" y1="256.54" x2="373.38" y2="254" width="0.1524" layer="91"/>
<pinref part="4543_YEAR_C" gate="A" pin="BI"/>
<wire x1="373.38" y1="254" x2="373.38" y2="251.46" width="0.1524" layer="91"/>
<wire x1="373.38" y1="251.46" x2="373.38" y2="243.84" width="0.1524" layer="91"/>
<wire x1="375.92" y1="254" x2="373.38" y2="254" width="0.1524" layer="91"/>
<junction x="373.38" y="254"/>
<pinref part="4543_YEAR_C" gate="A" pin="VSS"/>
<wire x1="375.92" y1="251.46" x2="373.38" y2="251.46" width="0.1524" layer="91"/>
<junction x="373.38" y="251.46"/>
<pinref part="2003_YEAR_C" gate="A" pin="GND"/>
<wire x1="416.56" y1="248.92" x2="406.4" y2="248.92" width="0.1524" layer="91"/>
<wire x1="406.4" y1="248.92" x2="406.4" y2="243.84" width="0.1524" layer="91"/>
<wire x1="406.4" y1="243.84" x2="373.38" y2="243.84" width="0.1524" layer="91"/>
<junction x="373.38" y="243.84"/>
</segment>
<segment>
<pinref part="4543_YEAR_D" gate="A" pin="PH"/>
<pinref part="GND13" gate="1" pin="GND"/>
<wire x1="500.38" y1="256.54" x2="497.84" y2="256.54" width="0.1524" layer="91"/>
<wire x1="497.84" y1="256.54" x2="497.84" y2="254" width="0.1524" layer="91"/>
<pinref part="4543_YEAR_D" gate="A" pin="BI"/>
<wire x1="497.84" y1="254" x2="497.84" y2="251.46" width="0.1524" layer="91"/>
<wire x1="497.84" y1="251.46" x2="497.84" y2="243.84" width="0.1524" layer="91"/>
<wire x1="500.38" y1="254" x2="497.84" y2="254" width="0.1524" layer="91"/>
<junction x="497.84" y="254"/>
<pinref part="4543_YEAR_D" gate="A" pin="VSS"/>
<wire x1="500.38" y1="251.46" x2="497.84" y2="251.46" width="0.1524" layer="91"/>
<junction x="497.84" y="251.46"/>
<pinref part="2003_YEAR_D" gate="A" pin="GND"/>
<wire x1="541.02" y1="248.92" x2="530.86" y2="248.92" width="0.1524" layer="91"/>
<wire x1="530.86" y1="248.92" x2="530.86" y2="243.84" width="0.1524" layer="91"/>
<wire x1="530.86" y1="243.84" x2="497.84" y2="243.84" width="0.1524" layer="91"/>
<junction x="497.84" y="243.84"/>
</segment>
<segment>
<pinref part="4543_TEMP_A" gate="A" pin="PH"/>
<pinref part="GND14" gate="1" pin="GND"/>
<wire x1="688.34" y1="254" x2="685.8" y2="254" width="0.1524" layer="91"/>
<wire x1="685.8" y1="254" x2="685.8" y2="251.46" width="0.1524" layer="91"/>
<pinref part="4543_TEMP_A" gate="A" pin="BI"/>
<wire x1="685.8" y1="251.46" x2="685.8" y2="248.92" width="0.1524" layer="91"/>
<wire x1="685.8" y1="248.92" x2="685.8" y2="241.3" width="0.1524" layer="91"/>
<wire x1="688.34" y1="251.46" x2="685.8" y2="251.46" width="0.1524" layer="91"/>
<junction x="685.8" y="251.46"/>
<pinref part="4543_TEMP_A" gate="A" pin="VSS"/>
<wire x1="688.34" y1="248.92" x2="685.8" y2="248.92" width="0.1524" layer="91"/>
<junction x="685.8" y="248.92"/>
<pinref part="2003_TEMP_A" gate="A" pin="GND"/>
<wire x1="728.98" y1="246.38" x2="718.82" y2="246.38" width="0.1524" layer="91"/>
<wire x1="718.82" y1="246.38" x2="718.82" y2="241.3" width="0.1524" layer="91"/>
<wire x1="718.82" y1="241.3" x2="685.8" y2="241.3" width="0.1524" layer="91"/>
<junction x="685.8" y="241.3"/>
</segment>
<segment>
<pinref part="4543_TEMP_B" gate="A" pin="PH"/>
<pinref part="GND15" gate="1" pin="GND"/>
<wire x1="820.42" y1="251.46" x2="817.88" y2="251.46" width="0.1524" layer="91"/>
<wire x1="817.88" y1="251.46" x2="817.88" y2="248.92" width="0.1524" layer="91"/>
<pinref part="4543_TEMP_B" gate="A" pin="BI"/>
<wire x1="817.88" y1="248.92" x2="817.88" y2="246.38" width="0.1524" layer="91"/>
<wire x1="817.88" y1="246.38" x2="817.88" y2="238.76" width="0.1524" layer="91"/>
<wire x1="820.42" y1="248.92" x2="817.88" y2="248.92" width="0.1524" layer="91"/>
<junction x="817.88" y="248.92"/>
<pinref part="4543_TEMP_B" gate="A" pin="VSS"/>
<wire x1="820.42" y1="246.38" x2="817.88" y2="246.38" width="0.1524" layer="91"/>
<junction x="817.88" y="246.38"/>
<pinref part="2003_TEMP_B" gate="A" pin="GND"/>
<wire x1="861.06" y1="243.84" x2="850.9" y2="243.84" width="0.1524" layer="91"/>
<wire x1="850.9" y1="243.84" x2="850.9" y2="238.76" width="0.1524" layer="91"/>
<wire x1="850.9" y1="238.76" x2="817.88" y2="238.76" width="0.1524" layer="91"/>
<junction x="817.88" y="238.76"/>
</segment>
<segment>
<pinref part="2003_WEEKDAY" gate="A" pin="GND"/>
<pinref part="GND17" gate="1" pin="GND"/>
<wire x1="269.24" y1="66.04" x2="259.08" y2="66.04" width="0.1524" layer="91"/>
<wire x1="259.08" y1="66.04" x2="259.08" y2="33.02" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U$2" gate="G$1" pin="GND"/>
<wire x1="43.18" y1="-12.7" x2="38.1" y2="-12.7" width="0.1524" layer="91"/>
<wire x1="38.1" y1="-12.7" x2="38.1" y2="-30.48" width="0.1524" layer="91"/>
<pinref part="GND16" gate="1" pin="GND"/>
</segment>
</net>
<net name="N$15" class="0">
<segment>
<pinref part="R12" gate="G$1" pin="2"/>
<pinref part="4543_HOUR_A" gate="A" pin="LD"/>
<wire x1="-264.16" y1="444.5" x2="-261.62" y2="444.5" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$16" class="0">
<segment>
<pinref part="R11" gate="G$1" pin="2"/>
<pinref part="4543_HOUR_A" gate="A" pin="IB"/>
<wire x1="-264.16" y1="441.96" x2="-261.62" y2="441.96" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$17" class="0">
<segment>
<pinref part="R10" gate="G$1" pin="2"/>
<pinref part="4543_HOUR_A" gate="A" pin="IC"/>
<wire x1="-264.16" y1="439.42" x2="-261.62" y2="439.42" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$18" class="0">
<segment>
<pinref part="R9" gate="G$1" pin="2"/>
<pinref part="4543_HOUR_A" gate="A" pin="ID"/>
<wire x1="-264.16" y1="436.88" x2="-261.62" y2="436.88" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$19" class="0">
<segment>
<pinref part="R8" gate="G$1" pin="2"/>
<pinref part="4543_HOUR_A" gate="A" pin="IA"/>
<wire x1="-264.16" y1="434.34" x2="-261.62" y2="434.34" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$20" class="0">
<segment>
<pinref part="2003_HOUR_A" gate="A" pin="O1"/>
<pinref part="R13" gate="G$1" pin="1"/>
<wire x1="-195.58" y1="441.96" x2="-193.04" y2="441.96" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$21" class="0">
<segment>
<pinref part="2003_HOUR_A" gate="A" pin="O2"/>
<pinref part="R14" gate="G$1" pin="1"/>
<wire x1="-195.58" y1="439.42" x2="-193.04" y2="439.42" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$22" class="0">
<segment>
<pinref part="2003_HOUR_A" gate="A" pin="O3"/>
<pinref part="R15" gate="G$1" pin="1"/>
<wire x1="-195.58" y1="436.88" x2="-193.04" y2="436.88" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$23" class="0">
<segment>
<pinref part="2003_HOUR_A" gate="A" pin="O4"/>
<pinref part="R16" gate="G$1" pin="1"/>
<wire x1="-195.58" y1="434.34" x2="-193.04" y2="434.34" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$24" class="0">
<segment>
<pinref part="2003_HOUR_A" gate="A" pin="O5"/>
<pinref part="R17" gate="G$1" pin="1"/>
<wire x1="-195.58" y1="431.8" x2="-193.04" y2="431.8" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$25" class="0">
<segment>
<pinref part="2003_HOUR_A" gate="A" pin="O6"/>
<pinref part="R18" gate="G$1" pin="1"/>
<wire x1="-195.58" y1="429.26" x2="-193.04" y2="429.26" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$26" class="0">
<segment>
<pinref part="2003_HOUR_A" gate="A" pin="O7"/>
<pinref part="R19" gate="G$1" pin="1"/>
<wire x1="-195.58" y1="426.72" x2="-193.04" y2="426.72" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$27" class="0">
<segment>
<pinref part="R19" gate="G$1" pin="2"/>
<wire x1="-182.88" y1="426.72" x2="-180.34" y2="426.72" width="0.1524" layer="91"/>
<wire x1="-180.34" y1="426.72" x2="-180.34" y2="419.1" width="0.1524" layer="91"/>
<pinref part="HOUR_A" gate="G$1" pin="A"/>
<wire x1="-180.34" y1="419.1" x2="-185.42" y2="419.1" width="0.1524" layer="91"/>
<wire x1="-185.42" y1="419.1" x2="-185.42" y2="406.4" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$28" class="0">
<segment>
<pinref part="R18" gate="G$1" pin="2"/>
<wire x1="-182.88" y1="429.26" x2="-177.8" y2="429.26" width="0.1524" layer="91"/>
<wire x1="-177.8" y1="429.26" x2="-177.8" y2="416.56" width="0.1524" layer="91"/>
<wire x1="-177.8" y1="416.56" x2="-187.96" y2="416.56" width="0.1524" layer="91"/>
<wire x1="-187.96" y1="416.56" x2="-187.96" y2="403.86" width="0.1524" layer="91"/>
<pinref part="HOUR_A" gate="G$1" pin="B"/>
<wire x1="-187.96" y1="403.86" x2="-185.42" y2="403.86" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$29" class="0">
<segment>
<pinref part="R17" gate="G$1" pin="2"/>
<wire x1="-182.88" y1="431.8" x2="-175.26" y2="431.8" width="0.1524" layer="91"/>
<wire x1="-175.26" y1="431.8" x2="-175.26" y2="414.02" width="0.1524" layer="91"/>
<wire x1="-175.26" y1="414.02" x2="-190.5" y2="414.02" width="0.1524" layer="91"/>
<wire x1="-190.5" y1="414.02" x2="-190.5" y2="401.32" width="0.1524" layer="91"/>
<pinref part="HOUR_A" gate="G$1" pin="C"/>
<wire x1="-190.5" y1="401.32" x2="-185.42" y2="401.32" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$30" class="0">
<segment>
<pinref part="R16" gate="G$1" pin="2"/>
<wire x1="-182.88" y1="434.34" x2="-172.72" y2="434.34" width="0.1524" layer="91"/>
<wire x1="-172.72" y1="434.34" x2="-172.72" y2="411.48" width="0.1524" layer="91"/>
<wire x1="-172.72" y1="411.48" x2="-193.04" y2="411.48" width="0.1524" layer="91"/>
<wire x1="-193.04" y1="411.48" x2="-193.04" y2="398.78" width="0.1524" layer="91"/>
<pinref part="HOUR_A" gate="G$1" pin="D"/>
<wire x1="-193.04" y1="398.78" x2="-185.42" y2="398.78" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$31" class="0">
<segment>
<pinref part="R15" gate="G$1" pin="2"/>
<wire x1="-182.88" y1="436.88" x2="-170.18" y2="436.88" width="0.1524" layer="91"/>
<wire x1="-170.18" y1="436.88" x2="-170.18" y2="421.64" width="0.1524" layer="91"/>
<wire x1="-170.18" y1="421.64" x2="-195.58" y2="421.64" width="0.1524" layer="91"/>
<wire x1="-195.58" y1="421.64" x2="-195.58" y2="396.24" width="0.1524" layer="91"/>
<pinref part="HOUR_A" gate="G$1" pin="E"/>
<wire x1="-195.58" y1="396.24" x2="-185.42" y2="396.24" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$32" class="0">
<segment>
<pinref part="R14" gate="G$1" pin="2"/>
<wire x1="-182.88" y1="439.42" x2="-167.64" y2="439.42" width="0.1524" layer="91"/>
<wire x1="-167.64" y1="439.42" x2="-167.64" y2="403.86" width="0.1524" layer="91"/>
<pinref part="HOUR_A" gate="G$1" pin="G"/>
<wire x1="-167.64" y1="403.86" x2="-170.18" y2="403.86" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$33" class="0">
<segment>
<pinref part="R13" gate="G$1" pin="2"/>
<wire x1="-182.88" y1="441.96" x2="-165.1" y2="441.96" width="0.1524" layer="91"/>
<wire x1="-165.1" y1="441.96" x2="-165.1" y2="406.4" width="0.1524" layer="91"/>
<pinref part="HOUR_A" gate="G$1" pin="F"/>
<wire x1="-165.1" y1="406.4" x2="-170.18" y2="406.4" width="0.1524" layer="91"/>
</segment>
</net>
<net name="+12V" class="0">
<segment>
<pinref part="P+9" gate="1" pin="+12V"/>
<pinref part="POWER_SWITCH" gate="1" pin="1"/>
<wire x1="208.28" y1="22.86" x2="208.28" y2="17.78" width="0.1524" layer="91"/>
<pinref part="POWER_SWITCH" gate="2" pin="S"/>
<wire x1="208.28" y1="17.78" x2="208.28" y2="10.16" width="0.1524" layer="91"/>
<wire x1="208.28" y1="17.78" x2="233.68" y2="17.78" width="0.1524" layer="91"/>
<wire x1="233.68" y1="17.78" x2="233.68" y2="10.16" width="0.1524" layer="91"/>
<junction x="208.28" y="17.78"/>
</segment>
<segment>
<pinref part="P+1" gate="1" pin="+12V"/>
<wire x1="383.54" y1="-27.94" x2="383.54" y2="-20.32" width="0.1524" layer="91"/>
<pinref part="12_POWER_IN" gate="G$1" pin="2"/>
<wire x1="383.54" y1="-20.32" x2="383.54" y2="-15.24" width="0.1524" layer="91"/>
<wire x1="383.54" y1="-27.94" x2="368.3" y2="-27.94" width="0.1524" layer="91"/>
<wire x1="383.54" y1="-20.32" x2="363.22" y2="-20.32" width="0.1524" layer="91"/>
<wire x1="363.22" y1="-20.32" x2="363.22" y2="-10.16" width="0.1524" layer="91"/>
<junction x="383.54" y="-20.32"/>
<pinref part="12V_POWER_OUT" gate="-1" pin="KL"/>
<wire x1="363.22" y1="-10.16" x2="345.44" y2="-10.16" width="0.1524" layer="91"/>
</segment>
</net>
<net name="A" class="0">
<segment>
<pinref part="R8" gate="G$1" pin="1"/>
<wire x1="-274.32" y1="434.34" x2="-276.86" y2="434.34" width="0.1524" layer="91"/>
<label x="-276.86" y="434.34" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="R27" gate="G$1" pin="1"/>
<wire x1="-287.02" y1="500.38" x2="-289.56" y2="500.38" width="0.1524" layer="91"/>
<label x="-289.56" y="500.38" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="R46" gate="G$1" pin="1"/>
<wire x1="-91.44" y1="434.34" x2="-93.98" y2="434.34" width="0.1524" layer="91"/>
<label x="-93.98" y="434.34" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="R65" gate="G$1" pin="1"/>
<wire x1="-104.14" y1="513.08" x2="-106.68" y2="513.08" width="0.1524" layer="91"/>
<label x="-106.68" y="513.08" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="R84" gate="G$1" pin="1"/>
<wire x1="93.98" y1="431.8" x2="91.44" y2="431.8" width="0.1524" layer="91"/>
<label x="91.44" y="431.8" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="R103" gate="G$1" pin="1"/>
<wire x1="81.28" y1="510.54" x2="78.74" y2="510.54" width="0.1524" layer="91"/>
<label x="78.74" y="510.54" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="U$1" gate="G$1" pin="G16"/>
<wire x1="83.82" y1="53.34" x2="86.36" y2="53.34" width="0.1524" layer="91"/>
<label x="86.36" y="53.34" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="R269" gate="G$1" pin="1"/>
<wire x1="-431.8" y1="241.3" x2="-434.34" y2="241.3" width="0.1524" layer="91"/>
<label x="-434.34" y="241.3" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="R126" gate="G$1" pin="1"/>
<wire x1="-307.34" y1="241.3" x2="-309.88" y2="241.3" width="0.1524" layer="91"/>
<label x="-309.88" y="241.3" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="R145" gate="G$1" pin="1"/>
<wire x1="-175.26" y1="248.92" x2="-177.8" y2="248.92" width="0.1524" layer="91"/>
<label x="-177.8" y="248.92" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="R164" gate="G$1" pin="1"/>
<wire x1="-58.42" y1="248.92" x2="-60.96" y2="248.92" width="0.1524" layer="91"/>
<label x="-60.96" y="248.92" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="R293" gate="G$1" pin="1"/>
<wire x1="114.3" y1="259.08" x2="111.76" y2="259.08" width="0.1524" layer="91"/>
<label x="111.76" y="259.08" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="R178" gate="G$1" pin="1"/>
<wire x1="241.3" y1="259.08" x2="238.76" y2="259.08" width="0.1524" layer="91"/>
<label x="238.76" y="259.08" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="R197" gate="G$1" pin="1"/>
<wire x1="365.76" y1="259.08" x2="363.22" y2="259.08" width="0.1524" layer="91"/>
<label x="363.22" y="259.08" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="R216" gate="G$1" pin="1"/>
<wire x1="490.22" y1="259.08" x2="487.68" y2="259.08" width="0.1524" layer="91"/>
<label x="487.68" y="259.08" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="R317" gate="G$1" pin="1"/>
<wire x1="678.18" y1="256.54" x2="675.64" y2="256.54" width="0.1524" layer="91"/>
<label x="675.64" y="256.54" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="R230" gate="G$1" pin="1"/>
<wire x1="810.26" y1="254" x2="807.72" y2="254" width="0.1524" layer="91"/>
<label x="807.72" y="254" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="D" class="0">
<segment>
<pinref part="R9" gate="G$1" pin="1"/>
<wire x1="-274.32" y1="436.88" x2="-276.86" y2="436.88" width="0.1524" layer="91"/>
<label x="-276.86" y="436.88" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="R28" gate="G$1" pin="1"/>
<wire x1="-287.02" y1="502.92" x2="-289.56" y2="502.92" width="0.1524" layer="91"/>
<label x="-289.56" y="502.92" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="R47" gate="G$1" pin="1"/>
<wire x1="-91.44" y1="436.88" x2="-93.98" y2="436.88" width="0.1524" layer="91"/>
<label x="-93.98" y="436.88" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="R66" gate="G$1" pin="1"/>
<wire x1="-104.14" y1="515.62" x2="-106.68" y2="515.62" width="0.1524" layer="91"/>
<label x="-106.68" y="515.62" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="R85" gate="G$1" pin="1"/>
<wire x1="93.98" y1="434.34" x2="91.44" y2="434.34" width="0.1524" layer="91"/>
<label x="91.44" y="434.34" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="R104" gate="G$1" pin="1"/>
<wire x1="81.28" y1="513.08" x2="78.74" y2="513.08" width="0.1524" layer="91"/>
<label x="78.74" y="513.08" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="R268" gate="G$1" pin="1"/>
<wire x1="-431.8" y1="243.84" x2="-434.34" y2="243.84" width="0.1524" layer="91"/>
<label x="-434.34" y="243.84" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="R125" gate="G$1" pin="1"/>
<wire x1="-307.34" y1="243.84" x2="-309.88" y2="243.84" width="0.1524" layer="91"/>
<label x="-309.88" y="243.84" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="R144" gate="G$1" pin="1"/>
<wire x1="-175.26" y1="251.46" x2="-177.8" y2="251.46" width="0.1524" layer="91"/>
<label x="-177.8" y="251.46" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="R163" gate="G$1" pin="1"/>
<wire x1="-58.42" y1="251.46" x2="-60.96" y2="251.46" width="0.1524" layer="91"/>
<label x="-60.96" y="251.46" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="R292" gate="G$1" pin="1"/>
<wire x1="114.3" y1="261.62" x2="111.76" y2="261.62" width="0.1524" layer="91"/>
<label x="111.76" y="261.62" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="R177" gate="G$1" pin="1"/>
<wire x1="241.3" y1="261.62" x2="238.76" y2="261.62" width="0.1524" layer="91"/>
<label x="238.76" y="261.62" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="R196" gate="G$1" pin="1"/>
<wire x1="365.76" y1="261.62" x2="363.22" y2="261.62" width="0.1524" layer="91"/>
<label x="363.22" y="261.62" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="R215" gate="G$1" pin="1"/>
<wire x1="490.22" y1="261.62" x2="487.68" y2="261.62" width="0.1524" layer="91"/>
<label x="487.68" y="261.62" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="R316" gate="G$1" pin="1"/>
<wire x1="678.18" y1="259.08" x2="675.64" y2="259.08" width="0.1524" layer="91"/>
<label x="675.64" y="259.08" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="R229" gate="G$1" pin="1"/>
<wire x1="810.26" y1="256.54" x2="807.72" y2="256.54" width="0.1524" layer="91"/>
<label x="807.72" y="256.54" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="U$1" gate="G$1" pin="G14"/>
<wire x1="53.34" y1="53.34" x2="50.8" y2="53.34" width="0.1524" layer="91"/>
<label x="50.8" y="53.34" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="C" class="0">
<segment>
<pinref part="R10" gate="G$1" pin="1"/>
<wire x1="-274.32" y1="439.42" x2="-276.86" y2="439.42" width="0.1524" layer="91"/>
<label x="-276.86" y="439.42" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="R29" gate="G$1" pin="1"/>
<wire x1="-287.02" y1="505.46" x2="-289.56" y2="505.46" width="0.1524" layer="91"/>
<label x="-289.56" y="505.46" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="R48" gate="G$1" pin="1"/>
<wire x1="-91.44" y1="439.42" x2="-93.98" y2="439.42" width="0.1524" layer="91"/>
<label x="-93.98" y="439.42" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="R67" gate="G$1" pin="1"/>
<wire x1="-104.14" y1="518.16" x2="-106.68" y2="518.16" width="0.1524" layer="91"/>
<label x="-106.68" y="518.16" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="R86" gate="G$1" pin="1"/>
<wire x1="93.98" y1="436.88" x2="91.44" y2="436.88" width="0.1524" layer="91"/>
<label x="91.44" y="436.88" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="R105" gate="G$1" pin="1"/>
<wire x1="81.28" y1="515.62" x2="78.74" y2="515.62" width="0.1524" layer="91"/>
<label x="78.74" y="515.62" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="R267" gate="G$1" pin="1"/>
<wire x1="-431.8" y1="246.38" x2="-434.34" y2="246.38" width="0.1524" layer="91"/>
<label x="-434.34" y="246.38" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="R124" gate="G$1" pin="1"/>
<wire x1="-307.34" y1="246.38" x2="-309.88" y2="246.38" width="0.1524" layer="91"/>
<label x="-309.88" y="246.38" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="R143" gate="G$1" pin="1"/>
<wire x1="-175.26" y1="254" x2="-177.8" y2="254" width="0.1524" layer="91"/>
<label x="-177.8" y="254" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="R162" gate="G$1" pin="1"/>
<wire x1="-58.42" y1="254" x2="-60.96" y2="254" width="0.1524" layer="91"/>
<label x="-60.96" y="254" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="R291" gate="G$1" pin="1"/>
<wire x1="114.3" y1="264.16" x2="111.76" y2="264.16" width="0.1524" layer="91"/>
<label x="111.76" y="264.16" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="R176" gate="G$1" pin="1"/>
<wire x1="241.3" y1="264.16" x2="238.76" y2="264.16" width="0.1524" layer="91"/>
<label x="238.76" y="264.16" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="R195" gate="G$1" pin="1"/>
<wire x1="365.76" y1="264.16" x2="363.22" y2="264.16" width="0.1524" layer="91"/>
<label x="363.22" y="264.16" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="R214" gate="G$1" pin="1"/>
<wire x1="490.22" y1="264.16" x2="487.68" y2="264.16" width="0.1524" layer="91"/>
<label x="487.68" y="264.16" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="R315" gate="G$1" pin="1"/>
<wire x1="678.18" y1="261.62" x2="675.64" y2="261.62" width="0.1524" layer="91"/>
<label x="675.64" y="261.62" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="R228" gate="G$1" pin="1"/>
<wire x1="810.26" y1="259.08" x2="807.72" y2="259.08" width="0.1524" layer="91"/>
<label x="807.72" y="259.08" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="U$1" gate="G$1" pin="G33"/>
<wire x1="53.34" y1="63.5" x2="48.26" y2="63.5" width="0.1524" layer="91"/>
<label x="48.26" y="63.5" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="B" class="0">
<segment>
<pinref part="R11" gate="G$1" pin="1"/>
<wire x1="-274.32" y1="441.96" x2="-276.86" y2="441.96" width="0.1524" layer="91"/>
<label x="-276.86" y="441.96" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="R30" gate="G$1" pin="1"/>
<wire x1="-287.02" y1="508" x2="-289.56" y2="508" width="0.1524" layer="91"/>
<label x="-289.56" y="508" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="R49" gate="G$1" pin="1"/>
<wire x1="-91.44" y1="441.96" x2="-93.98" y2="441.96" width="0.1524" layer="91"/>
<label x="-93.98" y="441.96" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="R68" gate="G$1" pin="1"/>
<wire x1="-104.14" y1="520.7" x2="-106.68" y2="520.7" width="0.1524" layer="91"/>
<label x="-106.68" y="520.7" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="R87" gate="G$1" pin="1"/>
<wire x1="93.98" y1="439.42" x2="91.44" y2="439.42" width="0.1524" layer="91"/>
<label x="91.44" y="439.42" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="R106" gate="G$1" pin="1"/>
<wire x1="81.28" y1="518.16" x2="78.74" y2="518.16" width="0.1524" layer="91"/>
<label x="78.74" y="518.16" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="U$1" gate="G$1" pin="G17"/>
<wire x1="83.82" y1="55.88" x2="91.44" y2="55.88" width="0.1524" layer="91"/>
<label x="91.44" y="55.88" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="R266" gate="G$1" pin="1"/>
<wire x1="-431.8" y1="248.92" x2="-434.34" y2="248.92" width="0.1524" layer="91"/>
<label x="-434.34" y="248.92" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="R123" gate="G$1" pin="1"/>
<wire x1="-307.34" y1="248.92" x2="-309.88" y2="248.92" width="0.1524" layer="91"/>
<label x="-309.88" y="248.92" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="R142" gate="G$1" pin="1"/>
<wire x1="-175.26" y1="256.54" x2="-177.8" y2="256.54" width="0.1524" layer="91"/>
<label x="-177.8" y="256.54" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="R161" gate="G$1" pin="1"/>
<wire x1="-58.42" y1="256.54" x2="-60.96" y2="256.54" width="0.1524" layer="91"/>
<label x="-60.96" y="256.54" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="R290" gate="G$1" pin="1"/>
<wire x1="114.3" y1="266.7" x2="111.76" y2="266.7" width="0.1524" layer="91"/>
<label x="111.76" y="266.7" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="R175" gate="G$1" pin="1"/>
<wire x1="241.3" y1="266.7" x2="238.76" y2="266.7" width="0.1524" layer="91"/>
<label x="238.76" y="266.7" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="R194" gate="G$1" pin="1"/>
<wire x1="365.76" y1="266.7" x2="363.22" y2="266.7" width="0.1524" layer="91"/>
<label x="363.22" y="266.7" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="R213" gate="G$1" pin="1"/>
<wire x1="490.22" y1="266.7" x2="487.68" y2="266.7" width="0.1524" layer="91"/>
<label x="487.68" y="266.7" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="R314" gate="G$1" pin="1"/>
<wire x1="678.18" y1="264.16" x2="675.64" y2="264.16" width="0.1524" layer="91"/>
<label x="675.64" y="264.16" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="R227" gate="G$1" pin="1"/>
<wire x1="810.26" y1="261.62" x2="807.72" y2="261.62" width="0.1524" layer="91"/>
<label x="807.72" y="261.62" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="HOUR_A_LD" class="0">
<segment>
<pinref part="R12" gate="G$1" pin="1"/>
<wire x1="-274.32" y1="444.5" x2="-276.86" y2="444.5" width="0.1524" layer="91"/>
<label x="-276.86" y="444.5" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="IC1" gate="G$1" pin="GPA0"/>
<wire x1="165.1" y1="78.74" x2="167.64" y2="78.74" width="0.1524" layer="91"/>
<label x="167.64" y="78.74" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="N$34" class="0">
<segment>
<pinref part="R20" gate="G$1" pin="1"/>
<pinref part="4543_HOUR_B" gate="A" pin="F"/>
<wire x1="-246.38" y1="508" x2="-248.92" y2="508" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$35" class="0">
<segment>
<pinref part="R21" gate="G$1" pin="1"/>
<pinref part="4543_HOUR_B" gate="A" pin="G"/>
<wire x1="-246.38" y1="505.46" x2="-248.92" y2="505.46" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$36" class="0">
<segment>
<pinref part="R22" gate="G$1" pin="1"/>
<pinref part="4543_HOUR_B" gate="A" pin="E"/>
<wire x1="-246.38" y1="502.92" x2="-248.92" y2="502.92" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$37" class="0">
<segment>
<pinref part="R23" gate="G$1" pin="1"/>
<pinref part="4543_HOUR_B" gate="A" pin="D"/>
<wire x1="-246.38" y1="500.38" x2="-248.92" y2="500.38" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$38" class="0">
<segment>
<pinref part="R24" gate="G$1" pin="1"/>
<pinref part="4543_HOUR_B" gate="A" pin="C"/>
<wire x1="-246.38" y1="497.84" x2="-248.92" y2="497.84" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$39" class="0">
<segment>
<pinref part="R25" gate="G$1" pin="1"/>
<pinref part="4543_HOUR_B" gate="A" pin="B"/>
<wire x1="-246.38" y1="495.3" x2="-248.92" y2="495.3" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$40" class="0">
<segment>
<pinref part="R26" gate="G$1" pin="1"/>
<pinref part="4543_HOUR_B" gate="A" pin="A"/>
<wire x1="-246.38" y1="492.76" x2="-248.92" y2="492.76" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$41" class="0">
<segment>
<pinref part="R26" gate="G$1" pin="2"/>
<pinref part="2003_HOUR_B" gate="A" pin="I7"/>
<wire x1="-236.22" y1="492.76" x2="-233.68" y2="492.76" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$42" class="0">
<segment>
<pinref part="R25" gate="G$1" pin="2"/>
<pinref part="2003_HOUR_B" gate="A" pin="I6"/>
<wire x1="-236.22" y1="495.3" x2="-233.68" y2="495.3" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$43" class="0">
<segment>
<pinref part="R24" gate="G$1" pin="2"/>
<pinref part="2003_HOUR_B" gate="A" pin="I5"/>
<wire x1="-236.22" y1="497.84" x2="-233.68" y2="497.84" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$44" class="0">
<segment>
<pinref part="R23" gate="G$1" pin="2"/>
<pinref part="2003_HOUR_B" gate="A" pin="I4"/>
<wire x1="-236.22" y1="500.38" x2="-233.68" y2="500.38" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$45" class="0">
<segment>
<pinref part="R22" gate="G$1" pin="2"/>
<pinref part="2003_HOUR_B" gate="A" pin="I3"/>
<wire x1="-236.22" y1="502.92" x2="-233.68" y2="502.92" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$46" class="0">
<segment>
<pinref part="R21" gate="G$1" pin="2"/>
<pinref part="2003_HOUR_B" gate="A" pin="I2"/>
<wire x1="-236.22" y1="505.46" x2="-233.68" y2="505.46" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$47" class="0">
<segment>
<pinref part="R20" gate="G$1" pin="2"/>
<pinref part="2003_HOUR_B" gate="A" pin="I1"/>
<wire x1="-236.22" y1="508" x2="-233.68" y2="508" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$48" class="0">
<segment>
<pinref part="R31" gate="G$1" pin="2"/>
<pinref part="4543_HOUR_B" gate="A" pin="LD"/>
<wire x1="-276.86" y1="510.54" x2="-274.32" y2="510.54" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$49" class="0">
<segment>
<pinref part="R30" gate="G$1" pin="2"/>
<pinref part="4543_HOUR_B" gate="A" pin="IB"/>
<wire x1="-276.86" y1="508" x2="-274.32" y2="508" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$50" class="0">
<segment>
<pinref part="R29" gate="G$1" pin="2"/>
<pinref part="4543_HOUR_B" gate="A" pin="IC"/>
<wire x1="-276.86" y1="505.46" x2="-274.32" y2="505.46" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$51" class="0">
<segment>
<pinref part="R28" gate="G$1" pin="2"/>
<pinref part="4543_HOUR_B" gate="A" pin="ID"/>
<wire x1="-276.86" y1="502.92" x2="-274.32" y2="502.92" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$52" class="0">
<segment>
<pinref part="R27" gate="G$1" pin="2"/>
<pinref part="4543_HOUR_B" gate="A" pin="IA"/>
<wire x1="-276.86" y1="500.38" x2="-274.32" y2="500.38" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$53" class="0">
<segment>
<pinref part="2003_HOUR_B" gate="A" pin="O1"/>
<pinref part="R32" gate="G$1" pin="1"/>
<wire x1="-208.28" y1="508" x2="-205.74" y2="508" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$54" class="0">
<segment>
<pinref part="2003_HOUR_B" gate="A" pin="O2"/>
<pinref part="R33" gate="G$1" pin="1"/>
<wire x1="-208.28" y1="505.46" x2="-205.74" y2="505.46" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$55" class="0">
<segment>
<pinref part="2003_HOUR_B" gate="A" pin="O3"/>
<pinref part="R34" gate="G$1" pin="1"/>
<wire x1="-208.28" y1="502.92" x2="-205.74" y2="502.92" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$56" class="0">
<segment>
<pinref part="2003_HOUR_B" gate="A" pin="O4"/>
<pinref part="R35" gate="G$1" pin="1"/>
<wire x1="-208.28" y1="500.38" x2="-205.74" y2="500.38" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$57" class="0">
<segment>
<pinref part="2003_HOUR_B" gate="A" pin="O5"/>
<pinref part="R36" gate="G$1" pin="1"/>
<wire x1="-208.28" y1="497.84" x2="-205.74" y2="497.84" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$58" class="0">
<segment>
<pinref part="2003_HOUR_B" gate="A" pin="O6"/>
<pinref part="R37" gate="G$1" pin="1"/>
<wire x1="-208.28" y1="495.3" x2="-205.74" y2="495.3" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$59" class="0">
<segment>
<pinref part="2003_HOUR_B" gate="A" pin="O7"/>
<pinref part="R38" gate="G$1" pin="1"/>
<wire x1="-208.28" y1="492.76" x2="-205.74" y2="492.76" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$60" class="0">
<segment>
<pinref part="R38" gate="G$1" pin="2"/>
<pinref part="HOUR_B" gate="G$1" pin="A"/>
<wire x1="-195.58" y1="492.76" x2="-144.78" y2="492.76" width="0.1524" layer="91"/>
<wire x1="-144.78" y1="492.76" x2="-144.78" y2="406.4" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$61" class="0">
<segment>
<pinref part="R37" gate="G$1" pin="2"/>
<wire x1="-195.58" y1="495.3" x2="-147.32" y2="495.3" width="0.1524" layer="91"/>
<wire x1="-147.32" y1="495.3" x2="-147.32" y2="403.86" width="0.1524" layer="91"/>
<pinref part="HOUR_B" gate="G$1" pin="B"/>
<wire x1="-147.32" y1="403.86" x2="-144.78" y2="403.86" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$62" class="0">
<segment>
<pinref part="R36" gate="G$1" pin="2"/>
<wire x1="-195.58" y1="497.84" x2="-149.86" y2="497.84" width="0.1524" layer="91"/>
<wire x1="-149.86" y1="497.84" x2="-149.86" y2="401.32" width="0.1524" layer="91"/>
<pinref part="HOUR_B" gate="G$1" pin="C"/>
<wire x1="-149.86" y1="401.32" x2="-144.78" y2="401.32" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$63" class="0">
<segment>
<pinref part="R35" gate="G$1" pin="2"/>
<wire x1="-195.58" y1="500.38" x2="-152.4" y2="500.38" width="0.1524" layer="91"/>
<wire x1="-152.4" y1="500.38" x2="-152.4" y2="398.78" width="0.1524" layer="91"/>
<pinref part="HOUR_B" gate="G$1" pin="D"/>
<wire x1="-152.4" y1="398.78" x2="-144.78" y2="398.78" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$64" class="0">
<segment>
<pinref part="R34" gate="G$1" pin="2"/>
<wire x1="-195.58" y1="502.92" x2="-154.94" y2="502.92" width="0.1524" layer="91"/>
<wire x1="-154.94" y1="502.92" x2="-154.94" y2="396.24" width="0.1524" layer="91"/>
<pinref part="HOUR_B" gate="G$1" pin="E"/>
<wire x1="-154.94" y1="396.24" x2="-144.78" y2="396.24" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$65" class="0">
<segment>
<pinref part="R33" gate="G$1" pin="2"/>
<wire x1="-195.58" y1="505.46" x2="-127" y2="505.46" width="0.1524" layer="91"/>
<wire x1="-127" y1="505.46" x2="-127" y2="403.86" width="0.1524" layer="91"/>
<pinref part="HOUR_B" gate="G$1" pin="G"/>
<wire x1="-127" y1="403.86" x2="-129.54" y2="403.86" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$66" class="0">
<segment>
<pinref part="R32" gate="G$1" pin="2"/>
<wire x1="-195.58" y1="508" x2="-121.92" y2="508" width="0.1524" layer="91"/>
<wire x1="-121.92" y1="508" x2="-121.92" y2="406.4" width="0.1524" layer="91"/>
<pinref part="HOUR_B" gate="G$1" pin="F"/>
<wire x1="-121.92" y1="406.4" x2="-129.54" y2="406.4" width="0.1524" layer="91"/>
</segment>
</net>
<net name="HOUR_B_LD" class="0">
<segment>
<pinref part="R31" gate="G$1" pin="1"/>
<wire x1="-287.02" y1="510.54" x2="-289.56" y2="510.54" width="0.1524" layer="91"/>
<label x="-289.56" y="510.54" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="IC1" gate="G$1" pin="GPA1"/>
<wire x1="165.1" y1="76.2" x2="185.42" y2="76.2" width="0.1524" layer="91"/>
<label x="185.42" y="76.2" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="N$67" class="0">
<segment>
<pinref part="R39" gate="G$1" pin="1"/>
<pinref part="4543_MIN_A" gate="A" pin="F"/>
<wire x1="-50.8" y1="441.96" x2="-53.34" y2="441.96" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$68" class="0">
<segment>
<pinref part="R40" gate="G$1" pin="1"/>
<pinref part="4543_MIN_A" gate="A" pin="G"/>
<wire x1="-50.8" y1="439.42" x2="-53.34" y2="439.42" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$69" class="0">
<segment>
<pinref part="R41" gate="G$1" pin="1"/>
<pinref part="4543_MIN_A" gate="A" pin="E"/>
<wire x1="-50.8" y1="436.88" x2="-53.34" y2="436.88" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$70" class="0">
<segment>
<pinref part="R42" gate="G$1" pin="1"/>
<pinref part="4543_MIN_A" gate="A" pin="D"/>
<wire x1="-50.8" y1="434.34" x2="-53.34" y2="434.34" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$71" class="0">
<segment>
<pinref part="R43" gate="G$1" pin="1"/>
<pinref part="4543_MIN_A" gate="A" pin="C"/>
<wire x1="-50.8" y1="431.8" x2="-53.34" y2="431.8" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$72" class="0">
<segment>
<pinref part="R44" gate="G$1" pin="1"/>
<pinref part="4543_MIN_A" gate="A" pin="B"/>
<wire x1="-50.8" y1="429.26" x2="-53.34" y2="429.26" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$73" class="0">
<segment>
<pinref part="R45" gate="G$1" pin="1"/>
<pinref part="4543_MIN_A" gate="A" pin="A"/>
<wire x1="-50.8" y1="426.72" x2="-53.34" y2="426.72" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$74" class="0">
<segment>
<pinref part="R45" gate="G$1" pin="2"/>
<pinref part="2003_MIN_A" gate="A" pin="I7"/>
<wire x1="-40.64" y1="426.72" x2="-38.1" y2="426.72" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$75" class="0">
<segment>
<pinref part="R44" gate="G$1" pin="2"/>
<pinref part="2003_MIN_A" gate="A" pin="I6"/>
<wire x1="-40.64" y1="429.26" x2="-38.1" y2="429.26" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$76" class="0">
<segment>
<pinref part="R43" gate="G$1" pin="2"/>
<pinref part="2003_MIN_A" gate="A" pin="I5"/>
<wire x1="-40.64" y1="431.8" x2="-38.1" y2="431.8" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$77" class="0">
<segment>
<pinref part="R42" gate="G$1" pin="2"/>
<pinref part="2003_MIN_A" gate="A" pin="I4"/>
<wire x1="-40.64" y1="434.34" x2="-38.1" y2="434.34" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$78" class="0">
<segment>
<pinref part="R41" gate="G$1" pin="2"/>
<pinref part="2003_MIN_A" gate="A" pin="I3"/>
<wire x1="-40.64" y1="436.88" x2="-38.1" y2="436.88" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$79" class="0">
<segment>
<pinref part="R40" gate="G$1" pin="2"/>
<pinref part="2003_MIN_A" gate="A" pin="I2"/>
<wire x1="-40.64" y1="439.42" x2="-38.1" y2="439.42" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$80" class="0">
<segment>
<pinref part="R39" gate="G$1" pin="2"/>
<pinref part="2003_MIN_A" gate="A" pin="I1"/>
<wire x1="-40.64" y1="441.96" x2="-38.1" y2="441.96" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$81" class="0">
<segment>
<pinref part="R50" gate="G$1" pin="2"/>
<pinref part="4543_MIN_A" gate="A" pin="LD"/>
<wire x1="-81.28" y1="444.5" x2="-78.74" y2="444.5" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$82" class="0">
<segment>
<pinref part="R49" gate="G$1" pin="2"/>
<pinref part="4543_MIN_A" gate="A" pin="IB"/>
<wire x1="-81.28" y1="441.96" x2="-78.74" y2="441.96" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$83" class="0">
<segment>
<pinref part="R48" gate="G$1" pin="2"/>
<pinref part="4543_MIN_A" gate="A" pin="IC"/>
<wire x1="-81.28" y1="439.42" x2="-78.74" y2="439.42" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$84" class="0">
<segment>
<pinref part="R47" gate="G$1" pin="2"/>
<pinref part="4543_MIN_A" gate="A" pin="ID"/>
<wire x1="-81.28" y1="436.88" x2="-78.74" y2="436.88" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$85" class="0">
<segment>
<pinref part="R46" gate="G$1" pin="2"/>
<pinref part="4543_MIN_A" gate="A" pin="IA"/>
<wire x1="-81.28" y1="434.34" x2="-78.74" y2="434.34" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$86" class="0">
<segment>
<pinref part="2003_MIN_A" gate="A" pin="O1"/>
<pinref part="R51" gate="G$1" pin="1"/>
<wire x1="-12.7" y1="441.96" x2="-10.16" y2="441.96" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$87" class="0">
<segment>
<pinref part="2003_MIN_A" gate="A" pin="O2"/>
<pinref part="R52" gate="G$1" pin="1"/>
<wire x1="-12.7" y1="439.42" x2="-10.16" y2="439.42" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$88" class="0">
<segment>
<pinref part="2003_MIN_A" gate="A" pin="O3"/>
<pinref part="R53" gate="G$1" pin="1"/>
<wire x1="-12.7" y1="436.88" x2="-10.16" y2="436.88" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$89" class="0">
<segment>
<pinref part="2003_MIN_A" gate="A" pin="O4"/>
<pinref part="R54" gate="G$1" pin="1"/>
<wire x1="-12.7" y1="434.34" x2="-10.16" y2="434.34" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$90" class="0">
<segment>
<pinref part="2003_MIN_A" gate="A" pin="O5"/>
<pinref part="R55" gate="G$1" pin="1"/>
<wire x1="-12.7" y1="431.8" x2="-10.16" y2="431.8" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$91" class="0">
<segment>
<pinref part="2003_MIN_A" gate="A" pin="O6"/>
<pinref part="R56" gate="G$1" pin="1"/>
<wire x1="-12.7" y1="429.26" x2="-10.16" y2="429.26" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$92" class="0">
<segment>
<pinref part="2003_MIN_A" gate="A" pin="O7"/>
<pinref part="R57" gate="G$1" pin="1"/>
<wire x1="-12.7" y1="426.72" x2="-10.16" y2="426.72" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$93" class="0">
<segment>
<wire x1="2.54" y1="419.1" x2="-2.54" y2="419.1" width="0.1524" layer="91"/>
<wire x1="-2.54" y1="419.1" x2="-2.54" y2="406.4" width="0.1524" layer="91"/>
<pinref part="MIN_A" gate="G$1" pin="A"/>
<pinref part="R57" gate="G$1" pin="2"/>
<wire x1="0" y1="426.72" x2="2.54" y2="426.72" width="0.1524" layer="91"/>
<wire x1="2.54" y1="426.72" x2="2.54" y2="419.1" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$94" class="0">
<segment>
<wire x1="5.08" y1="416.56" x2="-5.08" y2="416.56" width="0.1524" layer="91"/>
<wire x1="-5.08" y1="416.56" x2="-5.08" y2="403.86" width="0.1524" layer="91"/>
<wire x1="-5.08" y1="403.86" x2="-2.54" y2="403.86" width="0.1524" layer="91"/>
<pinref part="MIN_A" gate="G$1" pin="B"/>
<pinref part="R56" gate="G$1" pin="2"/>
<wire x1="0" y1="429.26" x2="5.08" y2="429.26" width="0.1524" layer="91"/>
<wire x1="5.08" y1="429.26" x2="5.08" y2="416.56" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$95" class="0">
<segment>
<wire x1="7.62" y1="414.02" x2="-7.62" y2="414.02" width="0.1524" layer="91"/>
<wire x1="-7.62" y1="414.02" x2="-7.62" y2="401.32" width="0.1524" layer="91"/>
<wire x1="-7.62" y1="401.32" x2="-2.54" y2="401.32" width="0.1524" layer="91"/>
<pinref part="MIN_A" gate="G$1" pin="C"/>
<pinref part="R55" gate="G$1" pin="2"/>
<wire x1="0" y1="431.8" x2="7.62" y2="431.8" width="0.1524" layer="91"/>
<wire x1="7.62" y1="431.8" x2="7.62" y2="414.02" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$96" class="0">
<segment>
<wire x1="10.16" y1="411.48" x2="-10.16" y2="411.48" width="0.1524" layer="91"/>
<wire x1="-10.16" y1="411.48" x2="-10.16" y2="398.78" width="0.1524" layer="91"/>
<wire x1="-10.16" y1="398.78" x2="-2.54" y2="398.78" width="0.1524" layer="91"/>
<pinref part="MIN_A" gate="G$1" pin="D"/>
<pinref part="R54" gate="G$1" pin="2"/>
<wire x1="0" y1="434.34" x2="10.16" y2="434.34" width="0.1524" layer="91"/>
<wire x1="10.16" y1="434.34" x2="10.16" y2="411.48" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$97" class="0">
<segment>
<wire x1="12.7" y1="421.64" x2="-12.7" y2="421.64" width="0.1524" layer="91"/>
<wire x1="-12.7" y1="421.64" x2="-12.7" y2="396.24" width="0.1524" layer="91"/>
<wire x1="-12.7" y1="396.24" x2="-2.54" y2="396.24" width="0.1524" layer="91"/>
<pinref part="MIN_A" gate="G$1" pin="E"/>
<pinref part="R53" gate="G$1" pin="2"/>
<wire x1="0" y1="436.88" x2="12.7" y2="436.88" width="0.1524" layer="91"/>
<wire x1="12.7" y1="436.88" x2="12.7" y2="421.64" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$98" class="0">
<segment>
<pinref part="R51" gate="G$1" pin="2"/>
<wire x1="0" y1="441.96" x2="20.32" y2="441.96" width="0.1524" layer="91"/>
<wire x1="20.32" y1="441.96" x2="20.32" y2="406.4" width="0.1524" layer="91"/>
<pinref part="MIN_A" gate="G$1" pin="F"/>
<wire x1="20.32" y1="406.4" x2="12.7" y2="406.4" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$99" class="0">
<segment>
<pinref part="R52" gate="G$1" pin="2"/>
<wire x1="0" y1="439.42" x2="17.78" y2="439.42" width="0.1524" layer="91"/>
<wire x1="17.78" y1="439.42" x2="17.78" y2="403.86" width="0.1524" layer="91"/>
<pinref part="MIN_A" gate="G$1" pin="G"/>
<wire x1="17.78" y1="403.86" x2="12.7" y2="403.86" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$100" class="0">
<segment>
<pinref part="R58" gate="G$1" pin="1"/>
<pinref part="4543_MIN_B" gate="A" pin="F"/>
<wire x1="-63.5" y1="520.7" x2="-66.04" y2="520.7" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$101" class="0">
<segment>
<pinref part="R59" gate="G$1" pin="1"/>
<pinref part="4543_MIN_B" gate="A" pin="G"/>
<wire x1="-63.5" y1="518.16" x2="-66.04" y2="518.16" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$102" class="0">
<segment>
<pinref part="R60" gate="G$1" pin="1"/>
<pinref part="4543_MIN_B" gate="A" pin="E"/>
<wire x1="-63.5" y1="515.62" x2="-66.04" y2="515.62" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$103" class="0">
<segment>
<pinref part="R61" gate="G$1" pin="1"/>
<pinref part="4543_MIN_B" gate="A" pin="D"/>
<wire x1="-63.5" y1="513.08" x2="-66.04" y2="513.08" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$104" class="0">
<segment>
<pinref part="R62" gate="G$1" pin="1"/>
<pinref part="4543_MIN_B" gate="A" pin="C"/>
<wire x1="-63.5" y1="510.54" x2="-66.04" y2="510.54" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$105" class="0">
<segment>
<pinref part="R63" gate="G$1" pin="1"/>
<pinref part="4543_MIN_B" gate="A" pin="B"/>
<wire x1="-63.5" y1="508" x2="-66.04" y2="508" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$106" class="0">
<segment>
<pinref part="R64" gate="G$1" pin="1"/>
<pinref part="4543_MIN_B" gate="A" pin="A"/>
<wire x1="-63.5" y1="505.46" x2="-66.04" y2="505.46" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$107" class="0">
<segment>
<pinref part="R64" gate="G$1" pin="2"/>
<pinref part="2003_MIN_B" gate="A" pin="I7"/>
<wire x1="-53.34" y1="505.46" x2="-50.8" y2="505.46" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$108" class="0">
<segment>
<pinref part="R63" gate="G$1" pin="2"/>
<pinref part="2003_MIN_B" gate="A" pin="I6"/>
<wire x1="-53.34" y1="508" x2="-50.8" y2="508" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$109" class="0">
<segment>
<pinref part="R62" gate="G$1" pin="2"/>
<pinref part="2003_MIN_B" gate="A" pin="I5"/>
<wire x1="-53.34" y1="510.54" x2="-50.8" y2="510.54" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$110" class="0">
<segment>
<pinref part="R61" gate="G$1" pin="2"/>
<pinref part="2003_MIN_B" gate="A" pin="I4"/>
<wire x1="-53.34" y1="513.08" x2="-50.8" y2="513.08" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$111" class="0">
<segment>
<pinref part="R60" gate="G$1" pin="2"/>
<pinref part="2003_MIN_B" gate="A" pin="I3"/>
<wire x1="-53.34" y1="515.62" x2="-50.8" y2="515.62" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$112" class="0">
<segment>
<pinref part="R59" gate="G$1" pin="2"/>
<pinref part="2003_MIN_B" gate="A" pin="I2"/>
<wire x1="-53.34" y1="518.16" x2="-50.8" y2="518.16" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$113" class="0">
<segment>
<pinref part="R58" gate="G$1" pin="2"/>
<pinref part="2003_MIN_B" gate="A" pin="I1"/>
<wire x1="-53.34" y1="520.7" x2="-50.8" y2="520.7" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$114" class="0">
<segment>
<pinref part="R69" gate="G$1" pin="2"/>
<pinref part="4543_MIN_B" gate="A" pin="LD"/>
<wire x1="-93.98" y1="523.24" x2="-91.44" y2="523.24" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$115" class="0">
<segment>
<pinref part="R68" gate="G$1" pin="2"/>
<pinref part="4543_MIN_B" gate="A" pin="IB"/>
<wire x1="-93.98" y1="520.7" x2="-91.44" y2="520.7" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$116" class="0">
<segment>
<pinref part="R67" gate="G$1" pin="2"/>
<pinref part="4543_MIN_B" gate="A" pin="IC"/>
<wire x1="-93.98" y1="518.16" x2="-91.44" y2="518.16" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$117" class="0">
<segment>
<pinref part="R66" gate="G$1" pin="2"/>
<pinref part="4543_MIN_B" gate="A" pin="ID"/>
<wire x1="-93.98" y1="515.62" x2="-91.44" y2="515.62" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$118" class="0">
<segment>
<pinref part="R65" gate="G$1" pin="2"/>
<pinref part="4543_MIN_B" gate="A" pin="IA"/>
<wire x1="-93.98" y1="513.08" x2="-91.44" y2="513.08" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$119" class="0">
<segment>
<pinref part="2003_MIN_B" gate="A" pin="O1"/>
<pinref part="R70" gate="G$1" pin="1"/>
<wire x1="-25.4" y1="520.7" x2="-22.86" y2="520.7" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$120" class="0">
<segment>
<pinref part="2003_MIN_B" gate="A" pin="O2"/>
<pinref part="R71" gate="G$1" pin="1"/>
<wire x1="-25.4" y1="518.16" x2="-22.86" y2="518.16" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$121" class="0">
<segment>
<pinref part="2003_MIN_B" gate="A" pin="O3"/>
<pinref part="R72" gate="G$1" pin="1"/>
<wire x1="-25.4" y1="515.62" x2="-22.86" y2="515.62" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$122" class="0">
<segment>
<pinref part="2003_MIN_B" gate="A" pin="O4"/>
<pinref part="R73" gate="G$1" pin="1"/>
<wire x1="-25.4" y1="513.08" x2="-22.86" y2="513.08" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$123" class="0">
<segment>
<pinref part="2003_MIN_B" gate="A" pin="O5"/>
<pinref part="R74" gate="G$1" pin="1"/>
<wire x1="-25.4" y1="510.54" x2="-22.86" y2="510.54" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$124" class="0">
<segment>
<pinref part="2003_MIN_B" gate="A" pin="O6"/>
<pinref part="R75" gate="G$1" pin="1"/>
<wire x1="-25.4" y1="508" x2="-22.86" y2="508" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$125" class="0">
<segment>
<pinref part="2003_MIN_B" gate="A" pin="O7"/>
<pinref part="R76" gate="G$1" pin="1"/>
<wire x1="-25.4" y1="505.46" x2="-22.86" y2="505.46" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$126" class="0">
<segment>
<pinref part="R76" gate="G$1" pin="2"/>
<wire x1="-12.7" y1="505.46" x2="38.1" y2="505.46" width="0.1524" layer="91"/>
<wire x1="38.1" y1="505.46" x2="38.1" y2="406.4" width="0.1524" layer="91"/>
<pinref part="MIN_B" gate="G$1" pin="A"/>
</segment>
</net>
<net name="N$127" class="0">
<segment>
<pinref part="R75" gate="G$1" pin="2"/>
<wire x1="-12.7" y1="508" x2="35.56" y2="508" width="0.1524" layer="91"/>
<wire x1="35.56" y1="508" x2="35.56" y2="403.86" width="0.1524" layer="91"/>
<pinref part="MIN_B" gate="G$1" pin="B"/>
<wire x1="35.56" y1="403.86" x2="38.1" y2="403.86" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$128" class="0">
<segment>
<pinref part="R74" gate="G$1" pin="2"/>
<wire x1="-12.7" y1="510.54" x2="33.02" y2="510.54" width="0.1524" layer="91"/>
<wire x1="33.02" y1="510.54" x2="33.02" y2="401.32" width="0.1524" layer="91"/>
<pinref part="MIN_B" gate="G$1" pin="C"/>
<wire x1="33.02" y1="401.32" x2="38.1" y2="401.32" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$129" class="0">
<segment>
<pinref part="R73" gate="G$1" pin="2"/>
<wire x1="-12.7" y1="513.08" x2="30.48" y2="513.08" width="0.1524" layer="91"/>
<wire x1="30.48" y1="513.08" x2="30.48" y2="398.78" width="0.1524" layer="91"/>
<pinref part="MIN_B" gate="G$1" pin="D"/>
<wire x1="30.48" y1="398.78" x2="38.1" y2="398.78" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$130" class="0">
<segment>
<pinref part="R72" gate="G$1" pin="2"/>
<wire x1="-12.7" y1="515.62" x2="27.94" y2="515.62" width="0.1524" layer="91"/>
<wire x1="27.94" y1="515.62" x2="27.94" y2="396.24" width="0.1524" layer="91"/>
<pinref part="MIN_B" gate="G$1" pin="E"/>
<wire x1="27.94" y1="396.24" x2="38.1" y2="396.24" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$131" class="0">
<segment>
<pinref part="R71" gate="G$1" pin="2"/>
<wire x1="-12.7" y1="518.16" x2="55.88" y2="518.16" width="0.1524" layer="91"/>
<wire x1="55.88" y1="518.16" x2="55.88" y2="403.86" width="0.1524" layer="91"/>
<pinref part="MIN_B" gate="G$1" pin="G"/>
<wire x1="55.88" y1="403.86" x2="53.34" y2="403.86" width="0.1524" layer="91"/>
</segment>
</net>
<net name="MIN_B_LD" class="0">
<segment>
<pinref part="R69" gate="G$1" pin="1"/>
<wire x1="-104.14" y1="523.24" x2="-106.68" y2="523.24" width="0.1524" layer="91"/>
<label x="-106.68" y="523.24" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="IC1" gate="G$1" pin="GPA3"/>
<wire x1="165.1" y1="71.12" x2="185.42" y2="71.12" width="0.1524" layer="91"/>
<label x="185.42" y="71.12" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="MIN_A_LD" class="0">
<segment>
<pinref part="R50" gate="G$1" pin="1"/>
<wire x1="-91.44" y1="444.5" x2="-93.98" y2="444.5" width="0.1524" layer="91"/>
<label x="-93.98" y="444.5" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="IC1" gate="G$1" pin="GPA2"/>
<wire x1="165.1" y1="73.66" x2="167.64" y2="73.66" width="0.1524" layer="91"/>
<label x="167.64" y="73.66" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="N$134" class="0">
<segment>
<pinref part="R77" gate="G$1" pin="1"/>
<pinref part="4543_SEC_A" gate="A" pin="F"/>
<wire x1="134.62" y1="439.42" x2="132.08" y2="439.42" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$135" class="0">
<segment>
<pinref part="R78" gate="G$1" pin="1"/>
<pinref part="4543_SEC_A" gate="A" pin="G"/>
<wire x1="134.62" y1="436.88" x2="132.08" y2="436.88" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$136" class="0">
<segment>
<pinref part="R79" gate="G$1" pin="1"/>
<pinref part="4543_SEC_A" gate="A" pin="E"/>
<wire x1="134.62" y1="434.34" x2="132.08" y2="434.34" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$137" class="0">
<segment>
<pinref part="R80" gate="G$1" pin="1"/>
<pinref part="4543_SEC_A" gate="A" pin="D"/>
<wire x1="134.62" y1="431.8" x2="132.08" y2="431.8" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$138" class="0">
<segment>
<pinref part="R81" gate="G$1" pin="1"/>
<pinref part="4543_SEC_A" gate="A" pin="C"/>
<wire x1="134.62" y1="429.26" x2="132.08" y2="429.26" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$139" class="0">
<segment>
<pinref part="R82" gate="G$1" pin="1"/>
<pinref part="4543_SEC_A" gate="A" pin="B"/>
<wire x1="134.62" y1="426.72" x2="132.08" y2="426.72" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$140" class="0">
<segment>
<pinref part="R83" gate="G$1" pin="1"/>
<pinref part="4543_SEC_A" gate="A" pin="A"/>
<wire x1="134.62" y1="424.18" x2="132.08" y2="424.18" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$141" class="0">
<segment>
<pinref part="R83" gate="G$1" pin="2"/>
<pinref part="2003_SEC_A" gate="A" pin="I7"/>
<wire x1="144.78" y1="424.18" x2="147.32" y2="424.18" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$142" class="0">
<segment>
<pinref part="R82" gate="G$1" pin="2"/>
<pinref part="2003_SEC_A" gate="A" pin="I6"/>
<wire x1="144.78" y1="426.72" x2="147.32" y2="426.72" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$143" class="0">
<segment>
<pinref part="R81" gate="G$1" pin="2"/>
<pinref part="2003_SEC_A" gate="A" pin="I5"/>
<wire x1="144.78" y1="429.26" x2="147.32" y2="429.26" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$144" class="0">
<segment>
<pinref part="R80" gate="G$1" pin="2"/>
<pinref part="2003_SEC_A" gate="A" pin="I4"/>
<wire x1="144.78" y1="431.8" x2="147.32" y2="431.8" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$145" class="0">
<segment>
<pinref part="R79" gate="G$1" pin="2"/>
<pinref part="2003_SEC_A" gate="A" pin="I3"/>
<wire x1="144.78" y1="434.34" x2="147.32" y2="434.34" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$146" class="0">
<segment>
<pinref part="R78" gate="G$1" pin="2"/>
<pinref part="2003_SEC_A" gate="A" pin="I2"/>
<wire x1="144.78" y1="436.88" x2="147.32" y2="436.88" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$147" class="0">
<segment>
<pinref part="R77" gate="G$1" pin="2"/>
<pinref part="2003_SEC_A" gate="A" pin="I1"/>
<wire x1="144.78" y1="439.42" x2="147.32" y2="439.42" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$148" class="0">
<segment>
<pinref part="R88" gate="G$1" pin="2"/>
<pinref part="4543_SEC_A" gate="A" pin="LD"/>
<wire x1="104.14" y1="441.96" x2="106.68" y2="441.96" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$149" class="0">
<segment>
<pinref part="R87" gate="G$1" pin="2"/>
<pinref part="4543_SEC_A" gate="A" pin="IB"/>
<wire x1="104.14" y1="439.42" x2="106.68" y2="439.42" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$150" class="0">
<segment>
<pinref part="R86" gate="G$1" pin="2"/>
<pinref part="4543_SEC_A" gate="A" pin="IC"/>
<wire x1="104.14" y1="436.88" x2="106.68" y2="436.88" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$151" class="0">
<segment>
<pinref part="R85" gate="G$1" pin="2"/>
<pinref part="4543_SEC_A" gate="A" pin="ID"/>
<wire x1="104.14" y1="434.34" x2="106.68" y2="434.34" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$152" class="0">
<segment>
<pinref part="R84" gate="G$1" pin="2"/>
<pinref part="4543_SEC_A" gate="A" pin="IA"/>
<wire x1="104.14" y1="431.8" x2="106.68" y2="431.8" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$153" class="0">
<segment>
<pinref part="2003_SEC_A" gate="A" pin="O1"/>
<pinref part="R89" gate="G$1" pin="1"/>
<wire x1="172.72" y1="439.42" x2="175.26" y2="439.42" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$154" class="0">
<segment>
<pinref part="2003_SEC_A" gate="A" pin="O2"/>
<pinref part="R90" gate="G$1" pin="1"/>
<wire x1="172.72" y1="436.88" x2="175.26" y2="436.88" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$155" class="0">
<segment>
<pinref part="2003_SEC_A" gate="A" pin="O3"/>
<pinref part="R91" gate="G$1" pin="1"/>
<wire x1="172.72" y1="434.34" x2="175.26" y2="434.34" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$156" class="0">
<segment>
<pinref part="2003_SEC_A" gate="A" pin="O4"/>
<pinref part="R92" gate="G$1" pin="1"/>
<wire x1="172.72" y1="431.8" x2="175.26" y2="431.8" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$157" class="0">
<segment>
<pinref part="2003_SEC_A" gate="A" pin="O5"/>
<pinref part="R93" gate="G$1" pin="1"/>
<wire x1="172.72" y1="429.26" x2="175.26" y2="429.26" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$158" class="0">
<segment>
<pinref part="2003_SEC_A" gate="A" pin="O6"/>
<pinref part="R94" gate="G$1" pin="1"/>
<wire x1="172.72" y1="426.72" x2="175.26" y2="426.72" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$159" class="0">
<segment>
<pinref part="2003_SEC_A" gate="A" pin="O7"/>
<pinref part="R95" gate="G$1" pin="1"/>
<wire x1="172.72" y1="424.18" x2="175.26" y2="424.18" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$160" class="0">
<segment>
<wire x1="187.96" y1="416.56" x2="182.88" y2="416.56" width="0.1524" layer="91"/>
<wire x1="182.88" y1="416.56" x2="182.88" y2="403.86" width="0.1524" layer="91"/>
<pinref part="SEC_A" gate="G$1" pin="A"/>
<pinref part="R95" gate="G$1" pin="2"/>
<wire x1="185.42" y1="424.18" x2="187.96" y2="424.18" width="0.1524" layer="91"/>
<wire x1="187.96" y1="424.18" x2="187.96" y2="416.56" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$161" class="0">
<segment>
<wire x1="190.5" y1="414.02" x2="180.34" y2="414.02" width="0.1524" layer="91"/>
<wire x1="180.34" y1="414.02" x2="180.34" y2="401.32" width="0.1524" layer="91"/>
<wire x1="180.34" y1="401.32" x2="182.88" y2="401.32" width="0.1524" layer="91"/>
<pinref part="SEC_A" gate="G$1" pin="B"/>
<pinref part="R94" gate="G$1" pin="2"/>
<wire x1="185.42" y1="426.72" x2="190.5" y2="426.72" width="0.1524" layer="91"/>
<wire x1="190.5" y1="426.72" x2="190.5" y2="414.02" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$162" class="0">
<segment>
<wire x1="193.04" y1="411.48" x2="177.8" y2="411.48" width="0.1524" layer="91"/>
<wire x1="177.8" y1="411.48" x2="177.8" y2="398.78" width="0.1524" layer="91"/>
<wire x1="177.8" y1="398.78" x2="182.88" y2="398.78" width="0.1524" layer="91"/>
<pinref part="SEC_A" gate="G$1" pin="C"/>
<pinref part="R93" gate="G$1" pin="2"/>
<wire x1="185.42" y1="429.26" x2="193.04" y2="429.26" width="0.1524" layer="91"/>
<wire x1="193.04" y1="429.26" x2="193.04" y2="411.48" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$163" class="0">
<segment>
<wire x1="195.58" y1="408.94" x2="175.26" y2="408.94" width="0.1524" layer="91"/>
<wire x1="175.26" y1="408.94" x2="175.26" y2="396.24" width="0.1524" layer="91"/>
<wire x1="175.26" y1="396.24" x2="182.88" y2="396.24" width="0.1524" layer="91"/>
<pinref part="SEC_A" gate="G$1" pin="D"/>
<pinref part="R92" gate="G$1" pin="2"/>
<wire x1="185.42" y1="431.8" x2="195.58" y2="431.8" width="0.1524" layer="91"/>
<wire x1="195.58" y1="431.8" x2="195.58" y2="408.94" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$164" class="0">
<segment>
<wire x1="198.12" y1="419.1" x2="172.72" y2="419.1" width="0.1524" layer="91"/>
<wire x1="172.72" y1="419.1" x2="172.72" y2="393.7" width="0.1524" layer="91"/>
<wire x1="172.72" y1="393.7" x2="182.88" y2="393.7" width="0.1524" layer="91"/>
<pinref part="SEC_A" gate="G$1" pin="E"/>
<pinref part="R91" gate="G$1" pin="2"/>
<wire x1="185.42" y1="434.34" x2="198.12" y2="434.34" width="0.1524" layer="91"/>
<wire x1="198.12" y1="434.34" x2="198.12" y2="419.1" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$165" class="0">
<segment>
<pinref part="R89" gate="G$1" pin="2"/>
<wire x1="185.42" y1="439.42" x2="205.74" y2="439.42" width="0.1524" layer="91"/>
<wire x1="205.74" y1="439.42" x2="205.74" y2="403.86" width="0.1524" layer="91"/>
<pinref part="SEC_A" gate="G$1" pin="F"/>
<wire x1="205.74" y1="403.86" x2="198.12" y2="403.86" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$166" class="0">
<segment>
<pinref part="R90" gate="G$1" pin="2"/>
<wire x1="185.42" y1="436.88" x2="203.2" y2="436.88" width="0.1524" layer="91"/>
<wire x1="203.2" y1="436.88" x2="203.2" y2="401.32" width="0.1524" layer="91"/>
<pinref part="SEC_A" gate="G$1" pin="G"/>
<wire x1="203.2" y1="401.32" x2="198.12" y2="401.32" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$167" class="0">
<segment>
<pinref part="R96" gate="G$1" pin="1"/>
<pinref part="4543_SEC_B" gate="A" pin="F"/>
<wire x1="121.92" y1="518.16" x2="119.38" y2="518.16" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$168" class="0">
<segment>
<pinref part="R97" gate="G$1" pin="1"/>
<pinref part="4543_SEC_B" gate="A" pin="G"/>
<wire x1="121.92" y1="515.62" x2="119.38" y2="515.62" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$169" class="0">
<segment>
<pinref part="R98" gate="G$1" pin="1"/>
<pinref part="4543_SEC_B" gate="A" pin="E"/>
<wire x1="121.92" y1="513.08" x2="119.38" y2="513.08" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$170" class="0">
<segment>
<pinref part="R99" gate="G$1" pin="1"/>
<pinref part="4543_SEC_B" gate="A" pin="D"/>
<wire x1="121.92" y1="510.54" x2="119.38" y2="510.54" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$171" class="0">
<segment>
<pinref part="R100" gate="G$1" pin="1"/>
<pinref part="4543_SEC_B" gate="A" pin="C"/>
<wire x1="121.92" y1="508" x2="119.38" y2="508" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$172" class="0">
<segment>
<pinref part="R101" gate="G$1" pin="1"/>
<pinref part="4543_SEC_B" gate="A" pin="B"/>
<wire x1="121.92" y1="505.46" x2="119.38" y2="505.46" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$173" class="0">
<segment>
<pinref part="R102" gate="G$1" pin="1"/>
<pinref part="4543_SEC_B" gate="A" pin="A"/>
<wire x1="121.92" y1="502.92" x2="119.38" y2="502.92" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$174" class="0">
<segment>
<pinref part="R102" gate="G$1" pin="2"/>
<pinref part="2003_SEC_B" gate="A" pin="I7"/>
<wire x1="132.08" y1="502.92" x2="134.62" y2="502.92" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$175" class="0">
<segment>
<pinref part="R101" gate="G$1" pin="2"/>
<pinref part="2003_SEC_B" gate="A" pin="I6"/>
<wire x1="132.08" y1="505.46" x2="134.62" y2="505.46" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$176" class="0">
<segment>
<pinref part="R100" gate="G$1" pin="2"/>
<pinref part="2003_SEC_B" gate="A" pin="I5"/>
<wire x1="132.08" y1="508" x2="134.62" y2="508" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$177" class="0">
<segment>
<pinref part="R99" gate="G$1" pin="2"/>
<pinref part="2003_SEC_B" gate="A" pin="I4"/>
<wire x1="132.08" y1="510.54" x2="134.62" y2="510.54" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$178" class="0">
<segment>
<pinref part="R98" gate="G$1" pin="2"/>
<pinref part="2003_SEC_B" gate="A" pin="I3"/>
<wire x1="132.08" y1="513.08" x2="134.62" y2="513.08" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$179" class="0">
<segment>
<pinref part="R97" gate="G$1" pin="2"/>
<pinref part="2003_SEC_B" gate="A" pin="I2"/>
<wire x1="132.08" y1="515.62" x2="134.62" y2="515.62" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$180" class="0">
<segment>
<pinref part="R96" gate="G$1" pin="2"/>
<pinref part="2003_SEC_B" gate="A" pin="I1"/>
<wire x1="132.08" y1="518.16" x2="134.62" y2="518.16" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$181" class="0">
<segment>
<pinref part="R107" gate="G$1" pin="2"/>
<pinref part="4543_SEC_B" gate="A" pin="LD"/>
<wire x1="91.44" y1="520.7" x2="93.98" y2="520.7" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$182" class="0">
<segment>
<pinref part="R106" gate="G$1" pin="2"/>
<pinref part="4543_SEC_B" gate="A" pin="IB"/>
<wire x1="91.44" y1="518.16" x2="93.98" y2="518.16" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$183" class="0">
<segment>
<pinref part="R105" gate="G$1" pin="2"/>
<pinref part="4543_SEC_B" gate="A" pin="IC"/>
<wire x1="91.44" y1="515.62" x2="93.98" y2="515.62" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$184" class="0">
<segment>
<pinref part="R104" gate="G$1" pin="2"/>
<pinref part="4543_SEC_B" gate="A" pin="ID"/>
<wire x1="91.44" y1="513.08" x2="93.98" y2="513.08" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$185" class="0">
<segment>
<pinref part="R103" gate="G$1" pin="2"/>
<pinref part="4543_SEC_B" gate="A" pin="IA"/>
<wire x1="91.44" y1="510.54" x2="93.98" y2="510.54" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$186" class="0">
<segment>
<pinref part="2003_SEC_B" gate="A" pin="O1"/>
<pinref part="R108" gate="G$1" pin="1"/>
<wire x1="160.02" y1="518.16" x2="162.56" y2="518.16" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$187" class="0">
<segment>
<pinref part="2003_SEC_B" gate="A" pin="O2"/>
<pinref part="R109" gate="G$1" pin="1"/>
<wire x1="160.02" y1="515.62" x2="162.56" y2="515.62" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$188" class="0">
<segment>
<pinref part="2003_SEC_B" gate="A" pin="O3"/>
<pinref part="R110" gate="G$1" pin="1"/>
<wire x1="160.02" y1="513.08" x2="162.56" y2="513.08" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$189" class="0">
<segment>
<pinref part="2003_SEC_B" gate="A" pin="O4"/>
<pinref part="R111" gate="G$1" pin="1"/>
<wire x1="160.02" y1="510.54" x2="162.56" y2="510.54" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$190" class="0">
<segment>
<pinref part="2003_SEC_B" gate="A" pin="O5"/>
<pinref part="R112" gate="G$1" pin="1"/>
<wire x1="160.02" y1="508" x2="162.56" y2="508" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$191" class="0">
<segment>
<pinref part="2003_SEC_B" gate="A" pin="O6"/>
<pinref part="R113" gate="G$1" pin="1"/>
<wire x1="160.02" y1="505.46" x2="162.56" y2="505.46" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$192" class="0">
<segment>
<pinref part="2003_SEC_B" gate="A" pin="O7"/>
<pinref part="R114" gate="G$1" pin="1"/>
<wire x1="160.02" y1="502.92" x2="162.56" y2="502.92" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$193" class="0">
<segment>
<pinref part="R114" gate="G$1" pin="2"/>
<wire x1="172.72" y1="502.92" x2="223.52" y2="502.92" width="0.1524" layer="91"/>
<wire x1="223.52" y1="502.92" x2="223.52" y2="403.86" width="0.1524" layer="91"/>
<pinref part="SEC_B" gate="G$1" pin="A"/>
</segment>
</net>
<net name="N$194" class="0">
<segment>
<pinref part="R113" gate="G$1" pin="2"/>
<wire x1="172.72" y1="505.46" x2="220.98" y2="505.46" width="0.1524" layer="91"/>
<wire x1="220.98" y1="505.46" x2="220.98" y2="401.32" width="0.1524" layer="91"/>
<pinref part="SEC_B" gate="G$1" pin="B"/>
<wire x1="220.98" y1="401.32" x2="223.52" y2="401.32" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$195" class="0">
<segment>
<pinref part="R112" gate="G$1" pin="2"/>
<wire x1="172.72" y1="508" x2="218.44" y2="508" width="0.1524" layer="91"/>
<wire x1="218.44" y1="508" x2="218.44" y2="398.78" width="0.1524" layer="91"/>
<pinref part="SEC_B" gate="G$1" pin="C"/>
<wire x1="218.44" y1="398.78" x2="223.52" y2="398.78" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$196" class="0">
<segment>
<pinref part="R111" gate="G$1" pin="2"/>
<wire x1="172.72" y1="510.54" x2="215.9" y2="510.54" width="0.1524" layer="91"/>
<wire x1="215.9" y1="510.54" x2="215.9" y2="396.24" width="0.1524" layer="91"/>
<pinref part="SEC_B" gate="G$1" pin="D"/>
<wire x1="215.9" y1="396.24" x2="223.52" y2="396.24" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$197" class="0">
<segment>
<pinref part="R110" gate="G$1" pin="2"/>
<wire x1="172.72" y1="513.08" x2="213.36" y2="513.08" width="0.1524" layer="91"/>
<wire x1="213.36" y1="513.08" x2="213.36" y2="393.7" width="0.1524" layer="91"/>
<pinref part="SEC_B" gate="G$1" pin="E"/>
<wire x1="213.36" y1="393.7" x2="223.52" y2="393.7" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$198" class="0">
<segment>
<pinref part="R109" gate="G$1" pin="2"/>
<wire x1="172.72" y1="515.62" x2="241.3" y2="515.62" width="0.1524" layer="91"/>
<wire x1="241.3" y1="515.62" x2="241.3" y2="401.32" width="0.1524" layer="91"/>
<pinref part="SEC_B" gate="G$1" pin="G"/>
<wire x1="241.3" y1="401.32" x2="238.76" y2="401.32" width="0.1524" layer="91"/>
</segment>
</net>
<net name="SEC_B_LD" class="0">
<segment>
<pinref part="R107" gate="G$1" pin="1"/>
<wire x1="81.28" y1="520.7" x2="78.74" y2="520.7" width="0.1524" layer="91"/>
<label x="78.74" y="520.7" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="IC1" gate="G$1" pin="GPA5"/>
<wire x1="165.1" y1="66.04" x2="185.42" y2="66.04" width="0.1524" layer="91"/>
<label x="185.42" y="66.04" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="SEC_A_LD" class="0">
<segment>
<pinref part="R88" gate="G$1" pin="1"/>
<wire x1="93.98" y1="441.96" x2="91.44" y2="441.96" width="0.1524" layer="91"/>
<label x="91.44" y="441.96" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="IC1" gate="G$1" pin="GPA4"/>
<wire x1="165.1" y1="68.58" x2="167.64" y2="68.58" width="0.1524" layer="91"/>
<label x="167.64" y="68.58" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="DAY_A_LD" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="GPA6"/>
<wire x1="165.1" y1="63.5" x2="167.64" y2="63.5" width="0.1524" layer="91"/>
<label x="167.64" y="63.5" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="R265" gate="G$1" pin="1"/>
<wire x1="-431.8" y1="251.46" x2="-434.34" y2="251.46" width="0.1524" layer="91"/>
<label x="-434.34" y="251.46" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="DAY_B_LD" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="GPA7"/>
<wire x1="165.1" y1="60.96" x2="185.42" y2="60.96" width="0.1524" layer="91"/>
<label x="185.42" y="60.96" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="R122" gate="G$1" pin="1"/>
<wire x1="-307.34" y1="251.46" x2="-309.88" y2="251.46" width="0.1524" layer="91"/>
<label x="-309.88" y="251.46" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="MONTH_A_LD" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="GPB0"/>
<wire x1="165.1" y1="55.88" x2="167.64" y2="55.88" width="0.1524" layer="91"/>
<label x="167.64" y="55.88" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="R141" gate="G$1" pin="1"/>
<wire x1="-175.26" y1="259.08" x2="-177.8" y2="259.08" width="0.1524" layer="91"/>
<label x="-177.8" y="259.08" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="MONTH_B_LD" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="GPB1"/>
<wire x1="165.1" y1="53.34" x2="185.42" y2="53.34" width="0.1524" layer="91"/>
<label x="185.42" y="53.34" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="R160" gate="G$1" pin="1"/>
<wire x1="-58.42" y1="259.08" x2="-60.96" y2="259.08" width="0.1524" layer="91"/>
<label x="-60.96" y="259.08" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="YEAR_A_LD" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="GPB2"/>
<wire x1="165.1" y1="50.8" x2="167.64" y2="50.8" width="0.1524" layer="91"/>
<label x="167.64" y="50.8" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="R289" gate="G$1" pin="1"/>
<wire x1="114.3" y1="269.24" x2="111.76" y2="269.24" width="0.1524" layer="91"/>
<label x="111.76" y="269.24" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="YEAR_B_LD" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="GPB3"/>
<wire x1="165.1" y1="48.26" x2="185.42" y2="48.26" width="0.1524" layer="91"/>
<label x="185.42" y="48.26" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="R174" gate="G$1" pin="1"/>
<wire x1="241.3" y1="269.24" x2="238.76" y2="269.24" width="0.1524" layer="91"/>
<label x="238.76" y="269.24" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="YEAR_C_LD" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="GPB4"/>
<wire x1="165.1" y1="45.72" x2="167.64" y2="45.72" width="0.1524" layer="91"/>
<label x="167.64" y="45.72" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="R193" gate="G$1" pin="1"/>
<wire x1="365.76" y1="269.24" x2="363.22" y2="269.24" width="0.1524" layer="91"/>
<label x="363.22" y="269.24" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="YEAR_D_LD" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="GPB5"/>
<wire x1="165.1" y1="43.18" x2="185.42" y2="43.18" width="0.1524" layer="91"/>
<label x="185.42" y="43.18" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="R212" gate="G$1" pin="1"/>
<wire x1="490.22" y1="269.24" x2="487.68" y2="269.24" width="0.1524" layer="91"/>
<label x="487.68" y="269.24" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="TEMP_A_LD" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="GPB6"/>
<wire x1="165.1" y1="40.64" x2="167.64" y2="40.64" width="0.1524" layer="91"/>
<label x="167.64" y="40.64" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="R313" gate="G$1" pin="1"/>
<wire x1="678.18" y1="266.7" x2="675.64" y2="266.7" width="0.1524" layer="91"/>
<label x="675.64" y="266.7" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="TEMP_B_LD" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="GPB7"/>
<wire x1="165.1" y1="38.1" x2="185.42" y2="38.1" width="0.1524" layer="91"/>
<label x="185.42" y="38.1" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="R226" gate="G$1" pin="1"/>
<wire x1="810.26" y1="264.16" x2="807.72" y2="264.16" width="0.1524" layer="91"/>
<label x="807.72" y="264.16" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="N$401" class="0">
<segment>
<pinref part="DEMUX_WEEKDAY" gate="A" pin="Y0"/>
<pinref part="R245" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$402" class="0">
<segment>
<pinref part="DEMUX_WEEKDAY" gate="A" pin="Y1"/>
<pinref part="R246" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$403" class="0">
<segment>
<pinref part="DEMUX_WEEKDAY" gate="A" pin="Y2"/>
<pinref part="R247" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$404" class="0">
<segment>
<pinref part="DEMUX_WEEKDAY" gate="A" pin="Y3"/>
<pinref part="R248" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$405" class="0">
<segment>
<pinref part="DEMUX_WEEKDAY" gate="A" pin="Y4"/>
<pinref part="R249" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$406" class="0">
<segment>
<pinref part="DEMUX_WEEKDAY" gate="A" pin="Y5"/>
<pinref part="R250" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$407" class="0">
<segment>
<pinref part="DEMUX_WEEKDAY" gate="A" pin="Y6"/>
<pinref part="R251" gate="G$1" pin="1"/>
</segment>
</net>
<net name="DEMUX0" class="0">
<segment>
<pinref part="DEMUX_WEEKDAY" gate="A" pin="A0"/>
<wire x1="226.06" y1="86.36" x2="220.98" y2="86.36" width="0.1524" layer="91"/>
<label x="220.98" y="86.36" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="U$1" gate="G$1" pin="G25"/>
<wire x1="53.34" y1="60.96" x2="43.18" y2="60.96" width="0.1524" layer="91"/>
<label x="43.18" y="60.96" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="DEMUX1" class="0">
<segment>
<pinref part="DEMUX_WEEKDAY" gate="A" pin="A1"/>
<wire x1="226.06" y1="83.82" x2="220.98" y2="83.82" width="0.1524" layer="91"/>
<label x="220.98" y="83.82" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="U$1" gate="G$1" pin="G26"/>
<wire x1="53.34" y1="58.42" x2="33.02" y2="58.42" width="0.1524" layer="91"/>
<label x="33.02" y="58.42" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="DEMUX2" class="0">
<segment>
<pinref part="DEMUX_WEEKDAY" gate="A" pin="A2"/>
<wire x1="226.06" y1="81.28" x2="220.98" y2="81.28" width="0.1524" layer="91"/>
<label x="220.98" y="81.28" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="U$1" gate="G$1" pin="G27"/>
<wire x1="53.34" y1="55.88" x2="43.18" y2="55.88" width="0.1524" layer="91"/>
<label x="43.18" y="55.88" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="N$415" class="0">
<segment>
<pinref part="R252" gate="G$1" pin="2"/>
<pinref part="DEMUX_WEEKDAY" gate="A" pin="E3"/>
<wire x1="220.98" y1="73.66" x2="226.06" y2="73.66" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$416" class="0">
<segment>
<pinref part="R253" gate="G$1" pin="1"/>
<pinref part="LED_COLON1" gate="G$1" pin="A"/>
<wire x1="-109.22" y1="368.3" x2="-109.22" y2="365.76" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$417" class="0">
<segment>
<pinref part="LED_COLON1" gate="G$1" pin="C"/>
<pinref part="LED_COLON2" gate="G$1" pin="A"/>
<wire x1="-109.22" y1="358.14" x2="-109.22" y2="340.36" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$418" class="0">
<segment>
<pinref part="R254" gate="G$1" pin="1"/>
<pinref part="LED_COLON3" gate="G$1" pin="A"/>
<wire x1="78.74" y1="363.22" x2="78.74" y2="360.68" width="0.1524" layer="91"/>
</segment>
</net>
<net name="LED_COLON3" class="0">
<segment>
<pinref part="LED_COLON3" gate="G$1" pin="C"/>
<pinref part="LED_COLON4" gate="G$1" pin="A"/>
<wire x1="78.74" y1="353.06" x2="78.74" y2="335.28" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$420" class="0">
<segment>
<pinref part="R255" gate="G$1" pin="2"/>
<wire x1="195.58" y1="-12.7" x2="203.2" y2="-12.7" width="0.1524" layer="91"/>
<pinref part="T2" gate="G$1" pin="B"/>
</segment>
</net>
<net name="N$421" class="0">
<segment>
<pinref part="POWER_SWITCH" gate="1" pin="2"/>
<wire x1="208.28" y1="0" x2="208.28" y2="-7.62" width="0.1524" layer="91"/>
<pinref part="T2" gate="G$1" pin="C"/>
</segment>
</net>
<net name="12VPOWER" class="0">
<segment>
<pinref part="POWER_SWITCH" gate="2" pin="P"/>
<wire x1="238.76" y1="2.54" x2="238.76" y2="-10.16" width="0.1524" layer="91"/>
<label x="238.76" y="-10.16" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<wire x1="-208.28" y1="525.78" x2="-170.18" y2="525.78" width="0.1524" layer="91"/>
<pinref part="2003_HOUR_B" gate="A" pin="CD+"/>
<wire x1="-170.18" y1="490.22" x2="-208.28" y2="490.22" width="0.1524" layer="91"/>
<wire x1="-170.18" y1="490.22" x2="-170.18" y2="525.78" width="0.1524" layer="91"/>
<wire x1="-170.18" y1="490.22" x2="-124.46" y2="490.22" width="0.1524" layer="91"/>
<junction x="-170.18" y="490.22"/>
<label x="-208.28" y="525.78" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="HOUR_B" gate="G$1" pin="CA2"/>
<pinref part="HOUR_B" gate="G$1" pin="CA1"/>
<wire x1="-129.54" y1="398.78" x2="-124.46" y2="398.78" width="0.1524" layer="91"/>
<wire x1="-124.46" y1="398.78" x2="-124.46" y2="396.24" width="0.1524" layer="91"/>
<wire x1="-124.46" y1="396.24" x2="-129.54" y2="396.24" width="0.1524" layer="91"/>
<wire x1="-124.46" y1="490.22" x2="-124.46" y2="398.78" width="0.1524" layer="91"/>
<junction x="-124.46" y="398.78"/>
</segment>
<segment>
<wire x1="-195.58" y1="459.74" x2="-157.48" y2="459.74" width="0.1524" layer="91"/>
<wire x1="-157.48" y1="459.74" x2="-157.48" y2="424.18" width="0.1524" layer="91"/>
<pinref part="2003_HOUR_A" gate="A" pin="CD+"/>
<wire x1="-157.48" y1="424.18" x2="-195.58" y2="424.18" width="0.1524" layer="91"/>
<pinref part="HOUR_A" gate="G$1" pin="CA2"/>
<wire x1="-170.18" y1="396.24" x2="-157.48" y2="396.24" width="0.1524" layer="91"/>
<wire x1="-157.48" y1="396.24" x2="-157.48" y2="398.78" width="0.1524" layer="91"/>
<junction x="-157.48" y="424.18"/>
<pinref part="HOUR_A" gate="G$1" pin="CA1"/>
<wire x1="-157.48" y1="398.78" x2="-157.48" y2="424.18" width="0.1524" layer="91"/>
<wire x1="-170.18" y1="398.78" x2="-157.48" y2="398.78" width="0.1524" layer="91"/>
<junction x="-157.48" y="398.78"/>
<label x="-195.58" y="459.74" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<wire x1="-25.4" y1="538.48" x2="12.7" y2="538.48" width="0.1524" layer="91"/>
<pinref part="2003_MIN_B" gate="A" pin="CD+"/>
<wire x1="12.7" y1="502.92" x2="-25.4" y2="502.92" width="0.1524" layer="91"/>
<wire x1="12.7" y1="502.92" x2="12.7" y2="538.48" width="0.1524" layer="91"/>
<wire x1="12.7" y1="502.92" x2="60.96" y2="502.92" width="0.1524" layer="91"/>
<junction x="12.7" y="502.92"/>
<pinref part="MIN_B" gate="G$1" pin="CA2"/>
<wire x1="60.96" y1="502.92" x2="60.96" y2="398.78" width="0.1524" layer="91"/>
<wire x1="60.96" y1="398.78" x2="60.96" y2="396.24" width="0.1524" layer="91"/>
<wire x1="60.96" y1="396.24" x2="53.34" y2="396.24" width="0.1524" layer="91"/>
<pinref part="MIN_B" gate="G$1" pin="CA1"/>
<wire x1="53.34" y1="398.78" x2="60.96" y2="398.78" width="0.1524" layer="91"/>
<junction x="60.96" y="398.78"/>
<label x="-25.4" y="538.48" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<wire x1="-12.7" y1="459.74" x2="25.4" y2="459.74" width="0.1524" layer="91"/>
<pinref part="2003_MIN_A" gate="A" pin="CD+"/>
<wire x1="25.4" y1="424.18" x2="-12.7" y2="424.18" width="0.1524" layer="91"/>
<wire x1="25.4" y1="459.74" x2="25.4" y2="424.18" width="0.1524" layer="91"/>
<wire x1="25.4" y1="424.18" x2="25.4" y2="398.78" width="0.1524" layer="91"/>
<junction x="25.4" y="424.18"/>
<pinref part="MIN_A" gate="G$1" pin="CA2"/>
<wire x1="25.4" y1="398.78" x2="25.4" y2="396.24" width="0.1524" layer="91"/>
<wire x1="25.4" y1="396.24" x2="12.7" y2="396.24" width="0.1524" layer="91"/>
<pinref part="MIN_A" gate="G$1" pin="CA1"/>
<wire x1="12.7" y1="398.78" x2="25.4" y2="398.78" width="0.1524" layer="91"/>
<junction x="25.4" y="398.78"/>
<label x="-12.7" y="459.74" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="R253" gate="G$1" pin="2"/>
<wire x1="-109.22" y1="381" x2="-109.22" y2="378.46" width="0.1524" layer="91"/>
<label x="-109.22" y="381" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="R254" gate="G$1" pin="2"/>
<wire x1="78.74" y1="375.92" x2="78.74" y2="373.38" width="0.1524" layer="91"/>
<label x="78.74" y="375.92" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<wire x1="172.72" y1="457.2" x2="210.82" y2="457.2" width="0.1524" layer="91"/>
<pinref part="2003_SEC_A" gate="A" pin="CD+"/>
<wire x1="210.82" y1="421.64" x2="172.72" y2="421.64" width="0.1524" layer="91"/>
<wire x1="210.82" y1="457.2" x2="210.82" y2="421.64" width="0.1524" layer="91"/>
<wire x1="210.82" y1="421.64" x2="210.82" y2="396.24" width="0.1524" layer="91"/>
<junction x="210.82" y="421.64"/>
<pinref part="SEC_A" gate="G$1" pin="CA2"/>
<wire x1="210.82" y1="396.24" x2="210.82" y2="393.7" width="0.1524" layer="91"/>
<wire x1="210.82" y1="393.7" x2="198.12" y2="393.7" width="0.1524" layer="91"/>
<pinref part="SEC_A" gate="G$1" pin="CA1"/>
<wire x1="198.12" y1="396.24" x2="210.82" y2="396.24" width="0.1524" layer="91"/>
<junction x="210.82" y="396.24"/>
<label x="172.72" y="457.2" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<wire x1="160.02" y1="535.94" x2="198.12" y2="535.94" width="0.1524" layer="91"/>
<pinref part="2003_SEC_B" gate="A" pin="CD+"/>
<wire x1="198.12" y1="500.38" x2="160.02" y2="500.38" width="0.1524" layer="91"/>
<wire x1="198.12" y1="500.38" x2="198.12" y2="535.94" width="0.1524" layer="91"/>
<wire x1="198.12" y1="500.38" x2="246.38" y2="500.38" width="0.1524" layer="91"/>
<junction x="198.12" y="500.38"/>
<pinref part="SEC_B" gate="G$1" pin="CA2"/>
<wire x1="246.38" y1="500.38" x2="246.38" y2="396.24" width="0.1524" layer="91"/>
<wire x1="246.38" y1="396.24" x2="246.38" y2="393.7" width="0.1524" layer="91"/>
<wire x1="246.38" y1="393.7" x2="238.76" y2="393.7" width="0.1524" layer="91"/>
<pinref part="SEC_B" gate="G$1" pin="CA1"/>
<wire x1="238.76" y1="396.24" x2="246.38" y2="396.24" width="0.1524" layer="91"/>
<junction x="246.38" y="396.24"/>
<label x="160.02" y="535.94" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="2003_DAY_A" gate="A" pin="CD+"/>
<wire x1="-355.6" y1="231.14" x2="-337.82" y2="231.14" width="0.1524" layer="91"/>
<wire x1="-337.82" y1="231.14" x2="-337.82" y2="264.16" width="0.1524" layer="91"/>
<label x="-337.82" y="264.16" size="1.778" layer="95" rot="R180" xref="yes"/>
<wire x1="-337.82" y1="231.14" x2="-337.82" y2="193.04" width="0.1524" layer="91"/>
<junction x="-337.82" y="231.14"/>
<pinref part="DAY_A" gate="G$1" pin="CA2"/>
<wire x1="-337.82" y1="193.04" x2="-337.82" y2="190.5" width="0.1524" layer="91"/>
<wire x1="-337.82" y1="190.5" x2="-358.14" y2="190.5" width="0.1524" layer="91"/>
<pinref part="DAY_A" gate="G$1" pin="CA1"/>
<wire x1="-358.14" y1="193.04" x2="-337.82" y2="193.04" width="0.1524" layer="91"/>
<junction x="-337.82" y="193.04"/>
</segment>
<segment>
<pinref part="2003_DAY_B" gate="A" pin="CD+"/>
<wire x1="-231.14" y1="231.14" x2="-213.36" y2="231.14" width="0.1524" layer="91"/>
<wire x1="-213.36" y1="231.14" x2="-213.36" y2="264.16" width="0.1524" layer="91"/>
<label x="-213.36" y="264.16" size="1.778" layer="95" rot="R180" xref="yes"/>
<wire x1="-213.36" y1="231.14" x2="-213.36" y2="193.04" width="0.1524" layer="91"/>
<junction x="-213.36" y="231.14"/>
<pinref part="DAY_B" gate="G$1" pin="CA2"/>
<wire x1="-213.36" y1="193.04" x2="-213.36" y2="190.5" width="0.1524" layer="91"/>
<wire x1="-213.36" y1="190.5" x2="-233.68" y2="190.5" width="0.1524" layer="91"/>
<pinref part="DAY_B" gate="G$1" pin="CA1"/>
<wire x1="-233.68" y1="193.04" x2="-213.36" y2="193.04" width="0.1524" layer="91"/>
<junction x="-213.36" y="193.04"/>
</segment>
<segment>
<pinref part="2003_MONTH_A" gate="A" pin="CD+"/>
<wire x1="-99.06" y1="238.76" x2="-81.28" y2="238.76" width="0.1524" layer="91"/>
<wire x1="-81.28" y1="238.76" x2="-81.28" y2="271.78" width="0.1524" layer="91"/>
<label x="-81.28" y="271.78" size="1.778" layer="95" rot="R180" xref="yes"/>
<wire x1="-81.28" y1="238.76" x2="-81.28" y2="200.66" width="0.1524" layer="91"/>
<junction x="-81.28" y="238.76"/>
<pinref part="MONTH_A" gate="G$1" pin="CA2"/>
<wire x1="-81.28" y1="200.66" x2="-81.28" y2="198.12" width="0.1524" layer="91"/>
<wire x1="-81.28" y1="198.12" x2="-101.6" y2="198.12" width="0.1524" layer="91"/>
<pinref part="MONTH_A" gate="G$1" pin="CA1"/>
<wire x1="-101.6" y1="200.66" x2="-81.28" y2="200.66" width="0.1524" layer="91"/>
<junction x="-81.28" y="200.66"/>
</segment>
<segment>
<pinref part="2003_MONTH_B" gate="A" pin="CD+"/>
<wire x1="17.78" y1="238.76" x2="35.56" y2="238.76" width="0.1524" layer="91"/>
<wire x1="35.56" y1="238.76" x2="35.56" y2="271.78" width="0.1524" layer="91"/>
<label x="35.56" y="271.78" size="1.778" layer="95" rot="R180" xref="yes"/>
<wire x1="35.56" y1="238.76" x2="35.56" y2="200.66" width="0.1524" layer="91"/>
<junction x="35.56" y="238.76"/>
<pinref part="MONTH_B" gate="G$1" pin="CA2"/>
<wire x1="35.56" y1="200.66" x2="35.56" y2="198.12" width="0.1524" layer="91"/>
<wire x1="35.56" y1="198.12" x2="15.24" y2="198.12" width="0.1524" layer="91"/>
<pinref part="MONTH_B" gate="G$1" pin="CA1"/>
<wire x1="15.24" y1="200.66" x2="35.56" y2="200.66" width="0.1524" layer="91"/>
<junction x="35.56" y="200.66"/>
</segment>
<segment>
<pinref part="2003_YEAR_A" gate="A" pin="CD+"/>
<wire x1="190.5" y1="248.92" x2="208.28" y2="248.92" width="0.1524" layer="91"/>
<wire x1="208.28" y1="248.92" x2="208.28" y2="281.94" width="0.1524" layer="91"/>
<label x="208.28" y="281.94" size="1.778" layer="95" rot="R180" xref="yes"/>
<wire x1="208.28" y1="248.92" x2="208.28" y2="210.82" width="0.1524" layer="91"/>
<junction x="208.28" y="248.92"/>
<pinref part="YEAR_A" gate="G$1" pin="CA2"/>
<wire x1="208.28" y1="210.82" x2="208.28" y2="208.28" width="0.1524" layer="91"/>
<wire x1="208.28" y1="208.28" x2="187.96" y2="208.28" width="0.1524" layer="91"/>
<pinref part="YEAR_A" gate="G$1" pin="CA1"/>
<wire x1="187.96" y1="210.82" x2="208.28" y2="210.82" width="0.1524" layer="91"/>
<junction x="208.28" y="210.82"/>
</segment>
<segment>
<pinref part="2003_YEAR_B" gate="A" pin="CD+"/>
<wire x1="317.5" y1="248.92" x2="335.28" y2="248.92" width="0.1524" layer="91"/>
<wire x1="335.28" y1="248.92" x2="335.28" y2="281.94" width="0.1524" layer="91"/>
<label x="335.28" y="281.94" size="1.778" layer="95" rot="R180" xref="yes"/>
<wire x1="335.28" y1="248.92" x2="335.28" y2="210.82" width="0.1524" layer="91"/>
<junction x="335.28" y="248.92"/>
<pinref part="YEAR_B" gate="G$1" pin="CA2"/>
<wire x1="335.28" y1="210.82" x2="335.28" y2="208.28" width="0.1524" layer="91"/>
<wire x1="335.28" y1="208.28" x2="314.96" y2="208.28" width="0.1524" layer="91"/>
<pinref part="YEAR_B" gate="G$1" pin="CA1"/>
<wire x1="314.96" y1="210.82" x2="335.28" y2="210.82" width="0.1524" layer="91"/>
<junction x="335.28" y="210.82"/>
</segment>
<segment>
<pinref part="2003_YEAR_C" gate="A" pin="CD+"/>
<wire x1="441.96" y1="248.92" x2="459.74" y2="248.92" width="0.1524" layer="91"/>
<wire x1="459.74" y1="248.92" x2="459.74" y2="281.94" width="0.1524" layer="91"/>
<label x="459.74" y="281.94" size="1.778" layer="95" rot="R180" xref="yes"/>
<wire x1="459.74" y1="248.92" x2="459.74" y2="210.82" width="0.1524" layer="91"/>
<junction x="459.74" y="248.92"/>
<pinref part="YEAR_C" gate="G$1" pin="CA2"/>
<wire x1="459.74" y1="210.82" x2="459.74" y2="208.28" width="0.1524" layer="91"/>
<wire x1="459.74" y1="208.28" x2="439.42" y2="208.28" width="0.1524" layer="91"/>
<pinref part="YEAR_C" gate="G$1" pin="CA1"/>
<wire x1="439.42" y1="210.82" x2="459.74" y2="210.82" width="0.1524" layer="91"/>
<junction x="459.74" y="210.82"/>
</segment>
<segment>
<pinref part="2003_YEAR_D" gate="A" pin="CD+"/>
<wire x1="566.42" y1="248.92" x2="584.2" y2="248.92" width="0.1524" layer="91"/>
<wire x1="584.2" y1="248.92" x2="584.2" y2="281.94" width="0.1524" layer="91"/>
<label x="584.2" y="281.94" size="1.778" layer="95" rot="R180" xref="yes"/>
<wire x1="584.2" y1="248.92" x2="584.2" y2="210.82" width="0.1524" layer="91"/>
<junction x="584.2" y="248.92"/>
<pinref part="YEAR_D" gate="G$1" pin="CA2"/>
<wire x1="584.2" y1="210.82" x2="584.2" y2="208.28" width="0.1524" layer="91"/>
<wire x1="584.2" y1="208.28" x2="563.88" y2="208.28" width="0.1524" layer="91"/>
<pinref part="YEAR_D" gate="G$1" pin="CA1"/>
<wire x1="563.88" y1="210.82" x2="584.2" y2="210.82" width="0.1524" layer="91"/>
<junction x="584.2" y="210.82"/>
</segment>
<segment>
<pinref part="2003_TEMP_A" gate="A" pin="CD+"/>
<wire x1="754.38" y1="246.38" x2="772.16" y2="246.38" width="0.1524" layer="91"/>
<wire x1="772.16" y1="246.38" x2="772.16" y2="279.4" width="0.1524" layer="91"/>
<label x="772.16" y="279.4" size="1.778" layer="95" rot="R180" xref="yes"/>
<wire x1="772.16" y1="246.38" x2="772.16" y2="208.28" width="0.1524" layer="91"/>
<junction x="772.16" y="246.38"/>
<pinref part="TEMP_A" gate="G$1" pin="CA2"/>
<wire x1="772.16" y1="208.28" x2="772.16" y2="205.74" width="0.1524" layer="91"/>
<wire x1="772.16" y1="205.74" x2="751.84" y2="205.74" width="0.1524" layer="91"/>
<pinref part="TEMP_A" gate="G$1" pin="CA1"/>
<wire x1="751.84" y1="208.28" x2="772.16" y2="208.28" width="0.1524" layer="91"/>
<junction x="772.16" y="208.28"/>
</segment>
<segment>
<pinref part="2003_TEMP_B" gate="A" pin="CD+"/>
<wire x1="886.46" y1="243.84" x2="904.24" y2="243.84" width="0.1524" layer="91"/>
<wire x1="904.24" y1="243.84" x2="904.24" y2="276.86" width="0.1524" layer="91"/>
<label x="904.24" y="276.86" size="1.778" layer="95" rot="R180" xref="yes"/>
<wire x1="904.24" y1="243.84" x2="904.24" y2="205.74" width="0.1524" layer="91"/>
<junction x="904.24" y="243.84"/>
<pinref part="TEMP_B" gate="G$1" pin="CA2"/>
<wire x1="904.24" y1="205.74" x2="904.24" y2="203.2" width="0.1524" layer="91"/>
<wire x1="904.24" y1="203.2" x2="883.92" y2="203.2" width="0.1524" layer="91"/>
<pinref part="TEMP_B" gate="G$1" pin="CA1"/>
<wire x1="883.92" y1="205.74" x2="904.24" y2="205.74" width="0.1524" layer="91"/>
<junction x="904.24" y="205.74"/>
</segment>
<segment>
<pinref part="LED_WED" gate="G$1" pin="A"/>
<wire x1="350.52" y1="139.7" x2="350.52" y2="134.62" width="0.1524" layer="91"/>
<label x="350.52" y="139.7" size="1.778" layer="95" rot="R90" xref="yes"/>
<pinref part="LED_TUE" gate="G$1" pin="A"/>
<wire x1="350.52" y1="134.62" x2="350.52" y2="129.54" width="0.1524" layer="91"/>
<wire x1="335.28" y1="129.54" x2="335.28" y2="134.62" width="0.1524" layer="91"/>
<wire x1="335.28" y1="134.62" x2="350.52" y2="134.62" width="0.1524" layer="91"/>
<junction x="350.52" y="134.62"/>
<pinref part="LED_MON" gate="G$1" pin="A"/>
<wire x1="322.58" y1="129.54" x2="322.58" y2="134.62" width="0.1524" layer="91"/>
<wire x1="322.58" y1="134.62" x2="335.28" y2="134.62" width="0.1524" layer="91"/>
<junction x="335.28" y="134.62"/>
<pinref part="LED_SUN" gate="G$1" pin="A"/>
<wire x1="309.88" y1="129.54" x2="309.88" y2="134.62" width="0.1524" layer="91"/>
<wire x1="309.88" y1="134.62" x2="322.58" y2="134.62" width="0.1524" layer="91"/>
<junction x="322.58" y="134.62"/>
<pinref part="LED_THU" gate="G$1" pin="A"/>
<wire x1="363.22" y1="129.54" x2="363.22" y2="134.62" width="0.1524" layer="91"/>
<wire x1="363.22" y1="134.62" x2="350.52" y2="134.62" width="0.1524" layer="91"/>
<pinref part="LED_FRI" gate="G$1" pin="A"/>
<wire x1="375.92" y1="129.54" x2="375.92" y2="134.62" width="0.1524" layer="91"/>
<wire x1="375.92" y1="134.62" x2="363.22" y2="134.62" width="0.1524" layer="91"/>
<junction x="363.22" y="134.62"/>
<pinref part="LED_SAT" gate="G$1" pin="A"/>
<wire x1="388.62" y1="129.54" x2="388.62" y2="134.62" width="0.1524" layer="91"/>
<wire x1="388.62" y1="134.62" x2="375.92" y2="134.62" width="0.1524" layer="91"/>
<junction x="375.92" y="134.62"/>
</segment>
<segment>
<pinref part="2003_WEEKDAY" gate="A" pin="CD+"/>
<wire x1="294.64" y1="66.04" x2="294.64" y2="53.34" width="0.1524" layer="91"/>
<label x="294.64" y="53.34" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="POWER_SWITCH" class="0">
<segment>
<pinref part="R255" gate="G$1" pin="1"/>
<wire x1="185.42" y1="-12.7" x2="172.72" y2="-12.7" width="0.1524" layer="91"/>
<label x="172.72" y="-12.7" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="U$1" gate="G$1" pin="G2"/>
<wire x1="83.82" y1="45.72" x2="88.9" y2="45.72" width="0.1524" layer="91"/>
<label x="88.9" y="45.72" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="N$422" class="0">
<segment>
<pinref part="R256" gate="G$1" pin="1"/>
<pinref part="IC1" gate="G$1" pin="SCL"/>
<wire x1="121.92" y1="68.58" x2="121.92" y2="55.88" width="0.1524" layer="91"/>
<wire x1="121.92" y1="55.88" x2="139.7" y2="55.88" width="0.1524" layer="91"/>
<pinref part="U$1" gate="G$1" pin="G22/SCL"/>
<wire x1="83.82" y1="76.2" x2="111.76" y2="76.2" width="0.1524" layer="91"/>
<wire x1="111.76" y1="76.2" x2="111.76" y2="55.88" width="0.1524" layer="91"/>
<wire x1="111.76" y1="55.88" x2="121.92" y2="55.88" width="0.1524" layer="91"/>
<junction x="121.92" y="55.88"/>
</segment>
</net>
<net name="N$423" class="0">
<segment>
<pinref part="R257" gate="G$1" pin="1"/>
<pinref part="IC1" gate="G$1" pin="SDA"/>
<wire x1="116.84" y1="68.58" x2="116.84" y2="53.34" width="0.1524" layer="91"/>
<wire x1="116.84" y1="53.34" x2="139.7" y2="53.34" width="0.1524" layer="91"/>
<pinref part="U$1" gate="G$1" pin="G21/SDA"/>
<wire x1="83.82" y1="68.58" x2="109.22" y2="68.58" width="0.1524" layer="91"/>
<wire x1="109.22" y1="68.58" x2="109.22" y2="53.34" width="0.1524" layer="91"/>
<wire x1="109.22" y1="53.34" x2="116.84" y2="53.34" width="0.1524" layer="91"/>
<junction x="116.84" y="53.34"/>
</segment>
</net>
<net name="N$132" class="0">
<segment>
<pinref part="R70" gate="G$1" pin="2"/>
<wire x1="-12.7" y1="520.7" x2="58.42" y2="520.7" width="0.1524" layer="91"/>
<wire x1="58.42" y1="520.7" x2="58.42" y2="406.4" width="0.1524" layer="91"/>
<pinref part="MIN_B" gate="G$1" pin="F"/>
<wire x1="58.42" y1="406.4" x2="53.34" y2="406.4" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$133" class="0">
<segment>
<pinref part="R108" gate="G$1" pin="2"/>
<wire x1="172.72" y1="518.16" x2="243.84" y2="518.16" width="0.1524" layer="91"/>
<wire x1="243.84" y1="518.16" x2="243.84" y2="403.86" width="0.1524" layer="91"/>
<pinref part="SEC_B" gate="G$1" pin="F"/>
<wire x1="243.84" y1="403.86" x2="238.76" y2="403.86" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$199" class="0">
<segment>
<pinref part="4543_DAY_A" gate="A" pin="F"/>
<pinref part="R258" gate="G$1" pin="1"/>
<wire x1="-396.24" y1="248.92" x2="-393.7" y2="248.92" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$200" class="0">
<segment>
<pinref part="4543_DAY_A" gate="A" pin="G"/>
<pinref part="R259" gate="G$1" pin="1"/>
<wire x1="-396.24" y1="246.38" x2="-393.7" y2="246.38" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$419" class="0">
<segment>
<pinref part="4543_DAY_A" gate="A" pin="E"/>
<pinref part="R260" gate="G$1" pin="1"/>
<wire x1="-396.24" y1="243.84" x2="-393.7" y2="243.84" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$424" class="0">
<segment>
<pinref part="4543_DAY_A" gate="A" pin="D"/>
<pinref part="R261" gate="G$1" pin="1"/>
<wire x1="-396.24" y1="241.3" x2="-393.7" y2="241.3" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$425" class="0">
<segment>
<pinref part="4543_DAY_A" gate="A" pin="C"/>
<pinref part="R262" gate="G$1" pin="1"/>
<wire x1="-396.24" y1="238.76" x2="-393.7" y2="238.76" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$426" class="0">
<segment>
<pinref part="4543_DAY_A" gate="A" pin="B"/>
<pinref part="R263" gate="G$1" pin="1"/>
<wire x1="-396.24" y1="236.22" x2="-393.7" y2="236.22" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$427" class="0">
<segment>
<pinref part="4543_DAY_A" gate="A" pin="A"/>
<pinref part="R264" gate="G$1" pin="1"/>
<wire x1="-396.24" y1="233.68" x2="-393.7" y2="233.68" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$428" class="0">
<segment>
<pinref part="R264" gate="G$1" pin="2"/>
<pinref part="2003_DAY_A" gate="A" pin="I7"/>
<wire x1="-383.54" y1="233.68" x2="-381" y2="233.68" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$429" class="0">
<segment>
<pinref part="R263" gate="G$1" pin="2"/>
<pinref part="2003_DAY_A" gate="A" pin="I6"/>
<wire x1="-383.54" y1="236.22" x2="-381" y2="236.22" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$430" class="0">
<segment>
<pinref part="R262" gate="G$1" pin="2"/>
<pinref part="2003_DAY_A" gate="A" pin="I5"/>
<wire x1="-383.54" y1="238.76" x2="-381" y2="238.76" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$431" class="0">
<segment>
<pinref part="R261" gate="G$1" pin="2"/>
<pinref part="2003_DAY_A" gate="A" pin="I4"/>
<wire x1="-383.54" y1="241.3" x2="-381" y2="241.3" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$432" class="0">
<segment>
<pinref part="R260" gate="G$1" pin="2"/>
<pinref part="2003_DAY_A" gate="A" pin="I3"/>
<wire x1="-383.54" y1="243.84" x2="-381" y2="243.84" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$433" class="0">
<segment>
<pinref part="R259" gate="G$1" pin="2"/>
<pinref part="2003_DAY_A" gate="A" pin="I2"/>
<wire x1="-383.54" y1="246.38" x2="-381" y2="246.38" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$434" class="0">
<segment>
<pinref part="R258" gate="G$1" pin="2"/>
<pinref part="2003_DAY_A" gate="A" pin="I1"/>
<wire x1="-383.54" y1="248.92" x2="-381" y2="248.92" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$435" class="0">
<segment>
<pinref part="4543_DAY_A" gate="A" pin="LD"/>
<pinref part="R265" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$436" class="0">
<segment>
<pinref part="4543_DAY_A" gate="A" pin="IB"/>
<pinref part="R266" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$437" class="0">
<segment>
<pinref part="4543_DAY_A" gate="A" pin="IC"/>
<pinref part="R267" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$438" class="0">
<segment>
<pinref part="4543_DAY_A" gate="A" pin="ID"/>
<pinref part="R268" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$439" class="0">
<segment>
<pinref part="4543_DAY_A" gate="A" pin="IA"/>
<pinref part="R269" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$441" class="0">
<segment>
<pinref part="2003_DAY_A" gate="A" pin="O1"/>
<pinref part="R270" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$442" class="0">
<segment>
<pinref part="2003_DAY_A" gate="A" pin="O2"/>
<pinref part="R271" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$443" class="0">
<segment>
<pinref part="2003_DAY_A" gate="A" pin="O3"/>
<pinref part="R272" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$444" class="0">
<segment>
<pinref part="2003_DAY_A" gate="A" pin="O4"/>
<pinref part="R273" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$445" class="0">
<segment>
<pinref part="2003_DAY_A" gate="A" pin="O5"/>
<pinref part="R274" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$446" class="0">
<segment>
<pinref part="2003_DAY_A" gate="A" pin="O6"/>
<pinref part="R275" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$447" class="0">
<segment>
<pinref part="2003_DAY_A" gate="A" pin="O7"/>
<pinref part="R276" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$440" class="0">
<segment>
<pinref part="R276" gate="G$1" pin="2"/>
<wire x1="-345.44" y1="233.68" x2="-345.44" y2="205.74" width="0.1524" layer="91"/>
<pinref part="DAY_A" gate="G$1" pin="A"/>
<wire x1="-345.44" y1="205.74" x2="-373.38" y2="205.74" width="0.1524" layer="91"/>
<wire x1="-373.38" y1="205.74" x2="-373.38" y2="200.66" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$448" class="0">
<segment>
<pinref part="R275" gate="G$1" pin="2"/>
<wire x1="-345.44" y1="236.22" x2="-342.9" y2="236.22" width="0.1524" layer="91"/>
<wire x1="-342.9" y1="236.22" x2="-342.9" y2="208.28" width="0.1524" layer="91"/>
<wire x1="-342.9" y1="208.28" x2="-375.92" y2="208.28" width="0.1524" layer="91"/>
<wire x1="-375.92" y1="208.28" x2="-375.92" y2="198.12" width="0.1524" layer="91"/>
<pinref part="DAY_A" gate="G$1" pin="B"/>
<wire x1="-375.92" y1="198.12" x2="-373.38" y2="198.12" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$449" class="0">
<segment>
<pinref part="R274" gate="G$1" pin="2"/>
<wire x1="-345.44" y1="238.76" x2="-340.36" y2="238.76" width="0.1524" layer="91"/>
<wire x1="-340.36" y1="238.76" x2="-340.36" y2="210.82" width="0.1524" layer="91"/>
<wire x1="-340.36" y1="210.82" x2="-378.46" y2="210.82" width="0.1524" layer="91"/>
<wire x1="-378.46" y1="210.82" x2="-378.46" y2="195.58" width="0.1524" layer="91"/>
<pinref part="DAY_A" gate="G$1" pin="C"/>
<wire x1="-378.46" y1="195.58" x2="-373.38" y2="195.58" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$450" class="0">
<segment>
<pinref part="R273" gate="G$1" pin="2"/>
<wire x1="-345.44" y1="241.3" x2="-335.28" y2="241.3" width="0.1524" layer="91"/>
<wire x1="-335.28" y1="241.3" x2="-335.28" y2="213.36" width="0.1524" layer="91"/>
<wire x1="-335.28" y1="213.36" x2="-381" y2="213.36" width="0.1524" layer="91"/>
<wire x1="-381" y1="213.36" x2="-381" y2="193.04" width="0.1524" layer="91"/>
<pinref part="DAY_A" gate="G$1" pin="D"/>
<wire x1="-381" y1="193.04" x2="-373.38" y2="193.04" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$451" class="0">
<segment>
<pinref part="R272" gate="G$1" pin="2"/>
<wire x1="-345.44" y1="243.84" x2="-332.74" y2="243.84" width="0.1524" layer="91"/>
<wire x1="-332.74" y1="243.84" x2="-332.74" y2="215.9" width="0.1524" layer="91"/>
<wire x1="-332.74" y1="215.9" x2="-383.54" y2="215.9" width="0.1524" layer="91"/>
<wire x1="-383.54" y1="215.9" x2="-383.54" y2="190.5" width="0.1524" layer="91"/>
<pinref part="DAY_A" gate="G$1" pin="E"/>
<wire x1="-383.54" y1="190.5" x2="-373.38" y2="190.5" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$452" class="0">
<segment>
<pinref part="R271" gate="G$1" pin="2"/>
<wire x1="-345.44" y1="246.38" x2="-330.2" y2="246.38" width="0.1524" layer="91"/>
<wire x1="-330.2" y1="246.38" x2="-330.2" y2="198.12" width="0.1524" layer="91"/>
<wire x1="-330.2" y1="198.12" x2="-358.14" y2="198.12" width="0.1524" layer="91"/>
<pinref part="DAY_A" gate="G$1" pin="G"/>
</segment>
</net>
<net name="N$453" class="0">
<segment>
<pinref part="R270" gate="G$1" pin="2"/>
<wire x1="-345.44" y1="248.92" x2="-327.66" y2="248.92" width="0.1524" layer="91"/>
<wire x1="-327.66" y1="248.92" x2="-327.66" y2="200.66" width="0.1524" layer="91"/>
<pinref part="DAY_A" gate="G$1" pin="F"/>
<wire x1="-327.66" y1="200.66" x2="-358.14" y2="200.66" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$201" class="0">
<segment>
<pinref part="4543_DAY_B" gate="A" pin="F"/>
<pinref part="R115" gate="G$1" pin="1"/>
<wire x1="-271.78" y1="248.92" x2="-269.24" y2="248.92" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$202" class="0">
<segment>
<pinref part="4543_DAY_B" gate="A" pin="G"/>
<pinref part="R116" gate="G$1" pin="1"/>
<wire x1="-271.78" y1="246.38" x2="-269.24" y2="246.38" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$203" class="0">
<segment>
<pinref part="4543_DAY_B" gate="A" pin="E"/>
<pinref part="R117" gate="G$1" pin="1"/>
<wire x1="-271.78" y1="243.84" x2="-269.24" y2="243.84" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$204" class="0">
<segment>
<pinref part="4543_DAY_B" gate="A" pin="D"/>
<pinref part="R118" gate="G$1" pin="1"/>
<wire x1="-271.78" y1="241.3" x2="-269.24" y2="241.3" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$205" class="0">
<segment>
<pinref part="4543_DAY_B" gate="A" pin="C"/>
<pinref part="R119" gate="G$1" pin="1"/>
<wire x1="-271.78" y1="238.76" x2="-269.24" y2="238.76" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$206" class="0">
<segment>
<pinref part="4543_DAY_B" gate="A" pin="B"/>
<pinref part="R120" gate="G$1" pin="1"/>
<wire x1="-271.78" y1="236.22" x2="-269.24" y2="236.22" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$207" class="0">
<segment>
<pinref part="4543_DAY_B" gate="A" pin="A"/>
<pinref part="R121" gate="G$1" pin="1"/>
<wire x1="-271.78" y1="233.68" x2="-269.24" y2="233.68" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$208" class="0">
<segment>
<pinref part="R121" gate="G$1" pin="2"/>
<pinref part="2003_DAY_B" gate="A" pin="I7"/>
<wire x1="-259.08" y1="233.68" x2="-256.54" y2="233.68" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$209" class="0">
<segment>
<pinref part="R120" gate="G$1" pin="2"/>
<pinref part="2003_DAY_B" gate="A" pin="I6"/>
<wire x1="-259.08" y1="236.22" x2="-256.54" y2="236.22" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$210" class="0">
<segment>
<pinref part="R119" gate="G$1" pin="2"/>
<pinref part="2003_DAY_B" gate="A" pin="I5"/>
<wire x1="-259.08" y1="238.76" x2="-256.54" y2="238.76" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$211" class="0">
<segment>
<pinref part="R118" gate="G$1" pin="2"/>
<pinref part="2003_DAY_B" gate="A" pin="I4"/>
<wire x1="-259.08" y1="241.3" x2="-256.54" y2="241.3" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$212" class="0">
<segment>
<pinref part="R117" gate="G$1" pin="2"/>
<pinref part="2003_DAY_B" gate="A" pin="I3"/>
<wire x1="-259.08" y1="243.84" x2="-256.54" y2="243.84" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$213" class="0">
<segment>
<pinref part="R116" gate="G$1" pin="2"/>
<pinref part="2003_DAY_B" gate="A" pin="I2"/>
<wire x1="-259.08" y1="246.38" x2="-256.54" y2="246.38" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$214" class="0">
<segment>
<pinref part="R115" gate="G$1" pin="2"/>
<pinref part="2003_DAY_B" gate="A" pin="I1"/>
<wire x1="-259.08" y1="248.92" x2="-256.54" y2="248.92" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$215" class="0">
<segment>
<pinref part="4543_DAY_B" gate="A" pin="LD"/>
<pinref part="R122" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$216" class="0">
<segment>
<pinref part="4543_DAY_B" gate="A" pin="IB"/>
<pinref part="R123" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$217" class="0">
<segment>
<pinref part="4543_DAY_B" gate="A" pin="IC"/>
<pinref part="R124" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$218" class="0">
<segment>
<pinref part="4543_DAY_B" gate="A" pin="ID"/>
<pinref part="R125" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$219" class="0">
<segment>
<pinref part="4543_DAY_B" gate="A" pin="IA"/>
<pinref part="R126" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$220" class="0">
<segment>
<pinref part="2003_DAY_B" gate="A" pin="O1"/>
<pinref part="R127" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$221" class="0">
<segment>
<pinref part="2003_DAY_B" gate="A" pin="O2"/>
<pinref part="R128" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$222" class="0">
<segment>
<pinref part="2003_DAY_B" gate="A" pin="O3"/>
<pinref part="R129" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$223" class="0">
<segment>
<pinref part="2003_DAY_B" gate="A" pin="O4"/>
<pinref part="R130" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$224" class="0">
<segment>
<pinref part="2003_DAY_B" gate="A" pin="O5"/>
<pinref part="R131" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$225" class="0">
<segment>
<pinref part="2003_DAY_B" gate="A" pin="O6"/>
<pinref part="R132" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$226" class="0">
<segment>
<pinref part="2003_DAY_B" gate="A" pin="O7"/>
<pinref part="R133" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$227" class="0">
<segment>
<pinref part="R133" gate="G$1" pin="2"/>
<wire x1="-220.98" y1="233.68" x2="-220.98" y2="205.74" width="0.1524" layer="91"/>
<pinref part="DAY_B" gate="G$1" pin="A"/>
<wire x1="-220.98" y1="205.74" x2="-248.92" y2="205.74" width="0.1524" layer="91"/>
<wire x1="-248.92" y1="205.74" x2="-248.92" y2="200.66" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$228" class="0">
<segment>
<pinref part="R132" gate="G$1" pin="2"/>
<wire x1="-220.98" y1="236.22" x2="-218.44" y2="236.22" width="0.1524" layer="91"/>
<wire x1="-218.44" y1="236.22" x2="-218.44" y2="208.28" width="0.1524" layer="91"/>
<wire x1="-218.44" y1="208.28" x2="-251.46" y2="208.28" width="0.1524" layer="91"/>
<wire x1="-251.46" y1="208.28" x2="-251.46" y2="198.12" width="0.1524" layer="91"/>
<pinref part="DAY_B" gate="G$1" pin="B"/>
<wire x1="-251.46" y1="198.12" x2="-248.92" y2="198.12" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$229" class="0">
<segment>
<pinref part="R131" gate="G$1" pin="2"/>
<wire x1="-220.98" y1="238.76" x2="-215.9" y2="238.76" width="0.1524" layer="91"/>
<wire x1="-215.9" y1="238.76" x2="-215.9" y2="210.82" width="0.1524" layer="91"/>
<wire x1="-215.9" y1="210.82" x2="-254" y2="210.82" width="0.1524" layer="91"/>
<wire x1="-254" y1="210.82" x2="-254" y2="195.58" width="0.1524" layer="91"/>
<pinref part="DAY_B" gate="G$1" pin="C"/>
<wire x1="-254" y1="195.58" x2="-248.92" y2="195.58" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$230" class="0">
<segment>
<pinref part="R130" gate="G$1" pin="2"/>
<wire x1="-220.98" y1="241.3" x2="-210.82" y2="241.3" width="0.1524" layer="91"/>
<wire x1="-210.82" y1="241.3" x2="-210.82" y2="213.36" width="0.1524" layer="91"/>
<wire x1="-210.82" y1="213.36" x2="-256.54" y2="213.36" width="0.1524" layer="91"/>
<wire x1="-256.54" y1="213.36" x2="-256.54" y2="193.04" width="0.1524" layer="91"/>
<pinref part="DAY_B" gate="G$1" pin="D"/>
<wire x1="-256.54" y1="193.04" x2="-248.92" y2="193.04" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$231" class="0">
<segment>
<pinref part="R129" gate="G$1" pin="2"/>
<wire x1="-220.98" y1="243.84" x2="-208.28" y2="243.84" width="0.1524" layer="91"/>
<wire x1="-208.28" y1="243.84" x2="-208.28" y2="215.9" width="0.1524" layer="91"/>
<wire x1="-208.28" y1="215.9" x2="-259.08" y2="215.9" width="0.1524" layer="91"/>
<wire x1="-259.08" y1="215.9" x2="-259.08" y2="190.5" width="0.1524" layer="91"/>
<pinref part="DAY_B" gate="G$1" pin="E"/>
<wire x1="-259.08" y1="190.5" x2="-248.92" y2="190.5" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$232" class="0">
<segment>
<pinref part="R128" gate="G$1" pin="2"/>
<wire x1="-220.98" y1="246.38" x2="-205.74" y2="246.38" width="0.1524" layer="91"/>
<wire x1="-205.74" y1="246.38" x2="-205.74" y2="198.12" width="0.1524" layer="91"/>
<wire x1="-205.74" y1="198.12" x2="-233.68" y2="198.12" width="0.1524" layer="91"/>
<pinref part="DAY_B" gate="G$1" pin="G"/>
</segment>
</net>
<net name="N$233" class="0">
<segment>
<pinref part="R127" gate="G$1" pin="2"/>
<wire x1="-220.98" y1="248.92" x2="-203.2" y2="248.92" width="0.1524" layer="91"/>
<wire x1="-203.2" y1="248.92" x2="-203.2" y2="200.66" width="0.1524" layer="91"/>
<pinref part="DAY_B" gate="G$1" pin="F"/>
<wire x1="-203.2" y1="200.66" x2="-233.68" y2="200.66" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$234" class="0">
<segment>
<pinref part="4543_MONTH_A" gate="A" pin="F"/>
<pinref part="R134" gate="G$1" pin="1"/>
<wire x1="-139.7" y1="256.54" x2="-137.16" y2="256.54" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$235" class="0">
<segment>
<pinref part="4543_MONTH_A" gate="A" pin="G"/>
<pinref part="R135" gate="G$1" pin="1"/>
<wire x1="-139.7" y1="254" x2="-137.16" y2="254" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$236" class="0">
<segment>
<pinref part="4543_MONTH_A" gate="A" pin="E"/>
<pinref part="R136" gate="G$1" pin="1"/>
<wire x1="-139.7" y1="251.46" x2="-137.16" y2="251.46" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$237" class="0">
<segment>
<pinref part="4543_MONTH_A" gate="A" pin="D"/>
<pinref part="R137" gate="G$1" pin="1"/>
<wire x1="-139.7" y1="248.92" x2="-137.16" y2="248.92" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$238" class="0">
<segment>
<pinref part="4543_MONTH_A" gate="A" pin="C"/>
<pinref part="R138" gate="G$1" pin="1"/>
<wire x1="-139.7" y1="246.38" x2="-137.16" y2="246.38" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$239" class="0">
<segment>
<pinref part="4543_MONTH_A" gate="A" pin="B"/>
<pinref part="R139" gate="G$1" pin="1"/>
<wire x1="-139.7" y1="243.84" x2="-137.16" y2="243.84" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$240" class="0">
<segment>
<pinref part="4543_MONTH_A" gate="A" pin="A"/>
<pinref part="R140" gate="G$1" pin="1"/>
<wire x1="-139.7" y1="241.3" x2="-137.16" y2="241.3" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$241" class="0">
<segment>
<pinref part="R140" gate="G$1" pin="2"/>
<pinref part="2003_MONTH_A" gate="A" pin="I7"/>
<wire x1="-127" y1="241.3" x2="-124.46" y2="241.3" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$242" class="0">
<segment>
<pinref part="R139" gate="G$1" pin="2"/>
<pinref part="2003_MONTH_A" gate="A" pin="I6"/>
<wire x1="-127" y1="243.84" x2="-124.46" y2="243.84" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$243" class="0">
<segment>
<pinref part="R138" gate="G$1" pin="2"/>
<pinref part="2003_MONTH_A" gate="A" pin="I5"/>
<wire x1="-127" y1="246.38" x2="-124.46" y2="246.38" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$244" class="0">
<segment>
<pinref part="R137" gate="G$1" pin="2"/>
<pinref part="2003_MONTH_A" gate="A" pin="I4"/>
<wire x1="-127" y1="248.92" x2="-124.46" y2="248.92" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$245" class="0">
<segment>
<pinref part="R136" gate="G$1" pin="2"/>
<pinref part="2003_MONTH_A" gate="A" pin="I3"/>
<wire x1="-127" y1="251.46" x2="-124.46" y2="251.46" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$246" class="0">
<segment>
<pinref part="R135" gate="G$1" pin="2"/>
<pinref part="2003_MONTH_A" gate="A" pin="I2"/>
<wire x1="-127" y1="254" x2="-124.46" y2="254" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$247" class="0">
<segment>
<pinref part="R134" gate="G$1" pin="2"/>
<pinref part="2003_MONTH_A" gate="A" pin="I1"/>
<wire x1="-127" y1="256.54" x2="-124.46" y2="256.54" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$248" class="0">
<segment>
<pinref part="4543_MONTH_A" gate="A" pin="LD"/>
<pinref part="R141" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$249" class="0">
<segment>
<pinref part="4543_MONTH_A" gate="A" pin="IB"/>
<pinref part="R142" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$250" class="0">
<segment>
<pinref part="4543_MONTH_A" gate="A" pin="IC"/>
<pinref part="R143" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$251" class="0">
<segment>
<pinref part="4543_MONTH_A" gate="A" pin="ID"/>
<pinref part="R144" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$252" class="0">
<segment>
<pinref part="4543_MONTH_A" gate="A" pin="IA"/>
<pinref part="R145" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$253" class="0">
<segment>
<pinref part="2003_MONTH_A" gate="A" pin="O1"/>
<pinref part="R146" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$254" class="0">
<segment>
<pinref part="2003_MONTH_A" gate="A" pin="O2"/>
<pinref part="R147" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$255" class="0">
<segment>
<pinref part="2003_MONTH_A" gate="A" pin="O3"/>
<pinref part="R148" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$256" class="0">
<segment>
<pinref part="2003_MONTH_A" gate="A" pin="O4"/>
<pinref part="R149" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$257" class="0">
<segment>
<pinref part="2003_MONTH_A" gate="A" pin="O5"/>
<pinref part="R150" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$258" class="0">
<segment>
<pinref part="2003_MONTH_A" gate="A" pin="O6"/>
<pinref part="R151" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$259" class="0">
<segment>
<pinref part="2003_MONTH_A" gate="A" pin="O7"/>
<pinref part="R152" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$260" class="0">
<segment>
<pinref part="R152" gate="G$1" pin="2"/>
<wire x1="-88.9" y1="241.3" x2="-88.9" y2="213.36" width="0.1524" layer="91"/>
<pinref part="MONTH_A" gate="G$1" pin="A"/>
<wire x1="-88.9" y1="213.36" x2="-116.84" y2="213.36" width="0.1524" layer="91"/>
<wire x1="-116.84" y1="213.36" x2="-116.84" y2="208.28" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$454" class="0">
<segment>
<pinref part="R151" gate="G$1" pin="2"/>
<wire x1="-88.9" y1="243.84" x2="-86.36" y2="243.84" width="0.1524" layer="91"/>
<wire x1="-86.36" y1="243.84" x2="-86.36" y2="215.9" width="0.1524" layer="91"/>
<wire x1="-86.36" y1="215.9" x2="-119.38" y2="215.9" width="0.1524" layer="91"/>
<wire x1="-119.38" y1="215.9" x2="-119.38" y2="205.74" width="0.1524" layer="91"/>
<pinref part="MONTH_A" gate="G$1" pin="B"/>
<wire x1="-119.38" y1="205.74" x2="-116.84" y2="205.74" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$455" class="0">
<segment>
<pinref part="R150" gate="G$1" pin="2"/>
<wire x1="-88.9" y1="246.38" x2="-83.82" y2="246.38" width="0.1524" layer="91"/>
<wire x1="-83.82" y1="246.38" x2="-83.82" y2="218.44" width="0.1524" layer="91"/>
<wire x1="-83.82" y1="218.44" x2="-121.92" y2="218.44" width="0.1524" layer="91"/>
<wire x1="-121.92" y1="218.44" x2="-121.92" y2="203.2" width="0.1524" layer="91"/>
<pinref part="MONTH_A" gate="G$1" pin="C"/>
<wire x1="-121.92" y1="203.2" x2="-116.84" y2="203.2" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$456" class="0">
<segment>
<pinref part="R149" gate="G$1" pin="2"/>
<wire x1="-88.9" y1="248.92" x2="-78.74" y2="248.92" width="0.1524" layer="91"/>
<wire x1="-78.74" y1="248.92" x2="-78.74" y2="220.98" width="0.1524" layer="91"/>
<wire x1="-78.74" y1="220.98" x2="-124.46" y2="220.98" width="0.1524" layer="91"/>
<wire x1="-124.46" y1="220.98" x2="-124.46" y2="200.66" width="0.1524" layer="91"/>
<pinref part="MONTH_A" gate="G$1" pin="D"/>
<wire x1="-124.46" y1="200.66" x2="-116.84" y2="200.66" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$457" class="0">
<segment>
<pinref part="R148" gate="G$1" pin="2"/>
<wire x1="-88.9" y1="251.46" x2="-76.2" y2="251.46" width="0.1524" layer="91"/>
<wire x1="-76.2" y1="251.46" x2="-76.2" y2="223.52" width="0.1524" layer="91"/>
<wire x1="-76.2" y1="223.52" x2="-127" y2="223.52" width="0.1524" layer="91"/>
<wire x1="-127" y1="223.52" x2="-127" y2="198.12" width="0.1524" layer="91"/>
<pinref part="MONTH_A" gate="G$1" pin="E"/>
<wire x1="-127" y1="198.12" x2="-116.84" y2="198.12" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$458" class="0">
<segment>
<pinref part="R147" gate="G$1" pin="2"/>
<wire x1="-88.9" y1="254" x2="-73.66" y2="254" width="0.1524" layer="91"/>
<wire x1="-73.66" y1="254" x2="-73.66" y2="205.74" width="0.1524" layer="91"/>
<wire x1="-73.66" y1="205.74" x2="-101.6" y2="205.74" width="0.1524" layer="91"/>
<pinref part="MONTH_A" gate="G$1" pin="G"/>
</segment>
</net>
<net name="N$459" class="0">
<segment>
<pinref part="R146" gate="G$1" pin="2"/>
<wire x1="-88.9" y1="256.54" x2="-71.12" y2="256.54" width="0.1524" layer="91"/>
<wire x1="-71.12" y1="256.54" x2="-71.12" y2="208.28" width="0.1524" layer="91"/>
<pinref part="MONTH_A" gate="G$1" pin="F"/>
<wire x1="-71.12" y1="208.28" x2="-101.6" y2="208.28" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$261" class="0">
<segment>
<pinref part="4543_MONTH_B" gate="A" pin="F"/>
<pinref part="R153" gate="G$1" pin="1"/>
<wire x1="-22.86" y1="256.54" x2="-20.32" y2="256.54" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$262" class="0">
<segment>
<pinref part="4543_MONTH_B" gate="A" pin="G"/>
<pinref part="R154" gate="G$1" pin="1"/>
<wire x1="-22.86" y1="254" x2="-20.32" y2="254" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$263" class="0">
<segment>
<pinref part="4543_MONTH_B" gate="A" pin="E"/>
<pinref part="R155" gate="G$1" pin="1"/>
<wire x1="-22.86" y1="251.46" x2="-20.32" y2="251.46" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$264" class="0">
<segment>
<pinref part="4543_MONTH_B" gate="A" pin="D"/>
<pinref part="R156" gate="G$1" pin="1"/>
<wire x1="-22.86" y1="248.92" x2="-20.32" y2="248.92" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$265" class="0">
<segment>
<pinref part="4543_MONTH_B" gate="A" pin="C"/>
<pinref part="R157" gate="G$1" pin="1"/>
<wire x1="-22.86" y1="246.38" x2="-20.32" y2="246.38" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$266" class="0">
<segment>
<pinref part="4543_MONTH_B" gate="A" pin="B"/>
<pinref part="R158" gate="G$1" pin="1"/>
<wire x1="-22.86" y1="243.84" x2="-20.32" y2="243.84" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$267" class="0">
<segment>
<pinref part="4543_MONTH_B" gate="A" pin="A"/>
<pinref part="R159" gate="G$1" pin="1"/>
<wire x1="-22.86" y1="241.3" x2="-20.32" y2="241.3" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$268" class="0">
<segment>
<pinref part="R159" gate="G$1" pin="2"/>
<pinref part="2003_MONTH_B" gate="A" pin="I7"/>
<wire x1="-10.16" y1="241.3" x2="-7.62" y2="241.3" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$269" class="0">
<segment>
<pinref part="R158" gate="G$1" pin="2"/>
<pinref part="2003_MONTH_B" gate="A" pin="I6"/>
<wire x1="-10.16" y1="243.84" x2="-7.62" y2="243.84" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$270" class="0">
<segment>
<pinref part="R157" gate="G$1" pin="2"/>
<pinref part="2003_MONTH_B" gate="A" pin="I5"/>
<wire x1="-10.16" y1="246.38" x2="-7.62" y2="246.38" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$271" class="0">
<segment>
<pinref part="R156" gate="G$1" pin="2"/>
<pinref part="2003_MONTH_B" gate="A" pin="I4"/>
<wire x1="-10.16" y1="248.92" x2="-7.62" y2="248.92" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$272" class="0">
<segment>
<pinref part="R155" gate="G$1" pin="2"/>
<pinref part="2003_MONTH_B" gate="A" pin="I3"/>
<wire x1="-10.16" y1="251.46" x2="-7.62" y2="251.46" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$273" class="0">
<segment>
<pinref part="R154" gate="G$1" pin="2"/>
<pinref part="2003_MONTH_B" gate="A" pin="I2"/>
<wire x1="-10.16" y1="254" x2="-7.62" y2="254" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$274" class="0">
<segment>
<pinref part="R153" gate="G$1" pin="2"/>
<pinref part="2003_MONTH_B" gate="A" pin="I1"/>
<wire x1="-10.16" y1="256.54" x2="-7.62" y2="256.54" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$275" class="0">
<segment>
<pinref part="4543_MONTH_B" gate="A" pin="LD"/>
<pinref part="R160" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$276" class="0">
<segment>
<pinref part="4543_MONTH_B" gate="A" pin="IB"/>
<pinref part="R161" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$277" class="0">
<segment>
<pinref part="4543_MONTH_B" gate="A" pin="IC"/>
<pinref part="R162" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$278" class="0">
<segment>
<pinref part="4543_MONTH_B" gate="A" pin="ID"/>
<pinref part="R163" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$279" class="0">
<segment>
<pinref part="4543_MONTH_B" gate="A" pin="IA"/>
<pinref part="R164" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$280" class="0">
<segment>
<pinref part="2003_MONTH_B" gate="A" pin="O1"/>
<pinref part="R165" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$460" class="0">
<segment>
<pinref part="2003_MONTH_B" gate="A" pin="O2"/>
<pinref part="R166" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$461" class="0">
<segment>
<pinref part="2003_MONTH_B" gate="A" pin="O3"/>
<pinref part="R277" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$462" class="0">
<segment>
<pinref part="2003_MONTH_B" gate="A" pin="O4"/>
<pinref part="R278" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$463" class="0">
<segment>
<pinref part="2003_MONTH_B" gate="A" pin="O5"/>
<pinref part="R279" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$464" class="0">
<segment>
<pinref part="2003_MONTH_B" gate="A" pin="O6"/>
<pinref part="R280" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$465" class="0">
<segment>
<pinref part="2003_MONTH_B" gate="A" pin="O7"/>
<pinref part="R281" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$466" class="0">
<segment>
<pinref part="R281" gate="G$1" pin="2"/>
<wire x1="27.94" y1="241.3" x2="27.94" y2="213.36" width="0.1524" layer="91"/>
<pinref part="MONTH_B" gate="G$1" pin="A"/>
<wire x1="27.94" y1="213.36" x2="0" y2="213.36" width="0.1524" layer="91"/>
<wire x1="0" y1="213.36" x2="0" y2="208.28" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$467" class="0">
<segment>
<pinref part="R280" gate="G$1" pin="2"/>
<wire x1="27.94" y1="243.84" x2="30.48" y2="243.84" width="0.1524" layer="91"/>
<wire x1="30.48" y1="243.84" x2="30.48" y2="215.9" width="0.1524" layer="91"/>
<wire x1="30.48" y1="215.9" x2="-2.54" y2="215.9" width="0.1524" layer="91"/>
<wire x1="-2.54" y1="215.9" x2="-2.54" y2="205.74" width="0.1524" layer="91"/>
<pinref part="MONTH_B" gate="G$1" pin="B"/>
<wire x1="-2.54" y1="205.74" x2="0" y2="205.74" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$468" class="0">
<segment>
<pinref part="R279" gate="G$1" pin="2"/>
<wire x1="27.94" y1="246.38" x2="33.02" y2="246.38" width="0.1524" layer="91"/>
<wire x1="33.02" y1="246.38" x2="33.02" y2="218.44" width="0.1524" layer="91"/>
<wire x1="33.02" y1="218.44" x2="-5.08" y2="218.44" width="0.1524" layer="91"/>
<wire x1="-5.08" y1="218.44" x2="-5.08" y2="203.2" width="0.1524" layer="91"/>
<pinref part="MONTH_B" gate="G$1" pin="C"/>
<wire x1="-5.08" y1="203.2" x2="0" y2="203.2" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$469" class="0">
<segment>
<pinref part="R278" gate="G$1" pin="2"/>
<wire x1="27.94" y1="248.92" x2="38.1" y2="248.92" width="0.1524" layer="91"/>
<wire x1="38.1" y1="248.92" x2="38.1" y2="220.98" width="0.1524" layer="91"/>
<wire x1="38.1" y1="220.98" x2="-7.62" y2="220.98" width="0.1524" layer="91"/>
<wire x1="-7.62" y1="220.98" x2="-7.62" y2="200.66" width="0.1524" layer="91"/>
<pinref part="MONTH_B" gate="G$1" pin="D"/>
<wire x1="-7.62" y1="200.66" x2="0" y2="200.66" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$470" class="0">
<segment>
<pinref part="R277" gate="G$1" pin="2"/>
<wire x1="27.94" y1="251.46" x2="40.64" y2="251.46" width="0.1524" layer="91"/>
<wire x1="40.64" y1="251.46" x2="40.64" y2="223.52" width="0.1524" layer="91"/>
<wire x1="40.64" y1="223.52" x2="-10.16" y2="223.52" width="0.1524" layer="91"/>
<wire x1="-10.16" y1="223.52" x2="-10.16" y2="198.12" width="0.1524" layer="91"/>
<pinref part="MONTH_B" gate="G$1" pin="E"/>
<wire x1="-10.16" y1="198.12" x2="0" y2="198.12" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$471" class="0">
<segment>
<pinref part="R166" gate="G$1" pin="2"/>
<wire x1="27.94" y1="254" x2="43.18" y2="254" width="0.1524" layer="91"/>
<wire x1="43.18" y1="254" x2="43.18" y2="205.74" width="0.1524" layer="91"/>
<wire x1="43.18" y1="205.74" x2="15.24" y2="205.74" width="0.1524" layer="91"/>
<pinref part="MONTH_B" gate="G$1" pin="G"/>
</segment>
</net>
<net name="N$472" class="0">
<segment>
<pinref part="R165" gate="G$1" pin="2"/>
<wire x1="27.94" y1="256.54" x2="45.72" y2="256.54" width="0.1524" layer="91"/>
<wire x1="45.72" y1="256.54" x2="45.72" y2="208.28" width="0.1524" layer="91"/>
<pinref part="MONTH_B" gate="G$1" pin="F"/>
<wire x1="45.72" y1="208.28" x2="15.24" y2="208.28" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$473" class="0">
<segment>
<pinref part="4543_YEAR_A" gate="A" pin="F"/>
<pinref part="R282" gate="G$1" pin="1"/>
<wire x1="149.86" y1="266.7" x2="152.4" y2="266.7" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$474" class="0">
<segment>
<pinref part="4543_YEAR_A" gate="A" pin="G"/>
<pinref part="R283" gate="G$1" pin="1"/>
<wire x1="149.86" y1="264.16" x2="152.4" y2="264.16" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$475" class="0">
<segment>
<pinref part="4543_YEAR_A" gate="A" pin="E"/>
<pinref part="R284" gate="G$1" pin="1"/>
<wire x1="149.86" y1="261.62" x2="152.4" y2="261.62" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$476" class="0">
<segment>
<pinref part="4543_YEAR_A" gate="A" pin="D"/>
<pinref part="R285" gate="G$1" pin="1"/>
<wire x1="149.86" y1="259.08" x2="152.4" y2="259.08" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$477" class="0">
<segment>
<pinref part="4543_YEAR_A" gate="A" pin="C"/>
<pinref part="R286" gate="G$1" pin="1"/>
<wire x1="149.86" y1="256.54" x2="152.4" y2="256.54" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$478" class="0">
<segment>
<pinref part="4543_YEAR_A" gate="A" pin="B"/>
<pinref part="R287" gate="G$1" pin="1"/>
<wire x1="149.86" y1="254" x2="152.4" y2="254" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$479" class="0">
<segment>
<pinref part="4543_YEAR_A" gate="A" pin="A"/>
<pinref part="R288" gate="G$1" pin="1"/>
<wire x1="149.86" y1="251.46" x2="152.4" y2="251.46" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$480" class="0">
<segment>
<pinref part="R288" gate="G$1" pin="2"/>
<pinref part="2003_YEAR_A" gate="A" pin="I7"/>
<wire x1="162.56" y1="251.46" x2="165.1" y2="251.46" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$481" class="0">
<segment>
<pinref part="R287" gate="G$1" pin="2"/>
<pinref part="2003_YEAR_A" gate="A" pin="I6"/>
<wire x1="162.56" y1="254" x2="165.1" y2="254" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$482" class="0">
<segment>
<pinref part="R286" gate="G$1" pin="2"/>
<pinref part="2003_YEAR_A" gate="A" pin="I5"/>
<wire x1="162.56" y1="256.54" x2="165.1" y2="256.54" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$483" class="0">
<segment>
<pinref part="R285" gate="G$1" pin="2"/>
<pinref part="2003_YEAR_A" gate="A" pin="I4"/>
<wire x1="162.56" y1="259.08" x2="165.1" y2="259.08" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$484" class="0">
<segment>
<pinref part="R284" gate="G$1" pin="2"/>
<pinref part="2003_YEAR_A" gate="A" pin="I3"/>
<wire x1="162.56" y1="261.62" x2="165.1" y2="261.62" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$485" class="0">
<segment>
<pinref part="R283" gate="G$1" pin="2"/>
<pinref part="2003_YEAR_A" gate="A" pin="I2"/>
<wire x1="162.56" y1="264.16" x2="165.1" y2="264.16" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$486" class="0">
<segment>
<pinref part="R282" gate="G$1" pin="2"/>
<pinref part="2003_YEAR_A" gate="A" pin="I1"/>
<wire x1="162.56" y1="266.7" x2="165.1" y2="266.7" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$487" class="0">
<segment>
<pinref part="4543_YEAR_A" gate="A" pin="LD"/>
<pinref part="R289" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$488" class="0">
<segment>
<pinref part="4543_YEAR_A" gate="A" pin="IB"/>
<pinref part="R290" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$489" class="0">
<segment>
<pinref part="4543_YEAR_A" gate="A" pin="IC"/>
<pinref part="R291" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$490" class="0">
<segment>
<pinref part="4543_YEAR_A" gate="A" pin="ID"/>
<pinref part="R292" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$491" class="0">
<segment>
<pinref part="4543_YEAR_A" gate="A" pin="IA"/>
<pinref part="R293" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$492" class="0">
<segment>
<pinref part="2003_YEAR_A" gate="A" pin="O1"/>
<pinref part="R294" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$493" class="0">
<segment>
<pinref part="2003_YEAR_A" gate="A" pin="O2"/>
<pinref part="R295" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$494" class="0">
<segment>
<pinref part="2003_YEAR_A" gate="A" pin="O3"/>
<pinref part="R296" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$495" class="0">
<segment>
<pinref part="2003_YEAR_A" gate="A" pin="O4"/>
<pinref part="R297" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$496" class="0">
<segment>
<pinref part="2003_YEAR_A" gate="A" pin="O5"/>
<pinref part="R298" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$497" class="0">
<segment>
<pinref part="2003_YEAR_A" gate="A" pin="O6"/>
<pinref part="R299" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$498" class="0">
<segment>
<pinref part="2003_YEAR_A" gate="A" pin="O7"/>
<pinref part="R300" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$499" class="0">
<segment>
<pinref part="R300" gate="G$1" pin="2"/>
<wire x1="200.66" y1="251.46" x2="200.66" y2="223.52" width="0.1524" layer="91"/>
<pinref part="YEAR_A" gate="G$1" pin="A"/>
<wire x1="200.66" y1="223.52" x2="172.72" y2="223.52" width="0.1524" layer="91"/>
<wire x1="172.72" y1="223.52" x2="172.72" y2="218.44" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$500" class="0">
<segment>
<pinref part="R299" gate="G$1" pin="2"/>
<wire x1="200.66" y1="254" x2="203.2" y2="254" width="0.1524" layer="91"/>
<wire x1="203.2" y1="254" x2="203.2" y2="226.06" width="0.1524" layer="91"/>
<wire x1="203.2" y1="226.06" x2="170.18" y2="226.06" width="0.1524" layer="91"/>
<wire x1="170.18" y1="226.06" x2="170.18" y2="215.9" width="0.1524" layer="91"/>
<pinref part="YEAR_A" gate="G$1" pin="B"/>
<wire x1="170.18" y1="215.9" x2="172.72" y2="215.9" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$501" class="0">
<segment>
<pinref part="R298" gate="G$1" pin="2"/>
<wire x1="200.66" y1="256.54" x2="205.74" y2="256.54" width="0.1524" layer="91"/>
<wire x1="205.74" y1="256.54" x2="205.74" y2="228.6" width="0.1524" layer="91"/>
<wire x1="205.74" y1="228.6" x2="167.64" y2="228.6" width="0.1524" layer="91"/>
<wire x1="167.64" y1="228.6" x2="167.64" y2="213.36" width="0.1524" layer="91"/>
<pinref part="YEAR_A" gate="G$1" pin="C"/>
<wire x1="167.64" y1="213.36" x2="172.72" y2="213.36" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$502" class="0">
<segment>
<pinref part="R297" gate="G$1" pin="2"/>
<wire x1="200.66" y1="259.08" x2="210.82" y2="259.08" width="0.1524" layer="91"/>
<wire x1="210.82" y1="259.08" x2="210.82" y2="231.14" width="0.1524" layer="91"/>
<wire x1="210.82" y1="231.14" x2="165.1" y2="231.14" width="0.1524" layer="91"/>
<wire x1="165.1" y1="231.14" x2="165.1" y2="210.82" width="0.1524" layer="91"/>
<pinref part="YEAR_A" gate="G$1" pin="D"/>
<wire x1="165.1" y1="210.82" x2="172.72" y2="210.82" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$503" class="0">
<segment>
<pinref part="R296" gate="G$1" pin="2"/>
<wire x1="200.66" y1="261.62" x2="213.36" y2="261.62" width="0.1524" layer="91"/>
<wire x1="213.36" y1="261.62" x2="213.36" y2="233.68" width="0.1524" layer="91"/>
<wire x1="213.36" y1="233.68" x2="162.56" y2="233.68" width="0.1524" layer="91"/>
<wire x1="162.56" y1="233.68" x2="162.56" y2="208.28" width="0.1524" layer="91"/>
<pinref part="YEAR_A" gate="G$1" pin="E"/>
<wire x1="162.56" y1="208.28" x2="172.72" y2="208.28" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$504" class="0">
<segment>
<pinref part="R295" gate="G$1" pin="2"/>
<wire x1="200.66" y1="264.16" x2="215.9" y2="264.16" width="0.1524" layer="91"/>
<wire x1="215.9" y1="264.16" x2="215.9" y2="215.9" width="0.1524" layer="91"/>
<wire x1="215.9" y1="215.9" x2="187.96" y2="215.9" width="0.1524" layer="91"/>
<pinref part="YEAR_A" gate="G$1" pin="G"/>
</segment>
</net>
<net name="N$505" class="0">
<segment>
<pinref part="R294" gate="G$1" pin="2"/>
<wire x1="200.66" y1="266.7" x2="218.44" y2="266.7" width="0.1524" layer="91"/>
<wire x1="218.44" y1="266.7" x2="218.44" y2="218.44" width="0.1524" layer="91"/>
<pinref part="YEAR_A" gate="G$1" pin="F"/>
<wire x1="218.44" y1="218.44" x2="187.96" y2="218.44" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$281" class="0">
<segment>
<pinref part="4543_YEAR_B" gate="A" pin="F"/>
<pinref part="R167" gate="G$1" pin="1"/>
<wire x1="276.86" y1="266.7" x2="279.4" y2="266.7" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$282" class="0">
<segment>
<pinref part="4543_YEAR_B" gate="A" pin="G"/>
<pinref part="R168" gate="G$1" pin="1"/>
<wire x1="276.86" y1="264.16" x2="279.4" y2="264.16" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$283" class="0">
<segment>
<pinref part="4543_YEAR_B" gate="A" pin="E"/>
<pinref part="R169" gate="G$1" pin="1"/>
<wire x1="276.86" y1="261.62" x2="279.4" y2="261.62" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$284" class="0">
<segment>
<pinref part="4543_YEAR_B" gate="A" pin="D"/>
<pinref part="R170" gate="G$1" pin="1"/>
<wire x1="276.86" y1="259.08" x2="279.4" y2="259.08" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$285" class="0">
<segment>
<pinref part="4543_YEAR_B" gate="A" pin="C"/>
<pinref part="R171" gate="G$1" pin="1"/>
<wire x1="276.86" y1="256.54" x2="279.4" y2="256.54" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$286" class="0">
<segment>
<pinref part="4543_YEAR_B" gate="A" pin="B"/>
<pinref part="R172" gate="G$1" pin="1"/>
<wire x1="276.86" y1="254" x2="279.4" y2="254" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$287" class="0">
<segment>
<pinref part="4543_YEAR_B" gate="A" pin="A"/>
<pinref part="R173" gate="G$1" pin="1"/>
<wire x1="276.86" y1="251.46" x2="279.4" y2="251.46" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$288" class="0">
<segment>
<pinref part="R173" gate="G$1" pin="2"/>
<pinref part="2003_YEAR_B" gate="A" pin="I7"/>
<wire x1="289.56" y1="251.46" x2="292.1" y2="251.46" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$289" class="0">
<segment>
<pinref part="R172" gate="G$1" pin="2"/>
<pinref part="2003_YEAR_B" gate="A" pin="I6"/>
<wire x1="289.56" y1="254" x2="292.1" y2="254" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$290" class="0">
<segment>
<pinref part="R171" gate="G$1" pin="2"/>
<pinref part="2003_YEAR_B" gate="A" pin="I5"/>
<wire x1="289.56" y1="256.54" x2="292.1" y2="256.54" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$291" class="0">
<segment>
<pinref part="R170" gate="G$1" pin="2"/>
<pinref part="2003_YEAR_B" gate="A" pin="I4"/>
<wire x1="289.56" y1="259.08" x2="292.1" y2="259.08" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$292" class="0">
<segment>
<pinref part="R169" gate="G$1" pin="2"/>
<pinref part="2003_YEAR_B" gate="A" pin="I3"/>
<wire x1="289.56" y1="261.62" x2="292.1" y2="261.62" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$293" class="0">
<segment>
<pinref part="R168" gate="G$1" pin="2"/>
<pinref part="2003_YEAR_B" gate="A" pin="I2"/>
<wire x1="289.56" y1="264.16" x2="292.1" y2="264.16" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$294" class="0">
<segment>
<pinref part="R167" gate="G$1" pin="2"/>
<pinref part="2003_YEAR_B" gate="A" pin="I1"/>
<wire x1="289.56" y1="266.7" x2="292.1" y2="266.7" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$295" class="0">
<segment>
<pinref part="4543_YEAR_B" gate="A" pin="LD"/>
<pinref part="R174" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$296" class="0">
<segment>
<pinref part="4543_YEAR_B" gate="A" pin="IB"/>
<pinref part="R175" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$297" class="0">
<segment>
<pinref part="4543_YEAR_B" gate="A" pin="IC"/>
<pinref part="R176" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$298" class="0">
<segment>
<pinref part="4543_YEAR_B" gate="A" pin="ID"/>
<pinref part="R177" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$299" class="0">
<segment>
<pinref part="4543_YEAR_B" gate="A" pin="IA"/>
<pinref part="R178" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$300" class="0">
<segment>
<pinref part="2003_YEAR_B" gate="A" pin="O1"/>
<pinref part="R179" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$301" class="0">
<segment>
<pinref part="2003_YEAR_B" gate="A" pin="O2"/>
<pinref part="R180" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$302" class="0">
<segment>
<pinref part="2003_YEAR_B" gate="A" pin="O3"/>
<pinref part="R181" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$303" class="0">
<segment>
<pinref part="2003_YEAR_B" gate="A" pin="O4"/>
<pinref part="R182" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$304" class="0">
<segment>
<pinref part="2003_YEAR_B" gate="A" pin="O5"/>
<pinref part="R183" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$305" class="0">
<segment>
<pinref part="2003_YEAR_B" gate="A" pin="O6"/>
<pinref part="R184" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$306" class="0">
<segment>
<pinref part="2003_YEAR_B" gate="A" pin="O7"/>
<pinref part="R185" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$307" class="0">
<segment>
<pinref part="R185" gate="G$1" pin="2"/>
<wire x1="327.66" y1="251.46" x2="327.66" y2="223.52" width="0.1524" layer="91"/>
<pinref part="YEAR_B" gate="G$1" pin="A"/>
<wire x1="327.66" y1="223.52" x2="299.72" y2="223.52" width="0.1524" layer="91"/>
<wire x1="299.72" y1="223.52" x2="299.72" y2="218.44" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$308" class="0">
<segment>
<pinref part="R184" gate="G$1" pin="2"/>
<wire x1="327.66" y1="254" x2="330.2" y2="254" width="0.1524" layer="91"/>
<wire x1="330.2" y1="254" x2="330.2" y2="226.06" width="0.1524" layer="91"/>
<wire x1="330.2" y1="226.06" x2="297.18" y2="226.06" width="0.1524" layer="91"/>
<wire x1="297.18" y1="226.06" x2="297.18" y2="215.9" width="0.1524" layer="91"/>
<pinref part="YEAR_B" gate="G$1" pin="B"/>
<wire x1="297.18" y1="215.9" x2="299.72" y2="215.9" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$309" class="0">
<segment>
<pinref part="R183" gate="G$1" pin="2"/>
<wire x1="327.66" y1="256.54" x2="332.74" y2="256.54" width="0.1524" layer="91"/>
<wire x1="332.74" y1="256.54" x2="332.74" y2="228.6" width="0.1524" layer="91"/>
<wire x1="332.74" y1="228.6" x2="294.64" y2="228.6" width="0.1524" layer="91"/>
<wire x1="294.64" y1="228.6" x2="294.64" y2="213.36" width="0.1524" layer="91"/>
<pinref part="YEAR_B" gate="G$1" pin="C"/>
<wire x1="294.64" y1="213.36" x2="299.72" y2="213.36" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$310" class="0">
<segment>
<pinref part="R182" gate="G$1" pin="2"/>
<wire x1="327.66" y1="259.08" x2="337.82" y2="259.08" width="0.1524" layer="91"/>
<wire x1="337.82" y1="259.08" x2="337.82" y2="231.14" width="0.1524" layer="91"/>
<wire x1="337.82" y1="231.14" x2="292.1" y2="231.14" width="0.1524" layer="91"/>
<wire x1="292.1" y1="231.14" x2="292.1" y2="210.82" width="0.1524" layer="91"/>
<pinref part="YEAR_B" gate="G$1" pin="D"/>
<wire x1="292.1" y1="210.82" x2="299.72" y2="210.82" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$311" class="0">
<segment>
<pinref part="R181" gate="G$1" pin="2"/>
<wire x1="327.66" y1="261.62" x2="340.36" y2="261.62" width="0.1524" layer="91"/>
<wire x1="340.36" y1="261.62" x2="340.36" y2="233.68" width="0.1524" layer="91"/>
<wire x1="340.36" y1="233.68" x2="289.56" y2="233.68" width="0.1524" layer="91"/>
<wire x1="289.56" y1="233.68" x2="289.56" y2="208.28" width="0.1524" layer="91"/>
<pinref part="YEAR_B" gate="G$1" pin="E"/>
<wire x1="289.56" y1="208.28" x2="299.72" y2="208.28" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$312" class="0">
<segment>
<pinref part="R180" gate="G$1" pin="2"/>
<wire x1="327.66" y1="264.16" x2="342.9" y2="264.16" width="0.1524" layer="91"/>
<wire x1="342.9" y1="264.16" x2="342.9" y2="215.9" width="0.1524" layer="91"/>
<wire x1="342.9" y1="215.9" x2="314.96" y2="215.9" width="0.1524" layer="91"/>
<pinref part="YEAR_B" gate="G$1" pin="G"/>
</segment>
</net>
<net name="N$313" class="0">
<segment>
<pinref part="R179" gate="G$1" pin="2"/>
<wire x1="327.66" y1="266.7" x2="345.44" y2="266.7" width="0.1524" layer="91"/>
<wire x1="345.44" y1="266.7" x2="345.44" y2="218.44" width="0.1524" layer="91"/>
<pinref part="YEAR_B" gate="G$1" pin="F"/>
<wire x1="345.44" y1="218.44" x2="314.96" y2="218.44" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$314" class="0">
<segment>
<pinref part="4543_YEAR_C" gate="A" pin="F"/>
<pinref part="R186" gate="G$1" pin="1"/>
<wire x1="401.32" y1="266.7" x2="403.86" y2="266.7" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$315" class="0">
<segment>
<pinref part="4543_YEAR_C" gate="A" pin="G"/>
<pinref part="R187" gate="G$1" pin="1"/>
<wire x1="401.32" y1="264.16" x2="403.86" y2="264.16" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$316" class="0">
<segment>
<pinref part="4543_YEAR_C" gate="A" pin="E"/>
<pinref part="R188" gate="G$1" pin="1"/>
<wire x1="401.32" y1="261.62" x2="403.86" y2="261.62" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$317" class="0">
<segment>
<pinref part="4543_YEAR_C" gate="A" pin="D"/>
<pinref part="R189" gate="G$1" pin="1"/>
<wire x1="401.32" y1="259.08" x2="403.86" y2="259.08" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$318" class="0">
<segment>
<pinref part="4543_YEAR_C" gate="A" pin="C"/>
<pinref part="R190" gate="G$1" pin="1"/>
<wire x1="401.32" y1="256.54" x2="403.86" y2="256.54" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$319" class="0">
<segment>
<pinref part="4543_YEAR_C" gate="A" pin="B"/>
<pinref part="R191" gate="G$1" pin="1"/>
<wire x1="401.32" y1="254" x2="403.86" y2="254" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$320" class="0">
<segment>
<pinref part="4543_YEAR_C" gate="A" pin="A"/>
<pinref part="R192" gate="G$1" pin="1"/>
<wire x1="401.32" y1="251.46" x2="403.86" y2="251.46" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$321" class="0">
<segment>
<pinref part="R192" gate="G$1" pin="2"/>
<pinref part="2003_YEAR_C" gate="A" pin="I7"/>
<wire x1="414.02" y1="251.46" x2="416.56" y2="251.46" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$322" class="0">
<segment>
<pinref part="R191" gate="G$1" pin="2"/>
<pinref part="2003_YEAR_C" gate="A" pin="I6"/>
<wire x1="414.02" y1="254" x2="416.56" y2="254" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$323" class="0">
<segment>
<pinref part="R190" gate="G$1" pin="2"/>
<pinref part="2003_YEAR_C" gate="A" pin="I5"/>
<wire x1="414.02" y1="256.54" x2="416.56" y2="256.54" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$324" class="0">
<segment>
<pinref part="R189" gate="G$1" pin="2"/>
<pinref part="2003_YEAR_C" gate="A" pin="I4"/>
<wire x1="414.02" y1="259.08" x2="416.56" y2="259.08" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$325" class="0">
<segment>
<pinref part="R188" gate="G$1" pin="2"/>
<pinref part="2003_YEAR_C" gate="A" pin="I3"/>
<wire x1="414.02" y1="261.62" x2="416.56" y2="261.62" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$326" class="0">
<segment>
<pinref part="R187" gate="G$1" pin="2"/>
<pinref part="2003_YEAR_C" gate="A" pin="I2"/>
<wire x1="414.02" y1="264.16" x2="416.56" y2="264.16" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$327" class="0">
<segment>
<pinref part="R186" gate="G$1" pin="2"/>
<pinref part="2003_YEAR_C" gate="A" pin="I1"/>
<wire x1="414.02" y1="266.7" x2="416.56" y2="266.7" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$328" class="0">
<segment>
<pinref part="4543_YEAR_C" gate="A" pin="LD"/>
<pinref part="R193" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$329" class="0">
<segment>
<pinref part="4543_YEAR_C" gate="A" pin="IB"/>
<pinref part="R194" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$330" class="0">
<segment>
<pinref part="4543_YEAR_C" gate="A" pin="IC"/>
<pinref part="R195" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$331" class="0">
<segment>
<pinref part="4543_YEAR_C" gate="A" pin="ID"/>
<pinref part="R196" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$332" class="0">
<segment>
<pinref part="4543_YEAR_C" gate="A" pin="IA"/>
<pinref part="R197" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$333" class="0">
<segment>
<pinref part="2003_YEAR_C" gate="A" pin="O1"/>
<pinref part="R198" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$334" class="0">
<segment>
<pinref part="2003_YEAR_C" gate="A" pin="O2"/>
<pinref part="R199" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$335" class="0">
<segment>
<pinref part="2003_YEAR_C" gate="A" pin="O3"/>
<pinref part="R200" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$336" class="0">
<segment>
<pinref part="2003_YEAR_C" gate="A" pin="O4"/>
<pinref part="R201" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$337" class="0">
<segment>
<pinref part="2003_YEAR_C" gate="A" pin="O5"/>
<pinref part="R202" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$338" class="0">
<segment>
<pinref part="2003_YEAR_C" gate="A" pin="O6"/>
<pinref part="R203" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$339" class="0">
<segment>
<pinref part="2003_YEAR_C" gate="A" pin="O7"/>
<pinref part="R204" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$340" class="0">
<segment>
<pinref part="R204" gate="G$1" pin="2"/>
<wire x1="452.12" y1="251.46" x2="452.12" y2="223.52" width="0.1524" layer="91"/>
<pinref part="YEAR_C" gate="G$1" pin="A"/>
<wire x1="452.12" y1="223.52" x2="424.18" y2="223.52" width="0.1524" layer="91"/>
<wire x1="424.18" y1="223.52" x2="424.18" y2="218.44" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$506" class="0">
<segment>
<pinref part="R203" gate="G$1" pin="2"/>
<wire x1="452.12" y1="254" x2="454.66" y2="254" width="0.1524" layer="91"/>
<wire x1="454.66" y1="254" x2="454.66" y2="226.06" width="0.1524" layer="91"/>
<wire x1="454.66" y1="226.06" x2="421.64" y2="226.06" width="0.1524" layer="91"/>
<wire x1="421.64" y1="226.06" x2="421.64" y2="215.9" width="0.1524" layer="91"/>
<pinref part="YEAR_C" gate="G$1" pin="B"/>
<wire x1="421.64" y1="215.9" x2="424.18" y2="215.9" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$507" class="0">
<segment>
<pinref part="R202" gate="G$1" pin="2"/>
<wire x1="452.12" y1="256.54" x2="457.2" y2="256.54" width="0.1524" layer="91"/>
<wire x1="457.2" y1="256.54" x2="457.2" y2="228.6" width="0.1524" layer="91"/>
<wire x1="457.2" y1="228.6" x2="419.1" y2="228.6" width="0.1524" layer="91"/>
<wire x1="419.1" y1="228.6" x2="419.1" y2="213.36" width="0.1524" layer="91"/>
<pinref part="YEAR_C" gate="G$1" pin="C"/>
<wire x1="419.1" y1="213.36" x2="424.18" y2="213.36" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$508" class="0">
<segment>
<pinref part="R201" gate="G$1" pin="2"/>
<wire x1="452.12" y1="259.08" x2="462.28" y2="259.08" width="0.1524" layer="91"/>
<wire x1="462.28" y1="259.08" x2="462.28" y2="231.14" width="0.1524" layer="91"/>
<wire x1="462.28" y1="231.14" x2="416.56" y2="231.14" width="0.1524" layer="91"/>
<wire x1="416.56" y1="231.14" x2="416.56" y2="210.82" width="0.1524" layer="91"/>
<pinref part="YEAR_C" gate="G$1" pin="D"/>
<wire x1="416.56" y1="210.82" x2="424.18" y2="210.82" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$509" class="0">
<segment>
<pinref part="R200" gate="G$1" pin="2"/>
<wire x1="452.12" y1="261.62" x2="464.82" y2="261.62" width="0.1524" layer="91"/>
<wire x1="464.82" y1="261.62" x2="464.82" y2="233.68" width="0.1524" layer="91"/>
<wire x1="464.82" y1="233.68" x2="414.02" y2="233.68" width="0.1524" layer="91"/>
<wire x1="414.02" y1="233.68" x2="414.02" y2="208.28" width="0.1524" layer="91"/>
<pinref part="YEAR_C" gate="G$1" pin="E"/>
<wire x1="414.02" y1="208.28" x2="424.18" y2="208.28" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$510" class="0">
<segment>
<pinref part="R199" gate="G$1" pin="2"/>
<wire x1="452.12" y1="264.16" x2="467.36" y2="264.16" width="0.1524" layer="91"/>
<wire x1="467.36" y1="264.16" x2="467.36" y2="215.9" width="0.1524" layer="91"/>
<wire x1="467.36" y1="215.9" x2="439.42" y2="215.9" width="0.1524" layer="91"/>
<pinref part="YEAR_C" gate="G$1" pin="G"/>
</segment>
</net>
<net name="N$511" class="0">
<segment>
<pinref part="R198" gate="G$1" pin="2"/>
<wire x1="452.12" y1="266.7" x2="469.9" y2="266.7" width="0.1524" layer="91"/>
<wire x1="469.9" y1="266.7" x2="469.9" y2="218.44" width="0.1524" layer="91"/>
<pinref part="YEAR_C" gate="G$1" pin="F"/>
<wire x1="469.9" y1="218.44" x2="439.42" y2="218.44" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$341" class="0">
<segment>
<pinref part="4543_YEAR_D" gate="A" pin="F"/>
<pinref part="R205" gate="G$1" pin="1"/>
<wire x1="525.78" y1="266.7" x2="528.32" y2="266.7" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$342" class="0">
<segment>
<pinref part="4543_YEAR_D" gate="A" pin="G"/>
<pinref part="R206" gate="G$1" pin="1"/>
<wire x1="525.78" y1="264.16" x2="528.32" y2="264.16" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$343" class="0">
<segment>
<pinref part="4543_YEAR_D" gate="A" pin="E"/>
<pinref part="R207" gate="G$1" pin="1"/>
<wire x1="525.78" y1="261.62" x2="528.32" y2="261.62" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$344" class="0">
<segment>
<pinref part="4543_YEAR_D" gate="A" pin="D"/>
<pinref part="R208" gate="G$1" pin="1"/>
<wire x1="525.78" y1="259.08" x2="528.32" y2="259.08" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$345" class="0">
<segment>
<pinref part="4543_YEAR_D" gate="A" pin="C"/>
<pinref part="R209" gate="G$1" pin="1"/>
<wire x1="525.78" y1="256.54" x2="528.32" y2="256.54" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$346" class="0">
<segment>
<pinref part="4543_YEAR_D" gate="A" pin="B"/>
<pinref part="R210" gate="G$1" pin="1"/>
<wire x1="525.78" y1="254" x2="528.32" y2="254" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$347" class="0">
<segment>
<pinref part="4543_YEAR_D" gate="A" pin="A"/>
<pinref part="R211" gate="G$1" pin="1"/>
<wire x1="525.78" y1="251.46" x2="528.32" y2="251.46" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$348" class="0">
<segment>
<pinref part="R211" gate="G$1" pin="2"/>
<pinref part="2003_YEAR_D" gate="A" pin="I7"/>
<wire x1="538.48" y1="251.46" x2="541.02" y2="251.46" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$349" class="0">
<segment>
<pinref part="R210" gate="G$1" pin="2"/>
<pinref part="2003_YEAR_D" gate="A" pin="I6"/>
<wire x1="538.48" y1="254" x2="541.02" y2="254" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$350" class="0">
<segment>
<pinref part="R209" gate="G$1" pin="2"/>
<pinref part="2003_YEAR_D" gate="A" pin="I5"/>
<wire x1="538.48" y1="256.54" x2="541.02" y2="256.54" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$351" class="0">
<segment>
<pinref part="R208" gate="G$1" pin="2"/>
<pinref part="2003_YEAR_D" gate="A" pin="I4"/>
<wire x1="538.48" y1="259.08" x2="541.02" y2="259.08" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$352" class="0">
<segment>
<pinref part="R207" gate="G$1" pin="2"/>
<pinref part="2003_YEAR_D" gate="A" pin="I3"/>
<wire x1="538.48" y1="261.62" x2="541.02" y2="261.62" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$353" class="0">
<segment>
<pinref part="R206" gate="G$1" pin="2"/>
<pinref part="2003_YEAR_D" gate="A" pin="I2"/>
<wire x1="538.48" y1="264.16" x2="541.02" y2="264.16" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$354" class="0">
<segment>
<pinref part="R205" gate="G$1" pin="2"/>
<pinref part="2003_YEAR_D" gate="A" pin="I1"/>
<wire x1="538.48" y1="266.7" x2="541.02" y2="266.7" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$355" class="0">
<segment>
<pinref part="4543_YEAR_D" gate="A" pin="LD"/>
<pinref part="R212" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$356" class="0">
<segment>
<pinref part="4543_YEAR_D" gate="A" pin="IB"/>
<pinref part="R213" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$357" class="0">
<segment>
<pinref part="4543_YEAR_D" gate="A" pin="IC"/>
<pinref part="R214" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$358" class="0">
<segment>
<pinref part="4543_YEAR_D" gate="A" pin="ID"/>
<pinref part="R215" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$359" class="0">
<segment>
<pinref part="4543_YEAR_D" gate="A" pin="IA"/>
<pinref part="R216" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$360" class="0">
<segment>
<pinref part="2003_YEAR_D" gate="A" pin="O1"/>
<pinref part="R217" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$512" class="0">
<segment>
<pinref part="2003_YEAR_D" gate="A" pin="O2"/>
<pinref part="R218" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$513" class="0">
<segment>
<pinref part="2003_YEAR_D" gate="A" pin="O3"/>
<pinref part="R301" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$514" class="0">
<segment>
<pinref part="2003_YEAR_D" gate="A" pin="O4"/>
<pinref part="R302" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$515" class="0">
<segment>
<pinref part="2003_YEAR_D" gate="A" pin="O5"/>
<pinref part="R303" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$516" class="0">
<segment>
<pinref part="2003_YEAR_D" gate="A" pin="O6"/>
<pinref part="R304" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$517" class="0">
<segment>
<pinref part="2003_YEAR_D" gate="A" pin="O7"/>
<pinref part="R305" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$518" class="0">
<segment>
<pinref part="R305" gate="G$1" pin="2"/>
<wire x1="576.58" y1="251.46" x2="576.58" y2="223.52" width="0.1524" layer="91"/>
<pinref part="YEAR_D" gate="G$1" pin="A"/>
<wire x1="576.58" y1="223.52" x2="548.64" y2="223.52" width="0.1524" layer="91"/>
<wire x1="548.64" y1="223.52" x2="548.64" y2="218.44" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$519" class="0">
<segment>
<pinref part="R304" gate="G$1" pin="2"/>
<wire x1="576.58" y1="254" x2="579.12" y2="254" width="0.1524" layer="91"/>
<wire x1="579.12" y1="254" x2="579.12" y2="226.06" width="0.1524" layer="91"/>
<wire x1="579.12" y1="226.06" x2="546.1" y2="226.06" width="0.1524" layer="91"/>
<wire x1="546.1" y1="226.06" x2="546.1" y2="215.9" width="0.1524" layer="91"/>
<pinref part="YEAR_D" gate="G$1" pin="B"/>
<wire x1="546.1" y1="215.9" x2="548.64" y2="215.9" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$520" class="0">
<segment>
<pinref part="R303" gate="G$1" pin="2"/>
<wire x1="576.58" y1="256.54" x2="581.66" y2="256.54" width="0.1524" layer="91"/>
<wire x1="581.66" y1="256.54" x2="581.66" y2="228.6" width="0.1524" layer="91"/>
<wire x1="581.66" y1="228.6" x2="543.56" y2="228.6" width="0.1524" layer="91"/>
<wire x1="543.56" y1="228.6" x2="543.56" y2="213.36" width="0.1524" layer="91"/>
<pinref part="YEAR_D" gate="G$1" pin="C"/>
<wire x1="543.56" y1="213.36" x2="548.64" y2="213.36" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$521" class="0">
<segment>
<pinref part="R302" gate="G$1" pin="2"/>
<wire x1="576.58" y1="259.08" x2="586.74" y2="259.08" width="0.1524" layer="91"/>
<wire x1="586.74" y1="259.08" x2="586.74" y2="231.14" width="0.1524" layer="91"/>
<wire x1="586.74" y1="231.14" x2="541.02" y2="231.14" width="0.1524" layer="91"/>
<wire x1="541.02" y1="231.14" x2="541.02" y2="210.82" width="0.1524" layer="91"/>
<pinref part="YEAR_D" gate="G$1" pin="D"/>
<wire x1="541.02" y1="210.82" x2="548.64" y2="210.82" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$522" class="0">
<segment>
<pinref part="R301" gate="G$1" pin="2"/>
<wire x1="576.58" y1="261.62" x2="589.28" y2="261.62" width="0.1524" layer="91"/>
<wire x1="589.28" y1="261.62" x2="589.28" y2="233.68" width="0.1524" layer="91"/>
<wire x1="589.28" y1="233.68" x2="538.48" y2="233.68" width="0.1524" layer="91"/>
<wire x1="538.48" y1="233.68" x2="538.48" y2="208.28" width="0.1524" layer="91"/>
<pinref part="YEAR_D" gate="G$1" pin="E"/>
<wire x1="538.48" y1="208.28" x2="548.64" y2="208.28" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$523" class="0">
<segment>
<pinref part="R218" gate="G$1" pin="2"/>
<wire x1="576.58" y1="264.16" x2="591.82" y2="264.16" width="0.1524" layer="91"/>
<wire x1="591.82" y1="264.16" x2="591.82" y2="215.9" width="0.1524" layer="91"/>
<wire x1="591.82" y1="215.9" x2="563.88" y2="215.9" width="0.1524" layer="91"/>
<pinref part="YEAR_D" gate="G$1" pin="G"/>
</segment>
</net>
<net name="N$524" class="0">
<segment>
<pinref part="R217" gate="G$1" pin="2"/>
<wire x1="576.58" y1="266.7" x2="594.36" y2="266.7" width="0.1524" layer="91"/>
<wire x1="594.36" y1="266.7" x2="594.36" y2="218.44" width="0.1524" layer="91"/>
<pinref part="YEAR_D" gate="G$1" pin="F"/>
<wire x1="594.36" y1="218.44" x2="563.88" y2="218.44" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$525" class="0">
<segment>
<pinref part="4543_TEMP_A" gate="A" pin="F"/>
<pinref part="R306" gate="G$1" pin="1"/>
<wire x1="713.74" y1="264.16" x2="716.28" y2="264.16" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$526" class="0">
<segment>
<pinref part="4543_TEMP_A" gate="A" pin="G"/>
<pinref part="R307" gate="G$1" pin="1"/>
<wire x1="713.74" y1="261.62" x2="716.28" y2="261.62" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$527" class="0">
<segment>
<pinref part="4543_TEMP_A" gate="A" pin="E"/>
<pinref part="R308" gate="G$1" pin="1"/>
<wire x1="713.74" y1="259.08" x2="716.28" y2="259.08" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$528" class="0">
<segment>
<pinref part="4543_TEMP_A" gate="A" pin="D"/>
<pinref part="R309" gate="G$1" pin="1"/>
<wire x1="713.74" y1="256.54" x2="716.28" y2="256.54" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$529" class="0">
<segment>
<pinref part="4543_TEMP_A" gate="A" pin="C"/>
<pinref part="R310" gate="G$1" pin="1"/>
<wire x1="713.74" y1="254" x2="716.28" y2="254" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$530" class="0">
<segment>
<pinref part="4543_TEMP_A" gate="A" pin="B"/>
<pinref part="R311" gate="G$1" pin="1"/>
<wire x1="713.74" y1="251.46" x2="716.28" y2="251.46" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$531" class="0">
<segment>
<pinref part="4543_TEMP_A" gate="A" pin="A"/>
<pinref part="R312" gate="G$1" pin="1"/>
<wire x1="713.74" y1="248.92" x2="716.28" y2="248.92" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$532" class="0">
<segment>
<pinref part="R312" gate="G$1" pin="2"/>
<pinref part="2003_TEMP_A" gate="A" pin="I7"/>
<wire x1="726.44" y1="248.92" x2="728.98" y2="248.92" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$533" class="0">
<segment>
<pinref part="R311" gate="G$1" pin="2"/>
<pinref part="2003_TEMP_A" gate="A" pin="I6"/>
<wire x1="726.44" y1="251.46" x2="728.98" y2="251.46" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$534" class="0">
<segment>
<pinref part="R310" gate="G$1" pin="2"/>
<pinref part="2003_TEMP_A" gate="A" pin="I5"/>
<wire x1="726.44" y1="254" x2="728.98" y2="254" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$535" class="0">
<segment>
<pinref part="R309" gate="G$1" pin="2"/>
<pinref part="2003_TEMP_A" gate="A" pin="I4"/>
<wire x1="726.44" y1="256.54" x2="728.98" y2="256.54" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$536" class="0">
<segment>
<pinref part="R308" gate="G$1" pin="2"/>
<pinref part="2003_TEMP_A" gate="A" pin="I3"/>
<wire x1="726.44" y1="259.08" x2="728.98" y2="259.08" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$537" class="0">
<segment>
<pinref part="R307" gate="G$1" pin="2"/>
<pinref part="2003_TEMP_A" gate="A" pin="I2"/>
<wire x1="726.44" y1="261.62" x2="728.98" y2="261.62" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$538" class="0">
<segment>
<pinref part="R306" gate="G$1" pin="2"/>
<pinref part="2003_TEMP_A" gate="A" pin="I1"/>
<wire x1="726.44" y1="264.16" x2="728.98" y2="264.16" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$539" class="0">
<segment>
<pinref part="4543_TEMP_A" gate="A" pin="LD"/>
<pinref part="R313" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$540" class="0">
<segment>
<pinref part="4543_TEMP_A" gate="A" pin="IB"/>
<pinref part="R314" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$541" class="0">
<segment>
<pinref part="4543_TEMP_A" gate="A" pin="IC"/>
<pinref part="R315" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$542" class="0">
<segment>
<pinref part="4543_TEMP_A" gate="A" pin="ID"/>
<pinref part="R316" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$543" class="0">
<segment>
<pinref part="4543_TEMP_A" gate="A" pin="IA"/>
<pinref part="R317" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$544" class="0">
<segment>
<pinref part="2003_TEMP_A" gate="A" pin="O1"/>
<pinref part="R318" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$545" class="0">
<segment>
<pinref part="2003_TEMP_A" gate="A" pin="O2"/>
<pinref part="R319" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$546" class="0">
<segment>
<pinref part="2003_TEMP_A" gate="A" pin="O3"/>
<pinref part="R320" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$547" class="0">
<segment>
<pinref part="2003_TEMP_A" gate="A" pin="O4"/>
<pinref part="R321" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$548" class="0">
<segment>
<pinref part="2003_TEMP_A" gate="A" pin="O5"/>
<pinref part="R322" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$549" class="0">
<segment>
<pinref part="2003_TEMP_A" gate="A" pin="O6"/>
<pinref part="R323" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$550" class="0">
<segment>
<pinref part="2003_TEMP_A" gate="A" pin="O7"/>
<pinref part="R324" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$551" class="0">
<segment>
<pinref part="R324" gate="G$1" pin="2"/>
<wire x1="764.54" y1="248.92" x2="764.54" y2="220.98" width="0.1524" layer="91"/>
<pinref part="TEMP_A" gate="G$1" pin="A"/>
<wire x1="764.54" y1="220.98" x2="736.6" y2="220.98" width="0.1524" layer="91"/>
<wire x1="736.6" y1="220.98" x2="736.6" y2="215.9" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$552" class="0">
<segment>
<pinref part="R323" gate="G$1" pin="2"/>
<wire x1="764.54" y1="251.46" x2="767.08" y2="251.46" width="0.1524" layer="91"/>
<wire x1="767.08" y1="251.46" x2="767.08" y2="223.52" width="0.1524" layer="91"/>
<wire x1="767.08" y1="223.52" x2="734.06" y2="223.52" width="0.1524" layer="91"/>
<wire x1="734.06" y1="223.52" x2="734.06" y2="213.36" width="0.1524" layer="91"/>
<pinref part="TEMP_A" gate="G$1" pin="B"/>
<wire x1="734.06" y1="213.36" x2="736.6" y2="213.36" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$553" class="0">
<segment>
<pinref part="R322" gate="G$1" pin="2"/>
<wire x1="764.54" y1="254" x2="769.62" y2="254" width="0.1524" layer="91"/>
<wire x1="769.62" y1="254" x2="769.62" y2="226.06" width="0.1524" layer="91"/>
<wire x1="769.62" y1="226.06" x2="731.52" y2="226.06" width="0.1524" layer="91"/>
<wire x1="731.52" y1="226.06" x2="731.52" y2="210.82" width="0.1524" layer="91"/>
<pinref part="TEMP_A" gate="G$1" pin="C"/>
<wire x1="731.52" y1="210.82" x2="736.6" y2="210.82" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$554" class="0">
<segment>
<pinref part="R321" gate="G$1" pin="2"/>
<wire x1="764.54" y1="256.54" x2="774.7" y2="256.54" width="0.1524" layer="91"/>
<wire x1="774.7" y1="256.54" x2="774.7" y2="228.6" width="0.1524" layer="91"/>
<wire x1="774.7" y1="228.6" x2="728.98" y2="228.6" width="0.1524" layer="91"/>
<wire x1="728.98" y1="228.6" x2="728.98" y2="208.28" width="0.1524" layer="91"/>
<pinref part="TEMP_A" gate="G$1" pin="D"/>
<wire x1="728.98" y1="208.28" x2="736.6" y2="208.28" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$555" class="0">
<segment>
<pinref part="R320" gate="G$1" pin="2"/>
<wire x1="764.54" y1="259.08" x2="777.24" y2="259.08" width="0.1524" layer="91"/>
<wire x1="777.24" y1="259.08" x2="777.24" y2="231.14" width="0.1524" layer="91"/>
<wire x1="777.24" y1="231.14" x2="726.44" y2="231.14" width="0.1524" layer="91"/>
<wire x1="726.44" y1="231.14" x2="726.44" y2="205.74" width="0.1524" layer="91"/>
<pinref part="TEMP_A" gate="G$1" pin="E"/>
<wire x1="726.44" y1="205.74" x2="736.6" y2="205.74" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$556" class="0">
<segment>
<pinref part="R319" gate="G$1" pin="2"/>
<wire x1="764.54" y1="261.62" x2="779.78" y2="261.62" width="0.1524" layer="91"/>
<wire x1="779.78" y1="261.62" x2="779.78" y2="213.36" width="0.1524" layer="91"/>
<wire x1="779.78" y1="213.36" x2="751.84" y2="213.36" width="0.1524" layer="91"/>
<pinref part="TEMP_A" gate="G$1" pin="G"/>
</segment>
</net>
<net name="N$557" class="0">
<segment>
<pinref part="R318" gate="G$1" pin="2"/>
<wire x1="764.54" y1="264.16" x2="782.32" y2="264.16" width="0.1524" layer="91"/>
<wire x1="782.32" y1="264.16" x2="782.32" y2="215.9" width="0.1524" layer="91"/>
<pinref part="TEMP_A" gate="G$1" pin="F"/>
<wire x1="782.32" y1="215.9" x2="751.84" y2="215.9" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$361" class="0">
<segment>
<pinref part="4543_TEMP_B" gate="A" pin="F"/>
<pinref part="R219" gate="G$1" pin="1"/>
<wire x1="845.82" y1="261.62" x2="848.36" y2="261.62" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$362" class="0">
<segment>
<pinref part="4543_TEMP_B" gate="A" pin="G"/>
<pinref part="R220" gate="G$1" pin="1"/>
<wire x1="845.82" y1="259.08" x2="848.36" y2="259.08" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$363" class="0">
<segment>
<pinref part="4543_TEMP_B" gate="A" pin="E"/>
<pinref part="R221" gate="G$1" pin="1"/>
<wire x1="845.82" y1="256.54" x2="848.36" y2="256.54" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$364" class="0">
<segment>
<pinref part="4543_TEMP_B" gate="A" pin="D"/>
<pinref part="R222" gate="G$1" pin="1"/>
<wire x1="845.82" y1="254" x2="848.36" y2="254" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$365" class="0">
<segment>
<pinref part="4543_TEMP_B" gate="A" pin="C"/>
<pinref part="R223" gate="G$1" pin="1"/>
<wire x1="845.82" y1="251.46" x2="848.36" y2="251.46" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$366" class="0">
<segment>
<pinref part="4543_TEMP_B" gate="A" pin="B"/>
<pinref part="R224" gate="G$1" pin="1"/>
<wire x1="845.82" y1="248.92" x2="848.36" y2="248.92" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$367" class="0">
<segment>
<pinref part="4543_TEMP_B" gate="A" pin="A"/>
<pinref part="R225" gate="G$1" pin="1"/>
<wire x1="845.82" y1="246.38" x2="848.36" y2="246.38" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$368" class="0">
<segment>
<pinref part="R225" gate="G$1" pin="2"/>
<pinref part="2003_TEMP_B" gate="A" pin="I7"/>
<wire x1="858.52" y1="246.38" x2="861.06" y2="246.38" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$369" class="0">
<segment>
<pinref part="R224" gate="G$1" pin="2"/>
<pinref part="2003_TEMP_B" gate="A" pin="I6"/>
<wire x1="858.52" y1="248.92" x2="861.06" y2="248.92" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$370" class="0">
<segment>
<pinref part="R223" gate="G$1" pin="2"/>
<pinref part="2003_TEMP_B" gate="A" pin="I5"/>
<wire x1="858.52" y1="251.46" x2="861.06" y2="251.46" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$371" class="0">
<segment>
<pinref part="R222" gate="G$1" pin="2"/>
<pinref part="2003_TEMP_B" gate="A" pin="I4"/>
<wire x1="858.52" y1="254" x2="861.06" y2="254" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$372" class="0">
<segment>
<pinref part="R221" gate="G$1" pin="2"/>
<pinref part="2003_TEMP_B" gate="A" pin="I3"/>
<wire x1="858.52" y1="256.54" x2="861.06" y2="256.54" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$373" class="0">
<segment>
<pinref part="R220" gate="G$1" pin="2"/>
<pinref part="2003_TEMP_B" gate="A" pin="I2"/>
<wire x1="858.52" y1="259.08" x2="861.06" y2="259.08" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$374" class="0">
<segment>
<pinref part="R219" gate="G$1" pin="2"/>
<pinref part="2003_TEMP_B" gate="A" pin="I1"/>
<wire x1="858.52" y1="261.62" x2="861.06" y2="261.62" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$375" class="0">
<segment>
<pinref part="4543_TEMP_B" gate="A" pin="LD"/>
<pinref part="R226" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$376" class="0">
<segment>
<pinref part="4543_TEMP_B" gate="A" pin="IB"/>
<pinref part="R227" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$377" class="0">
<segment>
<pinref part="4543_TEMP_B" gate="A" pin="IC"/>
<pinref part="R228" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$378" class="0">
<segment>
<pinref part="4543_TEMP_B" gate="A" pin="ID"/>
<pinref part="R229" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$379" class="0">
<segment>
<pinref part="4543_TEMP_B" gate="A" pin="IA"/>
<pinref part="R230" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$380" class="0">
<segment>
<pinref part="2003_TEMP_B" gate="A" pin="O1"/>
<pinref part="R231" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$558" class="0">
<segment>
<pinref part="2003_TEMP_B" gate="A" pin="O2"/>
<pinref part="R325" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$559" class="0">
<segment>
<pinref part="2003_TEMP_B" gate="A" pin="O3"/>
<pinref part="R326" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$560" class="0">
<segment>
<pinref part="2003_TEMP_B" gate="A" pin="O4"/>
<pinref part="R327" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$561" class="0">
<segment>
<pinref part="2003_TEMP_B" gate="A" pin="O5"/>
<pinref part="R328" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$562" class="0">
<segment>
<pinref part="2003_TEMP_B" gate="A" pin="O6"/>
<pinref part="R329" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$563" class="0">
<segment>
<pinref part="2003_TEMP_B" gate="A" pin="O7"/>
<pinref part="R330" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$564" class="0">
<segment>
<pinref part="R330" gate="G$1" pin="2"/>
<wire x1="896.62" y1="246.38" x2="896.62" y2="218.44" width="0.1524" layer="91"/>
<pinref part="TEMP_B" gate="G$1" pin="A"/>
<wire x1="896.62" y1="218.44" x2="868.68" y2="218.44" width="0.1524" layer="91"/>
<wire x1="868.68" y1="218.44" x2="868.68" y2="213.36" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$565" class="0">
<segment>
<pinref part="R329" gate="G$1" pin="2"/>
<wire x1="896.62" y1="248.92" x2="899.16" y2="248.92" width="0.1524" layer="91"/>
<wire x1="899.16" y1="248.92" x2="899.16" y2="220.98" width="0.1524" layer="91"/>
<wire x1="899.16" y1="220.98" x2="866.14" y2="220.98" width="0.1524" layer="91"/>
<wire x1="866.14" y1="220.98" x2="866.14" y2="210.82" width="0.1524" layer="91"/>
<pinref part="TEMP_B" gate="G$1" pin="B"/>
<wire x1="866.14" y1="210.82" x2="868.68" y2="210.82" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$566" class="0">
<segment>
<pinref part="R328" gate="G$1" pin="2"/>
<wire x1="896.62" y1="251.46" x2="901.7" y2="251.46" width="0.1524" layer="91"/>
<wire x1="901.7" y1="251.46" x2="901.7" y2="223.52" width="0.1524" layer="91"/>
<wire x1="901.7" y1="223.52" x2="863.6" y2="223.52" width="0.1524" layer="91"/>
<wire x1="863.6" y1="223.52" x2="863.6" y2="208.28" width="0.1524" layer="91"/>
<pinref part="TEMP_B" gate="G$1" pin="C"/>
<wire x1="863.6" y1="208.28" x2="868.68" y2="208.28" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$567" class="0">
<segment>
<pinref part="R327" gate="G$1" pin="2"/>
<wire x1="896.62" y1="254" x2="906.78" y2="254" width="0.1524" layer="91"/>
<wire x1="906.78" y1="254" x2="906.78" y2="226.06" width="0.1524" layer="91"/>
<wire x1="906.78" y1="226.06" x2="861.06" y2="226.06" width="0.1524" layer="91"/>
<wire x1="861.06" y1="226.06" x2="861.06" y2="205.74" width="0.1524" layer="91"/>
<pinref part="TEMP_B" gate="G$1" pin="D"/>
<wire x1="861.06" y1="205.74" x2="868.68" y2="205.74" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$568" class="0">
<segment>
<pinref part="R326" gate="G$1" pin="2"/>
<wire x1="896.62" y1="256.54" x2="909.32" y2="256.54" width="0.1524" layer="91"/>
<wire x1="909.32" y1="256.54" x2="909.32" y2="228.6" width="0.1524" layer="91"/>
<wire x1="909.32" y1="228.6" x2="858.52" y2="228.6" width="0.1524" layer="91"/>
<wire x1="858.52" y1="228.6" x2="858.52" y2="203.2" width="0.1524" layer="91"/>
<pinref part="TEMP_B" gate="G$1" pin="E"/>
<wire x1="858.52" y1="203.2" x2="868.68" y2="203.2" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$569" class="0">
<segment>
<pinref part="R325" gate="G$1" pin="2"/>
<wire x1="896.62" y1="259.08" x2="911.86" y2="259.08" width="0.1524" layer="91"/>
<wire x1="911.86" y1="259.08" x2="911.86" y2="210.82" width="0.1524" layer="91"/>
<wire x1="911.86" y1="210.82" x2="883.92" y2="210.82" width="0.1524" layer="91"/>
<pinref part="TEMP_B" gate="G$1" pin="G"/>
</segment>
</net>
<net name="N$570" class="0">
<segment>
<pinref part="R231" gate="G$1" pin="2"/>
<wire x1="896.62" y1="261.62" x2="914.4" y2="261.62" width="0.1524" layer="91"/>
<wire x1="914.4" y1="261.62" x2="914.4" y2="213.36" width="0.1524" layer="91"/>
<pinref part="TEMP_B" gate="G$1" pin="F"/>
<wire x1="914.4" y1="213.36" x2="883.92" y2="213.36" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$381" class="0">
<segment>
<pinref part="LED_SUN" gate="G$1" pin="C"/>
<pinref part="R232" gate="G$1" pin="2"/>
<wire x1="309.88" y1="121.92" x2="309.88" y2="119.38" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$382" class="0">
<segment>
<pinref part="LED_MON" gate="G$1" pin="C"/>
<pinref part="R233" gate="G$1" pin="2"/>
<wire x1="322.58" y1="121.92" x2="322.58" y2="119.38" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$383" class="0">
<segment>
<pinref part="LED_TUE" gate="G$1" pin="C"/>
<pinref part="R234" gate="G$1" pin="2"/>
<wire x1="335.28" y1="121.92" x2="335.28" y2="119.38" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$384" class="0">
<segment>
<pinref part="LED_WED" gate="G$1" pin="C"/>
<pinref part="R235" gate="G$1" pin="2"/>
<wire x1="350.52" y1="121.92" x2="350.52" y2="119.38" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$385" class="0">
<segment>
<pinref part="LED_THU" gate="G$1" pin="C"/>
<pinref part="R236" gate="G$1" pin="2"/>
<wire x1="363.22" y1="121.92" x2="363.22" y2="119.38" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$386" class="0">
<segment>
<pinref part="LED_FRI" gate="G$1" pin="C"/>
<pinref part="R237" gate="G$1" pin="2"/>
<wire x1="375.92" y1="121.92" x2="375.92" y2="119.38" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$387" class="0">
<segment>
<pinref part="LED_SAT" gate="G$1" pin="C"/>
<pinref part="R238" gate="G$1" pin="2"/>
<wire x1="388.62" y1="121.92" x2="388.62" y2="119.38" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$388" class="0">
<segment>
<pinref part="R245" gate="G$1" pin="2"/>
<pinref part="2003_WEEKDAY" gate="A" pin="I1"/>
<wire x1="261.62" y1="83.82" x2="269.24" y2="83.82" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$389" class="0">
<segment>
<pinref part="R246" gate="G$1" pin="2"/>
<pinref part="2003_WEEKDAY" gate="A" pin="I2"/>
<wire x1="261.62" y1="81.28" x2="269.24" y2="81.28" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$390" class="0">
<segment>
<pinref part="R247" gate="G$1" pin="2"/>
<pinref part="2003_WEEKDAY" gate="A" pin="I3"/>
<wire x1="261.62" y1="78.74" x2="269.24" y2="78.74" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$391" class="0">
<segment>
<pinref part="R248" gate="G$1" pin="2"/>
<pinref part="2003_WEEKDAY" gate="A" pin="I4"/>
<wire x1="261.62" y1="76.2" x2="269.24" y2="76.2" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$392" class="0">
<segment>
<pinref part="R249" gate="G$1" pin="2"/>
<pinref part="2003_WEEKDAY" gate="A" pin="I5"/>
<wire x1="261.62" y1="73.66" x2="269.24" y2="73.66" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$393" class="0">
<segment>
<pinref part="R250" gate="G$1" pin="2"/>
<pinref part="2003_WEEKDAY" gate="A" pin="I6"/>
<wire x1="261.62" y1="71.12" x2="269.24" y2="71.12" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$394" class="0">
<segment>
<pinref part="R251" gate="G$1" pin="2"/>
<pinref part="2003_WEEKDAY" gate="A" pin="I7"/>
<wire x1="261.62" y1="68.58" x2="269.24" y2="68.58" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$395" class="0">
<segment>
<pinref part="2003_WEEKDAY" gate="A" pin="O1"/>
<pinref part="R232" gate="G$1" pin="1"/>
<wire x1="294.64" y1="83.82" x2="309.88" y2="83.82" width="0.1524" layer="91"/>
<wire x1="309.88" y1="83.82" x2="309.88" y2="109.22" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$396" class="0">
<segment>
<pinref part="2003_WEEKDAY" gate="A" pin="O2"/>
<pinref part="R233" gate="G$1" pin="1"/>
<wire x1="294.64" y1="81.28" x2="322.58" y2="81.28" width="0.1524" layer="91"/>
<wire x1="322.58" y1="81.28" x2="322.58" y2="109.22" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$397" class="0">
<segment>
<pinref part="2003_WEEKDAY" gate="A" pin="O3"/>
<pinref part="R234" gate="G$1" pin="1"/>
<wire x1="294.64" y1="78.74" x2="335.28" y2="78.74" width="0.1524" layer="91"/>
<wire x1="335.28" y1="78.74" x2="335.28" y2="109.22" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$398" class="0">
<segment>
<pinref part="2003_WEEKDAY" gate="A" pin="O4"/>
<pinref part="R235" gate="G$1" pin="1"/>
<wire x1="294.64" y1="76.2" x2="350.52" y2="76.2" width="0.1524" layer="91"/>
<wire x1="350.52" y1="76.2" x2="350.52" y2="109.22" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$399" class="0">
<segment>
<pinref part="2003_WEEKDAY" gate="A" pin="O5"/>
<pinref part="R236" gate="G$1" pin="1"/>
<wire x1="294.64" y1="73.66" x2="363.22" y2="73.66" width="0.1524" layer="91"/>
<wire x1="363.22" y1="73.66" x2="363.22" y2="109.22" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$400" class="0">
<segment>
<pinref part="2003_WEEKDAY" gate="A" pin="O6"/>
<pinref part="R237" gate="G$1" pin="1"/>
<wire x1="294.64" y1="71.12" x2="375.92" y2="71.12" width="0.1524" layer="91"/>
<wire x1="375.92" y1="71.12" x2="375.92" y2="109.22" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$408" class="0">
<segment>
<pinref part="2003_WEEKDAY" gate="A" pin="O7"/>
<pinref part="R238" gate="G$1" pin="1"/>
<wire x1="294.64" y1="68.58" x2="388.62" y2="68.58" width="0.1524" layer="91"/>
<wire x1="388.62" y1="68.58" x2="388.62" y2="109.22" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$410" class="0">
<segment>
<pinref part="R239" gate="G$1" pin="2"/>
<wire x1="-22.86" y1="78.74" x2="-5.08" y2="78.74" width="0.1524" layer="91"/>
<pinref part="U$1" gate="G$1" pin="G34"/>
<wire x1="-5.08" y1="78.74" x2="2.54" y2="78.74" width="0.1524" layer="91"/>
<wire x1="53.34" y1="71.12" x2="-5.08" y2="71.12" width="0.1524" layer="91"/>
<wire x1="-5.08" y1="71.12" x2="-5.08" y2="78.74" width="0.1524" layer="91"/>
<junction x="-5.08" y="78.74"/>
<wire x1="-22.86" y1="78.74" x2="-22.86" y2="76.2" width="0.1524" layer="91"/>
<pinref part="BUTTON34" gate="-2" pin="KL"/>
<wire x1="-22.86" y1="76.2" x2="-25.4" y2="76.2" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$411" class="0">
<segment>
<pinref part="BUTTON36" gate="-2" pin="KL"/>
<pinref part="R241" gate="G$1" pin="2"/>
<wire x1="-25.4" y1="101.6" x2="0" y2="101.6" width="0.1524" layer="91"/>
<pinref part="U$1" gate="G$1" pin="G36/SP"/>
<wire x1="0" y1="101.6" x2="2.54" y2="101.6" width="0.1524" layer="91"/>
<wire x1="53.34" y1="76.2" x2="0" y2="76.2" width="0.1524" layer="91"/>
<wire x1="0" y1="76.2" x2="0" y2="101.6" width="0.1524" layer="91"/>
<junction x="0" y="101.6"/>
</segment>
</net>
<net name="N$412" class="0">
<segment>
<pinref part="BUTTON35" gate="-2" pin="KL"/>
<pinref part="R240" gate="G$1" pin="2"/>
<wire x1="-25.4" y1="88.9" x2="-2.54" y2="88.9" width="0.1524" layer="91"/>
<pinref part="U$1" gate="G$1" pin="G35"/>
<wire x1="-2.54" y1="88.9" x2="2.54" y2="88.9" width="0.1524" layer="91"/>
<wire x1="53.34" y1="68.58" x2="-2.54" y2="68.58" width="0.1524" layer="91"/>
<wire x1="-2.54" y1="68.58" x2="-2.54" y2="88.9" width="0.1524" layer="91"/>
<junction x="-2.54" y="88.9"/>
</segment>
</net>
<net name="CS" class="0">
<segment>
<pinref part="U$2" gate="G$1" pin="CS"/>
<wire x1="43.18" y1="0" x2="40.64" y2="0" width="0.1524" layer="91"/>
<label x="40.64" y="0" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="U$1" gate="G$1" pin="G5"/>
<wire x1="83.82" y1="58.42" x2="96.52" y2="58.42" width="0.1524" layer="91"/>
<label x="96.52" y="58.42" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="CLK" class="0">
<segment>
<pinref part="U$2" gate="G$1" pin="CLK"/>
<wire x1="43.18" y1="-2.54" x2="25.4" y2="-2.54" width="0.1524" layer="91"/>
<label x="25.4" y="-2.54" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="U$1" gate="G$1" pin="G18"/>
<wire x1="83.82" y1="60.96" x2="86.36" y2="60.96" width="0.1524" layer="91"/>
<label x="86.36" y="60.96" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="MOSI" class="0">
<segment>
<pinref part="U$2" gate="G$1" pin="MOSI"/>
<wire x1="43.18" y1="-5.08" x2="40.64" y2="-5.08" width="0.1524" layer="91"/>
<label x="40.64" y="-5.08" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="U$1" gate="G$1" pin="G23"/>
<wire x1="83.82" y1="78.74" x2="86.36" y2="78.74" width="0.1524" layer="91"/>
<label x="86.36" y="78.74" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="MISO" class="0">
<segment>
<pinref part="U$2" gate="G$1" pin="MISO"/>
<wire x1="43.18" y1="-7.62" x2="25.4" y2="-7.62" width="0.1524" layer="91"/>
<label x="25.4" y="-7.62" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="U$1" gate="G$1" pin="G19"/>
<wire x1="83.82" y1="63.5" x2="91.44" y2="63.5" width="0.1524" layer="91"/>
<label x="91.44" y="63.5" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="+5V" class="0">
<segment>
<pinref part="P+3" gate="1" pin="+5V"/>
<pinref part="U$1" gate="G$1" pin="5V"/>
<wire x1="45.72" y1="35.56" x2="53.34" y2="35.56" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U$2" gate="G$1" pin="VCC"/>
<wire x1="43.18" y1="-10.16" x2="2.54" y2="-10.16" width="0.1524" layer="91"/>
<pinref part="P+2" gate="1" pin="+5V"/>
<wire x1="0" y1="10.16" x2="2.54" y2="10.16" width="0.1524" layer="91"/>
<wire x1="2.54" y1="10.16" x2="2.54" y2="-10.16" width="0.1524" layer="91"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
<errors>
<approved hash="104,1,139.7,78.74,IC1,VDD,+3V3,,,"/>
<approved hash="104,1,139.7,38.1,IC1,VSS,GND,,,"/>
<approved hash="206,1,-236.22,444.5,+3V3,,,,,"/>
<approved hash="206,1,-248.92,510.54,+3V3,,,,,"/>
<approved hash="206,1,-53.34,444.5,+3V3,,,,,"/>
<approved hash="206,1,-66.04,523.24,+3V3,,,,,"/>
<approved hash="206,1,132.08,441.96,+3V3,,,,,"/>
<approved hash="206,1,119.38,520.7,+3V3,,,,,"/>
<approved hash="206,1,-396.24,251.46,+3V3,,,,,"/>
<approved hash="206,1,-271.78,251.46,+3V3,,,,,"/>
<approved hash="206,1,-139.7,259.08,+3V3,,,,,"/>
<approved hash="206,1,-22.86,259.08,+3V3,,,,,"/>
<approved hash="206,1,149.86,269.24,+3V3,,,,,"/>
<approved hash="206,1,276.86,269.24,+3V3,,,,,"/>
<approved hash="206,1,401.32,269.24,+3V3,,,,,"/>
<approved hash="206,1,525.78,269.24,+3V3,,,,,"/>
<approved hash="206,1,713.74,266.7,+3V3,,,,,"/>
<approved hash="206,1,845.82,264.16,+3V3,,,,,"/>
<approved hash="208,1,40.64,93.98,+3V3,sup,,,,"/>
<approved hash="208,1,134.62,93.98,+3V3,sup,,,,"/>
<approved hash="208,1,-236.22,444.5,+3V3,out,,,,"/>
<approved hash="208,1,-236.22,454.66,+3V3,sup,,,,"/>
<approved hash="208,1,-248.92,510.54,+3V3,out,,,,"/>
<approved hash="208,1,-248.92,520.7,+3V3,sup,,,,"/>
<approved hash="208,1,-53.34,444.5,+3V3,out,,,,"/>
<approved hash="208,1,-53.34,454.66,+3V3,sup,,,,"/>
<approved hash="208,1,-66.04,523.24,+3V3,out,,,,"/>
<approved hash="208,1,-66.04,533.4,+3V3,sup,,,,"/>
<approved hash="208,1,132.08,441.96,+3V3,out,,,,"/>
<approved hash="208,1,132.08,452.12,+3V3,sup,,,,"/>
<approved hash="208,1,119.38,520.7,+3V3,out,,,,"/>
<approved hash="208,1,119.38,530.86,+3V3,sup,,,,"/>
<approved hash="208,1,251.46,99.06,+3V3,sup,,,,"/>
<approved hash="208,1,-396.24,264.16,+3V3,sup,,,,"/>
<approved hash="208,1,-396.24,251.46,+3V3,out,,,,"/>
<approved hash="208,1,-271.78,264.16,+3V3,sup,,,,"/>
<approved hash="208,1,-271.78,251.46,+3V3,out,,,,"/>
<approved hash="208,1,-139.7,271.78,+3V3,sup,,,,"/>
<approved hash="208,1,-139.7,259.08,+3V3,out,,,,"/>
<approved hash="208,1,-22.86,271.78,+3V3,sup,,,,"/>
<approved hash="208,1,-22.86,259.08,+3V3,out,,,,"/>
<approved hash="208,1,149.86,281.94,+3V3,sup,,,,"/>
<approved hash="208,1,149.86,269.24,+3V3,out,,,,"/>
<approved hash="208,1,276.86,281.94,+3V3,sup,,,,"/>
<approved hash="208,1,276.86,269.24,+3V3,out,,,,"/>
<approved hash="208,1,401.32,281.94,+3V3,sup,,,,"/>
<approved hash="208,1,401.32,269.24,+3V3,out,,,,"/>
<approved hash="208,1,525.78,281.94,+3V3,sup,,,,"/>
<approved hash="208,1,525.78,269.24,+3V3,out,,,,"/>
<approved hash="208,1,713.74,279.4,+3V3,sup,,,,"/>
<approved hash="208,1,713.74,266.7,+3V3,out,,,,"/>
<approved hash="208,1,845.82,276.86,+3V3,sup,,,,"/>
<approved hash="208,1,845.82,264.16,+3V3,out,,,,"/>
<approved hash="110,1,-124.46,490.22,N$66,12VPOWER,,,,"/>
<approved hash="110,1,-124.46,490.22,N$66,12VPOWER,,,,"/>
<approved hash="110,1,-124.46,406.4,12VPOWER,N$66,,,,"/>
<approved hash="110,1,-124.46,406.4,12VPOWER,N$66,,,,"/>
<approved hash="113,1,-177.8,402.721,HOUR_A,,,,,"/>
<approved hash="113,1,-137.16,402.721,HOUR_B,,,,,"/>
<approved hash="113,1,5.08,402.721,MIN_A,,,,,"/>
<approved hash="113,1,45.72,402.721,MIN_B,,,,,"/>
<approved hash="113,1,310.202,125.73,LED_SUN,,,,,"/>
<approved hash="113,1,322.902,125.948,LED_MON,,,,,"/>
<approved hash="113,1,335.602,125.73,LED_TUE,,,,,"/>
<approved hash="113,1,350.842,125.939,LED_WED,,,,,"/>
<approved hash="113,1,363.542,125.73,LED_THU,,,,,"/>
<approved hash="113,1,376.242,125.73,LED_FRI,,,,,"/>
<approved hash="113,1,388.942,125.73,LED_SAT,,,,,"/>
<approved hash="113,1,190.5,400.181,SEC_A,,,,,"/>
<approved hash="113,1,231.14,400.181,SEC_B,,,,,"/>
<approved hash="113,1,-108.898,364.181,LED_COLON1,,,,,"/>
<approved hash="113,1,-108.898,338.781,LED_COLON2,,,,,"/>
<approved hash="113,1,79.0617,359.101,LED_COLON3,,,,,"/>
<approved hash="113,1,79.0617,333.701,LED_COLON4,,,,,"/>
<approved hash="113,1,333.329,-10.16,12V_POWER_OUT,,,,,"/>
<approved hash="113,1,363.751,-30.6112,12_POWER_IN,,,,,"/>
<approved hash="113,1,-365.76,196.981,DAY_A,,,,,"/>
<approved hash="113,1,-241.3,196.981,DAY_B,,,,,"/>
<approved hash="113,1,-109.22,204.601,MONTH_A,,,,,"/>
<approved hash="113,1,7.62,204.601,MONTH_B,,,,,"/>
<approved hash="113,1,180.34,214.761,YEAR_A,,,,,"/>
<approved hash="113,1,307.34,214.761,YEAR_B,,,,,"/>
<approved hash="113,1,431.8,214.761,YEAR_C,,,,,"/>
<approved hash="113,1,556.26,214.761,YEAR_D,,,,,"/>
<approved hash="113,1,744.22,212.221,TEMP_A,,,,,"/>
<approved hash="113,1,876.3,209.681,TEMP_B,,,,,"/>
</errors>
</schematic>
</drawing>
<compatibility>
<note version="6.3" minversion="6.2.2" severity="warning">
Since Version 6.2.2 text objects can contain more than one line,
which will not be processed correctly with this version.
</note>
</compatibility>
</eagle>
