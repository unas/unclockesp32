# README #

Gerberfiles and code for digital clock with date, day of week and temperature

### Setup ###

- Install visual studio code
- Install python 3
	- Add python to path
- In VSCode, press Ctrl+Shift+X, search and install "PlatformIO IDE"
- Open PlatformIO from the left bar, select "PIO Home" -> "Open" -> "Open Project", select "UNClockESP32Code" folder
- In PlatformIO, open "esp32doit-devkit-v1" -> "General", double click on build to download required libraries and build project
- Connect esp32 to usb, upload install code to esp32